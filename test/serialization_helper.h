#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/serialization.hpp>
#include <istream>
#include <ostream>
#include <sstream>

template <class T>
inline void serial_write(const T& in, std::ostream& out)
{
    boost::archive::binary_oarchive oarch(out);
    oarch << in;
}

template <class T>
inline void serial_read(std::istream& in, T& out)
{
    boost::archive::binary_iarchive iarch(in);
    iarch >> out;
}

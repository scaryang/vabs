#include "./helper.h"
#include "./pbc_helper.h"

//#include "db_dup/db.hpp"
#include "db/db.hpp"
//#include "db/update.hpp"

#include <set>

using namespace ABS;
using namespace DB;

struct Fixture {
    Fixture()
    {
        global_roles = set<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        roles = set<int>{1, 2};
        pseudo_role = 0;
        range = Rectangle({0, 0}, {3, 3});
    }
    ~Fixture() {}
    set<int> global_roles;
    set<int> roles;
    int pseudo_role;
    Rectangle range;
};

BOOST_FIXTURE_TEST_SUITE(db, Fixture)

BOOST_AUTO_TEST_CASE(inaccessible_policy_test)
{
    typedef Type<int, int> DB_Type;
    Database<DB_Type> db(":memory:");
    db.create_meta(TYPE_A_PARAM, pseudo_role, global_roles, 2, range);
    BOOST_TEST(
        db.compute_inaccessible_policy(set<int>{1, 2, 3, 4, 5, 6, 7, 8, 9})
            .dump() == "0");
    BOOST_TEST(db.compute_inaccessible_policy(set<int>{1, 2, 4, 5, 6, 7, 8, 9})
                   .dump() == "(OR, [0, 3])");
}

BOOST_AUTO_TEST_CASE(db_with_grid_index)
{
    typedef Type<int, int> DB_Type;
    Database<DB_Type> db(":memory:");
    unordered_map<Point, RawDataRecord<DB_Type>> data{
        {{0, 2}, RawDataRecord<DB_Type>(0, string("1"))},
        {{1, 2}, RawDataRecord<DB_Type>(1, string("1 OR 2"))},
        {{2, 2}, RawDataRecord<DB_Type>(2, string("1 AND 3"))},
        {{3, 2}, RawDataRecord<DB_Type>(3, string("3"))},
        {{1, 1}, RawDataRecord<DB_Type>(4, string("2"))},
    };
    db.create_meta(TYPE_A_PARAM, pseudo_role, global_roles, 2, range);
    db.create_index(data, IndexType::grid);
    db.save(true);
    db.reset_sig();
    db.create_data_sig();
    db.create_grid_sig();
    auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(roles);
    // equality query
    auto r1 = db.equality_query({1, 2}, roles);
    BOOST_TEST(r1.accessible);
    BOOST_TEST(r1.verify(mvk, {1, 2}, inaccessible_policy));
    BOOST_TEST(r1.get_vo_size() > 0);
    auto r2 = db.equality_query({2, 2}, roles);
    BOOST_TEST(!r2.accessible);
    BOOST_TEST(r2.verify(mvk, {2, 2}, inaccessible_policy));
    BOOST_TEST(r2.get_vo_size() > 0);
    auto r3 = db.equality_query({0, 0}, roles);
    BOOST_TEST(!r3.accessible);
    BOOST_TEST(r3.verify(mvk, {0, 0}, inaccessible_policy));
    BOOST_TEST(r3.get_vo_size() > 0);
    /*
    // range query
    auto r4 = db.range_query(Rectangle({1, 0}, {3, 2}), roles);
    BOOST_TEST(r4.accessible_data.size() == 2);
    BOOST_TEST(r4.inaccessible_data.size() == 3);
    BOOST_TEST(r4.inaccessible_grid.size() == 1);
    BOOST_TEST(r4.verify(mvk, Rectangle({1, 0}, {3, 2}), inaccessible_policy));
    BOOST_TEST(r4.get_vo_size() > 0);
    // range query naive
    auto r5 = db.range_query_naive(Rectangle({1, 0}, {3, 2}), roles);
    BOOST_TEST(r5.accessible_data.size() == 2);
    BOOST_TEST(r5.inaccessible_data.size() == 7);
    BOOST_TEST(r5.inaccessible_grid.size() == 0);
    BOOST_TEST(r5.verify(mvk, Rectangle({1, 0}, {3, 2}), inaccessible_policy));
    BOOST_TEST(r5.get_vo_size() > 0);
    // with mht
    db.create_mht();
    auto r6 = db.range_query(Rectangle({1, 0}, {3, 2}), roles);
    BOOST_TEST(r6.verify(mvk, Rectangle({1, 0}, {3, 2}), inaccessible_policy,
                         db.get_mht_hash()));
    BOOST_TEST(r6.verify_mht(db.get_mht_hash()));
    BOOST_TEST(r6.get_mht_size() > 0);
    
    // update
    remove_record<DB_Type>(db, Point{1, 1});
    auto r7 = db.range_query(Rectangle({1, 0}, {3, 2}), roles);
    BOOST_TEST(r7.verify(mvk, Rectangle({1, 0}, {3, 2}), inaccessible_policy,
                         db.get_mht_hash()));
    BOOST_TEST(r7.accessible_data.size() == 1);
    update_record<DB_Type>(db, Point{1, 1},
                           RawDataRecord<DB_Type>(1, string("1 OR 2")));
    auto r8 = db.range_query(Rectangle({1, 0}, {3, 2}), roles);
    BOOST_TEST(r8.verify(mvk, Rectangle({1, 0}, {3, 2}), inaccessible_policy,
                         db.get_mht_hash()));
    BOOST_TEST(r8.accessible_data.size() == 2);
    */
}
/*
BOOST_AUTO_TEST_CASE(db_with_kdtree_index)
{
    typedef Type<int, int> DB_Type;
    Database<DB_Type> db(":memory:");
    unordered_map<Point, RawDataRecord<DB_Type>> data{
        {{1, 0}, RawDataRecord<DB_Type>(0, string("1"))},
        {{1, 1}, RawDataRecord<DB_Type>(1, string("1"))},
        {{1, 2}, RawDataRecord<DB_Type>(2, string("2"))},
        {{1, 3}, RawDataRecord<DB_Type>(3, string("2"))},
        {{2, 0}, RawDataRecord<DB_Type>(4, string("3"))},
        {{3, 0}, RawDataRecord<DB_Type>(5, string("3"))},
    };
    db.create_meta(TYPE_A_PARAM, pseudo_role, global_roles, 2, range);
    db.create_index(data, IndexType::kdtree);
    db.save(true);
    db.reset_sig();
    db.create_data_sig();
    db.create_grid_sig();
    auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(roles);
    // range query
    auto r1 = db.range_query(Rectangle({1, 1}, {3, 3}), roles);
    BOOST_TEST(r1.accessible_data.size() == 3);
    BOOST_TEST(r1.inaccessible_data.size() == 0);
    BOOST_TEST(r1.inaccessible_grid.size() == 1);
    BOOST_TEST(r1.verify(mvk, Rectangle({1, 1}, {3, 3}), inaccessible_policy));
    BOOST_TEST(r1.get_vo_size() > 0);
    // with mht
    db.create_mht();
    auto r2 = db.range_query(Rectangle({1, 1}, {3, 3}), roles);
    BOOST_TEST(r2.verify_mht(db.get_mht_hash()));
    BOOST_TEST(r2.get_mht_size() > 0);
}

BOOST_AUTO_TEST_CASE(db_with_hybrid_index)
{
    typedef Type<int, int> DB_Type;
    Database<DB_Type> db(":memory:");
    unordered_map<Point, RawDataRecord<DB_Type>> data{
        {{1, 0}, RawDataRecord<DB_Type>(0, string("1"))},
        {{1, 1}, RawDataRecord<DB_Type>(1, string("1"))},
        {{1, 2}, RawDataRecord<DB_Type>(2, string("2"))},
        {{1, 3}, RawDataRecord<DB_Type>(3, string("2"))},
        {{2, 0}, RawDataRecord<DB_Type>(4, string("3"))},
        {{3, 0}, RawDataRecord<DB_Type>(5, string("3"))},
    };
    db.create_meta(TYPE_A_PARAM, pseudo_role, global_roles, 2, range);
    db.create_index(data, IndexType::hybrid);
    db.save(true);
    db.reset_sig();
    db.create_data_sig();
    db.create_grid_sig();
    auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(roles);
    // range query
    auto r1 = db.range_query(Rectangle({1, 1}, {3, 3}), roles);
    BOOST_TEST(r1.accessible_data.size() == 3);
    BOOST_TEST(r1.inaccessible_data.size() == 0);
    BOOST_TEST(r1.inaccessible_grid.size() == 1);
    BOOST_TEST(r1.verify(mvk, Rectangle({1, 1}, {3, 3}), inaccessible_policy));
    BOOST_TEST(r1.get_vo_size() > 0);
    // with mht
    db.create_mht();
    auto r2 = db.range_query(Rectangle({1, 1}, {3, 3}), roles);
    BOOST_TEST(r2.verify_mht(db.get_mht_hash()));
    BOOST_TEST(r2.get_mht_size() > 0);
}
*/


BOOST_AUTO_TEST_SUITE_END()

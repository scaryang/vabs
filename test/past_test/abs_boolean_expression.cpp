#include "./helper.h"
#include "./serialization_helper.h"
#include "abs/boolean_expression.hpp"
#include "abs/boolean_expression_dnf.hpp"

#include <set>
#include <unordered_set>

using namespace ABS;

template <class T = string>
void check(const BooleanExpression<T>& actual, const string& expect)
{
    auto _expect = BooleanExpression<T>::parse(expect);
    BOOST_TEST(_expect == actual);
}

template <class T = string>
void check_parser(const string& input, const string& expect)
{
    auto expr = BooleanExpression<T>::parse(input);
    auto actual = expr.dump();
    BOOST_TEST(expect == actual);
}

BOOST_AUTO_TEST_CASE(boolean_expression_parse)
{
    check_parser("A", "A");
    check_parser<int>("42", "42");
    check_parser("A AND B", "(AND, [A, B])");
    check_parser<int>("1 and 2", "(AND, [1, 2])");
    check_parser("A OR B", "(OR, [A, B])");
    check_parser<int>("1 or 2", "(OR, [1, 2])");
    check_parser("A AND B AND C", "(AND, [A, B, C])");
    check_parser("A OR B OR C", "(OR, [A, B, C])");
    check_parser("A AND (B OR C OR (D AND E))",
                 "(AND, [A, (OR, [B, C, (AND, [D, E])])])");
    check_parser("(A OR B) AND (OR, [C, D, E])",
                 "(AND, [(OR, [A, B]), (OR, [C, D, E])])");
    check_parser("(AND, [(OR, [A, B]), (OR, [C, D, E])])",
                 "(AND, [(OR, [A, B]), (OR, [C, D, E])])");
}

BOOST_AUTO_TEST_CASE(boolean_expression_eval)
{
    auto expr = BooleanExpression<int>::parse("1 AND (2 OR 3)");
    BOOST_TEST(expr.eval(set<int>{1, 2, 3}));
    BOOST_TEST(expr.eval(unordered_set<int>{1, 2}));
    BOOST_TEST(expr.eval(unordered_set<int>{1, 3}));
    BOOST_TEST(!expr.eval(set<int>{1}));
    BOOST_TEST(!expr.eval(set<int>{2, 3}));
}

BOOST_AUTO_TEST_CASE(boolean_expression_equal_and_hash)
{
    std::hash<BooleanExpression<int>> hasher;
    auto expr1 = BooleanExpression<int>::parse("1");
    auto expr2 = BooleanExpression<int>::parse("2");
    auto expr3 = BooleanExpression<int>::parse("2");
    auto expr4 = BooleanExpression<int>::parse("1 OR 2");
    auto expr5 = BooleanExpression<int>::parse("2 OR 3");
    auto expr6 = BooleanExpression<int>::parse("2 OR 3");
    BOOST_TEST(expr1 != expr2);
    BOOST_TEST(hasher(expr1) != hasher(expr2));
    BOOST_TEST(expr1 != expr4);
    BOOST_TEST(hasher(expr1) != hasher(expr4));
    BOOST_TEST(expr2 == expr3);
    BOOST_TEST(hasher(expr2) == hasher(expr3));
    BOOST_TEST(expr4 != expr5);
    BOOST_TEST(hasher(expr4) != hasher(expr5));
    BOOST_TEST(expr4 != expr1);
    BOOST_TEST(hasher(expr4) != hasher(expr1));
    BOOST_TEST(expr5 == expr6);
    BOOST_TEST(hasher(expr4) != hasher(expr1));
}

BOOST_AUTO_TEST_CASE(boolean_expression_or)
{
    auto expr1 = BooleanExpression<int>::parse("1");
    auto expr2 = BooleanExpression<int>::parse("2");
    auto expr3 = BooleanExpression<int>::parse("1 OR 2");
    auto expr4 = BooleanExpression<int>::parse("2 OR 3");
    auto expr5 = BooleanExpression<int>::parse("1 AND 2");
    auto expr6 = BooleanExpression<int>::parse("1 AND 3");
    check<int>(expr1 or expr1, "1");
    check<int>(expr1 or expr2, "1 OR 2");
    check<int>(expr1 or expr3, "1 OR 2");
    check<int>(expr1 or expr4, "1 OR 2 OR 3");
    check<int>(expr1 or expr5, "1");
    check<int>(expr3 or expr4, "1 OR 2 OR 3");
    check<int>(expr3 or expr5, "1 OR 2");
    check<int>(expr5 or expr6, "(1 AND 2) OR (1 AND 3)");
}

BOOST_AUTO_TEST_CASE(boolean_expression_and)
{
    auto expr1 = BooleanExpression<int>::parse("1");
    auto expr2 = BooleanExpression<int>::parse("2");
    auto expr3 = BooleanExpression<int>::parse("1 OR 2");
    auto expr4 = BooleanExpression<int>::parse("2 OR 3");
    auto expr5 = BooleanExpression<int>::parse("1 AND 2");
    auto expr6 = BooleanExpression<int>::parse("1 AND 3");
    check<int>(expr1 and expr1, "1");
    check<int>(expr1 and expr2, "1 AND 2");
    check<int>(expr1 and expr3, "1");
    check<int>(expr1 and expr4, "(1 AND 3) OR (1 AND 2)");
    check<int>(expr1 and expr5, "1 AND 2");
    check<int>(expr3 and expr4, "2 OR (1 AND 3)");
    check<int>(expr3 and expr5, "1 AND 2");
    check<int>(expr5 and expr6, "1 AND 2 AND 3");
}

BOOST_AUTO_TEST_CASE(boolean_expression_serialization)
{
    stringstream buf;
    auto expr1 = BooleanExpression<int>::parse("1 OR (2 AND 3)");
    auto expr2 = BooleanExpression<string>::parse("a OR (b AND c)");
    serial_write(expr1, buf);
    serial_write(expr2, buf);
    BooleanExpression<int> expr3;
    BooleanExpression<string> expr4;
    serial_read(buf, expr3);
    serial_read(buf, expr4);
    BOOST_TEST(expr1 == expr3);
    BOOST_TEST(expr2 == expr4);
}

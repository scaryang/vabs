#include "./helper.h"
#include "./pbc_helper.h"
#include "db/meta.hpp"

using namespace ABS;
using namespace DB;
using namespace sqlite;

BOOST_AUTO_TEST_CASE(meta_save_and_load)
{
    set<int> global_roles{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int pseudo_role = 0;
    Rectangle range({0, 0}, {2, 2});
    Meta<Type<int, int>> meta1(TYPE_A_PARAM, pseudo_role, global_roles, 2,
                               range),
        meta2;
    meta1.index_type = "some type";
    //meta1.index_create_time = 1.2;
    //meta1.index_sign_data_time = 3.4;
    //meta1.index_sign_grid_time = 5.6;
    database db(":memory:");
    meta1.save(db);
    meta2.load(db);
    BOOST_CHECK(meta1.pairing_param->to_str() == meta2.pairing_param->to_str());
    BOOST_CHECK(meta1.kdc.get_master_signing_key() ==
                meta2.kdc.get_master_signing_key());
    BOOST_CHECK(meta1.kdc.get_master_verifying_key() ==
                meta2.kdc.get_master_verifying_key());
    BOOST_CHECK(meta1.do_key == meta2.do_key);
    BOOST_CHECK(meta1.pseudo_role == meta2.pseudo_role);
    BOOST_CHECK(meta1.global_roles == meta2.global_roles);
    BOOST_CHECK(meta1.dimension == meta2.dimension);
    BOOST_CHECK(meta1.range == meta2.range);
    BOOST_CHECK(meta1.index_type == meta2.index_type);
    //BOOST_CHECK(meta1.index_create_time == meta2.index_create_time);
    //BOOST_CHECK(meta1.index_sign_data_time == meta2.index_sign_data_time);
    //BOOST_CHECK(meta1.index_sign_grid_time == meta2.index_sign_grid_time);
}

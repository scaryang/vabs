#include "./helper.h"
#include "db/index.hpp"
//#include "db-dup/index.hpp"

#include <set>

using namespace ABS;
using namespace DB;
using namespace sqlite;

struct Fixture {
    Fixture()
    {
        global_roles = set<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        pseudo_role = 0;
        pseudo_policy = BooleanExpressionDNF<int>{{pseudo_role}};
    }
    ~Fixture() {}
    set<int> global_roles;
    int pseudo_role;
    BooleanExpressionDNF<int> pseudo_policy;
};

BOOST_FIXTURE_TEST_SUITE(db_index_test, Fixture)

BOOST_AUTO_TEST_CASE(create_grid_index)
{
    typedef Type<int, int> DB_Type;
    unordered_map<Point, RawDataRecord<DB_Type>> data{
        {{0, 2}, RawDataRecord<DB_Type>(0, string("1"))},
        {{1, 2}, RawDataRecord<DB_Type>(1, string("1 OR 2"))},
        {{2, 2}, RawDataRecord<DB_Type>(2, string("1 AND 3"))},
        {{3, 2}, RawDataRecord<DB_Type>(3, string("3"))},
        {{1, 1}, RawDataRecord<DB_Type>(4, string("2"))},
    };
    auto index1 = Index<DB_Type>::create_grid(Rectangle({0, 0}, {3, 3}), data,
                                              pseudo_policy);
    database db(":memory:");
    index1->save(db);
    auto index2 = Index<DB_Type>::load(db);
    BOOST_TEST(*index1 == *index2);
}

BOOST_AUTO_TEST_CASE(create_kdtree_index)
{
    typedef Type<int, int> DB_Type;
    unordered_map<Point, RawDataRecord<DB_Type>> data{
        {{1, 0}, RawDataRecord<DB_Type>(0, string("1"))},
        {{1, 1}, RawDataRecord<DB_Type>(1, string("1"))},
        {{1, 2}, RawDataRecord<DB_Type>(2, string("2"))},
        {{1, 3}, RawDataRecord<DB_Type>(3, string("2"))},
        {{2, 0}, RawDataRecord<DB_Type>(4, string("3"))},
        {{3, 0}, RawDataRecord<DB_Type>(5, string("3"))},
    };
    auto index1 = Index<DB_Type>::create_kdtree(Rectangle({0, 0}, {3, 3}), data,
                                                pseudo_policy);
    database db(":memory:");
    index1->save(db);
    auto index2 = Index<DB_Type>::load(db);
    BOOST_TEST(*index1 == *index2);
}

BOOST_AUTO_TEST_CASE(create_hybrid_index)
{
    typedef Type<int, int> DB_Type;
    unordered_map<Point, RawDataRecord<DB_Type>> data{
        {{1, 0}, RawDataRecord<DB_Type>(0, string("1"))},
        {{1, 1}, RawDataRecord<DB_Type>(1, string("1"))},
        {{1, 2}, RawDataRecord<DB_Type>(2, string("2"))},
        {{1, 3}, RawDataRecord<DB_Type>(3, string("2"))},
        {{2, 0}, RawDataRecord<DB_Type>(4, string("3"))},
        {{3, 0}, RawDataRecord<DB_Type>(5, string("3"))},
    };
    auto index1 = Index<DB_Type>::create_hybrid(Rectangle({0, 0}, {3, 3}), data,
                                                pseudo_policy);
    database db(":memory:");
    index1->save(db);
    auto index2 = Index<DB_Type>::load(db);
    BOOST_TEST(*index1 == *index2);
}

/*
BOOST_AUTO_TEST_CASE(create_index_with_dup)
{
    typedef Type<int, int> DB_Type;
    unordered_multimap<Point, RawDataRecord<DB_Type>> data{
        {{0, 2}, RawDataRecord<DB_Type>(0, string("1"))},
        {{0, 2}, RawDataRecord<DB_Type>(1, string("1 OR 2"))},
        {{2, 2}, RawDataRecord<DB_Type>(2, string("1 AND 3"))},
        {{2, 2}, RawDataRecord<DB_Type>(3, string("3"))},
        {{1, 1}, RawDataRecord<DB_Type>(4, string("2"))},
    };
    auto index1 = DupIndex<DB_Type>::create_grid(Rectangle({0, 0}, {3, 3}), data,
                                              pseudo_policy);
    database db(":memory:");
    index1->save(db);
    auto index2 = DupIndex<DB_Type>::load(db);
    BOOST_TEST(*index1 == *index2);
}
*/
BOOST_AUTO_TEST_SUITE_END()

#include "./helper.h"
#include "abs/monotone_span_program.hpp"

#include <set>
#include <vector>

using namespace ABS;
using namespace ABS::linalg;

Matrix build_matrix(size_t row, size_t col, const vector<int>& data)
{
    Matrix m(row, col);
    for (size_t i = 0; i < row; ++i) {
        for (size_t j = 0; j < col; ++j) {
            m.at(i, j) = Integer(data[i * col + j]);
        }
    }
    return m;
}

void check_span_program(const string& expr, const MatrixSrcRef expect_m,
                        const vector<string>& expect_labels)
{
    MonotoneSpanProgram<string> actual(expr);
    MonotoneSpanProgram<string> expect(expect_m, expect_labels);
    BOOST_TEST(actual == expect);
}

BOOST_AUTO_TEST_CASE(expression_to_span_program)
{
    check_span_program("A AND B", build_matrix(2, 2, vector<int>{1, -1, 0, 1}),
                       vector<string>{"A", "B"});
    check_span_program("A OR B", build_matrix(2, 1, vector<int>{1, 1}),
                       vector<string>{"A", "B"});
    check_span_program(
        "A AND B AND C",
        build_matrix(3, 3, vector<int>{1, -1, -1, 0, 1, 0, 0, 0, 1}),
        vector<string>{"A", "B", "C"});
    check_span_program("(A AND B) OR (A AND C) OR (B AND C)",
                       build_matrix(6, 4,
                                    vector<int>{
                                        1, -1, 0,  0,   // A
                                        0, 1,  0,  0,   // B
                                        1, 0,  -1, 0,   // A
                                        0, 0,  1,  0,   // C
                                        1, 0,  0,  -1,  // B
                                        0, 0,  0,  1,   // C
                                    }),
                       vector<string>{"A", "B", "A", "C", "B", "C"});
    check_span_program("((A AND B) OR (A AND C) OR (B AND C)) AND D",
                       build_matrix(7, 5,
                                    vector<int>{
                                        1, -1, -1, 0,  0,   // A
                                        0, 0,  1,  0,  0,   // B
                                        1, -1, 0,  -1, 0,   // A
                                        0, 0,  0,  1,  0,   // C
                                        1, -1, 0,  0,  -1,  // B
                                        0, 0,  0,  0,  1,   // C
                                        0, 1,  0,  0,  0,   // D
                                    }),
                       vector<string>{"A", "B", "A", "C", "B", "C", "D"});
}

BOOST_AUTO_TEST_CASE(montone_span_program_eval)
{
    Integer mod(37);

    MonotoneSpanProgram<string> msp1(string("A AND B"));
    BOOST_TEST(!msp1.eval(build_matrix(1, 2, vector<int>{0, 0}), mod));
    BOOST_TEST(!msp1.eval(build_matrix(1, 2, vector<int>{1, 0}), mod));
    BOOST_TEST(!msp1.eval(build_matrix(1, 2, vector<int>{0, 1}), mod));
    BOOST_TEST(msp1.eval(build_matrix(1, 2, vector<int>{1, 1}), mod));

    MonotoneSpanProgram<string> msp2(string("A OR B"));
    BOOST_TEST(!msp2.eval(build_matrix(1, 2, vector<int>{0, 0}), mod));
    BOOST_TEST(msp2.eval(build_matrix(1, 2, vector<int>{1, 0}), mod));
    BOOST_TEST(msp2.eval(build_matrix(1, 2, vector<int>{0, 1}), mod));
    BOOST_TEST(msp2.eval(build_matrix(1, 2, vector<int>{19, 19}), mod));

    MonotoneSpanProgram<string> msp3(
        string("(A AND B) OR (A AND C) OR (B AND C)"));
    BOOST_TEST(
        !msp3.eval(build_matrix(1, 6, vector<int>{0, 0, 0, 0, 0, 0}), mod));
    BOOST_TEST(
        !msp3.eval(build_matrix(1, 6, vector<int>{1, 0, 0, 0, 0, 0}), mod));
    BOOST_TEST(
        msp3.eval(build_matrix(1, 6, vector<int>{1, 1, 0, 0, 0, 0}), mod));
    BOOST_TEST(
        msp3.eval(build_matrix(1, 6, vector<int>{19, 19, 19, 19, 0, 0}), mod));
    BOOST_TEST(msp3.eval(
        build_matrix(1, 6, vector<int>{25, 25, 25, 25, 25, 25}), mod));
}

BOOST_AUTO_TEST_CASE(montone_span_program_compute_vector)
{
    Integer mod(37);

    MonotoneSpanProgram<string> msp1(string("A AND B"));
    BOOST_TEST(msp1.eval(msp1.compute_vector(set<string>{"A", "B"}, mod), mod));
    BOOST_CHECK_THROW(msp1.compute_vector(set<string>{"A"}, mod),
                      runtime_error);
    BOOST_CHECK_THROW(msp1.compute_vector(set<string>{"B"}, mod),
                      runtime_error);

    MonotoneSpanProgram<string> msp2(string("A OR B"));
    BOOST_TEST(msp2.eval(msp2.compute_vector(set<string>{"A", "B"}, mod), mod));
    BOOST_TEST(msp2.eval(msp2.compute_vector(set<string>{"A"}, mod), mod));
    BOOST_TEST(msp2.eval(msp2.compute_vector(set<string>{"B"}, mod), mod));

    MonotoneSpanProgram<string> msp3(
        string("(A AND B) OR (A AND C) OR (B AND C)"));
    BOOST_CHECK_THROW(msp3.compute_vector(set<string>{}, mod), runtime_error);
    BOOST_CHECK_THROW(msp3.compute_vector(set<string>{"A"}, mod),
                      runtime_error);
    BOOST_TEST(msp3.eval(msp3.compute_vector(set<string>{"A", "B"}, mod), mod));
    BOOST_TEST(
        msp3.eval(msp3.compute_vector(set<string>{"A", "B", "C"}, mod), mod));
}

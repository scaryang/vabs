#include "./helper.h"
#include "./pbc_helper.h"
#include "db/sig.hpp"

#include <set>

using namespace ABS;
using namespace DB;

struct Fixture {
    Fixture()
    {
        global_roles = set<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        pseudo_role = 0;
        pseudo_policy = BooleanExpressionDNF<int>{{pseudo_role}};
        pairing = Pairing::init_from_param(TYPE_A_PARAM);
        kdc.setup(pairing);
        mvk = kdc.get_master_verifying_key();
        key = kdc.generate_key<int>(global_roles);
    }
    ~Fixture() {}
    set<int> global_roles;
    int pseudo_role;
    BooleanExpressionDNF<int> pseudo_policy;
    PairingPtr pairing;
    KeyDistributionCenter kdc;
    MasterVerifyingKey mvk;
    SigningKey<int> key;
};

BOOST_FIXTURE_TEST_SUITE(db_sig_test, Fixture)

BOOST_AUTO_TEST_CASE(data_sign_verify_relax)
{
    typedef Type<int, int> DB_Type;
    RawDataRecord<DB_Type> rd(1, "1 AND 2");
    Point p({1, 2, 3});
    BooleanExpression<int> policy =
        BooleanExpression<int>::parse("0 OR 1 OR 3");
    DataRecord<DB_Type> d1(1, 1, p, rd), d2(1, 1, p, pseudo_policy);
    DataSig<DB_Type> s1, s2;
    s1 = decltype(s1)::create(d1, mvk, key);
    s2 = decltype(s2)::create(d2, mvk, key);
    BOOST_TEST(s1.verify(mvk, p, rd));
    BOOST_TEST(s1.relax(mvk, set<int>{0, 1, 3}).verify(mvk, p, policy));
    BOOST_TEST(s2.verify(mvk, p, BooleanExpression<int>(0)));
    BOOST_TEST(s2.relax(mvk, set<int>{0, 1, 3}).verify(mvk, p, policy));
}

BOOST_AUTO_TEST_CASE(grid_sign_verify_relax)
{
    typedef Type<int, int> DB_Type;
    BooleanExpression<int> policy1 =
        BooleanExpression<int>::parse("1 OR (2 AND 3)");
    BooleanExpression<int> policy2 =
        BooleanExpression<int>::parse("0 OR 1 OR 2");
    Rectangle box({0, 0}, {1, 1});
    Grid<DB_Type> g(1, 1, box, policy1);
    GridSig<DB_Type> s;
    s = decltype(s)::create(g, mvk, key);
    BOOST_TEST(s.verify(mvk, box, policy1));
    BOOST_TEST(s.relax(mvk, set<int>{0, 1, 2}).verify(mvk, box, policy2));
}

BOOST_AUTO_TEST_SUITE_END()

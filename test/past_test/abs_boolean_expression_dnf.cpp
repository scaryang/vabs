#include "./helper.h"
#include "./serialization_helper.h"
#include "abs/boolean_expression_dnf.hpp"

using namespace ABS;

BOOST_AUTO_TEST_CASE(dnf_initialize_and_to_expr)
{
    BooleanExpressionDNF<int> dnf1{};
    BooleanExpressionDNF<int> dnf2{{}};
    BooleanExpressionDNF<int> dnf3{{1}};
    BooleanExpressionDNF<int> dnf4{{1}, {2, 3}, {4, 5, 6}};
    BOOST_TEST(dnf1.to_expr() == BooleanExpression<int>());
    BOOST_TEST(dnf2.to_expr() == BooleanExpression<int>());
    BOOST_TEST(dnf3.to_expr() == BooleanExpression<int>(1));
    BOOST_TEST(dnf4.to_expr() == BooleanExpression<int>::parse(
                                     "1 OR (2 AND 3) OR (4 AND 5 AND 6)"));
    BOOST_TEST(dnf1 == dnf2);
    BOOST_TEST(dnf3 == dnf3);
    BOOST_TEST(dnf4 == dnf4);
    BOOST_TEST(dnf1 != dnf3);
    BOOST_TEST(dnf3 != dnf4);
}

BOOST_AUTO_TEST_CASE(dnf_simplify)
{
    BooleanExpressionDNF<int> dnf{{1}, {1, 2}, {3, 4}, {3}};
    dnf.simplify();
    BOOST_TEST(dnf.to_expr() == BooleanExpression<int>::parse("1 OR 3"));
}

BOOST_AUTO_TEST_CASE(dnf_or)
{
    BooleanExpressionDNF<int> dnf1{{1}, {3, 4}, {5}, {6}};
    BooleanExpressionDNF<int> dnf2{{1, 2}, {3}, {5}, {7}};
    auto dnf = dnf1 or dnf2;
    BOOST_TEST(dnf.to_expr() ==
               BooleanExpression<int>::parse("1 OR 3 OR 5 OR 6 OR 7"));
}

BOOST_AUTO_TEST_CASE(dnf_and)
{
    BooleanExpressionDNF<int> dnf1{{1}, {3, 4}};
    BooleanExpressionDNF<int> dnf2{{1, 2}, {3}};
    auto dnf = dnf1 and dnf2;
    BOOST_TEST(dnf.to_expr() == BooleanExpression<int>::parse(
                                    "(1 AND 2) OR (1 AND 3) OR (3 AND 4)"));
}

BOOST_AUTO_TEST_CASE(dnf_cup)
{
    BooleanExpressionDNF<int> dnf1{{1, 2}, {2, 3}};
    BooleanExpressionDNF<int> dnf2{{1, 2}, {3, 4}};
    auto dnf = dnf1.cup(dnf2);
    BOOST_TEST(dnf.to_expr() == BooleanExpression<int>::parse(
                                    "(1 AND 2) OR (2 AND 3) OR (3 AND 4)"));
}

BOOST_AUTO_TEST_CASE(dnf_cap)
{
    BooleanExpressionDNF<int> dnf1{{1, 2}, {2, 3}};
    BooleanExpressionDNF<int> dnf2{{1, 2}, {3, 4}};
    auto dnf = dnf1.cap(dnf2);
    BOOST_TEST(dnf.to_expr() == BooleanExpression<int>::parse("1 AND 2"));
}

BOOST_AUTO_TEST_CASE(dnf_from_boolean_expression)
{
    BooleanExpression<int> expr =
        BooleanExpression<int>::parse("1 AND (2 OR (3 AND (4 OR 5)))");
    BooleanExpressionDNF<int> dnf(expr);
    BOOST_TEST(dnf.to_expr() ==
               BooleanExpression<int>::parse(
                   "(1 AND 2) OR (1 AND 3 AND 4) OR (1 AND 3 AND 5)"));
}

BOOST_AUTO_TEST_CASE(dnf_serialization)
{
    stringstream buf;
    auto expr1 = BooleanExpressionDNF<int>(
        BooleanExpression<int>::parse("1 OR (2 AND 3)"));
    auto expr2 = BooleanExpressionDNF<string>(
        BooleanExpression<string>::parse("a OR (b AND c)"));
    serial_write(expr1, buf);
    serial_write(expr2, buf);
    BooleanExpressionDNF<int> expr3;
    BooleanExpressionDNF<string> expr4;
    serial_read(buf, expr3);
    serial_read(buf, expr4);
    BOOST_TEST(expr1.to_expr() == expr3.to_expr());
    BOOST_TEST(expr2.to_expr() == expr4.to_expr());
}

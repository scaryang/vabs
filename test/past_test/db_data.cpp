#include "./helper.h"
#include "db/data.hpp"

#include <set>

using namespace ABS;
using namespace DB;

struct Fixture {
    Fixture()
    {
        global_roles = set<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        pseudo_role = 0;
        pseudo_policy = BooleanExpressionDNF<int>{{pseudo_role}};
    }
    ~Fixture() {}
    set<int> global_roles;
    int pseudo_role;
    BooleanExpressionDNF<int> pseudo_policy;
};

BOOST_FIXTURE_TEST_SUITE(db_data_test, Fixture)

BOOST_AUTO_TEST_CASE(data_record)
{
    typedef Type<int, int> DB_Type;
    RawDataRecord<DB_Type> rd(1, "1 AND 2");
    Point p({1, 2, 3});
    DataRecord<DB_Type> d1(1, 1, p, rd), d2(1, 1, p, pseudo_policy);
    BOOST_CHECK(d1.access(set<int>{1, 2}));
    BOOST_CHECK(!d2.access(set<int>{1, 2}));
    BOOST_CHECK(d1 == d1);
    BOOST_CHECK(d2 == d2);
    BOOST_CHECK(d1 != d2);
}

BOOST_AUTO_TEST_CASE(grid)
{
    typedef Type<int, int> DB_Type;
    BooleanExpression<int> policy1 =
        BooleanExpression<int>::parse("1 OR (2 AND 3)");
    BooleanExpression<int> policy2 =
        BooleanExpression<int>::parse("0 OR 1 OR 2");
    Rectangle box({0, 0}, {1, 1});
    Grid<DB_Type> g1(1, 1, box, policy1);
    Grid<DB_Type> g2(1, 1, box, policy2);
    BOOST_CHECK(g1.access(set<int>{1}));
    BOOST_CHECK(!g1.access(set<int>{2}));
    BOOST_CHECK(g1 == g1);
    BOOST_CHECK(g2 == g2);
    BOOST_CHECK(g1 != g2);
}

BOOST_AUTO_TEST_SUITE_END()

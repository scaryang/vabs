#include "./helper.h"
#include "./serialization_helper.h"
#include "db/rect.hpp"

using namespace DB;

BOOST_AUTO_TEST_CASE(rect_intersection)
{
    Rectangle r1({0, 0}, {2, 2});
    Rectangle r2({1, 1}, {3, 3});
    BOOST_TEST(r1.intersection(r2) == Rectangle({1, 1}, {2, 2}));
}

BOOST_AUTO_TEST_CASE(rect_cover)
{
    Rectangle r1({0, 0}, {2, 2});
    Rectangle r2({1, 1}, {3, 3});
    Rectangle r3({1, 1}, {2, 2});
    Rectangle r4({3, 3}, {4, 4});
    Point p1({1, 1});
    Point p2({3, 3});
    BOOST_TEST(r1.cover(p1));
    BOOST_TEST(!r1.cover(p2));
    BOOST_CHECK(r1.cover(r2) == RectCoverState::Partial);
    BOOST_CHECK(r1.cover(r3) == RectCoverState::Full);
    BOOST_CHECK(r1.cover(r4) == RectCoverState::None);
}

BOOST_AUTO_TEST_CASE(rect_subtract)
{
    Rectangle r1({0, 0}, {3, 3});
    Rectangle r2({1, 1}, {2, 2});
    BOOST_TEST(r1.subtract(r2).size() == 4);

    Rectangle r3({0, 0}, {2, 3});
    Rectangle r4({0, 1}, {1, 2});
    BOOST_TEST(r3.subtract(r4).size() == 3);

    Rectangle r5({0, 0}, {1, 3});
    Rectangle r6({0, 1}, {1, 2});
    BOOST_TEST(r5.subtract(r6).size() == 2);

    Rectangle r7({0, 0}, {1, 2});
    Rectangle r8({0, 0}, {1, 1});
    BOOST_TEST(r7.subtract(r8).size() == 1);

    Rectangle r9({0, 0}, {1, 1});
    BOOST_TEST(r9.subtract(r9).size() == 0);
}

BOOST_AUTO_TEST_CASE(rect_covered_by)
{
    Rectangle all({0, 0}, {3, 3});
    std::vector<Rectangle> fragments;
    fragments.push_back(Rectangle({0, 0}, {0, 0}));
    BOOST_TEST(!all.covered_by(fragments));
    fragments.push_back(Rectangle({0, 1}, {0, 1}));
    BOOST_TEST(!all.covered_by(fragments));
    fragments.push_back(Rectangle({0, 2}, {0, 3}));
    BOOST_TEST(!all.covered_by(fragments));
    fragments.push_back(Rectangle({1, 0}, {3, 1}));
    BOOST_TEST(!all.covered_by(fragments));
    fragments.push_back(Rectangle({1, 1}, {3, 3}));
    BOOST_TEST(all.covered_by(fragments));
}

BOOST_AUTO_TEST_CASE(rect_all_points)
{
    Rectangle r({0, 0}, {1, 1});
    BOOST_TEST(r.all_points() ==
               std::vector<Point>({{0, 0}, {1, 0}, {0, 1}, {1, 1}}));
}

BOOST_AUTO_TEST_CASE(rect_serialization)
{
    stringstream buf;
    Rectangle r1({0, 0}, {1, 1}), r2;
    serial_write(r1, buf);
    serial_read(buf, r2);
    BOOST_TEST(r1 == r2);
}

#include "./helper.h"
#include "abs/linear_algebra.hpp"

#include <vector>

using namespace ABS;
using namespace ABS::linalg;

Matrix build_matrix(size_t row, size_t col, const vector<int>& data)
{
    Matrix m(row, col);
    for (size_t i = 0; i < row; ++i) {
        for (size_t j = 0; j < col; ++j) {
            m.at(i, j) = Integer(data[i * col + j]);
        }
    }
    return m;
}

BOOST_AUTO_TEST_CASE(smith_normal_form_test)
{
    Matrix A = build_matrix(
        3, 4, vector<int>{2, 1, -3, -1, 1, -1, -3, 1, 4, -4, 0, 16});
    auto snf = smith_normal_form(A);
    Matrix D = std::get<0>(snf);
    Matrix L = std::get<1>(snf);
    Matrix R = std::get<2>(snf);
    BOOST_CHECK(
        D ==
        build_matrix(3, 4, vector<int>{1, 0, 0, 0, 0, 3, 0, 0, 0, 0, -12, 0}));
    BOOST_CHECK(D == L * A * R);
}

BOOST_AUTO_TEST_CASE(solve_linear_diophantine_test)
{
    Integer m(37);
    // A = [1 1 1; 1 2 3]
    Matrix A = build_matrix(2, 3, vector<int>{1, 1, 1, 1, 2, 3});
    // b = [1; 0]
    Matrix b = build_matrix(2, 1, vector<int>{1, 0});
    Matrix x = solve_linear_diophantine(A, b, m);
    BOOST_CHECK(mod_multiply(A, x, m) == b);
}

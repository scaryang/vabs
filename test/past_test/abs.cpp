#include "./helper.h"
#include "./pbc_helper.h"
#include "./serialization_helper.h"

#include "abs/abs.hpp"

#include <set>
#include <vector>

using namespace ABS;

BOOST_AUTO_TEST_CASE(abs_test)
{
    // setup
    PairingPtr pairing(Pairing::init_from_param(TYPE_A_PARAM));
    KeyDistributionCenter kdc;
    kdc.setup(pairing);
    // keygen
    SigningKey<string> key =
        kdc.generate_key<string>(set<string>{"A", "B", "D"});
    // sign
    string message = "Hello World!";
    Signature<string> signature = sign(
        kdc.get_master_verifying_key(), key, message,
        BooleanExpression<string>::parse("(((A AND B) OR (A AND C) OR (B AND "
                                         "C)) AND D) OR (E AND F AND H)"));
    // verify
    BOOST_TEST(verify(kdc.get_master_verifying_key(), signature, message));
    // relax
    Signature<string> new_signature =
        relax(kdc.get_master_verifying_key(), signature, message,
              set<string>{"A", "B", "E", "F", "G"});
    BOOST_TEST(verify(kdc.get_master_verifying_key(), new_signature, message));
    // serialization
    stringstream buf;
    GlobalPBCPairing::set(pairing);
    serial_write(signature, buf);
    serial_write(new_signature, buf);
    Signature<string> s1, s2;
    serial_read(buf, s1);
    serial_read(buf, s2);
    GlobalPBCPairing::clear();
    BOOST_CHECK(s1 == signature);
    BOOST_CHECK(s2 == new_signature);
    BOOST_TEST(verify(kdc.get_master_verifying_key(), s1, message));
    BOOST_TEST(verify(kdc.get_master_verifying_key(), s2, message));
}

#include "./helper.h"
#include "./pbc_helper.h"
#include "abs/utils.hpp"

#include <limits>
#include <vector>

using namespace ABS;
using namespace ABS::Utils;

BOOST_AUTO_TEST_CASE(hash_to_element_test)
{
    PairingPtr pairing(Pairing::init_from_param(TYPE_A_PARAM));
    BOOST_TEST(hash_to_element<string>("test", pairing).to_str(16) ==
               "294a8fe5ccb193a61c4c0873d391e987982fbbd2");
    BOOST_TEST(hash_to_element<int>(42, pairing).to_str(16) ==
               "14baecd88cd86197979e9592a3614e57bbd01235");
}

BOOST_AUTO_TEST_CASE(element_power_helper)
{
    PairingPtr pairing(Pairing::init_from_param(TYPE_A_PARAM));
    Element g, z, r1, r2;
    g.init_g1(pairing);
    z.init_zr(pairing);
    g.random();
    z.random();
    ElementPowerHelper p1(g, 1), p2(g, std::numeric_limits<size_t>::max());
    BOOST_TEST(p1.pow(z) == p2.pow(z));
}

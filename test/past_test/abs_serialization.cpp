#include "./helper.h"
#include "./pbc_helper.h"
#include "./serialization_helper.h"

#include <abs/key.hpp>
#include <abs/pbc_serialization.hpp>
#include <set>

using namespace ABS;
using namespace pbc;

BOOST_AUTO_TEST_CASE(global_pbc_pairing)
{
    GlobalPBCPairing::set(TYPE_A_PARAM);
    BOOST_TEST(GlobalPBCPairing::get()->symmetric());
    GlobalPBCPairing::clear();
}

BOOST_AUTO_TEST_CASE(pbc_element_serialization)
{
    GlobalPBCPairing::set(TYPE_A_PARAM);
    Element e1(GlobalPBCPairing::get(), ElementType::Zr);
    Element e2(GlobalPBCPairing::get(), ElementType::G1);
    e1.random();
    e2.random();
    stringstream buf;
    serial_write(e1, buf);
    serial_write(e2, buf);
    Element e3(GlobalPBCPairing::get(), ElementType::Zr);
    Element e4(GlobalPBCPairing::get(), ElementType::G1);
    serial_read(buf, e3);
    serial_read(buf, e4);
    BOOST_TEST(e1 == e3);
    BOOST_TEST(e2 == e4);
    GlobalPBCPairing::clear();
}

BOOST_AUTO_TEST_CASE(key_serialization)
{
    GlobalPBCPairing::set(TYPE_A_PARAM);
    KeyDistributionCenter kdc1, kdc2;
    SigningKey<int> key1, key2;
    kdc1.setup(GlobalPBCPairing::get());
    stringstream buf;
    serial_write(kdc1, buf);
    serial_read(buf, kdc2);
    BOOST_CHECK(kdc1.get_master_signing_key() == kdc2.get_master_signing_key());
    key1 = kdc2.generate_key<int>(set<int>{1, 2});
    auto mvk1 = kdc1.get_master_verifying_key();
    auto mvk2 = kdc2.get_master_verifying_key();
    BOOST_CHECK(mvk1 == mvk2);
    BOOST_TEST(mvk1.g_p->get_base() == mvk2.g_p->get_base());
    BOOST_TEST(mvk1.h0_p->get_base() == mvk2.h0_p->get_base());
    BOOST_TEST(mvk1.h_p->get_base() == mvk2.h_p->get_base());
    BOOST_TEST(mvk1.B_p->get_base() == mvk2.B_p->get_base());
    BOOST_CHECK(mvk1.pairing == mvk2.pairing);
    serial_write(key1, buf);
    serial_read(buf, key2);
    BOOST_CHECK(key1 == key2);
    GlobalPBCPairing::clear();
}

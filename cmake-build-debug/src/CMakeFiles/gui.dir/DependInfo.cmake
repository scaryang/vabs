# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yang/Pictures/aaa/cmake-build-debug/src/gui_autogen/mocs_compilation.cpp" "/home/yang/Pictures/aaa/cmake-build-debug/src/CMakeFiles/gui.dir/gui_autogen/mocs_compilation.cpp.o"
  "/home/yang/Pictures/aaa/src/main/gui-naive/logindialog.cpp" "/home/yang/Pictures/aaa/cmake-build-debug/src/CMakeFiles/gui.dir/main/gui-naive/logindialog.cpp.o"
  "/home/yang/Pictures/aaa/src/main/gui-naive/main.cpp" "/home/yang/Pictures/aaa/cmake-build-debug/src/CMakeFiles/gui.dir/main/gui-naive/main.cpp.o"
  "/home/yang/Pictures/aaa/src/main/gui-naive/mainwindow.cpp" "/home/yang/Pictures/aaa/cmake-build-debug/src/CMakeFiles/gui.dir/main/gui-naive/mainwindow.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/gui_autogen/include"
  "../src/vendor/include"
  "/usr/local/include"
  "/usr/local/include/flint"
  "../src/server"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

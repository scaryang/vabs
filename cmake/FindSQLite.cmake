if (SQLite_INCLUDE_DIR AND SQLite_LIBRARIES)
    set(SQLite_FIND_QUIETLY TRUE)
endif ()

if(APPLE)
    find_path(SQLite_INCLUDE_DIR NAMES sqlite3.h HINTS /usr/local/opt/sqlite/include)
    find_library(SQLite_LIBRARIES NAMES sqlite3 libsqlite3 HINTS /usr/local/opt/sqlite/lib)
else()
    find_path(SQLite_INCLUDE_DIR NAMES sqlite3.h)
    find_library(SQLite_LIBRARIES NAMES sqlite3 libsqlite3)
endif()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SQLite DEFAULT_MSG SQLite_INCLUDE_DIR SQLite_LIBRARIES)

mark_as_advanced(SQLite_INCLUDE_DIR SQLite_LIBRARIES)

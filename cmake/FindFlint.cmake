if (Flint_INCLUDE_DIR AND Flint_LIBRARIES)
    set(Flint_FIND_QUIETLY TRUE)
endif ()

find_path(Flint_INCLUDE_DIR NAMES flint/flint.h)
set(Flint_INCLUDE_DIRS ${Flint_INCLUDE_DIR} "${Flint_INCLUDE_DIR}/flint")
find_library(Flint_LIBRARIES NAMES flint libflint)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Flint DEFAULT_MSG Flint_INCLUDE_DIR Flint_LIBRARIES)

mark_as_advanced(Flint_INCLUDE_DIR Flint_LIBRARIES)

if (Cryptopp_INCLUDE_DIR AND Cryptopp_LIBRARIES)
    set(Cryptopp_FIND_QUIETLY TRUE)
endif ()

find_path(Cryptopp_INCLUDE_DIR NAMES cryptopp/cryptlib.h)
find_library(Cryptopp_LIBRARIES NAMES cryptopp libcryptopp)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Cryptopp DEFAULT_MSG Cryptopp_INCLUDE_DIR Cryptopp_LIBRARIES)

mark_as_advanced(Cryptopp_INCLUDE_DIR Cryptopp_LIBRARIES)

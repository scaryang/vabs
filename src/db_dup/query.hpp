#pragma once

#include "../db/data.hpp"
#include "../db/rect.hpp"
#include "../db/type.hpp"
#include "../db/utils.hpp"
#include "./index.hpp"
#include "../utils/sfinae_helper.hpp"


#include <algorithm>
#include <map>
#include <unordered_map>
#include <vector>

#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/unordered_map.hpp>



namespace DB
{
    template <class Type>
    class DupRangeQueryIntermediateResult
    {
    public:
        std::vector<int> accessible_data_ids() const
        {
            std::vector<int> result;
            result.reserve(accessible_data.size());
            for (const auto& p : accessible_data) result.push_back(p.first);
            return result;
        }
        std::vector<int> inaccessible_data_ids() const
        {
            std::vector<int> result;
            result.reserve(inaccessible_data.size());
            for (const auto& p : inaccessible_data) result.push_back(p.first);
            return result;
        }
        std::vector<int> inaccessible_grid_ids() const
        {
            std::vector<int> result;
            result.reserve(inaccessible_grid.size());
            for (const auto& p : inaccessible_grid) result.push_back(p.first);
            return result;
        }
        std::map<int, const DataRecord<Type>*> accessible_data;
        std::map<int, const DataRecord<Type>*> inaccessible_data;
        std::map<int, const Grid<Type>*> inaccessible_grid;
    };

    template <class Type, class Container,
              class = typename std::enable_if<
                  sfinae::is_container_with_fixed_type_and_find_method<
                      Container, typename Type::role_type>::value>::type>
    class DupRangeQueryWalker
        : public boost::static_visitor<DupRangeQueryIntermediateResult<Type>>
    {
    public:
        explicit DupRangeQueryWalker(const Rectangle& range,
                                     const Container& roles)
            : range(range), roles(roles)
        {
        }

        DupRangeQueryIntermediateResult<Type> operator()(
            const std::unique_ptr<DupIndexLeafNode<Type>>& node) const
        {
            DupRangeQueryIntermediateResult<Type> result;

            switch (range.cover(node->box)) {
                case RectCoverState::Full:
                    if (node->access(roles)) {
                        for (auto ptr = node->leaves.begin();
                             ptr != node->leaves.end(); ++ptr) {
                            if (ptr->second->access(roles)) {
                                result.accessible_data.insert(std::make_pair(
                                    ptr->second->id, ptr->second.get()));
                            } else {
                                result.inaccessible_data.insert(std::make_pair(
                                    ptr->second->id, ptr->second.get()));
                            }
                        }
                    } else {
                        result.inaccessible_grid.insert(
                            std::make_pair(node->id, node.get()));
                    }
                    break;
                case RectCoverState::Partial:
                    for (auto ptr = node->leaves.begin();
                         ptr != node->leaves.end(); ++ptr) {
                        if (!range.cover(ptr->first)) continue;
                        if (ptr->second->access(roles)) {
                            result.accessible_data.insert(std::make_pair(
                                ptr->second->id, ptr->second.get()));
                        } else {
                            result.inaccessible_data.insert(std::make_pair(
                                ptr->second->id, ptr->second.get()));
                        }
                    }
                    break;
                case RectCoverState::None: break;
            };
            return result;
        }

        DupRangeQueryIntermediateResult<Type> operator()(
            const std::unique_ptr<DupIndexNonLeafNode<Type>>& node) const
        {
            DupRangeQueryIntermediateResult<Type> result;
            switch (range.cover(node->box)) {
                case RectCoverState::Full:
                    if (!node->access(roles)) {
                        result.inaccessible_grid.insert(
                            std::make_pair(node->id, node.get()));
                        break;
                    }
                case RectCoverState::Partial:
                    for (auto ptr = node->leaves.begin();
                         ptr != node->leaves.end(); ++ptr) {
                        auto result_ =
                            boost::apply_visitor(*this, *ptr->second);
                        result.accessible_data.insert(
                            result_.accessible_data.begin(),
                            result_.accessible_data.end());
                        result.inaccessible_data.insert(
                            result_.inaccessible_data.begin(),
                            result_.inaccessible_data.end());
                        result.inaccessible_grid.insert(
                            result_.inaccessible_grid.begin(),
                            result_.inaccessible_grid.end());
                    }
                    break;
                case RectCoverState::None: break;
            };

            return result;
        }

    private:
        const Rectangle& range;
        const Container& roles;
    };

    template <class Type>
    class DupRangeQueryResult
    {
    public:
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            void>::type
        process_signs(const ABS::MasterVerifyingKey& mvk,
                      const Container& inaccessible_roles)
        {
#pragma omp parallel
#pragma omp single
            {
                for (auto ptr = inaccessible_data.begin();
                     ptr != inaccessible_data.end(); ++ptr) {
#pragma omp task firstprivate(ptr)
                    ptr->second = ptr->second.relax(mvk, inaccessible_roles);
                }

                for (auto ptr = inaccessible_grid.begin();
                     ptr != inaccessible_grid.end(); ++ptr) {
#pragma omp task firstprivate(ptr)
                    ptr->second = ptr->second.relax(mvk, inaccessible_roles);
                }

#pragma omp taskwait
            }
        }

        bool verify(const ABS::MasterVerifyingKey& mvk, const Rectangle& range,
                    const typename Type::policy_type& inaccessible_policy) const
        {
            size_t len = accessible_data.size() + inaccessible_data.size() +
                         inaccessible_grid.size();
            std::vector<Rectangle> fragments;
            fragments.reserve(len);
            std::vector<int> result(len + 1, 0);

            size_t i = 0;
#pragma omp parallel
#pragma omp single
            {
                for (auto ptr = accessible_data.begin();
                     ptr != accessible_data.end(); ++ptr, ++i) {
#pragma omp task firstprivate(ptr, i) shared(result)
                    {
                        bool r = ptr->second.second.verify(mvk, ptr->first,
                                                           ptr->second.first);
                        result[i] = (int)r;
                    }
                    fragments.push_back(Rectangle(ptr->first, ptr->first));
                }
                for (auto ptr = inaccessible_data.begin();
                     ptr != inaccessible_data.end(); ++ptr, ++i) {
#pragma omp task firstprivate(ptr, i) shared(result)
                    {
                        bool r = ptr->second.verify(mvk, ptr->first,
                                                    inaccessible_policy);
                        result[i] = (int)r;
                    }
                    fragments.push_back(Rectangle(ptr->first, ptr->first));
                }
                for (auto ptr = inaccessible_grid.begin();
                     ptr != inaccessible_grid.end(); ++ptr, ++i) {
#pragma omp task firstprivate(ptr, i) shared(result)
                    {
                        bool r = ptr->second.verify(mvk, ptr->first,
                                                    inaccessible_policy);
                        result[i] = (int)r;
                    }
                    fragments.push_back(ptr->first);
                }

#pragma omp task firstprivate(i) shared(result)
                {
                    bool r = range.covered_by(fragments);
                    result[i] = (int)r;
                }

#pragma omp taskwait
            }

            for (int r : result) {
                if (!(bool)r) return false;
            }
            return true;
        }

        size_t get_vo_size() const
        {
            size_t size = 0;
            for (const auto& ptr : accessible_data)
                size += ptr.second.second.get_size_when_accessible();
            for (const auto& ptr : inaccessible_data)
                size += ptr.first.get_vo_size() +
                        ptr.second.get_size_when_inaccessible();
            for (const auto& ptr : inaccessible_grid)
                size += ptr.first.get_vo_size() +
                        ptr.second.get_size_when_inaccessible();
            return size;
        }


/*
        friend class boost::serialization::access;
        template <class Archive>
        void save(Archive& ar, const unsigned int version) const
        {
            //std::unordered_multimap<Point, std::pair<RawDataRecord<Type>, DataSig<Type>>>::iterator p_map;
            for (auto p_map = accessible_data.begin(); p_map != accessible_data.end(); p_map++)
            {
                ar & p_map->first;
                ar & p_map->second.first;
                ar & p_map->second.second;               
            }
            for (auto p_map = inaccessible_data.begin(); p_map != inaccessible_data.end(); p_map++)
            {
                ar & p_map->first;
                ar & p_map->second; 
            }
            for (auto p_map = inaccessible_grid.begin(); p_map != inaccessible_grid.end(); p_map++)
            {
                ar & p_map->first;
                ar & p_map->second; 
            }
        }

        template <class Archive>
        void load(Archive& ar, const unsigned int version) 
        {
            //std::unordered_multimap<Point, std::pair<RawDataRecord<Type>, DataSig<Type>>>::iterator p_map;
            auto p_map = accessible_data.begin(); 
            for (int i = 0; i < 12 ; i ++)
            {
                ar & p_map->first;
                ar & p_map->second.first;
                ar & p_map->second.second;               
            }         
           /* for (auto p_map = accessible_data.begin(); p_map != accessible_data.end(); p_map++)
            {
                ar & p_map->first;
                ar & p_map->second.first;
                ar & p_map->second.second;               
            }
            for (auto p_map = inaccessible_data.begin(); p_map != inaccessible_data.end(); p_map++)
            {
                ar & p_map->first;
                ar & p_map->second; 
            }
            for (auto p_map = inaccessible_grid.begin(); p_map != inaccessible_grid.end(); p_map++)
            {
                ar & p_map->first;
                ar & p_map->second; 
            }
        
        }

        BOOST_SERIALIZATION_SPLIT_MEMBER()
*/
        std::unordered_multimap<Point, std::pair<RawDataRecord<Type>, DataSig<Type>>> accessible_data;
        std::unordered_multimap<Point, DataSig<Type>> inaccessible_data;
        std::unordered_multimap<Rectangle, GridSig<Type>> inaccessible_grid;
 //       std::multimap<Point, std::pair<RawDataRecord<Type>, DataSig<Type>>> accessible_data;
 //       std::multimap<Point, DataSig<Type>> inaccessible_data;
 //       std::multimap<Rectangle, GridSig<Type>> inaccessible_grid;

        private:
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& accessible_data;
            ar& inaccessible_data;
            ar& inaccessible_grid;
        }

    };
};

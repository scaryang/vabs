#pragma once

#include "../db/data.hpp"
#include "../db/meta.hpp"
#include "../db/sig.hpp"
#include "../db/type.hpp"
#include "../utils/sfinae_helper.hpp"
//#include "../utils/timer.hpp"
#include "./index.hpp"
#include "./query.hpp"

#include <sqlite_modern_cpp.h>
#include <map>
#include <set>
#include <string>

namespace DB
{
    template <class Type>
    class DupDatabase
    {
    public:
        DupDatabase(const std::string& db_path)
            : db_path(db_path), db(db_path)
        {
        }
        DupDatabase(const DupDatabase&) = delete;

        void create_meta(const pbc::PairingParamPtr& pairing_param,
                         typename Type::role_type& pseudo_role,
                         std::set<typename Type::role_type>& global_roles,
                         size_t dimension, Rectangle& range)
        {
            meta = Meta<Type>(pairing_param, pseudo_role, global_roles,
                              dimension, range);
        }
        const ABS::MasterVerifyingKey& get_mvk() const
        {
            return meta.kdc.get_master_verifying_key();
        }
        //double get_index_create_time() const { return meta.index_create_time; }
        //double get_index_sign_time() const { return meta.index_sign_time(); }
        Rectangle get_range() const { return meta.range; }
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            std::set<typename Type::role_type>>::type
        compute_inaccessible_roles(const Container& roles) const
        {
            std::set<typename Type::role_type> inaccessible_roles;
            for (const auto& role : meta.global_roles) {
                if (roles.find(role) == roles.end())
                    inaccessible_roles.insert(role);
            }
            return inaccessible_roles;
        }
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            typename Type::policy_type>::type
        compute_inaccessible_policy(const Container& roles) const
        {
            auto inaccessible_roles = compute_inaccessible_roles(roles);
            return compute_inaccessible_policy_from_inaccessible_roles(
                inaccessible_roles);
        }
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            typename Type::policy_type>::type
        compute_inaccessible_policy_from_inaccessible_roles(
            const Container& inaccessible_roles) const
        {
            ABS::BooleanGate<typename Type::role_type> gate;
            gate.exprs.reserve(inaccessible_roles.size());
            gate.type = ABS::BooleanGateType::OR;
            for (const auto& role : inaccessible_roles) {
                gate.exprs.push_back(typename Type::policy_type(role));
            }
            if (gate.exprs.size() == 1)
                return gate.exprs[0];
            else
                return typename Type::policy_type(gate);
        }
        template <class Container>
        typename std::enable_if<
            sfinae::is_map_container_with_fixed_type<
                Container, Point, RawDataRecord<Type>>::value,
            void>::type
        create_index(const Container& data, IndexType type)
        {
            //Timer index_create_timer;
            //index_create_timer.start();
            typename Type::dnf_policy_type pseudo_policy{{meta.pseudo_role}};
            switch (type) {
                case IndexType::grid:
                    index = std::move(DupIndex<Type>::create_grid(meta.range, data,
                                                               pseudo_policy));
                    meta.index_type = "grid";
                    break;
                case IndexType::kdtree:
                    index = std::move(DupIndex<Type>::create_kdtree(
                        meta.range, data, pseudo_policy));
                    meta.index_type = "kdtree";
                    break;
                case IndexType::hybrid:
                    index = std::move(DupIndex<Type>::create_hybrid(
                        meta.range, data, pseudo_policy));
                    meta.index_type = "hybrid";
                    break;
            }
            //index_create_timer.stop();
            //meta.index_create_time = index_create_timer.result();
        }
        void reset_sig()
        {
            db << "begin;";
            db << "DROP TABLE IF EXISTS DataSig;";
            db << "DROP TABLE IF EXISTS GridSig;";
            db << "end;";
        }
        int max_data_id()
        {
            int result;
            db << "SELECT MAX(id) FROM Data;" >> result;
            return result;
        }
        int max_grid_id()
        {
            int result;
            db << "SELECT MAX(id) FROM IndexTree;" >> result;
            return result;
        }
        void create_data_sig(int min_id = -1, int max_id = -1)
        {
            auto data_records = DataRecord<Type>::load(db, min_id, max_id);
            std::vector<DataSig<Type>> data_sigs;
            data_sigs.resize(data_records.size());
            auto mvk = get_mvk();
            //Timer index_sign_timer;
            //index_sign_timer.start();
#pragma omp parallel
#pragma omp single
            {
                size_t i = 0;
                for (auto ptr = data_records.begin(); ptr != data_records.end();
                     ++ptr, ++i) {
#pragma omp task firstprivate(ptr, i) shared(data_sigs)
                    {
                        auto sig = DataSig<Type>::create(ptr->second, mvk,
                                                         meta.do_key);
                        data_sigs[i] = sig;
                    }
                }
#pragma omp taskwait
            }
            
            //index_sign_timer.stop();
            //meta.index_sign_data_time += index_sign_timer.result();
            db << "begin;";
            db << R"(CREATE TABLE IF NOT EXISTS DataSig(
                     id          INTEGER PRIMARY KEY,
                     value_hash  BLOB,
                     sig_message BLOB,
                     sig         BLOB);)";
            auto insert_ps = db << "INSERT INTO DataSig VALUES (?,?,?,?);";
            for (auto sig : data_sigs) sig.save(insert_ps);
            insert_ps.used(true);
            //db << "UPDATE Meta SET value=value+? WHERE "
            //      "key='index_sign_data_time';"
            //   << index_sign_timer.result();
            db << "end;";
        }

        void create_grid_sig(int min_id = -1, int max_id = -1)
        {
            auto grids = Grid<Type>::load(db, min_id, max_id);
            std::vector<GridSig<Type>> grid_sigs;
            grid_sigs.resize(grids.size());
            auto mvk = get_mvk();
            //Timer index_sign_timer;
            //index_sign_timer.start();
#pragma omp parallel
#pragma omp single
            {
                size_t i = 0;
                for (auto ptr = grids.begin(); ptr != grids.end(); ++ptr, ++i) {
#pragma omp task firstprivate(ptr, i) shared(grid_sigs)
                    {
                        auto sig = GridSig<Type>::create(ptr->second, mvk,
                                                         meta.do_key);
                        grid_sigs[i] = sig;
                    }
                }
#pragma omp taskwait
            }
            //index_sign_timer.stop();
            //meta.index_sign_grid_time += index_sign_timer.result();
            db << "begin;";
            db << R"(CREATE TABLE IF NOT EXISTS GridSig(
                     id          INTEGER PRIMARY KEY,
                     sig_message BLOB,
                     sig         BLOB);)";
            auto insert_ps = db << "INSERT INTO GridSig VALUES (?,?,?);";
            for (auto sig : grid_sigs) sig.save(insert_ps);
            insert_ps.used(true);
            //db << "UPDATE Meta SET value=value+? WHERE "
            //      "key='index_sign_grid_time';"
            //   << index_sign_timer.result();
            db << "end;";
        }
        void load(bool load_index = true)
        {
            meta.load(db);
            if (load_index) index = DupIndex<Type>::load(db);
        }
        void save(bool save_index = true)
        {
            meta.save(db);
            if (index && save_index) index->save(db);
        }


/*


        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            EqualityQueryResult<Type>>::type
        equality_query(const Point& point, const Container& roles,
                       const Container& inaccessible_roles)
        {
            if (!index->get_box().cover(point))
                throw std::out_of_range("equality query out of range");
            
            //datarecord
            auto node =
                boost::apply_visitor(EqualityQueryWalker<Type>(point), *index);
                
            EqualityQueryResult<Type> result;

            result.accessible = node->access(roles);

            result.sig = DataSig<Type>::load(db, meta.pairing,
                                             std::vector<int>{node->id})
                             .begin()
                             ->second;
                             
            if (result.accessible) {
                result.record = node->to_raw_data_record();
            } else {
                result.sig = result.sig.relax(
                    meta.kdc.get_master_verifying_key(), inaccessible_roles);
            }
            return result;
        }
*/

        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            DupRangeQueryResult<Type>>::type
        range_query(const Rectangle& range, const Container& roles)
        {
            auto inaccessible_roles = compute_inaccessible_roles(roles);
            return range_query(range, roles, inaccessible_roles);
        }
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            DupRangeQueryResult<Type>>::type
        range_query(const Rectangle& range, const Container& roles,
                    const Container& inaccessible_roles)
        {
            DupRangeQueryIntermediateResult<Type> intermidate_result =
                boost::apply_visitor(
                    DupRangeQueryWalker<Type, Container>(range, roles),
                    *index);
            DupRangeQueryResult<Type> result;
            auto accessible_data_sig = DataSig<Type>::load(
                db, meta.pairing, intermidate_result.accessible_data_ids());
            auto inaccessible_data_sig = DataSig<Type>::load(
                db, meta.pairing, intermidate_result.inaccessible_data_ids());
            auto inaccessible_grid_sig = GridSig<Type>::load(
                db, meta.pairing, intermidate_result.inaccessible_grid_ids());
            for (auto p : intermidate_result.accessible_data) {
                result.accessible_data.insert(std::make_pair(
                    p.second->point,
                    std::make_pair(p.second->to_raw_data_record(),
                                   accessible_data_sig[p.first])));
            }
            for (auto p : intermidate_result.inaccessible_data) {
                result.inaccessible_data.insert(std::make_pair(
                    p.second->point, inaccessible_data_sig[p.first]));
            }
            for (auto p : intermidate_result.inaccessible_grid) {
                result.inaccessible_grid.insert(std::make_pair(
                    p.second->box, inaccessible_grid_sig[p.first]));
            }
            result.process_signs(get_mvk(), inaccessible_roles);
            return result;
        }

    private:
        std::string db_path;
        sqlite::database db;
        Meta<Type> meta;
        std::unique_ptr<DupIndex<Type>> index;
    };
};

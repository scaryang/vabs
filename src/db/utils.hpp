#pragma once

#include <cryptopp/sha.h>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/serialization.hpp>
#include <vector>

namespace DB
{
    namespace Utils
    {
        template <class Type>
        //inline
        inline std::string value_to_hash(const Type& input)
        {
            std::string output(CryptoPP::SHA1::DIGESTSIZE, 0);
            CryptoPP::SHA1 sha1;
            sha1.CalculateDigest((unsigned char*)output.data(),
                                 (const unsigned char*)&input, sizeof(input));
            return output;
        }

        template <>
        inline std::string value_to_hash(const std::string& input)
        {
            std::string output(CryptoPP::SHA1::DIGESTSIZE, 0);
            CryptoPP::SHA1 sha1;
            sha1.CalculateDigest((unsigned char*)output.data(),
                                 (const unsigned char*)input.data(),
                                 input.size());
            return output;
        }

        template <class T>
        std::vector<char> object_to_blob(const T& in)
        {
            using namespace boost::iostreams;
            std::vector<char> out;
            back_insert_device<std::vector<char>> sink{out};
            stream<back_insert_device<std::vector<char>>> buf{sink};
            boost::archive::binary_oarchive oarch(buf);
            oarch << in;
            return out;
        }

        template <class T>
        void blob_to_object(const std::vector<char>& in, T& out)
        {
            using namespace boost::iostreams;
            stream<array_source> buf(in.data(), in.size());
            boost::archive::binary_iarchive iarch(buf);
            iarch >> out;
        }
    };
};

#pragma once

#include "../abs/key.hpp"
#include "./rect.hpp"
#include "./type.hpp"
#include "./utils.hpp"

#include <sqlite_modern_cpp.h>
#include <boost/serialization/set.hpp>
#include <set>
#include <string>

namespace DB
{
    template <class Type>
    class Meta
    {
    public:
        Meta() = default;
        Meta(const Meta&) = default;
        Meta(const pbc::PairingParamPtr& pairing_param,
             typename Type::role_type& pseudo_role,
             std::set<typename Type::role_type>& global_roles, size_t dimension,
             Rectangle& range)
            : pairing_param(pairing_param),
              pseudo_role(pseudo_role),
              global_roles(global_roles),
              dimension(dimension),
              range(range),
              index_type("")
              //index_create_time(0),
              //index_sign_data_time(0),
              //index_sign_grid_time(0)
        {
            pairing = pbc::Pairing::init_from_param(pairing_param);
            kdc.setup(pairing);
            do_key = kdc.generate_key<typename Type::role_type>(global_roles);
        }
        /*
        double index_sign_time() const
        {
            return index_sign_data_time + index_sign_grid_time;
        }
        */
        void save(sqlite::database& db) const
        {
            db << "begin;";
            db << "DROP TABLE IF EXISTS Meta;";
            db << "CREATE TABLE Meta(key, value);";
            auto ps = db << "INSERT INTO Meta VALUES (?, ?);";
            ps << "pairing_param"
               << Utils::object_to_blob(pairing_param->to_str());
            ps++;
            ps << "kdc" << Utils::object_to_blob(kdc);
            ps++;
            ps << "do_key" << Utils::object_to_blob(do_key);
            ps++;
            ps << "pseudo_role" << Utils::object_to_blob(pseudo_role);
            ps++;
            ps << "global_roles" << Utils::object_to_blob(global_roles);
            ps++;
            ps << "dimension" << (int)dimension;
            ps++;
            ps << "range" << Utils::object_to_blob(range);
            ps++;
            ps << "index_type" << index_type;
            //ps++;
            //ps << "index_create_time" << index_create_time;
            //ps++;
            //ps << "index_sign_data_time" << index_sign_data_time;
            //ps++;
           // ps << "index_sign_grid_time" << index_sign_grid_time;
            ps.execute();
            db << "commit;";
        }
        void load(sqlite::database& db)
        {
            db << "SELECT value FROM Meta WHERE key='pairing_param';" >>
                [&](std::vector<char> value) {
                    std::string s;
                    Utils::blob_to_object(value, s);
                    pairing_param = pbc::PairingParam::init_from_str(s);
                    pairing = pbc::Pairing::init_from_param(pairing_param);
                    ABS::GlobalPBCPairing::set(pairing);
                };
            db << "SELECT key, value FROM Meta "
                  "WHERE key NOT IN ('pairing_param', 'dimension', "
                  "'index_type', 'index_create_time', 'index_sign_data_time', "
                  "'index_sign_grid_time');" >>
                [&](std::string key, std::vector<char> value) {
                    if (key == "kdc") {
                        Utils::blob_to_object(value, kdc);
                    } else if (key == "do_key") {
                        Utils::blob_to_object(value, do_key);
                    } else if (key == "pseudo_role") {
                        Utils::blob_to_object(value, pseudo_role);
                    } else if (key == "global_roles") {
                        Utils::blob_to_object(value, global_roles);
                    } else if (key == "range") {
                        Utils::blob_to_object(value, range);
                    }
                };
            db << "SELECT value FROM Meta WHERE key='dimension';" >>
                [&](int value) { dimension = (size_t)value; };
            db << "SELECT value FROM Meta WHERE key='index_type';" >>
                [&](std::string value) { index_type = value; };
            /*
            db << "SELECT key, value FROM Meta "
                  "WHERE key IN ('index_create_time', 'index_sign_data_time', "
                  "'index_sign_grid_time');" >>
                [&](std::string key, double value) {
                    if (key == "index_create_time") {
                        index_create_time = value;
                    } else if (key == "index_sign_data_time") {
                        index_sign_data_time = value;
                    } else if (key == "index_sign_grid_time") {
                        index_sign_grid_time = value;
                    }
                };
            */
            ABS::GlobalPBCPairing::clear();
        }

        // ABS related
        pbc::PairingParamPtr pairing_param;
        pbc::PairingPtr pairing;
        ABS::KeyDistributionCenter kdc;
        typename Type::do_key_type do_key;
        typename Type::role_type pseudo_role;
        std::set<typename Type::role_type> global_roles;
        // Dateset related
        size_t dimension;
        Rectangle range;
        std::string index_type;
        // Record time
        //double index_create_time;
        //double index_sign_data_time;
        //double index_sign_grid_time;
    };
};

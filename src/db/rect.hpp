#pragma once

#include "../utils/sfinae_helper.hpp"

#include <algorithm>
#include <boost/functional/hash.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <functional>
#include <list>
#include <prettyprint.hpp>
#include <ostream>
#include <vector>

namespace DB
{
    class Point : public std::vector<int>
    {
    public:
        using std::vector<int>::vector;

        size_t get_vo_size() const { return size() * sizeof(int); }

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& boost::serialization::base_object<std::vector<int>>(*this);
        }
    };

    enum class RectCoverState { Full, Partial, None };

    class Rectangle
    {
    public:
        Rectangle() = default;
        Rectangle(const Point& p1, const Point& p2) : p1(p1), p2(p2) {}
        Rectangle(const Rectangle&) = default;
        Rectangle(Rectangle&& other)
            : p1(std::move(other.p1)), p2(std::move(other.p2))
        {
        }

        size_t dimension() const { return p1.size(); }
        size_t get_vo_size() const { return 2 * p1.size() * sizeof(int); }
        const Point& get_p1() const { return p1; }
        const Point& get_p2() const { return p2; }
        int get_p1(size_t idx) const { return p1[idx]; }
        int get_p2(size_t idx) const { return p2[idx]; }
        void set_p1(const Point& p) { p1 = p; }
        void set_p2(const Point& p) { p2 = p; }
        void set_p1(size_t idx, int value) { p1[idx] = value; }
        void set_p2(size_t idx, int value) { p2[idx] = value; }
        Rectangle& operator=(const Rectangle& other)
        {
            p1 = other.p1;
            p2 = other.p2;
            return *this;
        }
        Rectangle& operator=(Rectangle&& other)
        {
            p1.swap(other.p1);
            p2.swap(other.p2);
            return *this;
        }
        bool operator==(const Rectangle& other) const
        {
            return p1 == other.p1 && p2 == other.p2;
        }
        bool operator!=(const Rectangle& other) const
        {
            return !(operator==(other));
        }

        // return true if the point is inside rectangle.
        bool cover(const Point& point) const
        {
            size_t d = p1.size();
            for (size_t i = 0; i < d; ++i) {
                if (point[i] < p1[i]) return false;
                if (p2[i] < point[i]) return false;
            }
            return true;
        }

        RectCoverState cover(const Rectangle& other) const
        {
            size_t d = p1.size();
            Rectangle intersec = intersection(other);
            for (size_t i = 0; i < d; ++i) {
                if (intersec.p1[i] > intersec.p2[i])
                    return RectCoverState::None;
            }
            if (intersec == other)
                return RectCoverState::Full;
            else
                return RectCoverState::Partial;
        }

        // return true if it is completely covered by the union of
        // rectangles.
        template <class Container>
        typename std::enable_if<sfinae::is_container_with_fixed_value_type<
                                    Container, Rectangle>::value,
                                bool>::type
        covered_by(const Container& rects) const
        {
            size_t d = p1.size();
            std::list<Rectangle> remains;
            remains.push_back(*this);
            for (const auto& rect : rects) {
                auto ptr = remains.begin();
                while (ptr != remains.end()) {
                    Rectangle intersec = ptr->intersection(rect);
                    bool not_intersec = false;
                    for (size_t i = 0; i < d; ++i) {
                        if (intersec.p1[i] > intersec.p2[i]) {
                            not_intersec = true;
                            break;
                        }
                    }
                    if (not_intersec)
                        ++ptr;
                    else {
                        remains.splice(remains.end(), ptr->subtract(intersec));
                        remains.erase(ptr++);

                        // if rect is completely covered by one fragment
                        if (intersec == rect) break;
                    }
                }
            }
            return remains.empty();
        }

        Rectangle intersection(const Rectangle& other) const
        {
            size_t d = p1.size();
            Point new_p1(d), new_p2(d);
            std::transform(p1.begin(), p1.end(), other.p1.begin(),
                           new_p1.begin(),
                           [](int a, int b) { return std::max(a, b); });
            std::transform(p2.begin(), p2.end(), other.p2.begin(),
                           new_p2.begin(),
                           [](int a, int b) { return std::min(a, b); });
            return Rectangle(new_p1, new_p2);
        }

        // require other is completely covered by the rectangle.
        std::list<Rectangle> subtract(const Rectangle& other,
                                      size_t axis = 0) const
        {
            size_t d = p1.size();
            std::list<Rectangle> fragments;
            int a1 = p1[axis];
            int a2 = p2[axis];
            int b1 = other.p1[axis];
            int b2 = other.p2[axis];

            if (a1 < b1) {
                Rectangle rect(*this);
                rect.p1[axis] = a1;
                rect.p2[axis] = b1 - 1;
                fragments.push_back(std::move(rect));
            }

            if (axis != d - 1) {
                Rectangle rect(*this);
                rect.p1[axis] = b1;
                rect.p2[axis] = b2;
                fragments.splice(fragments.end(),
                                 rect.subtract(other, axis + 1));
            }

            if (b2 < a2) {
                Rectangle rect(*this);
                rect.p1[axis] = b2 + 1;
                rect.p2[axis] = a2;
                fragments.push_back(std::move(rect));
            }

            return fragments;
        }

        // return all points inside rectangle
        std::vector<Point> all_points() const
        {
            size_t d = p1.size();
            std::vector<Point> result;

            for (int x = p1[0]; x <= p2[0]; ++x) {
                result.push_back(Point({x}));
            }

            for (size_t i = 1; i < d; ++i) {
                std::vector<Point> _result;
                for (int x = p1[i]; x <= p2[i]; ++x) {
                    for (const Point& p : result) {
                        Point _p(p);
                        _p.push_back(x);
                        _result.push_back(std::move(_p));
                    }
                }
                result.swap(_result);
            }
            return result;
        }

    private:
        // area covered by [p1, p2], where p1 <= p2
        Point p1, p2;

        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& p1;
            ar& p2;
        }
    };

    std::ostream& operator<<(std::ostream& o, const Rectangle& rect)
    {
        o << "(" << rect.get_p1() << ", " << rect.get_p2() << ")";
        return o;
    }
};

namespace std
{
    template <>
    struct hash<DB::Point> {
        size_t operator()(const DB::Point& point) const
        {
            return boost::hash_range(point.begin(), point.end());
        }
    };

    template <>
    struct hash<DB::Rectangle> {
        size_t operator()(const DB::Rectangle& rect) const
        {
            size_t seed = 0;
            boost::hash_combine(seed, rect.get_p1());
            boost::hash_combine(seed, rect.get_p2());
            return seed;
        }
    };
};

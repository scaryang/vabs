#pragma once

#include "./data.hpp"
#include "./index.hpp"
#include "./rect.hpp"
#include "./type.hpp"
#include "./utils.hpp"
#include "../utils/sfinae_helper.hpp"

#include <algorithm>
#include <unordered_map>
#include <vector>

#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/unordered_map.hpp>
//qt gui
//#include <QtCoreQCoreApplication>

namespace DB
{
    template <class Type>
    class EqualityQueryWalker
        : public boost::static_visitor<const DataRecord<Type>*>
    {
    public:
        //avoid implicit conversion
        explicit EqualityQueryWalker(const Point& point) : point(point) {}
        
        //leafnode
        const DataRecord<Type>* operator()(
            const std::unique_ptr<IndexLeafNode<Type>>& node) const
        {
            //output datarecord
            return node->leaves.at(point).get();
        }
        
        //nonleafnode
        const DataRecord<Type>* operator()(
            const std::unique_ptr<IndexNonLeafNode<Type>>& node) const
        {
            auto next_node = std::find_if(
                node->leaves.begin(), node->leaves.end(), [&](const auto& in) {
                    return in.second->get_box().cover(point);
                });
            // ->second is *index , *this is the object of whole class
                return boost::apply_visitor(*this, *next_node->second);
        }
                            
    private:
        const Point& point;
    };
    

    template <class Type>
    class EqualityQueryResult
    {
    public:
        bool verify(const ABS::MasterVerifyingKey& mvk, const Point& point,
                    const typename Type::policy_type& inaccessible_policy) const
        {
            if (accessible)
                return sig.verify(mvk, point, record);
            else
                return sig.verify(mvk, point, inaccessible_policy);
        }
        size_t get_vo_size() const
        {
            if (accessible)
                return sig.get_size_when_accessible();
            else
                return sig.get_size_when_inaccessible();
        }

        bool accessible;
        RawDataRecord<Type> record;
        DataSig<Type> sig;


        private:
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& accessible;
            ar& record;
            ar& sig;
        }

    };

    template <class Type>
    class RangeQueryIntermediateResult
    {
    public:
        //record accessible_data_id
        std::vector<int> accessible_data_ids() const
        {
            std::vector<int> result;
            result.reserve(accessible_data.size());
            for (const auto& p : accessible_data) result.push_back(p.first);
            return result;
        }
        //record inaccessible dataid
        std::vector<int> inaccessible_data_ids() const
        {
            std::vector<int> result;
            result.reserve(inaccessible_data.size());
            for (const auto& p : inaccessible_data) result.push_back(p.first);
            return result;
        }
        //record inaccessible grid_ids
        std::vector<int> inaccessible_grid_ids() const
        {
            std::vector<int> result;
            result.reserve(inaccessible_grid.size());
            for (const auto& p : inaccessible_grid) result.push_back(p.first);
            return result;
        }
        std::map<int, const DataRecord<Type>*> accessible_data;
        std::map<int, const DataRecord<Type>*> inaccessible_data;
        std::map<int, const Grid<Type>*> inaccessible_grid;
        //std::unique_ptr<MHTResult<Type>> mht_result;
    };

    template <class Type, class Container,
              class = typename std::enable_if<
                  sfinae::is_container_with_fixed_type_and_find_method<
                      Container, typename Type::role_type>::value>::type>
    class RangeQueryWalker
        : public boost::static_visitor<RangeQueryIntermediateResult<Type>>
    {
    public:
        explicit RangeQueryWalker(const Rectangle& range,
                                  const Container& roles)
            : range(range), roles(roles)
        {
        }

        RangeQueryIntermediateResult<Type> operator()(
            const std::unique_ptr<IndexLeafNode<Type>>& node) const
        {
            RangeQueryIntermediateResult<Type> result;

            switch (range.cover(node->box)) {

                case RectCoverState::Full:
                    if (node->access(roles)) {
                        for (auto ptr = node->leaves.begin();
                             ptr != node->leaves.end(); ++ptr) {
                            if (ptr->second->access(roles)) {
                                result.accessible_data.insert(std::make_pair(
                                    ptr->second->id, ptr->second.get()));
                            } else {
                                result.inaccessible_data.insert(std::make_pair(
                                    ptr->second->id, ptr->second.get()));
                            }
                        }
                        /*
                        if (has_mht) {
                            auto mht_leaf =
                                std::make_unique<MHTResultLeaf<Type>>();
                            mht_leaf->data_ids.reserve(node->leaves.size());
                            for (auto ptr = node->leaves.begin();
                                 ptr != node->leaves.end(); ++ptr) {
                                mht_leaf->data_ids.push_back(ptr->second->id);
                            }
                            mht_leaf->grid_id = node->id;
                            result.mht_result =
                                std::make_unique<MHTResult<Type>>(
                                    std::move(mht_leaf));
                        };*/
                    } else {
                        result.inaccessible_grid.insert(
                            std::make_pair(node->id, node.get()));
                        /*
                        if (has_mht) {
                            auto mht_grid =
                                std::make_unique<MHTResultGrid<Type>>();
                            mht_grid->leaf_hash = node->leaf_hash;
                            mht_grid->grid_id = node->id;
                            result.mht_result =
                                std::make_unique<MHTResult<Type>>(
                                    std::move(mht_grid));
                        };
                        */
                    }
                    break;

                case RectCoverState::Partial:
                    for (auto ptr = node->leaves.begin();
                         ptr != node->leaves.end(); ++ptr) {
                        //if node isnot covered, end for;
                        if (!range.cover(ptr->first)) continue;
                        if (ptr->second->access(roles)) {
                            result.accessible_data.insert(std::make_pair(
                                ptr->second->id, ptr->second.get()));
                        } else {
                            result.inaccessible_data.insert(std::make_pair(
                                ptr->second->id, ptr->second.get()));
                        }
                    }
                    /*
                    if (has_mht) {
                        auto mht_leaf = std::make_unique<MHTResultLeaf<Type>>();
                        mht_leaf->data_ids.reserve(node->leaves.size());
                        for (auto ptr = node->leaves.begin();
                             ptr != node->leaves.end(); ++ptr) {
                            mht_leaf->data_ids.push_back(ptr->second->id);
                        }
                        mht_leaf->grid_id = node->id;
                        result.mht_result = std::make_unique<MHTResult<Type>>(
                            std::move(mht_leaf));
                    };
                    */
                    break;

                case RectCoverState::None:
                    /*
                    if (has_mht) {
                        auto mht_hash = std::make_unique<MHTResultHash<Type>>();
                        mht_hash->hash = node->hash;
                        result.mht_result = std::make_unique<MHTResult<Type>>(
                            std::move(mht_hash));
                    */
                    break;

            };
            return result;
        }


        RangeQueryIntermediateResult<Type> operator()(
            const std::unique_ptr<IndexNonLeafNode<Type>>& node) const
        {
            RangeQueryIntermediateResult<Type> result;

            switch (range.cover(node->box)) {

                case RectCoverState::Full:
                    if (!node->access(roles)) {
                        result.inaccessible_grid.insert(
                            std::make_pair(node->id, node.get()));
                        /*
                        if (has_mht) {
                            auto mht_grid =
                                std::make_unique<MHTResultGrid<Type>>();
                            mht_grid->leaf_hash = node->leaf_hash;
                            mht_grid->grid_id = node->id;
                            result.mht_result =
                                std::make_unique<MHTResult<Type>>(
                                    std::move(mht_grid));
                        };
                        */
                        break;
                    }

                case RectCoverState::Partial: {
                    /*
                    auto mht_nonleaf =
                        std::make_unique<MHTResultNonLeaf<Type>>();
                    mht_nonleaf->leaves.resize(node->leaves.size());
                    size_t mht_nonleaf_idx = 0;
                    */
                    for (auto ptr = node->leaves.begin();
                         ptr != node->leaves.end(); ++ptr) {
                        auto result_ =
                            boost::apply_visitor(*this, *ptr->second);
                        result.accessible_data.insert(
                            result_.accessible_data.begin(),
                            result_.accessible_data.end());
                        result.inaccessible_data.insert(
                            result_.inaccessible_data.begin(),
                            result_.inaccessible_data.end());
                        result.inaccessible_grid.insert(
                            result_.inaccessible_grid.begin(),
                            result_.inaccessible_grid.end());
                        /*
                        if (has_mht) {
                            mht_nonleaf->leaves[mht_nonleaf_idx++].swap(
                                result_.mht_result);
                        }
                        */
                    }
                    /*
                    if (has_mht) {
                        mht_nonleaf->grid_id = node->id;
                        result.mht_result = std::make_unique<MHTResult<Type>>(
                            std::move(mht_nonleaf));
                    }
                    */
                } break;

                case RectCoverState::None: break;
                    /*
                    if (has_mht) {
                        auto mht_hash = std::make_unique<MHTResultHash<Type>>();
                        mht_hash->hash = node->hash;
                        result.mht_result = std::make_unique<MHTResult<Type>>(
                            std::move(mht_hash));
                    }
                    */    
            };
            return result;
        }

    private:
        const Rectangle& range;
        const Container& roles;
        //bool has_mht;
    };

    template <class Type>
    class RangeQueryResult
    {
    public:
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            void>::type
        process_signs(const ABS::MasterVerifyingKey& mvk,
                      const Container& inaccessible_roles)
        {
#pragma omp parallel
#pragma omp single
            {
                for (auto ptr = inaccessible_data.begin();
                     ptr != inaccessible_data.end(); ++ptr) {
#pragma omp task firstprivate(ptr)
                    ptr->second = ptr->second.relax(mvk, inaccessible_roles);
                }

                for (auto ptr = inaccessible_grid.begin();
                     ptr != inaccessible_grid.end(); ++ptr) {
#pragma omp task firstprivate(ptr)
                    ptr->second = ptr->second.relax(mvk, inaccessible_roles);
                }

#pragma omp taskwait
            }
        }


        bool verify(const ABS::MasterVerifyingKey& mvk, const Rectangle& range,
                    const typename Type::policy_type& inaccessible_policy) const
        {
            size_t len = accessible_data.size() + inaccessible_data.size() +
                         inaccessible_grid.size();
            std::vector<Rectangle> fragments;
            fragments.reserve(len);
            std::vector<int> result(len + 1 , 0);

            size_t i = 0;
#pragma omp parallel
#pragma omp single
            {
                for (auto ptr = accessible_data.begin();
                     ptr != accessible_data.end(); ++ptr, ++i) {
#pragma omp task firstprivate(ptr, i) shared(result)
                    {
                        bool r = ptr->second.second.verify(mvk, ptr->first,
                                                           ptr->second.first);
                        result[i] = (int)r;
                        //qt
                        //QCoreApplication::processEvents();
                    }
                    fragments.push_back(Rectangle(ptr->first, ptr->first));
                }
                for (auto ptr = inaccessible_data.begin();
                     ptr != inaccessible_data.end(); ++ptr, ++i) {
#pragma omp task firstprivate(ptr, i) shared(result)
                    {
                        bool r = ptr->second.verify(mvk, ptr->first,
                                                    inaccessible_policy);
                        result[i] = (int)r;
                        //qt
                        //QCoreApplication::processEvents();
                    }
                    fragments.push_back(Rectangle(ptr->first, ptr->first));
                }
                for (auto ptr = inaccessible_grid.begin();
                     ptr != inaccessible_grid.end(); ++ptr, ++i) {
#pragma omp task firstprivate(ptr, i) shared(result)
                    {
                        bool r = ptr->second.verify(mvk, ptr->first,
                                                    inaccessible_policy);
                        result[i] = (int)r;
                        //qt
                        //QCoreApplication::processEvents();
                    }
                    fragments.push_back(ptr->first);
                }

#pragma omp task firstprivate(i) shared(result)
                {
                    bool r = range.covered_by(fragments);
                    result[i] = (int)r;
                }

//                ++i;
/*
#pragma omp task firstprivate(i) shared(result)
                {
                    bool r = verify_mht(mht_hash);
                    result[i] = (int)r;
                }
*/
#pragma omp taskwait
            }

            for (int r : result) {
                if (!(bool)r) return false;
            }
            return true;
        }
        /*
        bool verify_mht(const std::string& root_hash) const
        {
            if (!mht_result) return true;

            return mht_result->compute_mht_hash() == root_hash;
        }
        */

        size_t get_vo_size() const
        {
            size_t size = 0;
            for (const auto& ptr : accessible_data)
                size += ptr.second.second.get_size_when_accessible();
            for (const auto& ptr : inaccessible_data)
                size += ptr.first.get_vo_size() +
                        ptr.second.get_size_when_inaccessible();
            for (const auto& ptr : inaccessible_grid)
                size += ptr.first.get_vo_size() +
                        ptr.second.get_size_when_inaccessible();
            //size += get_mht_size();
            return size;
        }
        /*
        size_t get_mht_size() const
        {
            if (!mht_result) return 0;

            return mht_result->get_size();
        }
        */

        std::unordered_map<Point, std::pair<RawDataRecord<Type>, DataSig<Type>>>
            accessible_data;
        std::unordered_map<Point, DataSig<Type>> inaccessible_data;
        std::unordered_map<Rectangle, GridSig<Type>> inaccessible_grid;
        //std::unique_ptr<MHTResult<Type>> mht_result;
                

        private:
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& accessible_data;
            ar& inaccessible_data;
            ar& inaccessible_grid;
        }


    };
};

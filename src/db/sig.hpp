#pragma once

#include "../utils/sfinae_helper.hpp"
#include "./data.hpp"
#include "./rect.hpp"
#include "./type.hpp"
#include "./utils.hpp"

#include <cryptopp/osrng.h>
#include <cryptopp/sha.h>
#include <sqlite_modern_cpp.h>
#include <map>
#include <string>
#include <vector>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>

namespace DB
{
    template <class Type>
    class DataSig
    {
    public:
        DataSig() = default;
        DataSig(const DataSig<Type>&) = default;

        static DataSig create(const DataRecord<Type>& record,
                              const ABS::MasterVerifyingKey& mvk,
                              const typename Type::do_key_type& key)
        {

            static CryptoPP::NonblockingRng rng;
            DataSig s;

            //sig id = data id
            s.id = record.id;
            
            //generate value_hash
            if (record.pseudo) {
                //false record
                s.value_hash.resize(CryptoPP::SHA1::DIGESTSIZE);
                //random..
                rng.GenerateBlock((unsigned char*)s.value_hash.data(),
                                  CryptoPP::SHA1::DIGESTSIZE);
            } else {
                //true record
                //datarecord valuehash
                s.value_hash = record.value_hash();
            }
            
            s.sig_message = s.compute_sig_message(record.point);
            
            //abs.sign needs mvk, Signingkey, sig message, policy
            s.sig = ABS::sign(mvk, key, s.sig_message, record.get_policy());
            return s;
        }

        //inaccessible data
        bool verify(const ABS::MasterVerifyingKey& mvk, const Point& point,
                    const RawDataRecord<Type>& record) const
        {
            bool result = true;
            std::string real_value_hash = record.value_hash();
            //sig belong to private
            //it's std::string type
            result &= ABS::verify(mvk, sig,
                                  compute_sig_message(point, real_value_hash));
            //and operator
            //verify the signature
            result &= sig.get_predicate() == record.get_policy();
            return result;
        }

        //bool accessible data
        bool verify(const ABS::MasterVerifyingKey& mvk, const Point& point,
                    const typename Type::policy_type& policy) const
        {
            bool result = true;
            result &= ABS::verify(mvk, sig, compute_sig_message(point));
            result &= sig.get_predicate() == policy;
            return result;
        }

        //type of relax is class datasig
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            DataSig>::type
        relax(const ABS::MasterVerifyingKey& mvk,
              const Container& inaccessible_roles) const
        {
            DataSig s(*this);
            s.value_hash = value_hash;
            s.sig = ABS::relax(mvk, sig, sig_message, inaccessible_roles);
            return s;
        }

        //get tag and size
        std::string get_sig_tag() const { return sig.get_tag(); }
        size_t get_size_when_accessible() const { return sig.get_size(); }
        size_t get_size_when_inaccessible() const
        {
            return sig.get_size() + CryptoPP::SHA1::DIGESTSIZE;
        }
        size_t get_index_size() const
        {
            return sig.get_size_compressed() + CryptoPP::SHA1::DIGESTSIZE;
        }

        void save(sqlite::database_binder& insert_ps) const
        {
            insert_ps << id << Utils::object_to_blob(value_hash)
                      << Utils::object_to_blob(sig_message)
                      << Utils::object_to_blob(sig);
            insert_ps++;
        }
        /*
        template <class Container>
        static typename std::enable_if<
               sfinae::is_container_with_fixed_value_type<Container, int>::value,
               std::map<int,DataSig<Type>>>::type
        load(std::vector<DataSig<Type>> data_sigs, const Container& ids)
        {   
            std::map<int, DataSig<Type>> result;
            size_t len = data_sigs.size();
            for (int id :ids)
            {
                for (size_t i = 0; i < len; i++)
                {
                    int cur_id = data_sigs[i].id; 
                    if (cur_id = id)
                    {
                        result.insert(std::make_pair(id, data_sigs[i]));
                    }
                }
            }
            return result;
        }
        */
        template <class Container>
        static typename std::enable_if<
            sfinae::is_container_with_fixed_value_type<Container, int>::value,
            std::map<int, DataSig<Type>>>::type
        load(sqlite::database& db, const pbc::PairingPtr& pairing,
             const Container& ids)
        {
            auto ps = db << "SELECT value_hash,sig_message,sig FROM DataSig"
                            " WHERE id=?";
            std::map<int, DataSig<Type>> result;
            ABS::GlobalPBCPairing::set(pairing);
            for (int id : ids) {
                ps << id >> [&](std::vector<char> value_hash,
                                std::vector<char> sig_message,
                                std::vector<char> sig) {
                    DataSig<Type> r;
                    r.id = id;
                    Utils::blob_to_object(value_hash, r.value_hash);
                    Utils::blob_to_object(sig_message, r.sig_message);
                    Utils::blob_to_object(sig, r.sig);
                    result.insert(std::make_pair(id, r));
                };
                ps++;
            }
            ps.used(true);
            ABS::GlobalPBCPairing::clear();
            return result;
        }
        template <class Container>
        static typename std::enable_if<
            sfinae::is_container_with_fixed_value_type<Container, int>::value,
            std::map<int, std::string>>::type
        load_sig_tag(sqlite::database& db, const pbc::PairingPtr& pairing,
                     const Container& ids)
        {
            // we really should store sig_tag separately !!!
            auto ps = db << "SELECT sig FROM DataSig WHERE id=?";
            std::map<int, std::string> result;
            for (int id : ids) {
                ps << id >> [&](std::vector<char> sig) {
                    std::string tag(&sig[sig.size() - 16], 16);
                    result.insert(std::make_pair(id, tag));
                };
                ps++;
            }
            ps.used(true);
            return result;
        }
//self define
        std::string output_hash() const
        {
            return value_hash;
        }

        std::string output_message() const
        {
            return sig_message;
        }


    private:

        //0923
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& id;
            ar& value_hash;
            ar& sig_message;
            ar& sig;
        }
        

        std::string compute_sig_message(const Point& point) const
        {
            return compute_sig_message(point, value_hash);
        }


        std::string compute_sig_message(
            const Point& point, const std::string& real_value_hash) const
        {

            std::string output(CryptoPP::SHA1::DIGESTSIZE, 0);
            CryptoPP::SHA1 sha1;

            sha1.Update((const unsigned char*)point.data(),
                        point.size() * sizeof(int));
            sha1.Update((const unsigned char*)real_value_hash.data(),
                        CryptoPP::SHA1::DIGESTSIZE);

            sha1.Final((unsigned char*)output.data());
            return output;
        }

        //data struct type
        int id;  // same as the id of the data record
        std::string value_hash;
        std::string sig_message;
        typename Type::sig_type sig;
    };


    template <class Type>
    class GridSig
    {
    public:
        GridSig() = default;
        GridSig(const GridSig<Type>&) = default;

        //
        static GridSig create(const Grid<Type>& grid,
                              const ABS::MasterVerifyingKey& mvk,
                              const typename Type::do_key_type& key)
        {
            GridSig s;
            s.id = grid.id;
            s.sig_message = s.compute_sig_message(grid.box);
            s.sig = ABS::sign(mvk, key, s.sig_message, grid.get_policy());
            return s;
        }

        bool verify(const ABS::MasterVerifyingKey& mvk, const Rectangle& box,
                    const typename Type::policy_type& policy) const
        {
            bool result = true;
            result &= ABS::verify(mvk, sig, compute_sig_message(box));
            result &= sig.get_predicate() == policy;
            return result;
        }

        //class grid sig
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            GridSig>::type
        relax(const ABS::MasterVerifyingKey& mvk,
              const Container& inaccessible_roles) const
        {
            GridSig s(*this);
            s.sig = ABS::relax(mvk, sig, sig_message, inaccessible_roles);
            return s;
        }


        std::string get_sig_tag() const { return sig.get_tag(); }
        size_t get_size_when_inaccessible() const { return sig.get_size(); }
        size_t get_index_size() const { return sig.get_size_compressed(); }
        
        void save(sqlite::database_binder& insert_ps) const
        {
            insert_ps << id << Utils::object_to_blob(sig_message)
                      << Utils::object_to_blob(sig);
            insert_ps++;
        }
   
        template <class Container>
        static typename std::enable_if<
            sfinae::is_container_with_fixed_value_type<Container, int>::value,
            std::map<int, GridSig<Type>>>::type
        load(sqlite::database& db, const pbc::PairingPtr& pairing,
             const Container& ids)
        {
            auto ps = db << "SELECT sig_message,sig FROM GridSig"
                            " WHERE id=?";
            std::map<int, GridSig<Type>> result;
            ABS::GlobalPBCPairing::set(pairing);
            for (int id : ids) {
                ps << id >>
                    [&](std::vector<char> sig_message, std::vector<char> sig) {
                        GridSig<Type> r;
                        r.id = id;
                        Utils::blob_to_object(sig_message, r.sig_message);
                        Utils::blob_to_object(sig, r.sig);
                        result.insert(std::make_pair(id, r));
                    };
                ps++;
            }
            ps.used(true);
            ABS::GlobalPBCPairing::clear();
            return result;
        }
        template <class Container>
        static typename std::enable_if<
            sfinae::is_container_with_fixed_value_type<Container, int>::value,
            std::map<int, std::string>>::type
        load_sig_tag(sqlite::database& db, const pbc::PairingPtr& pairing,
                     const Container& ids)
        {
            // we really should store sig_tag separately !!!
            auto ps = db << "SELECT sig FROM GridSig WHERE id=?";
            std::map<int, std::string> result;
            for (int id : ids) {
                ps << id >> [&](std::vector<char> sig) {
                    std::string tag(&sig[sig.size() - 16], 16);
                    result.insert(std::make_pair(id, tag));
                };
                ps++;
            }
            ps.used(true);
            return result;
        }

        std::string output_message() const
        {
            return sig_message;
        }

    private:

        
        //0923
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& id;
            ar& sig_message;
            //ar& boost::serialization::base_object<Signature>(*this);
            ar& sig;
        }


        //value is box
        std::string compute_sig_message(const Rectangle& box) const
        {
            std::string output(CryptoPP::SHA1::DIGESTSIZE, 0);
            CryptoPP::SHA1 sha1;
            sha1.Update((const unsigned char*)box.get_p1().data(),
                        box.get_p1().size() * sizeof(int));
            sha1.Update((const unsigned char*)box.get_p2().data(),
                        box.get_p2().size() * sizeof(int));
            sha1.Final((unsigned char*)output.data());
            return output;
        }

        int id;  // same as the id of index grid
        std::string sig_message;
        typename Type::sig_type sig;
    };
};

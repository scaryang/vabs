#pragma once

#include "../abs/abs.hpp"
#include "../abs/boolean_expression.hpp"
#include "../abs/boolean_expression_dnf.hpp"

namespace DB
{
    template <class ValueType, class RoleType>
    struct Type {
        typedef ValueType value_type;
        typedef RoleType role_type;
        typedef ABS::BooleanExpression<role_type> policy_type;
        //first dnf_policy
        typedef ABS::BooleanExpressionDNF<role_type> dnf_policy_type;
        //to verify
        typedef ABS::Signature<role_type> sig_type;
        //to sign
        typedef ABS::SigningKey<role_type> do_key_type;
    };
};

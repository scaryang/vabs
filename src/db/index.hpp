#pragma once

#include "../utils/sfinae_helper.hpp"
#include "./data.hpp"
#include "./rect.hpp"
#include "./sig.hpp"
#include "./type.hpp"

#include <cryptopp/sha.h>
#include <sqlite_modern_cpp.h>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/variant.hpp>
#include <boost/variant/recursive_wrapper.hpp>
#include <functional>
#include <map>
#include <ostream>
#include <sstream>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace DB
{
    using boost::variant;
    using boost::recursive_wrapper;

    enum class IndexType { grid, kdtree, hybrid };

    //define leafnode and nonleafnode
    template <class Type>
    class IndexLeafNode;
    template <class Type>
    class IndexNonLeafNode;

    template <class Type>
    class Index//inherit
        : public variant<
              std::unique_ptr<IndexLeafNode<Type>>,
              recursive_wrapper<std::unique_ptr<IndexNonLeafNode<Type>>>>

    {
    public:
        
        using variant<std::unique_ptr<IndexLeafNode<Type>>,
                      recursive_wrapper<
                          std::unique_ptr<IndexNonLeafNode<Type>>>>::variant;
        Index(const Index&) = delete;

        typename Type::policy_type get_policy() const
        {
            return boost::apply_visitor(
                [](const auto& node) { return node->get_policy(); }, *this);
        }
        typename Type::dnf_policy_type get_dnf_policy() const
        {
            return boost::apply_visitor(
                [](const auto& node) { return node->get_dnf_policy(); }, *this);
        }

        //verdict decision
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            bool>::type
        access(const Container& roles) const
        {
            return boost::apply_visitor(
                [&](const auto& node) { return node->access(roles); }, *this);
        }
           
        //grid 
        Rectangle get_box() const
        {
            return boost::apply_visitor(
                [](const auto& node) { return node->box; }, *this);
        }
         
        //grid
        int get_id() const
        {
            return boost::apply_visitor(
                [](const auto& node) { return node->id; }, *this);
        }

        int get_parent_id() const
        {
            return boost::apply_visitor(
                [](const auto& node) { return node->parent_id; }, *this);
        }
         
        const Grid<Type>* get_grid_p() const
        {
            return boost::apply_visitor(
                [](const auto& node) {
                    return static_cast<const Grid<Type>*>(node.get());
                },
                *this);
        }

        bool operator==(const Index& other) const
        {
            return boost::apply_visitor(EqualCheckVisitor(), *this, other);
        }
        bool operator!=(const Index& other) const { return !operator==(other); }

        /*
        //load datarecord from gridtree
        std::map<int,DataRecord<Type>> LoadData()
        {
            return boost::apply_visitor([&](auto& node){
                return node->LoadData();
            }, *this);
        }
        */

        void save(sqlite::database& db) const
        {
            db << "begin;";
            db << "DROP TABLE IF EXISTS IndexTree;";
            db << "DROP TABLE IF EXISTS Data;";
            db << R"(CREATE TABLE IndexTree(
                     id        INTEGER PRIMARY KEY,
                     parent_id INTEGER,
                     box       BLOB,
                     policy    BLOB);)";
            db << R"(CREATE TABLE Data(
                     id        INTEGER PRIMARY KEY,
                     grid_id   INTEGER,
                     point     BLOB,
                     policy    BLOB,
                     value     BLOB);)";
            auto insert_index_ps = db
                                   << "INSERT INTO IndexTree VALUES (?,?,?,?);";
            auto insert_data_ps = db << "INSERT INTO Data VALUES (?,?,?,?,?);";
            boost::apply_visitor(
                [&](const auto& node) {
                    node->save(insert_index_ps, insert_data_ps);
                },
                *this);
            insert_index_ps.used(true);
            insert_data_ps.used(true);
            db << "end;";
        }

        //load data
        static std::unique_ptr<Index> load(sqlite::database& db)
        {
            std::map<int, Grid<Type>> grids = Grid<Type>::load(db);
            std::map<int, DataRecord<Type>> data_records =
                DataRecord<Type>::load(db);
            std::map<int, std::unique_ptr<IndexLeafNode<Type>>> leaf_nodes;
            std::map<int, std::unique_ptr<IndexNonLeafNode<Type>>>
                nonleaf_nodes;
            std::map<int, IndexNonLeafNode<Type>*> nonleaf_nodes_p;
            for (auto ptr = data_records.begin(); ptr != data_records.end();
                 ++ptr) {
                int grid_id = ptr->second.grid_id;
                auto leaf_ptr = leaf_nodes.find(grid_id);
                if (leaf_ptr == leaf_nodes.end()) {
                    auto grid_ptr = grids.find(grid_id);
                    auto leaf_node = std::make_unique<IndexLeafNode<Type>>(
                        std::move(grid_ptr->second));
                    leaf_ptr = leaf_nodes
                                   .insert(std::make_pair(grid_id,
                                                          std::move(leaf_node)))
                                   .first;
                    grids.erase(grid_ptr);
                }
                auto record =
                    std::make_unique<DataRecord<Type>>(std::move(ptr->second));
                leaf_ptr->second->leaves.insert(
                    std::make_pair(ptr->second.point, std::move(record)));
            }
            data_records.clear();

            for (auto ptr = grids.begin(); ptr != grids.end(); ++ptr) {
                int id = ptr->first;
                auto nonleaf_node = std::make_unique<IndexNonLeafNode<Type>>(
                    std::move(ptr->second));
                nonleaf_nodes_p.insert(std::make_pair(id, nonleaf_node.get()));
                nonleaf_nodes.insert(
                    std::make_pair(id, std::move(nonleaf_node)));
            }
            grids.clear();

            for (auto ptr = leaf_nodes.begin(); ptr != leaf_nodes.end();) {
                int id = ptr->first;
                int parent_id = ptr->second->parent_id;
                if (id == 0)
                    ++ptr;
                else {
                    auto parent_ptr = nonleaf_nodes_p.find(parent_id);
                    auto box = ptr->second->box;
                    parent_ptr->second->leaves.insert(std::make_pair(
                        box,
                        std::make_unique<Index<Type>>(std::move(ptr->second))));
                    leaf_nodes.erase(ptr++);
                }
            }

            for (auto ptr = nonleaf_nodes.begin();
                 ptr != nonleaf_nodes.end();) {
                int id = ptr->first;
                int parent_id = ptr->second->parent_id;
                if (id == 0)
                    ++ptr;
                else {
                    auto parent_ptr = nonleaf_nodes_p.find(parent_id);
                    auto box = ptr->second->box;
                    parent_ptr->second->leaves.insert(std::make_pair(
                        box,
                        std::make_unique<Index<Type>>(std::move(ptr->second))));
                    nonleaf_nodes.erase(ptr++);
                }
            }

            if (!nonleaf_nodes.empty())
                return std::make_unique<Index<Type>>(
                    std::move(nonleaf_nodes.find(0)->second));
            else
                return std::make_unique<Index<Type>>(
                    std::move(leaf_nodes.find(0)->second));
        }



        template <class Container>
        static typename std::enable_if<
            sfinae::is_map_container_with_fixed_type<
                Container, Point, RawDataRecord<Type>>::value,
            std::unique_ptr<Index>>::type
        create_grid(const Rectangle& rect, const Container& data,
                    const typename Type::dnf_policy_type& pseudo_policy)
        {
            size_t d = rect.dimension();
            boost::dynamic_bitset<> skip_dimensions(d);
            auto is_leafnode =
                [d](const Rectangle& rect,
                    boost::dynamic_bitset<>& skip_dimensions) -> bool {
                for (size_t i = 0; i < d; ++i) {
                    if (!skip_dimensions.test(i) &&
                        rect.get_p2(i) - rect.get_p1(i) < 2)
                        skip_dimensions.set(i, true);
                }
                return skip_dimensions.all();
            };
            auto split_rectangle = [](const Rectangle& rect,
                                      boost::dynamic_bitset<>& skip_dimensions)
                -> std::vector<Rectangle> {
                return grid_split_rectangle(rect, skip_dimensions);
            };
            std::unique_ptr<Index> index;
#pragma omp parallel
#pragma omp single nowait
            index = create_tree<Container, boost::dynamic_bitset<>>(
                rect, data, pseudo_policy, is_leafnode, split_rectangle,
                skip_dimensions);
#pragma omp taskwait
            int cur_grid_id = 0;
            int cur_data_id = 0;
            index->assign_ids(0, cur_grid_id, cur_data_id);
            return std::move(index);
        }

        template <class Container>
        static typename std::enable_if<
            sfinae::is_map_container_with_fixed_type<
                Container, Point, RawDataRecord<Type>>::value,
            std::unique_ptr<Index>>::type
        create_kdtree(const Rectangle& rect, const Container& data,
                      const typename Type::dnf_policy_type& pseudo_policy)
        {
            size_t d = rect.dimension();
            size_t split_dimension = 0;
            auto is_leafnode = [d](const Rectangle& rect,
                                   size_t& split_dimension) -> bool {
                for (size_t i = 0; i < d; ++i) {
                    int dis = rect.get_p2(split_dimension) -
                              rect.get_p1(split_dimension);
                    if (dis > 1)
                        return false;
                    else if (dis > 0) {
                        size_t split_dimension_ = split_dimension;
                        ++i;
                        for (; i < d; ++i) {
                            split_dimension_ = (split_dimension_ + 1) % d;
                            int dis_ = rect.get_p2(split_dimension_) -
                                       rect.get_p1(split_dimension_);
                            if (dis_ > 0) return false;
                        }
                        return true;
                    }
                    split_dimension = (split_dimension + 1) % d;
                }
                return true;
            };
            auto split_rectangle =
                [&](const Rectangle& rect,
                    size_t& split_dimension) -> std::vector<Rectangle> {
                auto result = kdtree_split_rectangle(rect, split_dimension,
                                                     data, pseudo_policy);
                split_dimension = (split_dimension + 1) % d;
                return result;
            };
            std::unique_ptr<Index> index;
#pragma omp parallel
#pragma omp single nowait
            index = create_tree<Container, size_t>(rect, data, pseudo_policy,
                                                   is_leafnode, split_rectangle,
                                                   split_dimension);
#pragma omp taskwait
            int cur_grid_id = 0;
            int cur_data_id = 0;
            index->assign_ids(0, cur_grid_id, cur_data_id);
            return std::move(index);
        }

        template <class Container>
        static typename std::enable_if<
            sfinae::is_map_container_with_fixed_type<
                Container, Point, RawDataRecord<Type>>::value,
            std::unique_ptr<Index>>::type
        create_hybrid(const Rectangle& rect, const Container& data,
                      const typename Type::dnf_policy_type& pseudo_policy)
        {
            size_t d = rect.dimension();
            size_t max_kd_depth = 0;
            for (size_t i = 0; i < d; ++i) {
                size_t len = rect.get_p2(i) - rect.get_p1(i) + i;
                max_kd_depth += (size_t)std::floor(std::log2((double)len));
            }
            typedef std::tuple<size_t, size_t, boost::dynamic_bitset<>>
                temp_type;
            size_t tree_depth = 0;
            size_t kd_split_dimension = 0;
            boost::dynamic_bitset<> grid_skip_dimensions(d);
            temp_type temp = std::make_tuple(tree_depth, kd_split_dimension,
                                             grid_skip_dimensions);
            auto is_leafnode = [d, max_kd_depth](const Rectangle& rect,
                                                 temp_type& temp) -> bool {
                size_t cur_depth = std::get<0>(temp);
                if (cur_depth > max_kd_depth) {
                    for (size_t i = 0; i < d; ++i) {
                        if (!std::get<2>(temp).test(i) &&
                            rect.get_p2(i) - rect.get_p1(i) < 2)
                            std::get<2>(temp).set(i, true);
                    }
                    return std::get<2>(temp).all();
                } else {
                    for (size_t i = 0; i < d; ++i) {
                        int dis = rect.get_p2(std::get<1>(temp)) -
                                  rect.get_p1(std::get<1>(temp));
                        if (dis > 1)
                            return false;
                        else if (dis > 0) {
                            size_t split_dimension_ = std::get<1>(temp);
                            ++i;
                            for (; i < d; ++i) {
                                split_dimension_ = (split_dimension_ + 1) % d;
                                int dis_ = rect.get_p2(split_dimension_) -
                                           rect.get_p1(split_dimension_);
                                if (dis_ > 0) return false;
                            }
                            return true;
                        }
                        std::get<1>(temp) = (std::get<1>(temp) + 1) % d;
                    }
                    return true;
                }
            };
            auto split_rectangle =
                [&](const Rectangle& rect,
                    temp_type& temp) -> std::vector<Rectangle> {
                size_t cur_depth = std::get<0>(temp)++;
                if (cur_depth > max_kd_depth) {
                    return grid_split_rectangle(rect, std::get<2>(temp));
                } else {
                    auto result = kdtree_split_rectangle(
                        rect, std::get<1>(temp), data, pseudo_policy);
                    std::get<1>(temp) = (std::get<1>(temp) + 1) % d;
                    return result;
                }
            };
            std::unique_ptr<Index> index;
#pragma omp parallel
#pragma omp single nowait
            index = create_tree<Container, temp_type>(
                rect, data, pseudo_policy, is_leafnode, split_rectangle, temp);
#pragma omp taskwait
            int cur_grid_id = 0;
            int cur_data_id = 0;
            index->assign_ids(0, cur_grid_id, cur_data_id);
            return std::move(index);
        }

        //set id...
        void assign_ids(int parent_id, int& cur_grid_id, int& cur_data_id)
        {
            boost::apply_visitor(
                [&](auto& node) {
                    node->assign_ids(parent_id, cur_grid_id, cur_data_id);
                },
                *this);
        }

        

    private:
        template <class Container, class Temp>
        static typename std::enable_if<
            sfinae::is_map_container_with_fixed_type<
                Container, Point, RawDataRecord<Type>>::value,
            std::unique_ptr<Index>>::type
        create_tree(
            const Rectangle& rect, const Container& data,
            const typename Type::dnf_policy_type& pseudo_policy,
            const std::function<bool(const Rectangle&, Temp&)>& is_leafnode,
            const std::function<std::vector<Rectangle>(const Rectangle&,
                                                       Temp&)>& split_rectangle,
            Temp& temp)
        {
            if (is_leafnode(rect, temp)) {
                auto node = std::make_unique<IndexLeafNode<Type>>();
                node->box = rect;
                for (const Point& p : node->box.all_points()) {
                    auto ptr = data.find(p);
                    if (ptr == data.end()) {
                        auto record = std::make_unique<DataRecord<Type>>(
                            0, 0, p, pseudo_policy);
                        node->policy = node->policy or pseudo_policy;
                        node->leaves.insert(
                            std::make_pair(p, std::move(record)));
                    } else {
                        auto record = std::make_unique<DataRecord<Type>>(
                            0, 0, p, ptr->second);
                        node->policy = node->policy or record->get_dnf_policy();
                        node->leaves.insert(
                            std::make_pair(p, std::move(record)));
                    }
                }
                return std::make_unique<Index>(std::move(node));
            } else {
                auto node = std::make_unique<IndexNonLeafNode<Type>>();
                node->box = rect;
                auto rects = split_rectangle(rect, temp);
                std::vector<std::unique_ptr<Index>> indexes(rects.size());
#pragma omp taskgroup
                {
                    for (size_t i = 0; i < rects.size(); ++i) {
                        Temp temp_ = temp;
#pragma omp task firstprivate(i, temp_) default(shared)
                        indexes[i] = std::move(
                            create_tree(rects[i], data, pseudo_policy,
                                        is_leafnode, split_rectangle, temp_));
                    }
                }
                for (size_t i = 0; i < rects.size(); ++i) {
                    node->policy = node->policy or indexes[i]->get_dnf_policy();
                    node->leaves.insert(
                        std::make_pair(rects[i], std::move(indexes[i])));
                }
                return std::make_unique<Index>(std::move(node));
            }
        }


        static std::vector<Rectangle> grid_split_rectangle(
            const Rectangle& rect, boost::dynamic_bitset<>& skip_dimensions)
        {
            size_t d = rect.dimension();
            size_t fanout = 1 << (d - skip_dimensions.count());
            std::vector<Rectangle> results;
            //reserve at least two spaces
            results.reserve(fanout);

            Point mid(d);
            const auto& p1 = rect.get_p1();
            const auto& p2 = rect.get_p2();
            std::transform(
                p1.begin(), p1.end(), p2.begin(), mid.begin(),
                [](int a, int b) -> int { return std::floor((a + b) * 0.5); });
           
            for (size_t i = 0; i < fanout; ++i) {
                Point a(d), b(d);
                
                for (size_t j = 0, split_j = 0; j < d; ++j) {
                    if (skip_dimensions.test(j)) {
                        a[j] = rect.get_p1(j);
                        b[j] = rect.get_p2(j);
                    } else {
                        bool left = (i / (1 << split_j++) % 2 == 0);

                        if (left) {
                            a[j] = rect.get_p1(j);
                            b[j] = mid[j];
                        } else {
                            a[j] = mid[j] + 1;
                            b[j] = rect.get_p2(j);
                        }
                    }
                }
                results.push_back(Rectangle(a, b));
            }
            return results;
        }

        template <class Container>
        static typename std::enable_if<
            sfinae::is_map_container_with_fixed_type<
                Container, Point, RawDataRecord<Type>>::value,
            std::vector<Rectangle>>::type
        kdtree_split_rectangle(
            const Rectangle& rect, size_t split_dimension,
            const Container& data,
            const typename Type::dnf_policy_type& pseudo_policy)
        {
            int x_start = rect.get_p1(split_dimension);
            size_t x_length = rect.get_p2(split_dimension) - x_start + 1;

            if (x_length == 2) {
                Rectangle r1(rect), r2(rect);
                r1.set_p2(split_dimension, r1.get_p1(split_dimension));
                r2.set_p1(split_dimension, r2.get_p2(split_dimension));
                return std::vector<Rectangle>{r1, r2};
            }

            std::vector<typename Type::dnf_policy_type> dnfs(x_length);
            bool all_pseudo = true;
            for (const Point& p : rect.all_points()) {
                auto ptr = data.find(p);
                int x_index = p[split_dimension] - x_start;
                if (ptr == data.end()) {
                    dnfs[x_index] = dnfs[x_index] or pseudo_policy;
                } else {
                    dnfs[x_index] =
                        dnfs[x_index] or ptr->second.get_dnf_policy();
                    all_pseudo = false;
                }
            }

            if (all_pseudo) {
                Rectangle r1(rect), r2(rect);
                int mid = std::floor((rect.get_p1(split_dimension) +
                                      rect.get_p2(split_dimension)) *
                                     0.5);
                r1.set_p2(split_dimension, mid);
                r2.set_p1(split_dimension, mid + 1);
                return std::vector<Rectangle>{r1, r2};
            }

            size_t x_index;
            typename Type::dnf_policy_type left, right;
            if (dnfs[0].cap(dnfs[1]).size() < dnfs[1].cap(dnfs[2]).size()) {
                x_index = 0;
                left = dnfs[0];
                right = dnfs[1].cup(dnfs[2]);
            } else {
                x_index = 1;
                left = dnfs[0].cup(dnfs[1]);
                right = dnfs[2];
            }
            for (size_t cur = 2; cur < x_length - 1; ++cur) {
                size_t a = left.cap(right).size();
                size_t b = right.cap(dnfs[cur + 1]).size();
                if (a < b || (a == b && x_index + cur >= x_length)) {
                    right = right.cup(dnfs[cur + 1]);
                } else {
                    x_index = cur;
                    left = left.cup(right);
                    right = dnfs[cur + 1];
                }
            }

            Rectangle r1(rect), r2(rect);
            r1.set_p2(split_dimension, x_index + x_start);
            r2.set_p1(split_dimension, x_index + x_start + 1);
            return std::vector<Rectangle>{r1, r2};
        }
        
        //used to check therther two *index are equal 
        class EqualCheckVisitor : public boost::static_visitor<bool>
        {
        public:
            template <class T1, typename T2>
            bool operator()(const std::unique_ptr<T1>&,
                            const std::unique_ptr<T2>&) const
            {
                return false;
            }

            template <typename T1>
            bool operator()(const std::unique_ptr<T1>& lhs,
                            const std::unique_ptr<T1>& rhs) const
            {
                return *lhs == *rhs;
            }
        };
    };


    template <class Type>
    class IndexLeafNode : public Grid<Type>
    {
    public:
        using Grid<Type>::Grid;
        IndexLeafNode() = default;
        IndexLeafNode(const Grid<Type>& grid) : Grid<Type>(grid) {}
        IndexLeafNode(Grid<Type>&& grid) : Grid<Type>(std::move(grid)) {}

        void assign_ids(int parent_id, int& cur_grid_id, int& cur_data_id)
        {    
         
            this->parent_id = parent_id;
            this->id = cur_grid_id++;
        
            for (auto ptr = leaves.begin(); ptr != leaves.end(); ++ptr) {
                ptr->second->grid_id = this->id;
                ptr->second->id = cur_data_id++;
            }
        }
        /*
        std::map<int,DataRecord<Type>> LoadData()
        {
            std::map<int,DataRecord<Type>> result;
            for (auto ptr = leaves.begin();ptr !=leaves.end(); ++ptr){
                int Dataid = ptr->second->id;
                result.insert(std::make_pair(Dataid , std::move(ptr->second)));
            }
            return result;
        }
        */


        bool operator==(const IndexLeafNode& other) const
        {
            return Grid<Type>::operator==(other) && leaves == other.leaves;
        }
        bool operator!=(const IndexLeafNode& other) const
        {
            return !operator==(other);
        }

        void save(sqlite::database_binder& insert_index_ps,
                  sqlite::database_binder& insert_data_ps) const
        {
            Grid<Type>::save(insert_index_ps);
            for (auto ptr = leaves.begin(); ptr != leaves.end(); ++ptr)
                ptr->second->save(insert_data_ps);
        }


        std::unordered_map<Point, std::unique_ptr<DataRecord<Type>>> leaves;
        
    };

    template <class Type>
    bool operator==(const std::unique_ptr<DataRecord<Type>>& x,
                    const std::unique_ptr<DataRecord<Type>>& y)
    {
        return *x == *y;
    }

    template <class Type>
    class IndexNonLeafNode : public Grid<Type>
    {
    public:
        using Grid<Type>::Grid;
        IndexNonLeafNode() = default;
        IndexNonLeafNode(const Grid<Type>& grid) : Grid<Type>(grid) {}
        IndexNonLeafNode(Grid<Type>&& grid) : Grid<Type>(std::move(grid)) {}

        void assign_ids(int parent_id, int& cur_grid_id, int& cur_data_id)
        {
            this->parent_id = parent_id;
            this->id = cur_grid_id++;
            
            for (auto ptr = leaves.begin(); ptr != leaves.end(); ++ptr) {
                ptr->second->assign_ids(this->id, cur_grid_id, cur_data_id);
            }
        }

        bool operator==(const IndexNonLeafNode& other) const
        {
            return Grid<Type>::operator==(other) &&
                   leaves.size() == other.leaves.size() &&
                   std::is_permutation(leaves.begin(), leaves.end(),
                                       other.leaves.begin(),
                                       [](const auto& lhs, const auto& rhs) {
                                           return lhs.first == rhs.first &&
                                                  *lhs.second == *rhs.second;
                                       });
        }
        bool operator!=(const IndexNonLeafNode& other) const
        {
            return !operator==(other);
        }
        
        void save(sqlite::database_binder& insert_index_ps,
                  sqlite::database_binder& insert_data_ps) const
        {
            Grid<Type>::save(insert_index_ps);
            for (auto ptr = leaves.begin(); ptr != leaves.end(); ++ptr)
                boost::apply_visitor(
                    [&](const auto& node) {
                        node->save(insert_index_ps, insert_data_ps);
                    },
                    *ptr->second);
        }

        std::unordered_map<Rectangle, std::unique_ptr<Index<Type>>> leaves;
    };

    //overload operators
    template <class Type>
    std::ostream& operator<<(std::ostream& o, const IndexLeafNode<Type>& n)
    {
        o << "Leaf(id=" << n.id << ",parent_id=" << n.parent_id
          << "): " << n.box << " [" << std::endl;
        for (auto ptr = n.leaves.begin(); ptr != n.leaves.end(); ++ptr)
            o << ptr->first << " (id=" << ptr->second->id
              << ",grid_id=" << ptr->second->grid_id << ")" << std::endl;
        o << "]" << std::endl;
        return o;
    }

    template <class Type>
    std::ostream& operator<<(std::ostream& o, const IndexNonLeafNode<Type>& n)
    {
        o << "NonLeaf(id=" << n.id << ",parent_id=" << n.parent_id
          << "): " << n.box << " {" << std::endl;
        std::stringstream ss;
        for (auto ptr = n.leaves.begin(); ptr != n.leaves.end(); ++ptr)
            ss << *ptr->second;
        std::string s(ss.str());
        boost::replace_all(s, "\n", "\n  ");
        boost::trim_right(s);
        o << "  " << s << std::endl;
        o << "}" << std::endl;
        return o;
    }

    template <class Type>
    std::ostream& operator<<(std::ostream& o, const Index<Type>& i)
    {
        boost::apply_visitor([&o](const auto& input) { o << *input; }, i);
        return o;
    }
};

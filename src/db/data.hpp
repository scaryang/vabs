#pragma once

#include "../utils/sfinae_helper.hpp"
#include "./rect.hpp"
#include "./type.hpp"
#include "./utils.hpp"
#include "../abs/abs.hpp"
#include "../abs/boolean_expression_dnf.hpp"


#include <cryptopp/osrng.h>
#include <cryptopp/sha.h>
#include <sqlite_modern_cpp.h>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>

namespace DB
{
    template <class Type>
    class RawDataRecord
    {
    public:
        RawDataRecord() = default;
        RawDataRecord(const RawDataRecord&) = default;
        RawDataRecord(const typename Type::value_type& value,
                      const typename Type::dnf_policy_type& policy)
            : value(value), policy(policy)
        {
        }

        //support string policy
        RawDataRecord(const typename Type::value_type& value,
                      const std::string& policy)
            : value(value),
              policy(typename Type::dnf_policy_type(
                  Type::policy_type::parse(policy)))
        {
        }
        
        //get-policy
        typename Type::policy_type get_policy() const
        {
            return policy.to_expr();
        }
        //get dnf-policy
        typename Type::dnf_policy_type get_dnf_policy() const { return policy; }

        //trans value to hash
        std::string value_hash() const
        {
            return Utils::value_to_hash<typename Type::value_type>(value);
        }


        //basic data struct
        typename Type::value_type value;
        typename Type::dnf_policy_type policy;

    private:

        //0923
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& value;
            ar& policy;
        }


    };


    template <class Type>
    class DataRecord
    {
    public:
        DataRecord() = default;
        DataRecord(const DataRecord<Type>&) = default;
        DataRecord(int id, int grid_id, const Point& point,
                   const typename Type::value_type& value,
                   const typename Type::dnf_policy_type& policy)
            : id(id),
              grid_id(grid_id),
              point(point),
              pseudo(false),
              value(value),
              policy(policy)
        {
        }
        //support rawdatarecord input
        DataRecord(int id, int grid_id, const Point& point,
                   const RawDataRecord<Type>& record)
            : id(id),
              grid_id(grid_id),
              point(point),
              pseudo(false),
              value(record.value),
              policy(record.policy)
        {
        }

        //support pseudo_policy input
        DataRecord(int id, int grid_id, const Point& point,
                   const typename Type::dnf_policy_type& pseudo_policy)
            : id(id),
              grid_id(grid_id),
              point(point),
              pseudo(true),
              policy(pseudo_policy)
        {
        }

        //verdict wehther equality
        bool operator==(const DataRecord& other) const
        {
            if (id != other.id) return false;
            if (grid_id != other.grid_id) return false;
            if (point != other.point) return false;
            if (pseudo != other.pseudo) return false;
            if (policy != other.policy) return false;
            if (!pseudo && value != other.value) return false;
            return true;
        }
        bool operator!=(const DataRecord& other) const
        {
            return !operator==(other);
        }

        //policy and dnf-policy
        typename Type::policy_type get_policy() const
        {
            return policy.to_expr();
        }
        typename Type::dnf_policy_type get_dnf_policy() const { return policy; }

        //return to raw data record
        RawDataRecord<Type> to_raw_data_record() const
        {
            return RawDataRecord<Type>(value, policy);
        }
        
        //verdict roles can access the datas
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            bool>::type
        
        access(const Container& roles) const
        {
            return get_policy().eval(roles);
        }

        //trans value to hash
        std::string value_hash() const
        {
            return Utils::value_to_hash<typename Type::value_type>(value);
        }
        
        void save(sqlite::database_binder& insert_data_ps) const
        {
            if (pseudo)
                insert_data_ps << id << grid_id << Utils::object_to_blob(point)
                               << Utils::object_to_blob(policy) << nullptr;
            else
                insert_data_ps << id << grid_id << Utils::object_to_blob(point)
                               << Utils::object_to_blob(policy)
                               << Utils::object_to_blob(value);
            insert_data_ps++;
        }

        static std::map<int, DataRecord<Type>> load(sqlite::database& db,
                                                    int min_id = -1,
                                                    int max_id = -1)
        {
            std::map<int, DataRecord<Type>> result;
            auto load_proc = [&](int id, int grid_id, std::vector<char> point,
                                 std::vector<char> policy,
                                 std::unique_ptr<std::vector<char>> value_p) {
                Point _point;
                typename Type::dnf_policy_type _policy;
                Utils::blob_to_object(point, _point);
                Utils::blob_to_object(policy, _policy);
                if (value_p) {
                    typename Type::value_type _value;
                    Utils::blob_to_object(*value_p, _value);
                    result.insert(
                        std::make_pair(id, DataRecord<Type>(id, grid_id, _point,
                                                            _value, _policy)));
                } else {
                    result.insert(std::make_pair(
                        id, DataRecord<Type>(id, grid_id, _point, _policy)));
                }
            };
            if (min_id != -1 && max_id != -1) {
                db << "SELECT id,grid_id,point,policy,value FROM Data "
                      "WHERE id BETWEEN ? AND ?;"
                   << min_id << max_id >>
                    load_proc;
            } else if (min_id != -1) {
                db << "SELECT id,grid_id,point,policy,value FROM Data "
                      "WHERE id >= ?;"
                   << min_id >>
                    load_proc;
            } else if (max_id != -1) {
                db << "SELECT id,grid_id,point,policy,value FROM Data "
                      "WHERE id <= ?;"
                   << max_id >>
                    load_proc;
            } else {
                db << "SELECT id,grid_id,point,policy,value FROM Data;" >>
                    load_proc;
            }
            return result;
        }

        
        //basic data strcut
        int id;
        int grid_id;
        Point point;
        bool pseudo;
        typename Type::value_type value;
        typename Type::dnf_policy_type policy;

    private:
        //0923
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& id;
            ar& grid_id;
            ar& point;
            ar& pseudo;
            ar& value;
            ar& policy;
        }
    };


    template <class Type>
    class Grid
    {
    public:
        Grid() = default;
        Grid(const Grid<Type>&) = default;
        Grid(int id, int parent_id, const Rectangle& box,
             const typename Type::dnf_policy_type& policy)
            : id(id), parent_id(parent_id), box(box), policy(policy)
        {
        }

        bool operator==(const Grid& other) const
        {
            return id == other.id && parent_id == other.parent_id &&
                   box == other.box && policy == other.policy;
        }
        bool operator!=(const Grid& other) const { return !operator==(other); }
        
        //policy
        typename Type::policy_type get_policy() const
        {
            return policy.to_expr();
        }
        typename Type::dnf_policy_type get_dnf_policy() const { return policy; }
        
        //verdict whether roles can access the data 
        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<
                Container, typename Type::role_type>::value,
            bool>::type
        
        access(const Container& roles) const
        {
            return get_policy().eval(roles);
        }

        void save(sqlite::database_binder& insert_index_ps) const
        {
            insert_index_ps << id << parent_id << Utils::object_to_blob(box)
                            << Utils::object_to_blob(policy);
            insert_index_ps++;
        }

        static std::map<int, Grid<Type>> load(sqlite::database& db,
                                              int min_id = -1, int max_id = -1)

        {
            std::map<int, Grid<Type>> result;
            auto load_proc = [&](int id, int parent_id, std::vector<char> box,
                                 std::vector<char> policy) {
                Rectangle _box;
                typename Type::dnf_policy_type _policy;
                Utils::blob_to_object(box, _box);
                Utils::blob_to_object(policy, _policy);
                result.insert(std::make_pair(
                    id, Grid<Type>(id, parent_id, _box, _policy)));
            };
            if (min_id != -1 && max_id != -1) {
                db << "SELECT id,parent_id,box,policy FROM IndexTree "
                      "WHERE id BETWEEN ? AND ?;"
                   << min_id << max_id >>
                    load_proc;
            } else if (min_id != -1) {
                db << "SELECT id,parent_id,box,policy FROM IndexTree "
                      "WHERE id >= ?;"
                   << min_id >>
                    load_proc;
            } else if (max_id != -1) {
                db << "SELECT id,parent_id,box,policy FROM IndexTree "
                      "WHERE id <= ?;"
                   << max_id >>
                    load_proc;
            } else {
                db << "SELECT id,parent_id,box,policy FROM IndexTree;" >>
                    load_proc;
            }
            return result;
        }

        //basic data struct
        int id;
        int parent_id;
        Rectangle box;
        typename Type::dnf_policy_type policy;

    private:


        //0923
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& id;
            ar& parent_id;
            ar& box;
            ar& policy;
        }
    };
};

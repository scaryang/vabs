#include "mainwindow.h"
#include <QApplication>
#include "logindialog.h"
#include <QThread>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QThread::currentThread()->setPriority(QThread::HighPriority);

    MainWindow w;
    LoginDialog dlg;

    if(dlg.exec()==QDialog::Accepted)
    {
        w.show();

        return a.exec();
    }
    else return 0;
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <map>
#include <string>
//#include "../../abs/boolean_expression.hpp"
//#include "../../abs/boolean_expression_dnf.hpp"
//#include "../../abs/key.hpp"
//#include "../../db_dup/db.hpp"


//typedef DB::Type<string,int> DB_Type;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void on_actionAccount_Info_triggered();

    void on_actionDatabase_Info_triggered();

    void on_pushButton_2_clicked();

    void on_querybtn_clicked();

    void on_verifybtn_clicked();

    void on_verifybtn2_clicked();

private:
    Ui::MainWindow *ui;
    QString querypid;
    //std::string  querypid_str;
    QString queryaid;
    //std::string  queryaid_str;
    QString res_verification;
};

#endif // MAINWINDOW_H

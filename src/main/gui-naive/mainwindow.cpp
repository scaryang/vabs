#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "logindialog.h"

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include "../../utils/timer.hpp"
#include "../../utils/pbc_helper.h"
#include "../../utils/serialization_helper.h"

#include <nlohmann/json.hpp>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>
#include "../../db_dup/db.hpp"
//#include "./pbc_helper.h"
#include "../../abs/abs.hpp"
#include "../../abs/boolean_expression.hpp"
#include "../../abs/boolean_expression_dnf.hpp"
#include "../../abs/pbc_serialization.hpp"


#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_free.hpp>
#include <pbc/pbc.h>

#include "../../db/sig.hpp"
#include "../../db/data.hpp"
#include "../../db/type.hpp"
#include "../../db/rect.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <omp.h>

#include <set>
#include <string>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>

//cryptopp
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;
#include "cryptopp/cryptlib.h"
using CryptoPP::Exception;

#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "cryptopp/filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include "cryptopp/aes.h"
using CryptoPP::AES;

#include "cryptopp/ccm.h"
using CryptoPP::CTR_Mode;

#include "assert.h"

using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;
using json = nlohmann::json;
using namespace oabe;
using namespace oabe::crypto;

struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};

typedef Type<string,int> DB_Type;

ABS::BooleanExpression<int> inac_policy{};
ABS::MasterVerifyingKey mvk;
DB::DupRangeQueryResult<DB_Type> r3;
std::string querypid_str;
std::string queryaid_str;
pbc::PairingPtr pairing;   


/// Callback must be declared static, otherwise it won't link...
size_t WriteCallback(char* ptr, size_t size, size_t nmemb, void *f)
{
  FILE *file = (FILE *)f;
  return fwrite(ptr, size, nmemb, file);
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionAccount_Info_triggered()
{
    QString strresult = tr("account-infomation-test");
    QMessageBox::information(this,tr("test"),strresult);
}

void MainWindow::on_actionDatabase_Info_triggered()
{
    QString strresult = tr("database-infomation-test");
    QMessageBox::information(this,tr("test"),strresult);
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->pidlineEdit->clear();
    ui->aidlineEdit->clear();
    ui->resultBrowser->clear();
    ui->resultBrowser2->clear();
    ui->veBrowser->clear();
    ui->pidlineEdit->setFocus();
}

void MainWindow::on_querybtn_clicked()
{
    if (ui->pidlineEdit->text().isEmpty()||ui->aidlineEdit->text().isEmpty())
    {
        QMessageBox::warning(this,tr("warning"),tr("Query information can not be empty!"));
        return;
    }
    querypid = ui->pidlineEdit->text();
    queryaid = ui->aidlineEdit->text();
    querypid_str = querypid.toUtf8().constData();
    queryaid_str = queryaid.toUtf8().constData();
 
    char url[] = "127.0.0.1:18080/upload";
    char filename[] = "results.txt";

    json query;
    query["pid"] = querypid_str;
    query["aid"] = queryaid_str;

    std::string s = query.dump();
    cout << s << endl;
    std::istringstream myStream(s);
    int size = myStream.str().size();

    //set options of file
    char buf[100];
    //set options of client
    curlpp::Cleanup cleaner;
    curlpp::Easy request;

      
    std::list< std::string > headers;
    headers.push_back("Content-Type: text/*"); 
    sprintf(buf, "Content-Length: %d", size); 
    headers.push_back(buf);
      
    using namespace curlpp::Options;
    request.setOpt(new curlpp::options::CustomRequest{"POST"});
    curlpp::options::WriteFunctionCurlFunction
    myFunction(WriteCallback);

    FILE *file = stdout;
    if(filename != NULL)
    {
        file = fopen(filename, "wb");
        if(file == NULL)
        {
           fprintf(stderr, "%s/n", strerror(errno));
           return;
        }
    } 
    curlpp::OptionTrait<void *, CURLOPT_WRITEDATA> 
              myData(file);

    request.setOpt(myFunction);
    request.setOpt(myData);

    request.setOpt(new Verbose(true));
    request.setOpt(new ReadStream(&myStream));
    request.setOpt(new InfileSize(size));
    request.setOpt(new Upload(true));
    request.setOpt(new HttpHeader(headers));
    request.setOpt(new Url(url));
    request.perform();
          
    //remember to close file
    fclose(file);
    file = NULL;


    Timer client_db;
    client_db.start();


    GlobalPBCPairing::set(TYPE_A_PARAM);
    ifstream ifs("serial.txt");
    {
        boost::archive::text_iarchive ipp(ifs);
        ipp & mvk;
    }


    // 1. Decode iv: 
    // At the moment our input is encoded in string format...
    // we need it in raw hex: 
    ifstream inx("iv.txt");
    string iv_string((istreambuf_iterator<char>(inx)),istreambuf_iterator<char>());    

    byte iv[CryptoPP::AES::BLOCKSIZE] = {};
    // this decoder would transform our std::string into raw hex:
    CryptoPP::HexDecoder decoder1;
    decoder1.Put((byte*)iv_string.data(), iv_string.size());
    decoder1.MessageEnd();
    decoder1.Get(iv, sizeof(iv));


    client_db.stop();
    cout << endl << "client loading db files cost: " << client_db.result() << endl;

    
    Timer client_abe;
    //verification
    client_abe.start();

    //DupRangeQueryResult<DB_Type> r3;
    //BooleanExpression<int> inac_policy{};

    InitializeOpenABE();
    OpenABECryptoContext cpabe("CP-ABE");
    string pt2;
    
    //cpabe.generateParams();
    ifstream finx("msk.txt");
    string msk((istreambuf_iterator<char>(finx)),istreambuf_iterator<char>());
    cpabe.importSecretParams(msk);

    ifstream finc("mpk.txt");
    string mpk((istreambuf_iterator<char>(finc)),istreambuf_iterator<char>());
    cpabe.importPublicParams(mpk);
    cpabe.keygen("|attr1|attr2","key0");

  
    //ifstream fin("results.txt");
    //string tempin((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>()); 
    //bool result123 = cpabe.decrypt("key0", tempin, pt2);
    //cout << "cp-abe result is: "<< result123 << endl;

    //1007


    // 3. Decode the key:
    // And finally the same for the key:
    string key_string;
    ifstream inf("key.txt");
    string key_string2((istreambuf_iterator<char>(inf)),istreambuf_iterator<char>());  
    bool result123 = cpabe.decrypt("key0", key_string2, key_string);
    cout << "cp-abe result is: "<< result123 << endl;
    ShutdownOpenABE();


    byte key[CryptoPP::AES::DEFAULT_KEYLENGTH];
    {
        CryptoPP::HexDecoder decoder2;
        decoder2.Put((byte*)key_string.data(), key_string.size());
        decoder2.MessageEnd();
        decoder2.Get(key, sizeof(key));
    }    


    client_abe.stop();
    cout << endl << "client abe decryption cost: " << client_abe.result() << endl;


    string tempin;
{
    ifstream fin("results.txt");
    //string tempin((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>()); 
    fin >> tempin;
}
    // 2. Decode cipher:
    // Next, we do a similar trick for cipher, only here we would leave raw hex
    //  in a std::string.data(), since it is convenient for us to pass this
    // std::string to the decryptor mechanism:
    std::string cipher_raw;
    {
        CryptoPP::HexDecoder decoder3;
        decoder3.Put((byte*)tempin.data(), tempin.size());
        decoder3.MessageEnd();

        long long size1 = decoder3.MaxRetrievable();
        cipher_raw.resize(size1);       
        decoder3.Get((byte*)cipher_raw.data(), cipher_raw.size());
        // If we print this string it's completely rubbish: 
        // std::cout << "Raw cipher: " << cipher_raw << std::endl;
    }

    // 4. Decrypt:
    std::string decrypted_text;
    
    CryptoPP::CTR_Mode<CryptoPP::AES>::Decryption d;
    d.SetKeyWithIV(key, sizeof(key), iv);

    CryptoPP::StringSource ss(
            cipher_raw, 
            true, 
            new CryptoPP::StreamTransformationFilter(
            d,
            new CryptoPP::StringSink(decrypted_text)
                ) // StreamTransformationFilter
    ); // StringSource

    cout << endl << "AES Decryption successfully!" << endl;

    Timer client_read;
    client_read.start();
{
    ofstream fout("results7temp.txt");
    fout << decrypted_text;
}
    //string querydata;
     
{
    ifstream ifsa("results7temp.txt");
    boost::archive::text_iarchive ia(ifsa);
    ia >> r3;
    ia >> inac_policy;
}

    client_read.stop();
    cout << endl << "client reading files cost: " << client_read.result() << endl;

    //display results
    //pretty print~

    string result1, result2;
    int result_num = 0, circle = 1;
    for (auto iter = r3.accessible_data.begin(); iter != r3.accessible_data.end(); iter++){
        RawDataRecord<DB_Type> temp(iter->second.first);
        if(result_num < 10)
        {
            result1 += temp.value;
            result1 += "\n";
        }
        else
        {
            result2 += temp.value;
            result2 += " ";
            if((circle/4)==0) result2 += "\n";
            circle++;   
        }
        result_num++;
    }

    Timer client_show;
    client_show.start();
    //string strresult = result1.dump();
    //present query results
    QString strMsg = QString::fromStdString(result1);
    QString strMsg2 = QString::fromStdString(result2);
    //QString Querydata = QString::fromStdString(querydata);

    //replace punctuation
    //QString Querydata2 = Querydata.replace(QRegExp(","),"\n");
    //Querydata2 = Querydata2.replace(QRegExp("\\{"),"\n");
    //Querydata2 = Querydata2.replace(QRegExp("\\}"),"\n");

    ui->resultBrowser->setText(strMsg);
    ui->resultBrowser2->setText(strMsg2);
    client_show.stop();
    cout << endl << "client showing data cost: " << client_show.result() << endl;

    //ui->resultBrowser2->setText(querypid);
}

void MainWindow::on_verifybtn_clicked()
{
    //GlobalPBCPairing::set(pairing);
    //omp_set_num_threads(4);
    //bool aab_result = r3.verify(mvk, Rectangle({stoi(querypid_str), stoi(queryaid_str)}, {stoi(querypid_str), stoi(queryaid_str)}), inac_policy);
    bool aab_result = r3.verify(mvk, Rectangle({66, 2}, {66, 2}), inac_policy);
    if (aab_result == 1)
        res_verification = tr("The authentication result is true!");
    else
        res_verification = tr("The authentication result is false!");

    if(res_verification.isEmpty())
       res_verification = tr("No Data Received");
    ui->veBrowser->setText(res_verification);
}

void MainWindow::on_verifybtn2_clicked()
{
    //wait to be changed...
    this->close();
}

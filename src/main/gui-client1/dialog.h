#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

protected:
	void paintEvent(QPaintEvent*);

private slots:
    void timeout();


private:
    Ui::Dialog *ui;
    QTimer *timer;

};

#endif // DIALOG_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDialog>
#include "dialog.h"
#include "logindialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_2_clicked();

    void on_querybtn_clicked();

    void on_verifybtn_clicked();

    void on_pushButton_3_clicked();

    void on_querybtn_2_clicked();

    void on_querybtn_3_clicked();

    void on_mainexit_2_clicked();

    void on_mainexit_3_clicked();

private:
    Ui::MainWindow *ui;
    QString querypid;
    QString queryaid;
    QString queryaid2;
    QString res_verification;
    Dialog *dialog = new Dialog;
};

#endif // MAINWINDOW_H

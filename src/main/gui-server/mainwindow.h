#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
	void paintEvent(QPaintEvent*);

private slots:

    void on_mainexit_clicked();

    void on_mainexit_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();
 
    void timeout();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
};

#endif // MAINWINDOW_H

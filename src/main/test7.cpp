#include "../vendor/include/soci/soci.h"
//#include "sqlite3/soci-sqlite3.h"
#include "../vendor/include/soci/mysql/soci-mysql.h"

#include <iostream>
#include <sstream>
#include <time.h>

using namespace soci;
using namespace std;
int main()
{
    soci::session sql(mysql, "db=newfund user=root password='123456' charset=utf8");
 //   rowset<row> rs = (sql.prepare << "select projectName,beginTime,leaderName from project");
/*
for(rowset<row>::const_iterator it = rs.begin(); it != rs.end(); ++it)
{
    row const& row = *it;    
    //query data  
    string projectName = row.get<string>(0);
    tm when = row.get<tm>(1);
    string beginTime = asctime(&when);
    string leaderName = row.get<string>(2);

    cout << projectName << endl;
    cout << "---------------" << endl;
    cout << leaderName << endl;
    cout << "---------------" << endl;
    cout << beginTime << endl;
}
*/
    rowset<row> rs = (sql.prepare << "select projectName,technologyType,industryCode,subjectName,subjectCode,beginTime,endTime,leaderName,leaderSex,birthday,leaderDegree,leaderTitle,leaderPosition,researchCenter,basicFund,superiorFund,selfFund,otherFund,totalFund,summary,applyYear,schoolid,researchCenterType,status,projectNumber,projectStatus,keySupportTalent,projectType from project");
for(rowset<row>::const_iterator it = rs.begin(); it != rs.end(); ++it)
{
    row const& row = *it;
    
    //query data  
    string projectName = row.get<string>(0);
    string technologyType = row.get<string>(1);
    string industryCode = row.get<string>(2);
    string subjectName = row.get<string>(3);
    string subjectCode = row.get<string>(4);

    tm whenbegin = row.get<tm>(5);
    string beginTime = asctime(&whenbegin);
    tm whenend = row.get<tm>(6);
    string endTime = asctime(&whenend);

    string leaderName = row.get<string>(7);
    string leaderSex = row.get<string>(8);

    tm whenbirth = row.get<tm>(9);
    string birthday = asctime(&whenbirth);

    string leaderDegree = row.get<string>(10);
    string leaderTitle = row.get<string>(11);
    string leaderPosition = row.get<string>(12);
    string researchCenter = row.get<string>(13);

    string basicFund = to_string(row.get<double>(14));
    string superiorFund = to_string(row.get<double>(15));
    string selfFund = to_string(row.get<double>(16));
    string otherFund = to_string(row.get<double>(17));
    string totalFund = to_string(row.get<double>(18));
    string summary = row.get<string>(19);
 
    int fundYear = stoi(row.get<string>(20));
    int schoolid = row.get<long long>(21);  
    //cout << schoolid << endl;  
 
    string researchCenterType = row.get<string>(22);
    string status = row.get<string>(23);
    string projectNumber = row.get<string>(24);
    //string industryCodeOld = row.get<string>(25);
    //string subjectCodeOld = row.get<string>(26);
    string projectStatus = row.get<string>(25);
    string keySupportTalent = row.get<string>(26);
    string projectType = row.get<string>(27);
}
	return 0;
}
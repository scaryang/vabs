#include "crow.h"

#include <iostream>
#include <nlohmann/json.hpp>

#include <sstream>

#include "../db_dup/db.hpp"
#include "../db/db.hpp"

#include "./pbc_helper.h"
#include <map>

#include <iostream>
//#include "sql_parser.h"
#include <fstream>
#include <vector>

#include <set>
#include <string>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/nvp.hpp>

#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_free.hpp>

#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>
#include <boost/algorithm/string.hpp>

#include "../utils/timer.hpp"

//cryptopp
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;
#include "cryptopp/cryptlib.h"
using CryptoPP::Exception;

#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "cryptopp/filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include "cryptopp/aes.h"
using CryptoPP::AES;

#include "cryptopp/ccm.h"
using CryptoPP::CTR_Mode;

#include "assert.h"


using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;
using json = nlohmann::json;
using namespace oabe;
using namespace oabe::crypto;
using namespace boost::algorithm;



struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};


class ExampleLogHandler : public crow::ILogHandler {
    public:
        void log(std::string /*message*/, crow::LogLevel /*level*/) override {
//            cerr << "ExampleLogHandler -> " << message;
        }
};

struct ExampleMiddleware 
{
    std::string message;

    ExampleMiddleware() 
    {
        message = "foo";
    }

    void setMessage(std::string newMsg)
    {
        message = newMsg;
    }

    struct context
    {
    };

    void before_handle(crow::request& /*req*/, crow::response& /*res*/, context& /*ctx*/)
    {
        CROW_LOG_DEBUG << " - MESSAGE: " << message;
    }

    void after_handle(crow::request& /*req*/, crow::response& /*res*/, context& /*ctx*/)
    {
        // no-op
    }
};

    unordered_map<string,int> pid;
    datarecord teemo;
    typedef Type<string,int> DB_Type;
    //DupDatabase<DB_Type> db("test.db");
    Database<DB_Type> db("100patient.db");
    //PairingPtr pairing;

int main()
{
    crow::App<ExampleMiddleware> app;

    app.get_middleware<ExampleMiddleware>().setMessage("hello");

        //create global pid map
    //unordered_map<string,int> pid;
    //only patientid
    string line;
    ifstream infile0;
    int num = 1;
    infile0.open("/home/yang/Pictures/aaa/data/PatientCorePopulatedTable.txt");
    //init query attr
    while( getline(infile0,line) )
    {
        vector<string> result;
        split(result, line, is_any_of("\\"),token_compress_on);
        pid.insert(make_pair( result.at(0), num));
        num++;
        vector<string> free;
        result.swap(free);
    }
    infile0.close();


    for (int i = 0; i < 11; i++)
    {
        teemo.global_roles.insert(i);
    }
    teemo.roles = set<int>{8,4,5};
    teemo.pseudo_role = 0;
    teemo.range = Rectangle({1,1},{100,7});
    db.load(true);
    //iring = db.meta.pairing;


    //set ip_route
    CROW_ROUTE(app, "/upload")
        .methods("POST"_method)
    ([](const crow::request& req){
        auto x = crow::json::load(req.body);
        auto y = json::parse(req.body);
        cout << "-------receive successfully!-------" << endl;      
        if (!x)
            return crow::response(400);   
     
    //Timer response_tm;
    //response_tm.start();

    auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(teemo.roles);

    json xxx(y["pid"]);
    string trans_pid = xxx.dump();
    trans_pid.erase(trans_pid.begin(),trans_pid.begin()+1);
    trans_pid.erase(trans_pid.end()-1,trans_pid.end());

    json yyy(y["type"]);
    string query_type = yyy.dump();
    query_type.erase(query_type.begin(),query_type.begin()+1);
    query_type.erase(query_type.end()-1,query_type.end());
    cout << "query type is : " << query_type << endl;

    cout << endl << trans_pid << endl;;
    cout << endl << pid.at(trans_pid) << endl;

    int trans_aid = x["aid"].i();
    int trans_aid2 = x["aaid"].i();

    cout << endl << "-------------test range query result---------" << endl;
    if(!( pid.at(trans_pid) < 100 && pid.at(trans_pid) > 0)) 
    {
        cout << "error here~"  << endl;
        exit(0);
    }


    //Timer query_tm;
    //query_tm.start();

    DB::Point point1({pid.at(trans_pid), trans_aid});
    DB::Point point2({pid.at(trans_pid), trans_aid2});

    std::ostringstream strstr;

    if (query_type == "single"){
        auto r3 = db.equality_query({pid.at(trans_pid), trans_aid}, teemo.roles);

        //response_tm.stop();
        boost::archive::text_oarchive oa(strstr);
        oa << r3;
        oa << inaccessible_policy;
        oa << point1;
        oa << point2;
        //oa << response_tm.result();
    }
    else{   
        auto r3 = db.range_query(Rectangle({pid.at(trans_pid), trans_aid}, {pid.at(trans_pid), trans_aid2}), teemo.roles);       
        boost::archive::text_oarchive oa(strstr);
        oa << r3;
        oa << inaccessible_policy;
        oa << point1;
        oa << point2;
        oa << (trans_aid2-trans_aid+1);
    }
    //auto r3 = db.range_query(Rectangle({pid.at(trans_pid), trans_aid}, {pid.at(trans_pid), trans_aid}), teemo.roles);
    
    //query_tm.stop();
    //int query_tms = query_tm.result();
    //cout << "query time is :  " << query_tms << endl;

    //cout << r3.accessible_data.size() << endl;   
    //cout << endl << "---------------------------" << endl;

    //add cp-abe
    InitializeOpenABE();
    OpenABECryptoContext cpabe("CP-ABE");
    string ct;

    ifstream fin("mpk.txt");
    string mpk((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>());
    cpabe.importPublicParams(mpk);
    
/*
    std::ostringstream strstr;
{
    boost::archive::text_oarchive oa(strstr);
    oa << r3;
    oa << inaccessible_policy;
    oa << query_tms;
}
*/
    //aes ctr encrypt
    AutoSeededRandomPool prng;

    byte key[AES::DEFAULT_KEYLENGTH];
    prng.GenerateBlock(key, sizeof(key));

    byte iv[AES::BLOCKSIZE];
    prng.GenerateBlock(iv, sizeof(iv));

    string cipher, encoded, encoded1, encoded2, recovered;

    // string key
    //encoded.clear();
    StringSource(key, sizeof(key), true,
        new HexEncoder(
            new StringSink(encoded1)
        ) // HexEncoder
    ); // StringSource
    //cout << "key: " << encoded1 << endl;

    // string iv
    //encoded.clear();
    StringSource(iv, sizeof(iv), true,
        new HexEncoder(
            new StringSink(encoded2)
        ) // HexEncoder
    ); // StringSource
    //cout << "iv: " << encoded2 << endl;

    //cp-abe encrypt key
    cpabe.encrypt("attr1 and attr2", encoded1, ct);
    ShutdownOpenABE();

{
    ofstream outf("key.txt");
    outf << ct;
}

{
    ofstream outx("iv.txt");
    outx << encoded2;
}

    // string cipher
    try
    {
        //cout << "plain text: " << plain << endl;

        CTR_Mode< AES >::Encryption e;
        e.SetKeyWithIV(key, sizeof(key), iv);

        // The StreamTransformationFilter adds padding
        //  as required. ECB and CBC Mode must be padded
        //  to the block size of the cipher.
        StringSource(strstr.str(), true, 
            new StreamTransformationFilter(e,
                new StringSink(cipher)
            ) // StreamTransformationFilter      
        ); // StringSource
    }
    catch(const CryptoPP::Exception& e)
    {
        cerr << e.what() << endl;
        exit(1);
    }

    /*********************************\
    \*********************************/

    // string cipher
    encoded.clear();
    StringSource(cipher, true,
        new HexEncoder(
            new StringSink(encoded)
        ) // HexEncoder
    ); // StringSource
    //cout << "cipher text: " << encoded << endl;


    //cpabe.encrypt("attr1 and attr2",strstr.str(),ct);
    //close cpabe lib



    return crow::response{encoded};
});

    // enables all log
    app.loglevel(crow::LogLevel::DEBUG);
    //crow::logger::setHandler(std::make_shared<ExampleLogHandler>());

    app.port(18080)
        .multithreaded()
        .run();
}
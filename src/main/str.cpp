#include "../db_dup/db.hpp"
#include "../db/db.hpp"
#include "../utils/pbc_helper.h"
#include "../db/sig.hpp"
#include "../db/data.hpp"
#include "../db/type.hpp"
#include "../db/rect.hpp"
#include "../abs/boolean_expression.hpp"
#include "../abs/boolean_expression_dnf.hpp"
#include "../utils/timer.hpp"


#include <iostream>
#include <vector>

#include <set>
#include <string>


using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;

struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};


typedef Type<string,int> DB_Type;    


int main(){
    //typedef Type<string,int> DB_Type;

    datarecord teemo;

    for (int i = 0; i < 11; i++)
    {
        teemo.global_roles.insert(i);
    }
    teemo.roles = set<int>{8,4,5};
    teemo.pseudo_role = 0;
    teemo.range = Rectangle({1,1},{10000,30});

    Database<DB_Type> db("300000patient.db");
    db.load(true);
    auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(teemo.roles);

    Timer query_tm;
    query_tm.start();
    string result1;
    //auto r3 = db.range_query(Rectangle({66, 1}, {66, 7}), teemo.roles);
    auto r3 = db.equality_query({66, 1}, teemo.roles);

    query_tm.stop();
    
    cout << "query time is :" << query_tm.result() << endl;
/*
    ofstream ofs("serial.txt");
    {
        boost::archive::text_oarchive oa(ofs);
        oa & mvk;
    }
*/

    //cout << r3.accessible_data.size() << endl;
    //for (auto iter = r3.accessible_data.begin(); iter != r3.accessible_data.end(); iter++){
    //            RawDataRecord<DB_Type> temp(iter->second.first);
    //            result1 += temp.value;}
    //cout << result1 << endl;
    Timer verify_tm;
    verify_tm.start();
    bool result = r3.verify(mvk, {66, 1} , inaccessible_policy);
    verify_tm.stop();
    cout << "verify time is:" << verify_tm.result() << endl;

    cout << "the result of verification is :" << result << endl;

    return 0;
}
#include "../authdb/db.hpp"
#include "../authdb/update.hpp"
#include "../authdb/type.hpp"
#include <iostream>
#include <string>
#include "../../test/pbc_helper.h"
#include <set>

using namespace ABS;
using namespace DB;
using namespace std;
using namespace pbc;

struct datarecord{
	set<int> global_roles;
	set<int> roles;
	int pseudo_role;
	Rectangle range;
};

typedef Type<string, int> DB_Type;


int main()
{
	/*
	datarecord test;
    test.global_roles = set<int>{0,1,2,3,4};
    test.roles = set<int>{1,3,4};
    test.pseudo_role = 0;
    test.range = Rectangle({0,0},{4,3});
    */
    datarecord test;
    for (int i = 0; i < 11; i++)
    {
        test.global_roles.insert(i);
    }
    test.roles = set<int>{8,4,5};
    test.pseudo_role = 0;
    test.range = Rectangle({1,1},{100,7});

    Database<DB_Type> db("newtest.db");
/*
    unordered_map<Point, RawDataRecord<DB_Type>> data{
        {{0, 2}, RawDataRecord<DB_Type>(0, string("1"))},
        {{1, 2}, RawDataRecord<DB_Type>(1, string("1 OR 2"))},
        {{2, 2}, RawDataRecord<DB_Type>(2, string("1 AND 3"))},
        {{3, 2}, RawDataRecord<DB_Type>(3, string("3"))},
        {{1, 1}, RawDataRecord<DB_Type>(4, string("2"))},
    };
    db.create_meta(TYPE_A_PARAM, test.pseudo_role, test.global_roles, 2, test.range);
    db.create_index(data, IndexType::grid);
    db.save(true);
    db.reset_sig();
    db.create_data_sig();
    db.create_grid_sig();
*/
    db.load(true);
    auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(test.roles);

    //raw data
    auto r1 = db.equality_query({66, 2}, test.roles);
    cout << "Point(66,2) value is :" << r1.record.value << endl;
    cout << "Point(66,2) is :" << r1.accessible << endl;
    cout << "Verification Result is :" << r1.verify(mvk, {66, 2}, inaccessible_policy) << endl;
    cout << endl;


    //change value
    update_record<DB_Type>(db, Point{66,2}, RawDataRecord<DB_Type>("kdd", string("1 OR ((8 OR 9) AND (4 AND 5))")));

    auto r2 = db.equality_query({66, 2}, test.roles);
    cout << "Point(1,2) value is :" << r2.record.value << endl;
    cout << "Point(1,2) is :" << r2.accessible << endl;
    cout << "Verification Result is :" << r2.verify(mvk, {66, 2}, inaccessible_policy) << endl;
    cout << endl;
/*
    update_record<DB_Type>(db, Point{66,2}, RawDataRecord<DB_Type>(3, string("1 OR ((8 OR 9) AND (4 AND 5))")));

    auto r4 = db.equality_query({66, 2}, test.roles);
    cout << "Point(1,2) value is :" << r4.record.value << endl;
    cout << "Point(1,2) is :" << r4.accessible << endl;
    cout << "Verification Result is :" << r4.verify(mvk, {66, 2}, inaccessible_policy) << endl;
    cout << endl;


    //change value
    auto r3 = db.equality_query({66, 3}, test.roles);
    cout << "Point(2,2) value is :" << r3.record.value << endl;
    cout << "Point(2,2) is :" << r3.accessible << endl;
    cout << "Verification Result is :" << r3.verify(mvk, {66, 3}, inaccessible_policy) << endl;
    cout << endl;
*/
/*  //change policy
    update_record<DB_Type>(db, Point{1,2}, RawDataRecord<DB_Type>(2, string("1 AND 2")));

    auto r3 = db.equality_query({1, 2}, test.roles);
    cout << "Point(1,2) value is :" << r3.record.value << endl;
    cout << "Point(1,2) is :" << r3.accessible << endl;
    cout << "Verification Result is :" << r3.verify(mvk, {1, 2}, inaccessible_policy) << endl;
    cout << endl;
*/

    return 0;
}
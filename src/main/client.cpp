#include <sstream>

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <nlohmann/json.hpp>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>
#include "../db_dup/db.hpp"
#include "./pbc_helper.h"
#include "../abs/abs.hpp"
#include "../abs/boolean_expression.hpp"
#include "../abs/boolean_expression_dnf.hpp"

#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_free.hpp>

#include "../db/sig.hpp"
#include "../db/data.hpp"
#include "../db/type.hpp"
#include "../db/rect.hpp"

#include <iostream>
#include "sql_parser.h"
#include <fstream>
#include <vector>

#include <set>
#include <string>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>

using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;
using json = nlohmann::json;
using namespace oabe;
using namespace oabe::crypto;

struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};
 

/// Callback must be declared static, otherwise it won't link...
size_t WriteCallback(char* ptr, size_t size, size_t nmemb, void *f)
{
  FILE *file = (FILE *)f;
  return fwrite(ptr, size, nmemb, file);
};


int main(int argc, char *argv[])
{
  if(argc < 3) {
    std::cerr << "Example 2: Missing argument" << std::endl 
        << "Example 2: Usage: example02 url [file] sql.txt" 
        << std::endl;
    return EXIT_FAILURE;
  }

  int run_num = 1;
  //support multi input
  int print = 1;


  char *url = argv[1];
  char *filename = NULL;

  if(argc >= 3)
  {
    filename = argv[2];
  }
  
  std::ifstream ifs(argv[3], std::ios::in);
  std::vector<std::string> sqls;
  while (!ifs.eof()) {
      char sql_buf[255];
      ifs.getline(sql_buf, sizeof(sql_buf));
      size_t sql_len = strlen(sql_buf);
      if (sql_len <= 0) continue;
      sqls.push_back(sql_buf);
  }

    //define vec(field) and table 
    vector<string> field;
    string table;
    
    //sql parser
    SQLParser<std::string::const_iterator> sql_parser;
    for (int i = 0; i < run_num; ++i) {
        for (std::vector<std::string>::iterator iter = sqls.begin(); iter != sqls.end(); ++iter) {
            std::string::const_iterator begin = iter->begin();
            std::string::const_iterator end = iter->end();
            SelectSQL select_sql;
            bool ret = phrase_parse(begin, end, sql_parser, boost::spirit::ascii::space, select_sql);
            if (ret && begin == end) {
                if (print) {
                   std::cout << "phrase_parse succ. sql=" << *iter << std::endl;
                   for (std::vector<std::string>::iterator iter = select_sql.fields.begin(); iter != select_sql.fields.end(); ++iter) {
                   std::cout << "[field]=" << *iter << std::endl;
                   field.push_back(*iter);
                   }
                   std::cout << "[table]=" << select_sql.table << std::endl;
                   table = select_sql.table;
                   if (select_sql.has_condition) {
                   std::cout << "[condition]= " << std::endl;
                   std::cout << select_sql.condition.left << std::endl;
                   std::cout << select_sql.condition.right << std::endl;
                   //output to txt
                   ofstream file("rdbuf.txt");
                 streambuf *x = cout.rdbuf( file.rdbuf() );
                 std::cout << select_sql.condition.left << std::endl;
                   std::cout << select_sql.condition.right << std::endl;
                   cout.rdbuf(x);

                   //std::cout << "[condition]=" << select_sql.condition << std::endl;
                   }
                   std::cout << "=============" << std::endl;
                }
            } else {
                std::cerr << "[FAIL]phrase_parse fail. sql=" << *iter << std::endl;
                std::cerr << "=============" << std::endl;
            }
        }
    }
    
    //output of results parser
    cout << "Test field and table!" << endl;
    cout << "The table is   " << table << endl;
    cout << "The fields are   " << endl;
    for (int i = 0; i < field.size(); i++)
    {
      cout << field.at(i) << " ";
    }
    cout << endl;

    cout << "-----------------------" << endl;

    ifstream infile;
    infile.open("rdbuf.txt");

      if(!infile) cout << "Read Error!" << endl;
  
    string t1;
    cout << "save into vector" << endl;

    vector<string> ve;
{   
    while(infile >> t1)
    {
      ve.push_back(t1);
    }
}
    for (int i = 0; i < ve.size(); i++)
    {
      cout << ve.at(i) << " ";
    }
    cout << endl;
    cout << "-------------------------" << endl;


    string year,school;
    if ( ve.at(0) == string("fundYear") ) 
    {
      year = ve.at(1);
      if ( ve.at(2) == string("schoolid") ) 
      {
        school = ve.at(3);
      }
    }

    if ( ve.at(0) == string("schoolid") ) 
    {
      school = ve.at(1);
      if ( ve.at(2) == string("fundYear") )
      {
           year = ve.at(3);
      }
    }
    
    cout << "fundYear:  " << year << endl;
    cout << "Schoolid:  " << school << endl;
    cout << "----------------------" << endl;

    infile.close();

    //generate query
    //string field1[20];
    //std::copy(field.begin(),field.end(),field1); 
    //string* field1 = &field[0];

    json query;
    query["table"] = table;
    query["field"] = field;
    query["key"]["school"] = school;
    query["key"]["year"] = year;

    std::string s = query.dump();
    std::istringstream myStream(s);
  
    int size = myStream.str().size();

    //set options of file
    char buf[100];
    try
    {
      curlpp::Cleanup cleaner;
      curlpp::Easy request;

      
      std::list< std::string > headers;
      headers.push_back("Content-Type: text/*"); 
      sprintf(buf, "Content-Length: %d", size); 
      headers.push_back(buf);
      
      using namespace curlpp::Options;
      //self add
      request.setOpt(new curlpp::options::CustomRequest{"POST"});
      
      /// Set the writer callback to enable cURL to write result in a memory area
      curlpp::options::WriteFunctionCurlFunction
      myFunction(WriteCallback);

      FILE *file = stdout;
      if(filename != NULL)
         {
          file = fopen(filename, "wb");
           if(file == NULL)
           {
            fprintf(stderr, "%s/n", strerror(errno));
            return EXIT_FAILURE;
          }
         } 
      curlpp::OptionTrait<void *, CURLOPT_WRITEDATA> 
              myData(file);

      request.setOpt(myFunction);
      request.setOpt(myData);

      request.setOpt(new Verbose(true));
      request.setOpt(new ReadStream(&myStream));
      request.setOpt(new InfileSize(size));
      request.setOpt(new Upload(true));
      request.setOpt(new HttpHeader(headers));
      request.setOpt(new Url(url));
       
      request.perform();
      //remember to close file
      fclose(file);
      file = NULL;


 //verification
     typedef Type<string,int> DB_Type;
 //DupDatabase<DB_Type> db("fundAudit1.db");
 //db.load(true);
 //auto mvk = db.get_mvk();
 //auto inaccessible_policy = db.compute_inaccessible_policy(set<int>{1,2,118,115,123});

     PairingPtr pairing(Pairing::init_from_param(TYPE_A_PARAM));
     KeyDistributionCenter kdc;
     kdc.setup(pairing);
     auto mvk = kdc.get_master_verifying_key();
     GlobalPBCPairing::set(pairing);

/*
     PairingPtr pairing(Pairing::init_from_param(TYPE_A_PARAM));
     KeyDistributionCenter kdc;
     kdc.setup(pairing);
     GlobalPBCPairing::set(pairing);
     datarecord cl;
     for (int i = 0; i < 125; i++)
     {
        cl.global_roles.insert(i);
     }
     cl.roles = set<int>{1,2,118,115,123};
     cl.pseudo_role = 0;
     cl.range = Rectangle({2008,1},{2016,113});
*/
//   typedef Type<string,int> DB_Type;
//   DupDatabase<DB_Type> db("fundAudit1.db");
//   db.load(true);
//   auto mvk = db.get_mvk();
//   auto inaccessible_policy = db.compute_inaccessible_policy(cl.roles);


     //verification
    DupRangeQueryResult<DB_Type> r4;
    BooleanExpression<int> inac_policy{};


    InitializeOpenABE();
    OpenABECryptoContext cpabe("CP-ABE");
    string pt2;
     //cpabe.generateParams();
    ifstream finx("msk.txt");
    string msk((istreambuf_iterator<char>(finx)),istreambuf_iterator<char>());
    cpabe.importSecretParams(msk);

    ifstream finc("mpk.txt");
    string mpk((istreambuf_iterator<char>(finc)),istreambuf_iterator<char>());
    cpabe.importPublicParams(mpk);

    cpabe.keygen("|attr1|attr2","key0");

  
    ifstream fin("results7.txt");
    string tempin((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>()); 
    bool result123 = cpabe.decrypt("key0", tempin, pt2);
    cout << "cp-abe result is: "<< result123 << endl;

    //cout << tempin << endl;;

    ofstream fout("results7temp.txt");
    fout << pt2;


{
    ifstream ifsa("results7temp.txt");
    boost::archive::text_iarchive ia(ifsa);
    ia >> r4;
    ia >> inac_policy;
}

    bool aab_result = r4.verify(mvk, Rectangle({std::stoi(year), std::stoi(school)}, {std::stoi(year), std::stoi(school)}), inac_policy);
   //bool aab_result = r4.verify(mvk, Rectangle({std::stoi(year), std::stoi(school)}, {std::stoi(year), std::stoi(school)}), inaccessible_policy);
 
    cout << "The verification of results is " << " ";
    if (aab_result == true)
        cout << "true" << endl;
    else
        cout << "false" << endl;
  
   return 0;

   }
   catch ( curlpp::LogicError & e )
     {
       std::cout << e.what() << std::endl;
     }
   catch ( curlpp::RuntimeError & e )
     {
       std::cout << e.what() << std::endl;
     }


}


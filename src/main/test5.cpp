#include "../db_dup/db.hpp"
#include "./pbc_helper.h"
#include "../vendor/include/soci/soci.h"
//#include "sqlite3/soci-sqlite3.h"
#include "../vendor/include/soci/mysql/soci-mysql.h"

#include <iostream>
#include <vector>
#include <time.h>

#include <set>
#include <string>
#include <exception>


using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;
using namespace soci;

struct datarecord{
     /*
     datarecord()
     {
     	global_roles = set<int>{0,1,2,3,4,5,6,7,8,9};
     	roles = set<int>{1,2};
     	pseudo_role = 0;
     	range = Rectangle({2008,1},{2016,113});
     }
     ~datarecord(){}
     */
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};

int main()
{
	//serilization
	datarecord cl;
	for (int i = 0; i < 125; i++)
    {
    	cl.global_roles.insert(i);
    }
    cl.roles = set<int>{114};
    cl.pseudo_role = 0;
    cl.range = Rectangle({2008,1},{2040,113});
   
	typedef Type<string,int> DB_Type;
	DupDatabase<DB_Type> db("db_fundproject.db");
    //input data
    //int total_num = 5232472;

    std::vector<string> value;
    //value.resize(total_num);
    std::vector<DB::Point> point;
    //point.resize(total_num);
    std::vector<string> policy;
    //policy.resize(total_num);

    cout << value.size() << endl 
         << point.size() << endl
         << policy.size() << endl;
    /*
    std::vector<string> value = {"IN_RESEARCH","IN_RESEARCH2","M755","2010-10-10 00:00:00",                                 
                                "IN_RESEARCH5","-80.2","1、love斯嘉丽约翰逊。","IN_RESEARCH8",
                                  "IN_RESEARCH9"};
    std::vector<DB::Point> point = {{0,2},{0,2},{1,2},{1,2},{1,2},{2,2},{2,2},{3,2},{1,1}};
    std::vector<string> policy = {string("1"),string("1"),string("1 OR 2"),string("1 OR 2")
                                      ,string("3"),string("1 AND 2"),string("1 OR 3"),string("3")
                                      ,string("2")};
    */
    std::unordered_multimap<DB::Point, DB::RawDataRecord<DB_Type>> data;

    cout << "------test soci-------" << endl;
    //connect mysql database
    soci::session sql(mysql, "db=newfund user=root password='123456' charset=utf8");
    //query attribute -- point
    //int query_id = 1666 ;
    //int query_schoolid = 1 ;
    rowset<row> rs = (sql.prepare << "select projectName,technologyType,industryCode,subjectName,subjectCode,beginTime,endTime,leaderName,leaderSex,birthday,leaderDegree,leaderTitle,leaderPosition,researchCenter,basicFund,superiorFund,selfFund,otherFund,totalFund,summary,applyYear,schoolid,researchCenterType,status,projectNumber,projectStatus,keySupportTalent,projectType from project");
    int countnum = 0;
for(rowset<row>::const_iterator it = rs.begin(); it != rs.end(); ++it)
{
    row const& row = *it;
    
    //query data  
    string projectName = row.get<string>(0);
    string technologyType = row.get<string>(1);
    string industryCode = row.get<string>(2);
    string subjectName = row.get<string>(3);
    string subjectCode = row.get<string>(4);

    tm whenbegin = row.get<tm>(5);
    string beginTime = asctime(&whenbegin);
    tm whenend = row.get<tm>(6);
    string endTime = asctime(&whenend);

    string leaderName = row.get<string>(7);
    string leaderSex = row.get<string>(8);

    tm whenbirth = row.get<tm>(9);
    string birthday = asctime(&whenbirth);

    string leaderDegree = row.get<string>(10);
    string leaderTitle = row.get<string>(11);
    string leaderPosition = row.get<string>(12);
    string researchCenter = row.get<string>(13);

    string basicFund = to_string(row.get<double>(14));
    string superiorFund = to_string(row.get<double>(15));
    string selfFund = to_string(row.get<double>(16));
    string otherFund = to_string(row.get<double>(17));
    string totalFund = to_string(row.get<double>(18));
    string summary = row.get<string>(19);
 
    int fundYear = stoi(row.get<string>(20));
    int schoolid = row.get<long long>(21);  
    //cout << schoolid << endl;  
 
    string researchCenterType = row.get<string>(22);
    string status = row.get<string>(23);
    string projectNumber = row.get<string>(24);
    //string industryCodeOld = row.get<string>(25);
    //string subjectCodeOld = row.get<string>(26);
    string projectStatus = row.get<string>(25);
    string keySupportTalent = row.get<string>(26);
    string projectType = row.get<string>(27);

    for (int j = 0; j < 26 ;j ++ )
    {
         point.push_back({fundYear,schoolid});
    }
         //Here should be opposed 
         value.push_back(projectName);
         value.push_back(technologyType);
         value.push_back(industryCode);
         value.push_back(subjectName);
         value.push_back(subjectCode);
         value.push_back(beginTime);
         value.push_back(endTime);
         value.push_back(leaderName);
         value.push_back(leaderSex);
         value.push_back(birthday);
         value.push_back(leaderDegree);
         value.push_back(leaderTitle);
         value.push_back(leaderPosition);
         value.push_back(researchCenter);
         value.push_back(basicFund);
         value.push_back(superiorFund);
         value.push_back(selfFund);
         value.push_back(otherFund);
         value.push_back(totalFund);
         value.push_back(summary);
         value.push_back(researchCenterType);
         value.push_back(status);
         value.push_back(projectNumber);
         value.push_back(projectStatus);
         value.push_back(keySupportTalent);
         value.push_back(projectType);


         int a = schoolid;   
         char str[60];
 
         sprintf(str,"%s%d%s","114 OR ((117 OR 118) AND (121 OR 122 OR 123 OR 124)) OR ((120 OR 121 OR 122 OR 123 OR 124) AND ",a,")");
         std::string policy_s = str;

         for (int k = 0; k < 26 ;k ++ )
         {
            policy.push_back(policy_s);
         }

    cout << "-------line-------" << endl;
    int value_size = value.size();
    int point_size = point.size();
    int policy_size = policy.size();
    //cout << "------begin store-------" << endl
    //     << value_size << endl
    //     << point_size << endl
    //     << policy_size << endl;
    cout << countnum << endl;
    countnum++;

    if ((value_size != point_size)||(point_size != policy_size))
    {
        cout << "There is one problem in the database" << endl;
        return 0;
    }
    cout << "---------------" << endl;
    for (int i = 0; i < 26; i++)
    { 
        data.insert(std::make_pair(point.at(i),DB::RawDataRecord<DB_Type>(value.at(i),policy.at(i))));
    }
    cout << data.size() << endl;

    vector<DB::Point>().swap(point);
    vector<string>().swap(value);
    vector<string>().swap(policy);

}    


/*
    std::vector<int> fundYear;
    std::vector<int> schoolid;
    

    //query data  
    std::vector<string> projectName;
    std::vector<string> technologyType;
    std::vector<string> industryCode;
    std::vector<string> subjectName;
    std::vector<string> subjectCode;
    std::vector<string> beginTime;
    std::vector<string> endTime;
    std::vector<string> leaderName;
    std::vector<string> leaderSex;
    std::vector<string> birthday;
    std::vector<string> leaderDegree;
    std::vector<string> leaderTitle;
    std::vector<string> leaderPosition;
    std::vector<string> researchCenter;
    std::vector<string> basicFund;
    std::vector<string> superiorFund;
    std::vector<string> selfFund;
    std::vector<string> otherFund;
    std::vector<string> totalFund;
    std::vector<string> summary;
    std::vector<string> researchCenterType;
    std::vector<string> status;
    std::vector<string> projectNumber;
    std::vector<string> industryCodeOld;
    std::vector<string> subjectCodeOld;
    std::vector<string> projectStatus;
    std::vector<string> keySupportTalent;
    std::vector<string> projectType;

    int record_num = 186874;

    fundYear.resize(record_num);
    schoolid.resize(record_num);
    //point data
    projectName.resize(record_num);
    technologyType.resize(record_num);
    industryCode.resize(record_num);
    subjectName.resize(record_num);
    subjectCode.resize(record_num);
    beginTime.resize(record_num);
    endTime.resize(record_num);
    leaderName.resize(record_num);
    leaderSex.resize(record_num);
    birthday.resize(record_num);
    leaderDegree.resize(record_num);
    leaderTitle.resize(record_num);
    leaderPosition.resize(record_num);
    researchCenter.resize(record_num);
    basicFund.resize(record_num);
    superiorFund.resize(record_num);
    selfFund.resize(record_num);
    otherFund.resize(record_num);
    totalFund.resize(record_num);
    summary.resize(record_num);
    researchCenterType.resize(record_num);
    status.resize(record_num);
    projectNumber.resize(record_num);
    industryCodeOld.resize(record_num);
    subjectCodeOld.resize(record_num);
    projectStatus.resize(record_num);
    keySupportTalent.resize(record_num);
    projectType.resize(record_num);

    sql << "select projectName,technologyType,industryCode,subjectName,"
           "subjectCode,beginTime,"
           "endTime,leaderName,leaderSex,birthday," 
           "leaderDegree,leaderTitle,leaderPosition,researchCenter,basicFund,superiorFund,"
           "selfFund,otherFund,totalFund,summary,applyYear,schoolid,"
           "researchCenterType,status,projectNumber,subjectCodeOld,projectStatus,"
           "keySupportTalent,projectType from project",
            into(projectName), into(technologyType), into(industryCode),
            into(subjectName), into(subjectCode), into(beginTime),
            into(endTime), into(leaderName), into(leaderSex),
            into(birthday), into(leaderDegree), into(leaderTitle),
            into(leaderPosition),into(researchCenter),into(basicFund),
            into(superiorFund),into(selfFund),into(otherFund),into(totalFund),
            into(summary),into(fundYear),into(schoolid),into(researchCenterType),
            into(status),into(projectNumber),into(industryCodeOld),into(subjectCodeOld),
            into(projectStatus),into(keySupportTalent),into(projectType);

    int idx = 0;
    char str[60];

    for (int i = 0; i < record_num; i++)
    {
    	 for (int j = 0; j < 28 ;j ++ )
    	 {
    	 	point.push_back({fundYear.at(idx),schoolid.at(idx)});
         }
         //Here should be opposed 
         value.push_back(projectName.at(idx));
         value.push_back(technologyType.at(idx));
         value.push_back(industryCode.at(idx));
         value.push_back(subjectName.at(idx));
         value.push_back(subjectCode.at(idx));
         value.push_back(beginTime.at(idx));
         value.push_back(endTime.at(idx));
         value.push_back(leaderName.at(idx));
         value.push_back(leaderSex.at(idx));
         value.push_back(birthday.at(idx));
         value.push_back(leaderDegree.at(idx));
         value.push_back(leaderTitle.at(idx));
         value.push_back(leaderPosition.at(idx));
         value.push_back(researchCenter.at(idx));
         value.push_back(basicFund.at(idx));
         value.push_back(superiorFund.at(idx));
         value.push_back(selfFund.at(idx));
         value.push_back(otherFund.at(idx));
         value.push_back(totalFund.at(idx));
         value.push_back(summary.at(idx));
         value.push_back(researchCenterType.at(idx));
         value.push_back(status.at(idx));
         value.push_back(projectNumber.at(idx));
         value.push_back(industryCodeOld.at(idx));
         value.push_back(subjectCodeOld.at(idx));
         value.push_back(projectStatus.at(idx));
         value.push_back(keySupportTalent.at(idx));
         value.push_back(projectType.at(idx));


         int a = schoolid.at(idx);    
         sprintf(str,"%s%d%s","114 OR ((117 OR 118) AND (121 OR 122 OR 123 OR 124)) OR ((120 OR 121 OR 122 OR 123 OR 124) AND ",a,")");
         std::string policy_s = str;

         for (int k = 0; k < 28 ;k ++ )
    	 {
    	 	policy.push_back(policy_s);
         }
         idx++;
    }

    //check equal
    cout << "-------line-------" << endl;
    int value_size = value.size();
    int point_size = point.size();
    int policy_size = policy.size();
    cout << "------begin store-------" << endl
         << value_size << endl
         << point_size << endl
         << policy_size << endl;
    if ((value_size != point_size)||(point_size != policy_size))
    {
    	cout << "There is one problem in the database" << endl;
    	return 0;
    }

*/
/*
	//aggregate data
    cout << "---------------" << endl;
    for (int i = 0; i < total_num; i++)
    { 
        //cout << i << endl;
        data.insert(std::make_pair(point.at(i),DB::RawDataRecord<DB_Type>(value.at(i),policy.at(i))));
    }
*/
    /*
	unordered_multimap<Point, RawDataRecord<DB_Type>> data{
		{{0,2}, RawDataRecord<DB_Type>("IN_RESEARCH",string("1"))},
		{{0,2}, RawDataRecord<DB_Type>("IN_RESEARCH2",string("1"))},
		{{1,2}, RawDataRecord<DB_Type>("M755",string("1 OR 2"))},
		{{1,2}, RawDataRecord<DB_Type>("2010-10-10 00:00:00",string("1 OR 2"))},
		{{1,2}, RawDataRecord<DB_Type>("IN_RESEARCH5",string("3"))},
		{{2,2}, RawDataRecord<DB_Type>("-80.2",string("1 AND 2"))},
		{{2,2}, RawDataRecord<DB_Type>("1、love斯嘉丽约翰逊。",string("1 OR 3"))},
		{{3,2}, RawDataRecord<DB_Type>("IN_RESEARCH8",string("3"))},
		{{1,1}, RawDataRecord<DB_Type>("IN_RESEARCH9",string("2"))},
	};
	*/

	db.create_meta(TYPE_A_PARAM, cl.pseudo_role, cl.global_roles, 2, cl.range);
	db.create_index(data, IndexType::grid);
	db.save(true);
	db.reset_sig();
	db.create_data_sig();
	db.create_grid_sig();
	auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(cl.roles);
	
	/*
	//test equality query;
	cout << "output r1" << endl;
	auto r1 = db.equality_query({1,2},cl.roles);
	cout << r1.accessible << endl;
	cout << r1.verify(mvk,{1,2}, inaccessible_policy) << endl;
	cout << r1.get_vo_size() << endl;
	cout << "---------------" << endl;

	cout << "output r2" << endl;
	auto r2 = db.equality_query({2,2},cl.roles);
	cout << r2.accessible << endl;
	cout << r2.verify(mvk,{2,2}, inaccessible_policy) << endl;
	cout << r2.get_vo_size() << endl;
	cout << "---------------" << endl;
    */
    /*
	//test range query
	cout << "test range query result" << endl;
	auto r3 = db.range_query(Rectangle({1, 0}, {3, 2}), cl.roles);
	cout << r3.accessible_data.size() << endl;
	cout << r3.inaccessible_data.size() << endl;
	cout << r3.inaccessible_grid.size() << endl;
	bool r3_result = r3.verify(mvk, Rectangle({1, 0}, {3, 2}), inaccessible_policy);
	cout << r3_result << endl;
	cout << r3.get_vo_size() << endl;
	*/
	cout << "test range query result" << endl;
	auto r3 = db.range_query(Rectangle({2014, 113}, {2014, 113}), cl.roles);
	cout << r3.accessible_data.size() << endl;
	//cout << r3.inaccessible_data.size() << endl;
	//cout << r3.inaccessible_grid.size() << endl;
	auto iter = r3.accessible_data.begin();
	for (; iter != r3.accessible_data.end(); iter++){
		RawDataRecord<DB_Type> temp(iter->second.first);
		cout << temp.value << endl;
	}

	bool r3_result = r3.verify(mvk, Rectangle({2014, 113}, {2014, 113}), inaccessible_policy);
	cout << r3_result << endl;
	//cout << r3.get_vo_size() << endl;

	return 0;
}

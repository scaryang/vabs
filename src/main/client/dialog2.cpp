#include "dialog2.h"
#include "ui_dialog2.h"
#include <iostream>
#include <fstream>
#include <map>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <vector>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost::algorithm;

Dialog2::Dialog2(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog2)
{
    ui->setupUi(this);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);
    ui->tableWidget->verticalHeader()->setStretchLastSection(QHeaderView::Stretch);
    ui->tableWidget_2->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);
    
    ui->tableWidget->setWordWrap(false);
    ui->tableWidget_2->setWordWrap(false);
    //ui->tableWidget->resizeRowsToContents();
    //ui->tableWidget->resizeColumnsToContents();
}

Dialog2::~Dialog2()
{
    delete ui;
}

void Dialog2::transmit(int teemo)
{
	int type;
{
    ifstream ifsak("detail_type.txt");
    boost::archive::text_iarchive iak(ifsak);
    iak >> type;
}

    map<string,string> information;
{
    ifstream ifsa("detail.txt");
    boost::archive::text_iarchive iak(ifsa);
    iak >> information;
}

    for (auto iter = information.begin(); iter != information.end(); iter++)
        {
            if (iter->first == to_string(teemo)) 
            {
               string result1 = iter->second;
               int circle_x = 0;
               int rowindex = 0;

               vector<string> show;
               split(show, result1, is_any_of("\\"),token_compress_on);

            for (unsigned int r = 0; r < show.size(); ++r){
            	if(r < 10)
                {
                    QString aaa = QString::fromStdString(show[r]);
                    ui->tableWidget->setItem(0,r,new QTableWidgetItem(aaa));           
                }
               if(r >= 10)
                  {
                      QString bbb = QString::fromStdString(show[r]);
                      if (circle_x%4 == 0)
                          {
                              rowindex++;
                              ui->tableWidget_2->setRowCount(rowindex);
                          }
                            ui->tableWidget_2->setItem(rowindex-1,circle_x%4, new QTableWidgetItem(bbb));
                            circle_x++;        
                  }
              }
          }

      } 
}


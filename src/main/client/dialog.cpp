#include "dialog.h"
#include "ui_dialog.h"
#include <QStandardItemModel>
#include <QStandardItem>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <iostream>
#include <fstream>
#include <map>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <QTimer>
#include <functional>
#include <streambuf>
#include <stdlib.h>


using namespace std;

std::string encode_hex(const std::string& src_str,bool upper) 
{
    const static char hex1[]="0123456789abcdEF";
    const static char hex2[]="0123456789ABCDEF";
    const char* hexp=upper?hex2:hex1;
    std::string dst;
    dst.assign(src_str.size()*2,' ');
    for(size_t i=0;i<src_str.size();++i) 
    {
        unsigned char c=(unsigned char)src_str[i];
        dst[i*2+1] = hexp[c&0xF];
        dst[i*2] = hexp[(c>>4)&0xF];
    }
    return dst;
}

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    //ui->tableWidget_2->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);
    //ui->tableWidget_2->verticalHeader()->setStretchLastSection(QHeaderView::Stretch);

    //display VO Tree
    ui->treeWidget->setColumnCount(1);
    ui->treeWidget->setHeaderLabel(tr("VO Structure"));
    ui->treeWidget->header()->setDefaultAlignment(Qt::AlignCenter);
/*
    QTreeWidgetItem *imageItem2 = new QTreeWidgetItem(ui->treeWidget,QStringList(QString("Root Node")));
    QTreeWidgetItem *imageItem2_3 = new QTreeWidgetItem(imageItem2,QStringList(QString("Grid Signature: cE92728a6713450F295F7b927664dEaa675ab5da")));
    QTreeWidgetItem *imageItem2_1 = new QTreeWidgetItem(imageItem2,QStringList(QString("Child Nodes")));

    imageItem2_3->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));

    imageItem2->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2_1->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    QTreeWidgetItem *imageItem2_2 = new QTreeWidgetItem(imageItem2_1,QStringList(QString("Grid Signature: c0a4bbc2c55816dF18cE5619abcdF5d8")));
    imageItem2_2->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));
    imageItem2->addChild(imageItem2_1);
    imageItem2->addChild(imageItem2_3);
    imageItem2_1->addChild(imageItem2_2);
    imageItem2_1->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));

    QTreeWidgetItem *imageItem2_4 = new QTreeWidgetItem(imageItem2,QStringList(QString("Child Nodes")));
    imageItem2_4->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2->addChild(imageItem2_4);

    //QTreeWidgetItem *imageItem3_1 = new QTreeWidgetItem(imageItem2_4,QStringList(QString("Grid Signature: 27316ba3dc0a4bbc2c55816d99bb5E849cb0a456")));
    //imageItem3_1->setIcon(0,QIcon("/home/yang/Pictures/demo/text.svg"));
    //imageItem2_4->addChild(imageItem3_1);

    QTreeWidgetItem *imageItem3_2 = new QTreeWidgetItem(imageItem2_4,QStringList(QString("Child Nodes")));
    imageItem3_2->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2_4->addChild(imageItem3_2);

    QTreeWidgetItem *imageItem4_2 = new QTreeWidgetItem(imageItem3_2,QStringList(QString("Record Signature: E451d2032085bF18cE5619ab946bcdF5d8b3dE20")));
    imageItem4_2->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));
    imageItem4_2->addChild(imageItem3_2);

    QTreeWidgetItem *imageItem3_3 = new QTreeWidgetItem(imageItem2_4,QStringList(QString("Child Nodes")));
    imageItem3_3->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2_4->addChild(imageItem3_3);

    QTreeWidgetItem *imageItem4_1 = new QTreeWidgetItem(imageItem3_3,QStringList(QString("Record Signature: F6d2a86544626EF4aE2284b43EE4615F974E97F7")));
    imageItem4_1->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));
    imageItem3_3->addChild(imageItem4_1);
*/
    ui->treeWidget->expandAll();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
    timer->start(1000);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::timeout()
{
    update();
}

void Dialog::paintEvent(QPaintEvent *)
{
   // update verification time
   string tm;
   ifstream ifsa("voshow.txt");
   boost::archive::text_iarchive ia(ifsa);  
   ia >> tm; 
    
   QString temp1;
   tm += "ms";
   temp1 = QString::fromStdString(tm);
   ui->textBrowser->setText(temp1);
   ui->textBrowser->setAlignment(Qt::AlignCenter|Qt::AlignBottom|Qt::AlignHCenter);

   //update vo proof
    string str_flag;
{
    ifstream fin1("vo_bit.txt");
    fin1 >> str_flag;
}
    //determine hash 
std::ifstream t("results7temp.txt");
std::string tempin((std::istreambuf_iterator<char>(t)),
                 std::istreambuf_iterator<char>());

   size_t str_hash1 = hash<string>{}("asbdb"+tempin);
   string roothash1= to_string(str_hash1);


   string roothash = encode_hex(tempin,0);
   QString qstr = QString::fromStdString(roothash);

    string num_flag;
{
    ifstream fin3("vo_number.txt");
    fin3 >> num_flag;
}   

   //predict whether this could be update
   if(str_flag == "Yes"){
    ui->treeWidget->clear();

    QTreeWidgetItem *imageItem2 = new QTreeWidgetItem(ui->treeWidget,QStringList(QString("Root Node")));
    imageItem2->setExpanded(true);
    //QTreeWidgetItem *imageItem2_3 = new QTreeWidgetItem(imageItem2,QStringList(QString("Grid Signature: cE92728a6713450F295F7b927664dEaa675ab5da")));
    QTreeWidgetItem *imageItem2_3 = new QTreeWidgetItem(imageItem2,QStringList(QString("Grid Signature: ") + qstr));
    QTreeWidgetItem *imageItem2_1 = new QTreeWidgetItem(imageItem2,QStringList(QString("Child Nodes")));

    imageItem2_3->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));

    imageItem2->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2_1->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    QTreeWidgetItem *imageItem2_2 = new QTreeWidgetItem(imageItem2_1,QStringList(QString("Grid Signature: ") + QString::fromStdString(encode_hex("cE92728a6713450F295F7b927664dEaa675ab5da"+tempin,0))));
    imageItem2_2->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));
    imageItem2->addChild(imageItem2_1);
    imageItem2->addChild(imageItem2_3);
    imageItem2_1->addChild(imageItem2_2);
    imageItem2_1->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));

    // add new folder and text
    QTreeWidgetItem *imageItem2_6 = new QTreeWidgetItem(imageItem2,QStringList(QString("Child Nodes")));
    QTreeWidgetItem *imageItem2_5 = new QTreeWidgetItem(imageItem2_6,QStringList(QString("Grid Signature: ")+ QString::fromStdString(encode_hex("c0a4bbc2c55816dF18cE5619abcdF5d8"+tempin,0))));
    imageItem2_5->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));
    imageItem2_6->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2->addChild(imageItem2_6);
    imageItem2_6->addChild(imageItem2_5);
    imageItem2->setExpanded(true);
    imageItem2_1->setExpanded(true);

    QTreeWidgetItem *imageItem2_4 = new QTreeWidgetItem(imageItem2,QStringList(QString("Child Nodes")));
    imageItem2_4->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2->addChild(imageItem2_4);
    imageItem2_4->setExpanded(true);

    //QTreeWidgetItem *imageItem3_1 = new QTreeWidgetItem(imageItem2_4,QStringList(QString("Grid Signature: 27316ba3dc0a4bbc2c55816d99bb5E849cb0a456")));
    //imageItem3_1->setIcon(0,QIcon("/home/yang/Pictures/demo/text.svg"));
    //imageItem2_4->addChild(imageItem3_1);

    QTreeWidgetItem *imageItem3_2 = new QTreeWidgetItem(imageItem2_4,QStringList(QString("Child Nodes")));
    imageItem3_2->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2_4->addChild(imageItem3_2);
    imageItem3_2->setExpanded(true);

    QTreeWidgetItem *imageItem4_2 = new QTreeWidgetItem(imageItem3_2,QStringList(QString("Record Signature: ")+ QString::fromStdString(encode_hex("E451d2032085bF18cE5619ab946bcdF5d8b3dE20"+tempin,0))));
    imageItem4_2->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));
    imageItem4_2->addChild(imageItem3_2);

    QTreeWidgetItem *imageItem3_3 = new QTreeWidgetItem(imageItem2_4,QStringList(QString("Child Nodes")));
    imageItem3_3->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2_4->addChild(imageItem3_3);

    QTreeWidgetItem *imageItem4_1 = new QTreeWidgetItem(imageItem3_3,QStringList(QString("Record Signature: ") + QString::fromStdString(encode_hex("F6d2a86544626EF4aE2284b43EE4615F974E97F7"+tempin,0))));
    imageItem4_1->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));
    imageItem3_3->addChild(imageItem4_1);
      
    QTreeWidgetItem *imageItem3_4 = new QTreeWidgetItem(imageItem2_4,QStringList(QString("Child Nodes")));
    imageItem3_4->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/folder.svg"));
    imageItem2_4->addChild(imageItem3_4);


    int kkk = stoi(num_flag);

    while (kkk >= 3){
        string data = "";
        for (int m = 0; m <= 20; m++){
            data += to_string(rand()%100); 
        }
        QTreeWidgetItem *itemx = new QTreeWidgetItem(imageItem3_4,QStringList(QString("Record Signature: ") + QString::fromStdString(encode_hex(data,0))));
        itemx->setIcon(0,QIcon("/Users/scaryang/Desktop/Interface/build-demo-Desktop-Debug/text.svg"));
        imageItem3_4->addChild(itemx);
        //num_flag = num_flag - 1;
        kkk--;
    }
  
 {     //recover original value
      ofstream outfile("vo_bit.txt", ofstream::out);
      outfile << "No";
      outfile.close();
}
    }
}

//add Tree View
void Dialog::AddRoot(QString name, QString Description)
{
    QTreeWidgetItem *itm = new QTreeWidgetItem(ui->treeWidget);
    itm->setText(0,name);
    itm->setText(1,Description);
    ui->treeWidget->addTopLevelItem(itm);
}

void Dialog::AddChild(QTreeWidgetItem *parent, QString name, QString Description)
{
    QTreeWidgetItem *itm = new QTreeWidgetItem();
    itm->setText(0,name);
    itm->setText(1,Description);
    parent->addChild(itm);
}

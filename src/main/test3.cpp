#include "../db_dup/db.hpp"
#include "./pbc_helper.h"
#include "../vendor/include/soci/soci.h"
//#include "sqlite3/soci-sqlite3.h"
#include "../vendor/include/soci/mysql/soci-mysql.h"

#include <iostream>
#include <vector>

#include <set>
#include <string>
#include <exception>


using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;
using namespace soci;

struct datarecord{
     /*
     datarecord()
     {
     	global_roles = set<int>{0,1,2,3,4,5,6,7,8,9};
     	roles = set<int>{1,2};
     	pseudo_role = 0;
     	range = Rectangle({2008,1},{2016,113});
     }
     ~datarecord(){}
     */
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};

int main()
{
	//serilization
	datarecord cl;
	for (int i = 0; i < 125; i++)
    {
    	cl.global_roles.insert(i);
    }
    cl.roles = set<int>{114};
    cl.pseudo_role = 0;
    cl.range = Rectangle({2008,1},{2016,113});
   
	typedef Type<string,int> DB_Type;
	//DupDatabase<DB_Type> db("db_fundAudit.db");
    DupDatabase<DB_Type> db("test.db");

    int total_num = 8628;

    std::vector<string> value;
    //value.resize(total_num);
    std::vector<DB::Point> point;
    //point.resize(total_num);
    std::vector<string> policy;
    //policy.resize(total_num);

    cout << value.size() << endl 
         << point.size() << endl
         << policy.size() << endl;
    /*
    std::vector<string> value = {"IN_RESEARCH","IN_RESEARCH2","M755","2010-10-10 00:00:00",                                 
                                "IN_RESEARCH5","-80.2","1、love斯嘉丽约翰逊。","IN_RESEARCH8",
                                  "IN_RESEARCH9"};
    std::vector<DB::Point> point = {{0,2},{0,2},{1,2},{1,2},{1,2},{2,2},{2,2},{3,2},{1,1}};
    std::vector<string> policy = {string("1"),string("1"),string("1 OR 2"),string("1 OR 2")
                                      ,string("3"),string("1 AND 2"),string("1 OR 3"),string("3")
                                      ,string("2")};
    */
    std::unordered_multimap<DB::Point, DB::RawDataRecord<DB_Type>> data;

    cout << "------test soci-------" << endl;
    //connect mysql database
    soci::session sql(mysql, "db=newfund user=root password='123456' charset=utf8");
    //query attribute -- point
    int query_id = 1666 ;
    int query_schoolid = 1 ;
    //point data
    
    std::vector<int> fundYear;
    std::vector<int> schoolid;
    

    //query data  
    std::vector<string> projectsCnt;
    std::vector<string> arrangeFundSum;
    std::vector<string> carryOverSum;
    std::vector<string> surplusSum;
    std::vector<string> outlay1Sum;
    std::vector<string> outlay2Sum;
    std::vector<string> outlaySum;
    std::vector<string> carryForwardSum;
    std::vector<string> balanceSum;
    std::vector<string> implementDesc;
    std::vector<string> balanceDesc;
    std::vector<string> status;

    int record_num = 719;

    //point data
    fundYear.resize(record_num);
    schoolid.resize(record_num);
    //query data
    projectsCnt.resize(record_num);
    arrangeFundSum.resize(record_num);
    carryOverSum.resize(record_num);
    surplusSum.resize(record_num);
    outlay1Sum.resize(record_num);
    outlay2Sum.resize(record_num);
    outlaySum.resize(record_num);
    carryForwardSum.resize(record_num);
    balanceSum.resize(record_num);
    implementDesc.resize(record_num);
    balanceDesc.resize(record_num);
    status.resize(record_num);

    sql << "select fundYear,projectsCnt,arrangeFundSum,carryOverSum,"
           "surplusSum,outlay1Sum,"
           "outlay2Sum,outlaySum,carryForwardSum,balanceSum," 
           "implementDesc,balanceDesc,status,schoolid from fundaudit",
            into(fundYear), into(projectsCnt), into(arrangeFundSum),
            into(carryOverSum), into(surplusSum), into(outlay1Sum),
            into(outlay2Sum), into(outlaySum), into(carryForwardSum),
            into(balanceSum), into(implementDesc), into(balanceDesc),
            into(status),into(schoolid);

    int idx = 0;
    char str[60];

    for (int i = 0; i < record_num; i++)
    {
    	 for (int j = 0; j < 12 ;j ++ )
    	 {
    	 	point.push_back({fundYear.at(idx),schoolid.at(idx)});
         }
         //Here should be opposed 
         value.push_back(projectsCnt.at(idx));
         value.push_back(arrangeFundSum.at(idx));
         value.push_back(carryOverSum.at(idx));
         value.push_back(surplusSum.at(idx));
         value.push_back(outlay1Sum.at(idx));
         value.push_back(outlay2Sum.at(idx));
         value.push_back(outlaySum.at(idx));
         value.push_back(carryForwardSum.at(idx));
         value.push_back(balanceSum.at(idx));
         value.push_back(implementDesc.at(idx));
         value.push_back(balanceDesc.at(idx));
         value.push_back(status.at(idx));

         int a = schoolid.at(idx);    
         sprintf(str,"%s%d%s","114 OR ((117 OR 118) AND (121 OR 122 OR 123 OR 124)) OR ((120 OR 121 OR 122 OR 123 OR 124) AND ",a,")");
         std::string policy_s = str;

         for (int k = 0; k < 12 ;k ++ )
    	 {
    	 	policy.push_back(policy_s);
         }
         idx++;
    }

    //check equal
    cout << "-------line-------" << endl;
    int value_size = value.size();
    int point_size = point.size();
    int policy_size = policy.size();
    cout << value_size << endl
         << point_size << endl
         << policy_size << endl;
    if ((value_size != point_size)||(point_size != policy_size))
    {
    	cout << "There is one problem in the database" << endl;
    	return 0;
    }

    int countnum = 0;
	//aggregate data
	cout << "---------------" << endl;
	for (int i = 0; i < total_num; i++)
	{ 
		//cout << i << endl;
		data.insert(std::make_pair(point.at(i),DB::RawDataRecord<DB_Type>(value.at(i),policy.at(i))));
        cout << countnum << endl;
        countnum++;
	}
    /*
	unordered_multimap<Point, RawDataRecord<DB_Type>> data{
		{{0,2}, RawDataRecord<DB_Type>("IN_RESEARCH",string("1"))},
		{{0,2}, RawDataRecord<DB_Type>("IN_RESEARCH2",string("1"))},
		{{1,2}, RawDataRecord<DB_Type>("M755",string("1 OR 2"))},
		{{1,2}, RawDataRecord<DB_Type>("2010-10-10 00:00:00",string("1 OR 2"))},
		{{1,2}, RawDataRecord<DB_Type>("IN_RESEARCH5",string("3"))},
		{{2,2}, RawDataRecord<DB_Type>("-80.2",string("1 AND 2"))},
		{{2,2}, RawDataRecord<DB_Type>("1、love斯嘉丽约翰逊。",string("1 OR 3"))},
		{{3,2}, RawDataRecord<DB_Type>("IN_RESEARCH8",string("3"))},
		{{1,1}, RawDataRecord<DB_Type>("IN_RESEARCH9",string("2"))},
	};
	*/

	db.create_meta(TYPE_A_PARAM, cl.pseudo_role, cl.global_roles, 2, cl.range);
	db.create_index(data, IndexType::grid);
	db.save(true);
	db.reset_sig();
	db.create_data_sig();
	db.create_grid_sig();
	auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(cl.roles);
	
	/*
	//test equality query;
	cout << "output r1" << endl;
	auto r1 = db.equality_query({1,2},cl.roles);
	cout << r1.accessible << endl;
	cout << r1.verify(mvk,{1,2}, inaccessible_policy) << endl;
	cout << r1.get_vo_size() << endl;
	cout << "---------------" << endl;

	cout << "output r2" << endl;
	auto r2 = db.equality_query({2,2},cl.roles);
	cout << r2.accessible << endl;
	cout << r2.verify(mvk,{2,2}, inaccessible_policy) << endl;
	cout << r2.get_vo_size() << endl;
	cout << "---------------" << endl;
    */
    /*
	//test range query
	cout << "test range query result" << endl;
	auto r3 = db.range_query(Rectangle({1, 0}, {3, 2}), cl.roles);
	cout << r3.accessible_data.size() << endl;
	cout << r3.inaccessible_data.size() << endl;
	cout << r3.inaccessible_grid.size() << endl;
	bool r3_result = r3.verify(mvk, Rectangle({1, 0}, {3, 2}), inaccessible_policy);
	cout << r3_result << endl;
	cout << r3.get_vo_size() << endl;
	*/
	cout << "test range query result" << endl;
	auto r3 = db.range_query(Rectangle({2008, 1}, {2008, 1}), cl.roles);
	cout << r3.accessible_data.size() << endl;
	//cout << r3.inaccessible_data.size() << endl;
	//cout << r3.inaccessible_grid.size() << endl;
	auto iter = r3.accessible_data.begin();
	for (; iter != r3.accessible_data.end(); iter++){
		RawDataRecord<DB_Type> temp(iter->second.first);
		cout << temp.value << endl;
	}

	bool r3_result = r3.verify(mvk, Rectangle({2008, 1}, {2008, 1}), inaccessible_policy);
	cout << r3_result << endl;
	//cout << r3.get_vo_size() << endl;

	return 0;
}

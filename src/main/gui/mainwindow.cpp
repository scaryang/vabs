#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "logindialog.h"

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include "../../utils/timer.hpp"
#include "../../utils/pbc_helper.h"
#include "../../utils/serialization_helper.h"

#include <nlohmann/json.hpp>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>
#include "../../db_dup/db.hpp"
#include "../../db/db.hpp"
//#include "./pbc_helper.h"
#include "../../abs/abs.hpp"
#include "../../abs/boolean_expression.hpp"
#include "../../abs/boolean_expression_dnf.hpp"
#include "../../abs/pbc_serialization.hpp"


#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_free.hpp>
#include <pbc/pbc.h>

#include "../../db/sig.hpp"
#include "../../db/data.hpp"
#include "../../db/type.hpp"
#include "../../db/rect.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <omp.h>
#include <boost/algorithm/string.hpp>


#include <set>
#include <string>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>

//cryptopp
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;
#include "cryptopp/cryptlib.h"
using CryptoPP::Exception;

#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "cryptopp/filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include "cryptopp/aes.h"
using CryptoPP::AES;

#include "cryptopp/ccm.h"
using CryptoPP::CTR_Mode;

#include "assert.h"

using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;
using json = nlohmann::json;
using namespace oabe;
using namespace oabe::crypto;
using namespace boost::algorithm;


struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};

typedef Type<string,int> DB_Type;

ABS::BooleanExpression<int> inac_policy{};
ABS::MasterVerifyingKey mvk;
//DB::DupRangeQueryResult<DB_Type> r3;
//DB::RangeQueryResult<DB_Type> r3;
DB::EqualityQueryResult<DB_Type> r4;
DB::RangeQueryResult<DB_Type> r5;
std::string querypid_str;
std::string queryaid_str;
std::string queryaid_str2;
pbc::PairingPtr pairing;  
int verify_flag; 
DB::Point point1;
DB::Point point2;


/// Callback must be declared static, otherwise it won't link...
size_t WriteCallback(char* ptr, size_t size, size_t nmemb, void *f)
{
  FILE *file = (FILE *)f;
  return fwrite(ptr, size, nmemb, file);
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->resultBrowser->viewport()->setAutoFillBackground(false);
    ui->veBrowser->viewport()->setAutoFillBackground(false);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);

    ui->tableWidget_2->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);
    //ui->resultBrowser->setFrameStyle(QFrame::NoFrame);
    this->setWindowOpacity(0.98);
}



MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionAccount_Info_triggered()
{
    QString strresult = tr("account-infomation-test");
    QMessageBox::information(this,tr("test"),strresult);
}

void MainWindow::on_actionDatabase_Info_triggered()
{
    QString strresult = tr("database-infomation-test");
    QMessageBox::information(this,tr("test"),strresult);
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->pidlineEdit->clear();
    ui->aidlineEdit->clear();
    ui->resultBrowser->clear();
    ui->veBrowser->clear();
    ui->pidlineEdit->setFocus();
}

void MainWindow::on_querybtn_clicked()
{
    if (ui->pidlineEdit->text().isEmpty()||ui->aidlineEdit->text().isEmpty())
    {
        QMessageBox::warning(this,tr("warning"),tr("Query information can not be empty!"));
        return;
    }
    querypid = ui->pidlineEdit->text();
    queryaid = ui->aidlineEdit->text();
    // set a value
    //queryaid2 = ui->aidlineEdit->text();

    querypid_str = querypid.toUtf8().constData();
    queryaid_str = queryaid.toUtf8().constData();
    queryaid_str2 = queryaid.toUtf8().constData();

 
    char url[] = "127.0.0.1:18080/upload";
    char filename[] = "results.txt";

    json query;
    query["pid"] = "80AC01B2-BD55-4BE0-A59A-4024104CF4E9";
    //query["pid"] = querypid_str;
    query["aid"] = queryaid_str;
    query["aaid"] = queryaid_str2;

    query["type"] = "single";

    verify_flag = 0;

    std::string s = query.dump();
    cout << s << endl;
    std::istringstream myStream(s);
    int size = myStream.str().size();

    //set options of file
    char buf[100];
    //set options of client
    curlpp::Cleanup cleaner;
    curlpp::Easy request;

      
    std::list< std::string > headers;
    headers.push_back("Content-Type: text/*"); 
    sprintf(buf, "Content-Length: %d", size); 
    headers.push_back(buf);
      
    using namespace curlpp::Options;
    request.setOpt(new curlpp::options::CustomRequest{"POST"});
    curlpp::options::WriteFunctionCurlFunction
    myFunction(WriteCallback);

    FILE *file = stdout;
    if(filename != NULL)
    {
        file = fopen(filename, "wb");
        if(file == NULL)
        {
           fprintf(stderr, "%s/n", strerror(errno));
           return;
        }
    } 
    curlpp::OptionTrait<void *, CURLOPT_WRITEDATA> 
              myData(file);

    request.setOpt(myFunction);
    request.setOpt(myData);

    request.setOpt(new Verbose(true));
    request.setOpt(new ReadStream(&myStream));
    request.setOpt(new InfileSize(size));
    request.setOpt(new Upload(true));
    request.setOpt(new HttpHeader(headers));
    request.setOpt(new Url(url));
    request.perform();
          
    //remember to close file
    fclose(file);
    file = NULL;

    //time response
    Timer response_tm;
    response_tm.start();

    GlobalPBCPairing::set(TYPE_A_PARAM);
    ifstream ifs("serial.txt");
    {
        boost::archive::text_iarchive ipp(ifs);
        ipp & mvk;
    }

    // 1. Decode iv: 
    // At the moment our input is encoded in string format...
    // we need it in raw hex: 
    ifstream inx("iv.txt");
    string iv_string((istreambuf_iterator<char>(inx)),istreambuf_iterator<char>());    

    byte iv[CryptoPP::AES::BLOCKSIZE] = {};
    // this decoder would transform our std::string into raw hex:
    CryptoPP::HexDecoder decoder1;
    decoder1.Put((byte*)iv_string.data(), iv_string.size());
    decoder1.MessageEnd();
    decoder1.Get(iv, sizeof(iv));



    InitializeOpenABE();
    OpenABECryptoContext cpabe("CP-ABE");
    string pt2;
    
    //cpabe.generateParams();
    ifstream finx("msk.txt");
    string msk((istreambuf_iterator<char>(finx)),istreambuf_iterator<char>());
    cpabe.importSecretParams(msk);

    ifstream finc("mpk.txt");
    string mpk((istreambuf_iterator<char>(finc)),istreambuf_iterator<char>());
    cpabe.importPublicParams(mpk);
    cpabe.keygen("|attr1|attr2","key0");



    // 3. Decode the key:
    // And finally the same for the key:
    string key_string;
    ifstream inf("key.txt");
    string key_string2((istreambuf_iterator<char>(inf)),istreambuf_iterator<char>());  
    bool result123 = cpabe.decrypt("key0", key_string2, key_string);
    cout << "cp-abe result is: "<< result123 << endl;
    ShutdownOpenABE();


    byte key[CryptoPP::AES::DEFAULT_KEYLENGTH];
    {
        CryptoPP::HexDecoder decoder2;
        decoder2.Put((byte*)key_string.data(), key_string.size());
        decoder2.MessageEnd();
        decoder2.Get(key, sizeof(key));
    }    

    string tempin;
{
    ifstream fin("results.txt");
    //string tempin((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>()); 
    fin >> tempin;
}
    // 2. Decode cipher:
    // Next, we do a similar trick for cipher, only here we would leave raw hex
    //  in a std::string.data(), since it is convenient for us to pass this
    // std::string to the decryptor mechanism:
    std::string cipher_raw;
    {
        CryptoPP::HexDecoder decoder3;
        decoder3.Put((byte*)tempin.data(), tempin.size());
        decoder3.MessageEnd();

        long long size1 = decoder3.MaxRetrievable();
        cipher_raw.resize(size1);       
        decoder3.Get((byte*)cipher_raw.data(), cipher_raw.size());
        // If we print this string it's completely rubbish: 
        // std::cout << "Raw cipher: " << cipher_raw << std::endl;
    }

    // 4. Decrypt:
    std::string decrypted_text;
    
    CryptoPP::CTR_Mode<CryptoPP::AES>::Decryption d;
    d.SetKeyWithIV(key, sizeof(key), iv);

    CryptoPP::StringSource ss(
            cipher_raw, 
            true, 
            new CryptoPP::StreamTransformationFilter(
            d,
            new CryptoPP::StringSink(decrypted_text)
                ) // StreamTransformationFilter
    ); // StringSource

    cout << endl << "AES Decryption successfully!" << endl;


{
    ofstream fout("results7temp.txt");
    fout << decrypted_text;
}
    //string querydata;
    //int response_tm;

{
    ifstream ifsa("results7temp.txt");
    boost::archive::text_iarchive ia(ifsa);

    ia >> r4;
    ia >> inac_policy;
    ia >> point1;
    ia >> point2;
    //ia >> response_tm;
}

    response_tm.stop();
    cout << "response_tm is:" << response_tm.result() << endl;
    string response = to_string(response_tm.result());
    QString response_tm2 = QString::fromStdString(response);
    
    //display results
    //pretty print~

    //cout << "sig is " << r4.sig.output_message() << endl;
    //string result1 = r4.record.value;
    string result1 = "";
    int circle_x = 0;
    int rowindex = 0;

    vector<string> show;
    split(show, result1, is_any_of("\\"),token_compress_on);

    for (unsigned int r = 0; r < show.size(); ++r){
        
        if(r < 10)
        {
            QString aaa = QString::fromStdString(show[r]);
            ui->tableWidget->setItem(0,r,new QTableWidgetItem(aaa));           
        }
        else
        {

            QString bbb = QString::fromStdString(show[r]);
            if (circle_x%4 == 0)
            {
                rowindex++;
                ui->tableWidget_2->setRowCount(rowindex);
            }
            ui->tableWidget_2->setItem(rowindex-1,circle_x%4, new QTableWidgetItem(bbb));
            circle_x++;
        }
    }     

/*

    string result2;

    int circle = 1;

    }
    QString strMsg = QString::fromStdString(result2);
*/
    //QString strMsg2 = QString::fromStdString(result2);
    int show_s1 = 5;
    QString col("black");
    QString strMsg("INFORMATION OF TASK");
    //QString show1 = QString("<p ><font size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s1).arg(col).arg(strMsg);
    QString show1 = QString("<h1><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></h1>").arg(show_s1).arg(col).arg(strMsg);

    int show_s2 = 3;

    QString col2("black");
    QString strMsg2(" Query Range From Client:");
    QString show2 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s2).arg(col2).arg(strMsg2);

    int show_s3 = 3;

    QString strMsg3(" Patient ID: ");
    QString show3 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><u>%3</u></font></p>").arg(show_s3).arg(col2).arg(strMsg3);

    QString strMsg4(querypid);
    QString show4 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s3).arg(col2).arg(strMsg4);

    QString strMsg5(" Admission ID: " );
    QString show5 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><u>%3</u></font></p>").arg(show_s3).arg(col2).arg(strMsg5);

 
    QString strMsg6(" Query Type: ");
    QString show6 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><u>%3</u></font></p>").arg(show_s3).arg(col2).arg(strMsg6);

    QString strMsg11(" Single Query");
    QString show11 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s3).arg(col2).arg(strMsg11);

    QString strMsg7,show7;
    
    if( r4.accessible == true)
    {
         strMsg7 = tr(" Access Successfully!" );
         show7 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><b>%3</b></font></p>").arg(show_s3).arg(col2).arg(strMsg7);
    }
    else{
         strMsg7 = tr(" Access Denied!" );
         show7 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><b>%3</b></font></p>").arg(show_s3).arg(col2).arg(strMsg7);        
    }

    QString strMsg8(" Server Response Time: ");
    QString show8 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><u>%3</u></font></p>").arg(show_s3).arg(col2).arg(strMsg8);

    QString strMsg9(response_tm2 + " ms");
    QString show9 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s3).arg(col2).arg(strMsg9);

    //QString strMsg10(" Please Put The Button \'Show Details\' to Acquire Query Results" );
   // QString show10 = QString("<p><font size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s3).arg(col2).arg(strMsg10);

    ui->resultBrowser->setText(show1);

    //ui->resultBrowser->append("\n");
    ui->resultBrowser->append(show2);
    ui->resultBrowser->append(show3);
    ui->resultBrowser->append(show4);

    ui->resultBrowser->append(show5 + queryaid);

    //ui->resultBrowser->append("");
   
    ui->resultBrowser->append(show6);
    ui->resultBrowser->append(show11);

    ui->resultBrowser->append(show7);

    ui->resultBrowser->append("");

    ui->resultBrowser->append(show8);
    ui->resultBrowser->append(show9);
    //ui->resultBrowser->append("");

    //ui->resultBrowser->append(show10);


}

void MainWindow::on_verifybtn_clicked()
{
    //GlobalPBCPairing::set(pairing);
    //omp_set_num_threads(4);
    //bool aab_result = r3.verify(mvk, Rectangle({stoi(querypid_str), stoi(queryaid_str)}, {stoi(querypid_str), stoi(queryaid_str)}), inac_policy);
    //bool aab_result = r3.verify(mvk, Rectangle({66, 2}, {66, 2}), inac_policy);
    // 80AC01B2-BD55-4BE0-A59A-4024104CF4E9
    Timer verify_tm;
    verify_tm.start();

    bool aab_result;
    if (verify_flag == 0)
        aab_result = r4.verify(mvk, point1, inac_policy);
    else
        aab_result = r5.verify(mvk, Rectangle(point1, point2), inac_policy);
 
    verify_tm.stop();
    int verification_tm = verify_tm.result();
    string verification_tm2 = to_string(verification_tm);
    QString verify_tm1 = QString::fromStdString(verification_tm2);

    if (aab_result == 1)
    {
        res_verification = tr("The Authentication Result is True!");
        if(res_verification.isEmpty())
               res_verification = tr("No Data Received!");
        QString show_true = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg("3").arg("black").arg(res_verification);
        QString verify_time("The Verificaiton Time is " + verify_tm1 + "ms");
        QString show_true2 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg("3").arg("black").arg(verify_time);
        
        ui->veBrowser->setText("\n");
        ui->veBrowser->append(show_true);
        ui->veBrowser->append("");        
        ui->veBrowser->append(show_true2);
    }
    else
    {
        res_verification = tr("The Authentication Result is False!");
        if(res_verification.isEmpty())
               res_verification = tr("No Data Received!");
        QString show_true = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg("3").arg("black").arg(res_verification);
        QString verify_time("The Verificaiton Time is " + verify_tm1 + "ms");
        QString show_true2 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg("3").arg("black").arg(verify_time);
        
        ui->veBrowser->setText("\n");
        ui->veBrowser->append(show_true);
        ui->veBrowser->append("");        
        ui->veBrowser->append(show_true2);
    }

}


void MainWindow::on_pushButton_3_clicked()
{
    ui->pidlineEdit_2->clear();
    ui->aidlineEdit_2->clear();
    ui->aidlineEdit_3->clear();
    ui->resultBrowser->clear();
    ui->veBrowser->clear();
    ui->pidlineEdit_2->setFocus();
}

void MainWindow::on_querybtn_2_clicked()
{
    if (ui->pidlineEdit_2->text().isEmpty()||ui->aidlineEdit_2->text().isEmpty()||ui->aidlineEdit_3->text().isEmpty())
    {
        QMessageBox::warning(this,tr("warning"),tr("Query information can not be empty!"));
        return;
    }
    querypid = ui->pidlineEdit_2->text();
    queryaid = ui->aidlineEdit_2->text();
    queryaid2 = ui->aidlineEdit_3->text();

    querypid_str = querypid.toUtf8().constData();
    queryaid_str = queryaid.toUtf8().constData();
    queryaid_str2 = queryaid2.toUtf8().constData();
 
    char url[] = "127.0.0.1:18080/upload";
    char filename[] = "results.txt";

    json query;
    query["pid"] = querypid_str;
    query["aid"] = queryaid_str;
    query["aaid"] = queryaid_str2;

    query["type"] = "multiple";

    verify_flag = 1;

    std::string s = query.dump();
    cout << s << endl;
    std::istringstream myStream(s);
    int size = myStream.str().size();

    //set options of file
    char buf[100];
    //set options of client
    curlpp::Cleanup cleaner;
    curlpp::Easy request;

      
    std::list< std::string > headers;
    headers.push_back("Content-Type: text/*"); 
    sprintf(buf, "Content-Length: %d", size); 
    headers.push_back(buf);
      
    using namespace curlpp::Options;
    request.setOpt(new curlpp::options::CustomRequest{"POST"});
    curlpp::options::WriteFunctionCurlFunction
    myFunction(WriteCallback);

    FILE *file = stdout;
    if(filename != NULL)
    {
        file = fopen(filename, "wb");
        if(file == NULL)
        {
           fprintf(stderr, "%s/n", strerror(errno));
           return;
        }
    } 
    curlpp::OptionTrait<void *, CURLOPT_WRITEDATA> 
              myData(file);

    request.setOpt(myFunction);
    request.setOpt(myData);

    request.setOpt(new Verbose(true));
    request.setOpt(new ReadStream(&myStream));
    request.setOpt(new InfileSize(size));
    request.setOpt(new Upload(true));
    request.setOpt(new HttpHeader(headers));
    request.setOpt(new Url(url));
    request.perform();
          
    //remember to close file
    fclose(file);
    file = NULL;

    Timer response_tm2;
    response_tm2.start();


    GlobalPBCPairing::set(TYPE_A_PARAM);
    ifstream ifs("serial.txt");
    {
        boost::archive::text_iarchive ipp(ifs);
        ipp & mvk;
    }


    // 1. Decode iv: 
    // At the moment our input is encoded in string format...
    // we need it in raw hex: 
    ifstream inx("iv.txt");
    string iv_string((istreambuf_iterator<char>(inx)),istreambuf_iterator<char>());    

    byte iv[CryptoPP::AES::BLOCKSIZE] = {};
    // this decoder would transform our std::string into raw hex:
    CryptoPP::HexDecoder decoder1;
    decoder1.Put((byte*)iv_string.data(), iv_string.size());
    decoder1.MessageEnd();
    decoder1.Get(iv, sizeof(iv));

    InitializeOpenABE();
    OpenABECryptoContext cpabe("CP-ABE");
    string pt2;
    
    //cpabe.generateParams();
    ifstream finx("msk.txt");
    string msk((istreambuf_iterator<char>(finx)),istreambuf_iterator<char>());
    cpabe.importSecretParams(msk);

    ifstream finc("mpk.txt");
    string mpk((istreambuf_iterator<char>(finc)),istreambuf_iterator<char>());
    cpabe.importPublicParams(mpk);
    cpabe.keygen("|attr1|attr2","key0");


    // 3. Decode the key:
    // And finally the same for the key:
    string key_string;
    ifstream inf("key.txt");
    string key_string2((istreambuf_iterator<char>(inf)),istreambuf_iterator<char>());  
    bool result123 = cpabe.decrypt("key0", key_string2, key_string);
    cout << "cp-abe result is: "<< result123 << endl;
    ShutdownOpenABE();


    byte key[CryptoPP::AES::DEFAULT_KEYLENGTH];
    {
        CryptoPP::HexDecoder decoder2;
        decoder2.Put((byte*)key_string.data(), key_string.size());
        decoder2.MessageEnd();
        decoder2.Get(key, sizeof(key));
    }    




    string tempin;
{
    ifstream fin("results.txt");
    //string tempin((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>()); 
    fin >> tempin;
}
    // 2. Decode cipher:
    // Next, we do a similar trick for cipher, only here we would leave raw hex
    //  in a std::string.data(), since it is convenient for us to pass this
    // std::string to the decryptor mechanism:
    std::string cipher_raw;
    {
        CryptoPP::HexDecoder decoder3;
        decoder3.Put((byte*)tempin.data(), tempin.size());
        decoder3.MessageEnd();

        long long size1 = decoder3.MaxRetrievable();
        cipher_raw.resize(size1);       
        decoder3.Get((byte*)cipher_raw.data(), cipher_raw.size());
    }

    // 4. Decrypt:
    std::string decrypted_text;
    
    CryptoPP::CTR_Mode<CryptoPP::AES>::Decryption d;
    d.SetKeyWithIV(key, sizeof(key), iv);

    CryptoPP::StringSource ss(
            cipher_raw, 
            true, 
            new CryptoPP::StreamTransformationFilter(
            d,
            new CryptoPP::StringSink(decrypted_text)
                ) // StreamTransformationFilter
    ); // StringSource

    cout << endl << "AES Decryption successfully!" << endl;

{
    ofstream fout("results7temp.txt");
    fout << decrypted_text;
}

    response_tm2.stop();
    string response = to_string(response_tm2.result());
    QString response_tm2x = QString::fromStdString(response);

    int gap;
{
    ifstream ifsa("results7temp.txt");
    boost::archive::text_iarchive ia(ifsa);
    //ia >> r3;
    ia >> r5;
    ia >> inac_policy;
    ia >> point1;
    ia >> point2;
    ia >> gap;
}

   

    int show_s1 = 5;
    QString col("black");
    QString strMsg("INFORMATION OF TASK");
    //QString show1 = QString("<p ><font size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s1).arg(col).arg(strMsg);
    QString show1 = QString("<h1><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></h1>").arg(show_s1).arg(col).arg(strMsg);

    int show_s2 = 3;

    QString col2("black");
    QString strMsg2(" Query Range From Client:");
    QString show2 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s2).arg(col2).arg(strMsg2);

    int show_s3 = 3;

    QString strMsg3(" Patient ID: ");
    QString show3 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><u>%3</u></font></p>").arg(show_s3).arg(col2).arg(strMsg3);

    QString strMsg4(querypid);
    QString show4 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s3).arg(col2).arg(strMsg4);

    QString strMsg5(" Admission ID: " );
    QString show5 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><u>%3</u></font></p>").arg(show_s3).arg(col2).arg(strMsg5);

 
    QString strMsg6(" Query Type: ");
    QString show6 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><u>%3</u></font></p>").arg(show_s3).arg(col2).arg(strMsg6);

    QString strMsg11(" Range Query");
    QString show11 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s3).arg(col2).arg(strMsg11);

    QString strMsg7,show7;
    int number = gap - r5.accessible_data.size();
    
    if(r5.accessible_data.size() == gap)
    {
        strMsg7 = tr(" Access Successfully!" );
        show7 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><b>%3</b></font></p>").arg(show_s3).arg(col2).arg(strMsg7);
    }
    else{
        string acm;
        for (auto iter = r5.inaccessible_data.begin(); iter != r5.inaccessible_data.end(); iter++)
           {
                Point temp(iter->first);
                acm += to_string(temp[1]);
                
                if ( number > 1)
                    acm += " , ";
                else
                    acm += " . ";
                number--;
            }
        QString acm2 = QString::fromStdString(acm);
        strMsg7 = tr("Access Denied For Admission ") + acm2;
        show7 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><b>%3</b></font></p>").arg(show_s3).arg(col2).arg(strMsg7);
    }



    QString strMsg8(" Server Response Time: ");
    QString show8 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\"><u>%3</u></font></p>").arg(show_s3).arg(col2).arg(strMsg8);

    QString strMsg9(response_tm2x + " ms");
    QString show9 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s3).arg(col2).arg(strMsg9);

    QString strMsg10("From " + queryaid + " To " + queryaid2);
    QString show10 = QString("<p><font face = \"serif\" size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s3).arg(col2).arg(strMsg10);

    //QString strMsg10(" Please Put The Button \'Show Details\' to Acquire Query Results" );
   // QString show10 = QString("<p><font size=\"%1\" color=\"%2\">%3</font></p>").arg(show_s3).arg(col2).arg(strMsg10);

    ui->resultBrowser->setText(show1);

    //ui->resultBrowser->append("\n");
    ui->resultBrowser->append(show2);
    ui->resultBrowser->append(show3);
    ui->resultBrowser->append(show4);

    ui->resultBrowser->append(show5);

    ui->resultBrowser->append(show10);
    //ui->resultBrowser->append("");
   
    ui->resultBrowser->append(show6);
    ui->resultBrowser->append(show11);

    ui->resultBrowser->append(show7);

    //ui->resultBrowser->append(show10);

    ui->resultBrowser->append(show8);
    ui->resultBrowser->append(show9);
    //ui->resultBrowser->append("");

    //ui->resultBrowser->append(show10);
    //display results
    //pretty print~
/*
    string result1;

    cout << "accessible_data size is" << r5.accessible_data.size() << endl;
    for (auto iter = r5.accessible_data.begin(); iter != r5.accessible_data.end(); iter++)
    {
                RawDataRecord<DB_Type> temp(iter->second.first);
                result1 += temp.value;
    }
    //result1 = r4.record.value;
*/

 
/*
    int result_num = 0, circle = 1;
    for (auto iter = r3.accessible_data.begin(); iter != r3.accessible_data.end(); iter++){
        RawDataRecord<DB_Type> temp(iter->second.first);
        if(result_num < 10)
        {
            result1 += temp.value;
            result1 += "\n";
        }
        else
        {
            result2 += temp.value;
            result2 += " ";
            if((circle/4)==0) result2 += "\n";
            circle++;   
        }
        result_num++;
    }
*/
    auto iter = r5.accessible_data.begin();
    RawDataRecord<DB_Type> tempx(iter->second.first);

    string result1 = tempx.value;
    int circle_x = 0;
    int rowindex = 0;

    vector<string> show;
    split(show, result1, is_any_of("\\"),token_compress_on);

    for (unsigned int r = 0; r < show.size(); ++r){
        
        if(r < 10)
        {
            QString aaa = QString::fromStdString(show[r]);
            ui->tableWidget->setItem(0,r,new QTableWidgetItem(aaa));           
        }
        else
        {

            QString bbb = QString::fromStdString(show[r]);
            if (circle_x%4 == 0)
            {
                rowindex++;
                ui->tableWidget_2->setRowCount(rowindex);
            }
            ui->tableWidget_2->setItem(rowindex-1,circle_x%4, new QTableWidgetItem(bbb));
            circle_x++;
        }
    }     
}

void MainWindow::on_querybtn_3_clicked()
{
    if (ui->pidlineEdit_3->text().isEmpty())
    {
        QMessageBox::warning(this,tr("warning"),tr("Admission ID can not be empty!"));
        return;
    }
    //LOAD query results
    int gap;
{
    ifstream ifsa("results7temp.txt");
    boost::archive::text_iarchive ia(ifsa);
    //ia >> r3;
    ia >> r5;
    ia >> inac_policy;
    ia >> point1;
    ia >> point2;
    ia >> gap;
}

    QString admission3 = ui->pidlineEdit_3->text();

    for (auto iter = r5.accessible_data.begin(); iter != r5.accessible_data.end(); iter++){    
       
        RawDataRecord<DB_Type> tempx(iter->second.first);

        Point pptw(iter->first);
        string pptu = to_string(pptw[1]);
        QString ppty = QString::fromStdString(pptu);
        cout << "input :" << pptu << endl;

        if (ppty == admission3){
        string result1 = tempx.value;
        int circle_x = 0;
        int rowindex = 0;

        vector<string> show;
        split(show, result1, is_any_of("\\"),token_compress_on);

        for (unsigned int r = 0; r < show.size(); ++r){
        
             if(r < 10)
               {
                  QString aaa = QString::fromStdString(show[r]);
                  ui->tableWidget->setItem(0,r,new QTableWidgetItem(aaa));           
               }
             else
               {

                  QString bbb = QString::fromStdString(show[r]);
                  if (circle_x%4 == 0)
                  {
                     rowindex++;
                     ui->tableWidget_2->setRowCount(rowindex);
                  }
                     ui->tableWidget_2->setItem(rowindex-1,circle_x%4, new QTableWidgetItem(bbb));
                     circle_x++;
                }
            } 
            }

        //if (){
        //    QMessageBox::warning(this,tr("warning"),tr("Admission ID is error!"));
        //    return; 
        //} 
    } 
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionAccount_Info_triggered();

    void on_actionDatabase_Info_triggered();

    void on_pushButton_2_clicked();

    void on_querybtn_clicked();

    void on_verifybtn_clicked();

    void on_pushButton_3_clicked();

    void on_querybtn_2_clicked();

    void on_querybtn_3_clicked();

private:
    Ui::MainWindow *ui;
    QString querypid;
    QString queryaid;
    QString queryaid2;
    QString res_verification;

};

#endif // MAINWINDOW_H

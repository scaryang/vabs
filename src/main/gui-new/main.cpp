#include "mainwindow.h"
#include <QApplication>
#include "logindialog.h"
#include <QDesktopWidget>
#include <QThread>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QThread::currentThread()->setPriority(QThread::HighPriority);
    MainWindow w;
    LoginDialog dlg;
    if(dlg.exec()==QDialog::Accepted)
    {
        w.show();
        w.move ((QApplication::desktop()->width() - w.width())/2,(QApplication::desktop()->height() - w.height())/2);
        return a.exec();
    }
    else return 0;
}

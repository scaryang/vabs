#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>

using namespace std;
using namespace oabe;
using namespace oabe::crypto;

int main(int argc, char **argv) {

  //initialize lib
  InitializeOpenABE();

  cout << "Testing CP-ABE context" << endl;

  OpenABECryptoContext cpabe("CP-ABE");

  string ct, pt1 = "hello world!", pt2, pt3;

  cpabe.generateParams();

  cpabe.keygen("|attr1|attr2", "key0");

  cpabe.encrypt("attr1 and attr2", pt1, ct);

  {
    ofstream fout("test.txt");
    fout << ct;
  }

  {
    ifstream fin("test.txt");
    fin >> pt2;
  }

  string mpk,msk;
  cpabe.exportPublicParams(mpk);
  cpabe.exportSecretParams(msk);

  {
    ofstream fout1("mpk.txt");
    fout1 << mpk;
  }
  {
    ofstream fout2("msk.txt");
    fout2 << msk;
  }

/*  {
    ifstream fin1("test1.txt");
    string test2((istreambuf_iterator<char>(fin1)),istreambuf_iterator<char>());
  cout << test2 <<endl;
  }  
*/



  bool result = cpabe.decrypt("key0", pt2, pt3);

  assert(result && pt1 == pt3);

  cout << "Recovered message: " << pt3 << endl;

  ShutdownOpenABE();

  return 0;
}

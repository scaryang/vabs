#include "customtabstyle.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "crow.h"
#include <sqlite_modern_cpp.h>

#include <iostream>
#include <nlohmann/json.hpp>

#include <sstream>

#include "../../db_dup/db.hpp"
#include "../../db/db.hpp"

#include "./pbc_helper.h"
#include <map>

#include <iostream>
//#include "sql_parser.h"
#include <fstream>
#include <vector>

#include <set>
#include <string>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/nvp.hpp>

#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_free.hpp>

#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>
#include <boost/algorithm/string.hpp>

#include "../../utils/timer.hpp"

//cryptopp
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;
#include "cryptopp/cryptlib.h"
using CryptoPP::Exception;

#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "cryptopp/filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include "cryptopp/aes.h"
using CryptoPP::AES;

#include "cryptopp/ccm.h"
using CryptoPP::CTR_Mode;

#include "assert.h"
#include <QThread>
#include <QTimer>
#include <stdlib.h>



using namespace std;
using namespace pbc;
//using namespace ABS;
using namespace DB;
using json = nlohmann::json;
using namespace oabe;
using namespace oabe::crypto;
using namespace boost::algorithm;
using namespace sqlite;

typedef unsigned char byte;

struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};


class ExampleLogHandler : public crow::ILogHandler {
    public:
        void log(std::string /*message*/, crow::LogLevel /*level*/) override {
//            cerr << "ExampleLogHandler -> " << message;
        }
};

struct ExampleMiddleware 
{
    std::string message;

    ExampleMiddleware() 
    {
        message = "foo";
    }

    void setMessage(std::string newMsg)
    {
        message = newMsg;
    }

    struct context
    {
    };

    void before_handle(crow::request& /*req*/, crow::response& /*res*/, context& /*ctx*/)
    {
        CROW_LOG_DEBUG << " - MESSAGE: " << message;
    }

    void after_handle(crow::request& /*req*/, crow::response& /*res*/, context& /*ctx*/)
    {
        // no-op
    }
};

    unordered_map<string,int> pid;
    datarecord teemo;
    typedef Type<string,int> DB_Type;
    int dbtype;
    Point focus;
    //Database<DB_Type> db("100patient.db");
    //Database<DB_Type> dkb("kdtree.db");

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->setFixedSize(582,627);
    ui->setupUi(this);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);
    ui->tableWidget->setRowCount(700);

    //ui->textBrowser->setText(tr("Crow/0.1 server is running at 0.0.0.0:18080 using 4 threads..."));
    //ui->textBrowser->append(tr("(1-th Query Information)  Patient ID: P0066, Check-Up No: (C0002, C0005) \nQuery Type: range query, Username:Doctor;"));

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
    timer->start(1000);

    for (int i =0; i< 3; i++){
        for (int j =0; j <5; j++){
            ui->tableWidget->item(i,j)->setBackgroundColor(QColor(0,200,0));
        }
    }
    for (int j =0; j <5; j++){
        ui->tableWidget->item(3,j)->setBackgroundColor(QColor(200,0,0));
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timeout()
{
    update();
}

void MainWindow::paintEvent(QPaintEvent *)
{
    ifstream fin;
    string str;
    QString qstr;
    fin.open("/Users/scaryang/Desktop/Interface/server/test.txt");
    ui->textBrowser->setText(tr("Test whether it can display dynamically"));
    while(!fin.eof())
    {
        getline(fin,str);
        qstr = QString::fromStdString(str);
        ui->textBrowser->append(qstr);
    }
    fin.close();

    //read bit.txt then verdict use function. Last, update bit.txt (Yes/No)
    string str1;
{
    ifstream fin1("/Users/scaryang/Desktop/Interface/server/bit.txt");
    fin1 >> str1;
}
    //test output
    //cout << str1 << endl;

    if( str1 == "Yes")
    {
        checkbit();
        //modify the bit
        ofstream outfile("/Users/scaryang/Desktop/Interface/server/bit.txt", ofstream::out);
        outfile << "No";
        outfile.close();
    }
}

class WorkerThread : public QThread
{
   protected:
         void run()
{
    crow::App<ExampleMiddleware> app;
    app.get_middleware<ExampleMiddleware>().setMessage("hello");

    //create global pid map
    //unordered_map<string,int> pid;
    //only patientid
    string line;
    ifstream infile0;
    int num = 1;
    infile0.open("PatientCorePopulatedTable.txt");
    //init query attr
    while( getline(infile0,line) )
    {
        vector<string> result;
        split(result, line, is_any_of("\\"),token_compress_on);
        pid.insert(make_pair( result.at(0), num));
        num++;
        vector<string> free;
        result.swap(free);
    }
    infile0.close();

    for (int i = 0; i < 11; i++)
    {
        teemo.global_roles.insert(i);
    }
    teemo.roles = set<int>{8,4,5};
    teemo.pseudo_role = 0;
    teemo.range = Rectangle({1,1},{100,7});

    //db.load(true);
    //dkb.load(true);

    
    //set ip_route
    CROW_ROUTE(app, "/upload")
        .methods("POST"_method)
    ([](const crow::request& req){
        auto x = crow::json::load(req.body);
        auto y = json::parse(req.body);
        cout << "-------receive successfully!-------" << endl;      
        if (!x)
            return crow::response(400);   
     
    //Timer response_tm;
    //response_tm.start();
    Database<DB_Type> db("100patient.db");
    Database<DB_Type> dkb("kdtree.db");
    db.load(true);
    dkb.load(true);
    
    auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(teemo.roles);

    //output mvk
    ofstream oofs("serial.txt");
    {
        boost::archive::text_oarchive opp(oofs);
        opp & mvk;
    }

    json xxx(y["pid"]);
    string trans_pid = xxx.dump();
    trans_pid.erase(trans_pid.begin(),trans_pid.begin()+1);
    trans_pid.erase(trans_pid.end()-1,trans_pid.end());

    json yyy(y["type"]);
    string query_type = yyy.dump();
    query_type.erase(query_type.begin(),query_type.begin()+1);
    query_type.erase(query_type.end()-1,query_type.end());
    cout << "query type is : " << query_type << endl;

    cout << endl << trans_pid << endl;;
    cout << endl << pid.at(trans_pid) << endl;

    int trans_aid = x["aid"].i();
    int trans_aid2 = x["aaid"].i();

    cout << endl << "-------------test range query result---------" << endl;
    if(!( pid.at(trans_pid) < 100 && pid.at(trans_pid) > 0)) 
    {
        cout << "error here~"  << endl;
        //exit(0);
    }
    
    ofstream queryinfo("queryinfo.txt");
    if (queryinfo.is_open())
    {
        queryinfo << trans_pid;
        queryinfo << "\n";
        queryinfo << to_string(trans_aid) + " To " + to_string(trans_aid2);
        queryinfo << "\n";
        queryinfo << query_type;
        queryinfo.close();

    } 
    //ui->pidBrowser->setText(QString::fromStdString(trans_pid));
    //string trans_aaid = trans_aid + " To " + trans_aid2;
    //ui->aidBrowser->setText(QString::fromStdString(trans_aaid));

    DB::Point point1({pid.at(trans_pid), trans_aid});
    DB::Point point2({pid.at(trans_pid), trans_aid2});

    std::ostringstream strstr;

    if (query_type == "Equality Query"){
        
        if (dbtype == 1){

            auto r3 = db.equality_query({pid.at(trans_pid), trans_aid}, teemo.roles);

                boost::archive::text_oarchive oa(strstr);
                oa << r3;
                oa << inaccessible_policy;
                oa << point1;
                oa << point2;
        }
      else{
            auto r3 = dkb.equality_query({pid.at(trans_pid), trans_aid}, teemo.roles);

                boost::archive::text_oarchive oa(strstr);
                oa << r3;
                oa << inaccessible_policy;
                oa << point1;
                oa << point2;
            
        }
        
    }
    else{   
        if (dbtype == 1)
            {
                auto r3 = db.range_query(Rectangle({pid.at(trans_pid), trans_aid}, {pid.at(trans_pid), trans_aid2}), teemo.roles);       
                
                    boost::archive::text_oarchive oa(strstr);
                    oa << r3;
                    oa << inaccessible_policy;
                    oa << point1;
                    oa << point2;
                    oa << (trans_aid2-trans_aid+1);
           }
        else
        {
            auto r3 = dkb.range_query(Rectangle({pid.at(trans_pid), trans_aid}, {pid.at(trans_pid), trans_aid2}), teemo.roles);       
            
                    boost::archive::text_oarchive oa(strstr);
                    oa << r3;
                    oa << inaccessible_policy;
                    oa << point1;
                    oa << point2;
                    oa << (trans_aid2-trans_aid+1);
                     
        }
        
    }
    //auto r3 = db.range_query(Rectangle({pid.at(trans_pid), trans_aid}, {pid.at(trans_pid), trans_aid}), teemo.roles);


    //add cp-abe
    InitializeOpenABE();
    OpenABECryptoContext cpabe("CP-ABE");
    string ct;

    ifstream fin("mpk.txt");
    string mpk((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>());
    cpabe.importPublicParams(mpk);
    
    //aes ctr encrypt
    AutoSeededRandomPool prng;

    byte key[AES::DEFAULT_KEYLENGTH];
    prng.GenerateBlock(key, sizeof(key));

    byte iv[AES::BLOCKSIZE];
    prng.GenerateBlock(iv, sizeof(iv));

    string cipher, encoded, encoded1, encoded2, recovered;

    // string key
    //encoded.clear();
    StringSource(key, sizeof(key), true,
        new HexEncoder(
            new StringSink(encoded1)
        ) // HexEncoder
    ); // StringSource
    //cout << "key: " << encoded1 << endl;

    // string iv
    //encoded.clear();
    StringSource(iv, sizeof(iv), true,
        new HexEncoder(
            new StringSink(encoded2)
        ) // HexEncoder
    ); // StringSource

    //cp-abe encrypt key
    cpabe.encrypt("attr1 and attr2", encoded1, ct);
    ShutdownOpenABE();

{
    ofstream outf("key.txt");
    outf << ct;
}

{
    ofstream outx("iv.txt");
    outx << encoded2;
}

    try
    {
        //cout << "plain text: " << plain << endl;

        CTR_Mode< AES >::Encryption e;
        e.SetKeyWithIV(key, sizeof(key), iv);

        // The StreamTransformationFilter adds padding
        //  as required. ECB and CBC Mode must be padded
        //  to the block size of the cipher.
        StringSource(strstr.str(), true, 
            new StreamTransformationFilter(e,
                new StringSink(cipher)
            ) // StreamTransformationFilter      
        ); // StringSource
    }
    catch(const CryptoPP::Exception& e)
    {
        cerr << e.what() << endl;
        //exit(1);
    }

    /*********************************\
    \*********************************/

    // string cipher
    encoded.clear();
    StringSource(cipher, true,
        new HexEncoder(
            new StringSink(encoded)
        ) // HexEncoder
    ); // StringSource


    return crow::response{encoded};
});

    // enables all log
    app.loglevel(crow::LogLevel::DEBUG);
    //crow::logger::setHandler(std::make_shared<ExampleLogHandler>());

    app.port(18080)
        //.multithreaded()
        .run();
        }
};

void MainWindow::checkbit()
{
    //update the interface of database
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,0, new QTableWidgetItem("Check-Successfully"));
}

void MainWindow::on_mainexit_2_clicked()
{
    //disable 'start server' button once it is pressed
    ui->mainexit_2->setEnabled(false);
    dbtype = ui->radioButton_1->isChecked();
    WorkerThread *workerThread = new WorkerThread;
    workerThread->start();
}

void MainWindow::on_mainexit_clicked()
{
    //change the value of server parameter.
    dbtype = ui->radioButton_1->isChecked();

    ofstream ofs("dbtype_flag.txt");
    {
        boost::archive::text_oarchive oa(ofs);
        oa & dbtype;
    }
}

void MainWindow::on_mainexit_3_clicked()
{
    //change the data in the database directly...
    ofstream outfile;
    outfile.open("/Users/scaryang/Desktop/Interface/server/test.txt", ios::app);
    outfile << "Scarlett" << endl;
    outfile.close();
}


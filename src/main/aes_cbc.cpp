// g++ -g3 -ggdb -O0 -DDEBUG -I/usr/include/cryptopp Driver.cpp -o Driver.exe -lcryptopp -lpthread
// g++ -g -O2 -DNDEBUG -I/usr/include/cryptopp Driver.cpp -o Driver.exe -lcryptopp -lpthread

#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <string>
using std::string;

#include <cstdlib>
using std::exit;

#include "cryptopp/cryptlib.h"
using CryptoPP::Exception;

#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "cryptopp/filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include "cryptopp/aes.h"
using CryptoPP::AES;

#include "cryptopp/ccm.h"
using CryptoPP::CTR_Mode;

#include "assert.h"

int main(int argc, char* argv[])
{
	AutoSeededRandomPool prng;

	byte key[AES::DEFAULT_KEYLENGTH];
	prng.GenerateBlock(key, sizeof(key));

	byte iv[AES::BLOCKSIZE];
	prng.GenerateBlock(iv, sizeof(iv));

	string plain = "CTR Mode Test";
	string cipher, encoded, encoded1, encoded2, recovered;

	/*********************************\
	\*********************************/

	// Pretty print key
	encoded.clear();
	StringSource(key, sizeof(key), true,
		new HexEncoder(
			new StringSink(encoded1)
		) // HexEncoder
	); // StringSource
	cout << "key: " << encoded1 << endl;

	// Pretty print iv
	encoded.clear();
	StringSource(iv, sizeof(iv), true,
		new HexEncoder(
			new StringSink(encoded2)
		) // HexEncoder
	); // StringSource
	cout << "iv: " << encoded2 << endl;

	/*********************************\
	\*********************************/
    //recover key
    byte key1[CryptoPP::AES::DEFAULT_KEYLENGTH];
    {
        CryptoPP::HexDecoder decoder1;
        decoder1.Put((byte*)encoded1.data(), encoded1.size());
        decoder1.MessageEnd();
        decoder1.Get(key1, sizeof(key1));
    }

    // 1. Decode iv: 
    // At the moment our input is encoded in string format...
    // we need it in raw hex: 
    byte iv1[CryptoPP::AES::BLOCKSIZE] = {};
    // this decoder would transform our std::string into raw hex:
    CryptoPP::HexDecoder decoder2;
    decoder2.Put((byte*)encoded2.data(), encoded2.size());
    decoder2.MessageEnd();
    decoder2.Get(iv1, sizeof(iv1));

	try
	{
		cout << "plain text: " << plain << endl;

		CTR_Mode< AES >::Encryption e;
		e.SetKeyWithIV(key1, sizeof(key1), iv1);

		// The StreamTransformationFilter adds padding
		//  as required. ECB and CBC Mode must be padded
		//  to the block size of the cipher.
		StringSource(plain, true, 
			new StreamTransformationFilter(e,
				new StringSink(cipher)
			) // StreamTransformationFilter      
		); // StringSource
	}
	catch(const CryptoPP::Exception& e)
	{
		cerr << e.what() << endl;
		exit(1);
	}

	/*********************************\
	\*********************************/

	// Pretty print
	encoded.clear();
	StringSource(cipher, true,
		new HexEncoder(
			new StringSink(encoded)
		) // HexEncoder
	); // StringSource
	cout << "cipher text: " << encoded << endl;

	/*********************************\
	\*********************************/

    // 2. Decode cipher:
    // Next, we do a similar trick for cipher, only here we would leave raw hex
    //  in a std::string.data(), since it is convenient for us to pass this
    // std::string to the decryptor mechanism:
    std::string cipher_raw;
    {
        CryptoPP::HexDecoder decoder3;
        decoder3.Put((byte*)encoded.data(), encoded.size());
        decoder3.MessageEnd();

        long long size = decoder3.MaxRetrievable();
        cipher_raw.resize(size);       
        decoder3.Get((byte*)cipher_raw.data(), cipher_raw.size());
        // If we print this string it's completely rubbish: 
        // std::cout << "Raw cipher: " << cipher_raw << std::endl;
    }


    // 4. Decrypt:
    std::string decrypted_text;
    try {
            CryptoPP::CTR_Mode<CryptoPP::AES>::Decryption d;
            d.SetKeyWithIV(key, sizeof(key), iv);

            CryptoPP::StringSource ss(
                cipher_raw, 
                true, 
                new CryptoPP::StreamTransformationFilter(
                    d,
                    new CryptoPP::StringSink(decrypted_text)
                ) // StreamTransformationFilter
            ); // StringSource

            std::cout << "Decrypted text: " << decrypted_text << std::endl;
    }
    catch( CryptoPP::Exception& e ) {
            std::cerr << e.what() << std::endl;
            exit(1);
    }

/*
	try
	{
		CTR_Mode< AES >::Decryption d;
		d.SetKeyWithIV(key, sizeof(key), iv);

		// The StreamTransformationFilter removes
		//  padding as required.
		StringSource s(cipher, true, 
			new StreamTransformationFilter(d,
				new StringSink(recovered)
			) // StreamTransformationFilter
		); // StringSource

		cout << "recovered text: " << recovered << endl;
	}
	catch(const CryptoPP::Exception& e)
	{
		cerr << e.what() << endl;
		exit(1);
	}
*/
	/*********************************\
	\*********************************/

	return 0;
}


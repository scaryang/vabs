#include "dialog2.h"
#include "ui_dialog2.h"
#include <iostream>
#include <fstream>
#include <map>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/algorithm/string.hpp>


using namespace std;
using namespace boost::algorithm;


Dialog2::Dialog2(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog2)
{
    ui->setupUi(this);
   
    map<string,string> information;
{
    ifstream ifsa("detail.txt");
    boost::archive::text_iarchive iak(ifsa);
    iak >> information;
}
    
    for(auto iter = information.begin(); iter != information.end(); iter++)
    {
    	QString tempx = QString::fromStdString(iter->first);
        ui->comboBox->insertItem(0,tempx);
    }

}

Dialog2::~Dialog2()
{
    delete ui;
}

void Dialog2::on_querybtn_3_clicked()
{


    map<string,string> information;
{
    ifstream ifsa("detail.txt");
    boost::archive::text_iarchive iak(ifsa);
    iak >> information;
}
    QString tempy = ui->comboBox->currentText();
    string tempz = tempy.toUtf8().constData();

    for (auto iter = information.begin(); iter != information.end(); iter++)
        {
            if (iter->first == tempz) 
            {
               string result1 = iter->second;
               int circle_x = 0;
               int rowindex = 0;

               vector<string> show;
               split(show, result1, is_any_of("\\"),token_compress_on);

            for (unsigned int r = 0; r < show.size(); ++r){
               if(r >= 10)
                  {
                      QString bbb = QString::fromStdString(show[r]);
                      if (circle_x%4 == 0)
                          {
                              rowindex++;
                              ui->tableWidget_2->setRowCount(rowindex);
                          }
                            ui->tableWidget_2->setItem(rowindex-1,circle_x%4, new QTableWidgetItem(bbb));
                            circle_x++;        
                  }
              }
          }

      }     

}

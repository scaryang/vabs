#include "mainwindow.h"
#include <QApplication>
#include "logindialog.h"
#include <QDesktopWidget>
#include <QProcess>


QString gstrFilePath = "";
void relogin(void);

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    gstrFilePath = QCoreApplication::applicationFilePath();

    //MainWindow w;
    LoginDialog dlg;
    if(dlg.exec()==QDialog::Accepted)
    {
        QString loginname = dlg.name;
        MainWindow w(loginname);
        w.show();
        w.move ((QApplication::desktop()->width() - w.width())/2,(QApplication::desktop()->height() - w.height())/2);
        int nret = a.exec();
        if (nret == 2)
        {
            atexit(relogin);
        }
        return nret;
    }
    else return 0;
}

void relogin(){
    QProcess process;
    process.startDetached(gstrFilePath);
}

//dub_db
//#include "../db_dup/db.hpp"
//#include "../db/db.hpp"
//#include "../utils/pbc_helper.h"
//#include "../db/sig.hpp"
//#include "../db/data.hpp"
//#include "../db/type.hpp"
//#include "../db/rect.hpp"

#include "../authdb/db.hpp"
#include "../utils/pbc_helper.h"
#include "../authdb/sig.hpp"
#include "../authdb/data.hpp"
#include "../authdb/type.hpp"
#include "../authdb/rect.hpp"

#include "../abs/boolean_expression.hpp"
#include "../abs/boolean_expression_dnf.hpp"
#include "../utils/timer.hpp"


#include <boost/algorithm/string.hpp>
#include <iostream>
#include <set>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <utility>
#include <typeinfo>
#include <type_traits>

using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;
using namespace boost::algorithm;

struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};

typedef Type<string,int> DB_Type;

int main() 
{
    //create global pid map
    unordered_map<string,int> pid;
    //data
    std::unordered_map<DB::Point, DB::RawDataRecord<DB_Type>> data;
    string line;
    //only patientid
    ifstream infile0;
    int num = 1;

/*
    string pre;
    pre.reserve(5000);
    vector<string> vo(700,pre);
    cout << vo[0].capacity()<<endl;
*/

    unordered_multimap<Point,string> vo;


    infile0.open("/home/yang/Pictures/aaa/data/PatientCorePopulatedTable.txt");
    //init query attr
    while( getline(infile0,line) )
    {
        vector<string> result;
        split(result, line, is_any_of("\\"),token_compress_on);
        pid.insert(make_pair( result.at(0), num));
        num++;
        vector<string> free;
        result.swap(free);
    }
    infile0.close();

    ifstream infile1;
    infile1.open("/home/yang/Pictures/aaa/data/PatientCorePopulatedTable.txt");
    //insert patient basic information
    while( getline(infile1,line) )
    {
        vector<string> result;
        split(result, line, is_any_of("\\"),token_compress_on);
        for(int i = 1; i < result.size(); i++)
        {
            string ppid = result.at(0);
            for (int aaid = 1; aaid < 8; aaid++)
            {
                DB::Point point({pid.at(ppid), aaid});
                vo.insert(make_pair(point, result.at(i)));
            }
        }
        vector<string> free;
        result.swap(free);
    }
    infile1.close();


    //pid,aid,description
    ifstream infile2;
    infile2.open("/home/yang/Pictures/aaa/data/AdmissionsDiagnosesCorePopulatedTable.txt");
    while( getline(infile2,line))
    {
        vector<string> result2;
        split(result2, line, is_any_of("\\"),token_compress_on);
        for(int i = 2; i < result2.size(); i++)
        {
            string ppid = result2.at(0);
            int aaid = stoi(result2.at(1)); 
            //cout << pid.at(ppid) << endl;
           
            DB::Point point({pid.at(ppid), aaid});
            vo.insert(make_pair(point, result2.at(i)));
        }
        vector<string> free2;
        result2.swap(free2);
    }
    infile2.close();



    //admission starttime endtime()
    ifstream infile3;
    infile3.open("/home/yang/Pictures/aaa/data/AdmissionsCorePopulatedTable.txt");
    while( getline(infile3,line) )
    {
        vector<string> result;
        split(result, line, is_any_of("\\"),token_compress_on);
        for(int i = 2; i < result.size(); i++)
        {
            string ppid = result.at(0);
            int aaid = stoi(result.at(1)); 
            //cout << pid.at(ppid) << endl;
            DB::Point point({pid.at(ppid), aaid});
            vo.insert(make_pair(point, result.at(i)));

        }
        vector<string> free;
        result.swap(free);
    }
    infile3.close();


    //admission starttime endtime()
    ifstream infile4;
    infile4.open("/home/yang/Pictures/aaa/data/LabsCorePopulatedTable.txt");

    int count = 0;
    while( getline(infile4,line) )
    {
        vector<string> result;
        split(result, line, is_any_of("\\"),token_compress_on);
        for(int i = 2; i < result.size(); i++)
        {
            string ppid = result.at(0);
            int aaid = stoi(result.at(1)); 
            //cout << pid.at(ppid) << endl;
            DB::Point point({pid.at(ppid), aaid});
            vo.insert(make_pair(point, result.at(i)));

            //data.insert(make_pair(point, DB::RawDataRecord<DB_Type>(result.at(i), string("1 OR ((8 OR 9) AND (4 AND 5))"))));
        }
        vector<string> free;
        result.swap(free);
        count++;
        //cout << count << endl;
    }
    infile4.close();

    int number1 = 0;
    int number2 = 0;

for (int m = 1; m <= 100; m++){
    for (int n = 1; n <= 7; n++){

    DB::Point point1({m,n});
    auto it = vo.find(point1);
    int len = vo.count(point1);

    vector<string> vec;
    //vec.resize(1000);
    string rec = "";

    
    for (int i = 0; i < len; ++i,++it){
    	cout  << "init  " << it->second << endl;
        string temp(it->second);
        vec.push_back(temp);
    	//cout << "first: "<< zero << endl;
    	//vec.push_back(it->second);
    	//zero.append(temp);
    	//zero += one;
    	//cout << "second: " << zero << endl;
    	//cout << "---------" << endl;
    }
    for (int i = vec.size()-1; i >=0; i--)
    {
        rec += vec.at(i);
        rec += "\\";
    }
    
    if(vec.size() > 6){
          
          data.insert(make_pair(point1, DB::RawDataRecord<DB_Type>(rec, string("1 OR ((8 OR 9) AND (4 AND 5))"))));
          cout << number2 << endl;
          number2 ++;
    }

    cout << number1 << endl;
    number1 ++;

    vector<string> free3;
    vec.swap(free3);
}
}
/*
    for (int m = 101; m <= 10000; m++ ){
    	for (int n = 8; n <=30; n++){

    		DB::Point point1({m,n});
    	}
    }
*/
    //string rec = accumulate(vec.end(),vec.begin(),string(""));
    //cout << rec << endl;
    
    //string rec = accumulate(vec.begin(),vec.end(),string(""));
    //cout << rec << endl;
    //string str = "Hello!";
    //vector<string> vec(20,str);
    //string rec = accumulate(vec.begin(),vec.end(),string(""));
    //cout << rec << endl;
  
    datarecord teemo;

    for (int i = 0; i < 11; i++)
    {
        teemo.global_roles.insert(i);
    }
    teemo.roles = set<int>{8,4,5};
    teemo.pseudo_role = 0;
    teemo.range = Rectangle({1,1},{100,7});

    cout << "Creating the database:===" << endl;
    //create database
    Database<DB_Type> db1("newtest.db");
    db1.create_meta(TYPE_A_PARAM, teemo.pseudo_role, teemo.global_roles, 2, teemo.range);
    db1.create_index(data, IndexType::grid);
    db1.save(true);
    db1.reset_sig();
    db1.create_data_sig();
    db1.create_grid_sig();  

    auto mvk = db1.get_mvk();
    auto inaccessible_policy = db1.compute_inaccessible_policy(teemo.roles);
    // equality query
    //auto r1 = db1.equality_query({66, 2}, teemo.roles);
    auto r1 = db1.range_query(Rectangle({66, 2}, {66, 3}), teemo.roles);


    cout << r1.accessible_data.size() << endl;

    cout << "--------output the results -------------" << endl;
    auto iter = r1.accessible_data.begin();
    for (; iter != r1.accessible_data.end(); iter++){
        RawDataRecord<DB_Type> temp(iter->second.first);
        cout << temp.value << endl;
    }    
/*
   cout << endl << "-----building database------" << endl;

    datarecord teemo;
    for (int i = 0; i < 11; i++)
    {
        teemo.global_roles.insert(i);
    }
    teemo.roles = set<int>{8,4,5};
    teemo.pseudo_role = 0;
    teemo.range = Rectangle({1,1},{100,7});
    
    //create database
    DupDatabase<DB_Type> db("test.db");
    db.create_meta(TYPE_A_PARAM, teemo.pseudo_role, teemo.global_roles, 2, teemo.range);
    db.create_index(data, IndexType::hybrid);
    db.save(true);
    db.reset_sig();
    db.create_data_sig();
    db.create_grid_sig();
    auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(teemo.roles);


    cout << endl << "-------------test range query result---------" << endl;
    auto r3 = db.range_query(Rectangle({66, 2}, {66, 2}), teemo.roles);
    cout << r3.accessible_data.size() << endl;
    //cout << r3.inaccessible_data.size() << endl;
    //cout << r3.inaccessible_grid.size() << endl;
    auto iter = r3.accessible_data.begin();
    for (; iter != r3.accessible_data.end(); iter++){
        RawDataRecord<DB_Type> temp(iter->second.first);
        cout << temp.value << endl;
    }

    bool r3_result = r3.verify(mvk, Rectangle({66, 2}, {66, 2}), inaccessible_policy);
    cout << r3_result << endl;
*/

    return 0;
}
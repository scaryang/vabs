#include "../db_dup/db.hpp"
#include "../utils/pbc_helper.h"
#include "../db/sig.hpp"
#include "../db/data.hpp"
#include "../db/type.hpp"
#include "../db/rect.hpp"
#include "../abs/boolean_expression.hpp"
#include "../abs/boolean_expression_dnf.hpp"


#include <iostream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <fstream>

#include "sql_parser.h"
#include <fstream>
#include <vector>

#include <set>
#include <string>

#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_free.hpp>


using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;

struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};


typedef Type<string,int> DB_Type;
/*
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DupRangeQueryResult<DB_Type>);
BOOST_CLASS_VERSION(DupRangeQueryResult<DB_Type>,1);


namespace boost
{
    namespace serialization{
        //0923
    //friend class boost::serialization::access;
       template <class Archive>
        void serialize(Archive& ar, DupRangeQueryResult<DB_Type>& e, const unsigned int version)
       {
           split_free(ar,e,version);
       }

        template <class Archive>
        void save(Archive& ar, const DupRangeQueryResult<DB_Type>& e, const unsigned int _version)
       {
            int count1(e.accessible_data.size());
            int count2(e.inaccessible_data.size());
            int count3(e.inaccessible_grid.size());

            ar << count1;
            ar << count2;
            ar << count3;


            for (auto p_map = e.accessible_data.begin(); p_map != e.accessible_data.end(); p_map++)
            {
                ar << p_map->first;
                ar << p_map->second.first;
                ar << p_map->second.second;               
            }
            for (auto p_map = e.inaccessible_data.begin(); p_map != e.inaccessible_data.end(); p_map++)
            {
                ar << p_map->first;
                ar << p_map->second; 
            }
            for (auto p_map = e.inaccessible_grid.begin(); p_map != e.inaccessible_grid.end(); p_map++)
            {
                ar << p_map->first;
                ar << p_map->second; 
            }
       }
        
        template <class Archive>
        void load(Archive& ar, const DupRangeQueryResult<DB_Type>& e, const unsigned int _version)
       {
            int count1,count2,count3;
            ar >> count1;
            ar >> count2;
            ar >> count3;

            //Point point;
            //Rectangle rect;
            //RawDataRecord<DB_Type> record;
            //Datasig<DB_Type> datasig;
            //Gridsig<DB_Type> gridsig;

            //auto p_map1 = e.accessible_data.begin();
            //auto p_map2 = e.inaccessible_data.begin();
            //auto p_map3 = e.inaccessible_grid.begin();

            for (auto p_map = e.accessible_data.begin(); p_map != e.accessible_data.end(); p_map++)
            {
                ar >> p_map->first;
                ar >> p_map->second.first;
                ar >> p_map->second.second;               
            }
            for (auto p_map = e.inaccessible_data.begin(); p_map != e.inaccessible_data.end(); p_map++)
            {
                ar >> p_map->first;
                ar >> p_map->second; 
            }
            for (auto p_map = e.inaccessible_grid.begin(); p_map != e.inaccessible_grid.end(); p_map++)
            {
                ar >> p_map->first;
                ar >> p_map->second; 
            }
           
       }
}}
*/


int main(int argc, char *argv[])
{
    if (argc < 2) {
        std::cerr << "usage: " << argv[0] << " file [num] [print]" << std::endl;
        return -1;
    }
    int run_num = 1;
    if (argc >= 3) {
        run_num = atoi(argv[2]);
    }
    int print = 1;
    if (argc >= 4) {
        print = atoi(argv[3]);
    }

    std::ifstream ifs(argv[1], std::ios::in);
    std::vector<std::string> sqls;
    while (!ifs.eof()) {
        char sql_buf[255];
        ifs.getline(sql_buf, sizeof(sql_buf));
        size_t sql_len = strlen(sql_buf);
        if (sql_len <= 0) continue;
        sqls.push_back(sql_buf);
    }
    //define vec(field) and table 
    vector<string> field;
    string table;

    SQLParser<std::string::const_iterator> sql_parser;
    for (int i = 0; i < run_num; ++i) {
        for (std::vector<std::string>::iterator iter = sqls.begin(); iter != sqls.end(); ++iter) {
            std::string::const_iterator begin = iter->begin();
            std::string::const_iterator end = iter->end();
            SelectSQL select_sql;
            bool ret = phrase_parse(begin, end, sql_parser, boost::spirit::ascii::space, select_sql);
            if (ret && begin == end) {
                if (print) {
                   std::cout << "phrase_parse succ. sql=" << *iter << std::endl;
                   for (std::vector<std::string>::iterator iter = select_sql.fields.begin(); iter != select_sql.fields.end(); ++iter) {
                   std::cout << "[field]=" << *iter << std::endl;
                   field.push_back(*iter);
                   }
                   std::cout << "[table]=" << select_sql.table << std::endl;
                   table = select_sql.table;
                   if (select_sql.has_condition) {
                   std::cout << "[condition]= " << std::endl;
                   std::cout << select_sql.condition.left << std::endl;
                   std::cout << select_sql.condition.right << std::endl;
                   
                   //output to txt
                   ofstream file("rdbuf.txt");
	               streambuf *x = cout.rdbuf( file.rdbuf() );
	               std::cout << select_sql.condition.left << std::endl;
                   std::cout << select_sql.condition.right << std::endl;
                   cout.rdbuf(x);

                   //std::cout << "[condition]=" << select_sql.condition << std::endl;
                   }
                   std::cout << "=============" << std::endl;
                }
            } else {
                std::cerr << "[FAIL]phrase_parse fail. sql=" << *iter << std::endl;
                std::cerr << "=============" << std::endl;
            }
        }
    }

    cout << "Test field and table!" << endl;
    cout << "The table is   " << table << endl;
    cout << "The fields are   " << endl;
    for (int i = 0; i < field.size(); i++)
    {
    	cout << field.at(i) << " ";
    }
    cout << endl;

    cout << "-----------------------" << endl;

    ifstream infile;
	infile.open("rdbuf.txt");
	if(!infile) cout << "Read Error!" << endl;
	
	string t1;
    cout << "save into vector" << endl;
    vector<string> ve;

    while(infile >> t1)
    {
    	ve.push_back(t1);
    }

    for (int i = 0; i < ve.size(); i++)
    {
    	cout << ve.at(i) << " ";
    }
    cout << endl;
    cout << "-------------------------" << endl;



	
    //query
	datarecord cl;
	for (int i = 0; i < 125; i++)
    {
    	cl.global_roles.insert(i);
    }
    cl.roles = set<int>{1,120};
    cl.pseudo_role = 0;
    cl.range = Rectangle({2008,1},{2016,113});

	//typedef Type<string,int> DB_Type;
	DupDatabase<DB_Type> db("db_fundAudit.db");
	db.load(true);
	auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(cl.roles);

	//test range query
	//cout << "test range query result" << endl;
    string year,school;

    if ( ve.at(0) == string("fundYear") ) 
    {
    	year = ve.at(1);
    	if ( ve.at(2) == string("schoolid") ) 
    	{
    		school = ve.at(3);
    	}
    }

    if ( ve.at(0) == string("schoolid") ) 
    {
    	school = ve.at(1);
    	if ( ve.at(2) == string("fundYear") )
    	{
           year = ve.at(3);
    	}
    }
/*
    if (year == INIT_VALUE or school == INIT_VALUE )
    {
    	cout << "Input error!" <<endl;
    	return 0;
    }
*/
    cout << "fundYear:  " << year << endl;
    cout << "Schoolid:  " << school << endl;
    cout << "----------------------" << endl;

	auto r3 = db.range_query(Rectangle({std::stoi(year), std::stoi(school)}, {std::stoi(year), std::stoi(school)}), cl.roles);

	//cout << "The size of query results is   ";
	//cout << r3.accessible_data.size() << endl;
    
    if (r3.accessible_data.size() != 0)
    {
	    cout << "The query results are " << endl;
	    cout << endl;

	//cout << r3.inaccessible_data.size() << endl;
	//cout << r3.inaccessible_grid.size() << endl;
	    auto iter = r3.accessible_data.begin();
        vector<string> intro,intro1;
        for (int k = 0; k < 100 ; k ++)
        {
             intro.push_back("status");
             intro.push_back("balanceDesc");
             intro.push_back("implementDesc");
             intro.push_back("balanceSum");
             intro.push_back("carryForwardSum");
             intro.push_back("outlaySum");
             intro.push_back("outlay2Sum");
             intro.push_back("outlay1Sum");
             intro.push_back("surplusSum");
             intro.push_back("carryOverSum");
             intro.push_back("arrangeFundSum");
             intro.push_back("ProjectsCnt");
         }

         for (int k = 0; k < 100 ; k ++)
         {
             intro1.push_back("STATUS");
             intro1.push_back("BALANCEDESC");
             intro1.push_back("IMPLEMENTDESC");
             intro1.push_back("BALANCESUM");
             intro1.push_back("CARRYFORWARDSUM");
             intro1.push_back("OUTLAYSUM");
             intro1.push_back("OUTLAY1SUM");
             intro1.push_back("OUTLAY2SUM");
             intro1.push_back("SURPLUSSUM");
             intro1.push_back("CARYYOVERSUM");
             intro1.push_back("ARRANGEFUNDSUM");
             intro1.push_back("PROJECTSCNT");
         }

         int m = 0;
	     for (; iter != r3.accessible_data.end(); iter++){
		     RawDataRecord<DB_Type> temp(iter->second.first);

		     vector<string>::iterator result = find(field.begin(),field.end(),intro.at(m));
		     if ( result != field.end()) 
		     	cout <<  intro1.at(m) << " : " << temp.value << endl;
		     
             if(field.at(0) == string("all") )
             	cout <<  intro1.at(m) << " : " << " " << temp.value << endl;

		     m ++;
	      }
	      cout << endl;
    }
    else
    {
    	cout << "No results!" << endl;
    }

	bool r3_result = r3.verify(mvk, Rectangle({std::stoi(year), std::stoi(school)}, {std::stoi(year), std::stoi(school)}), inaccessible_policy);
	cout << "The verification of results is " << " ";
	if (r3_result = 1)
		cout << "true" << endl;
	else
		cout << "false" << endl;


    cout << r3.accessible_data.size() << endl;
    cout << r3.inaccessible_data.size() << endl;
    cout << r3.inaccessible_grid.size() << endl;

      

 //   r3.save();
 //   r3.load();

    //0923
    PairingPtr pairing(Pairing::init_from_param(TYPE_A_PARAM));
    GlobalPBCPairing::set(pairing);
{
    ofstream ofs("serial1.txt");
    boost::archive::text_oarchive oa(ofs);
    oa << r3;
    oa << inaccessible_policy;
}
    
    DupRangeQueryResult<DB_Type> r4;
    BooleanExpression<int> inac_policy{};

{
    ifstream ifsa("serial1.txt");
    boost::archive::text_iarchive ia(ifsa);
    ia >> r4;
    ia >> inac_policy;
}

    bool aab_result = r4.verify(mvk, Rectangle({std::stoi(year), std::stoi(school)}, {std::stoi(year), std::stoi(school)}), inac_policy);
    cout << "The verification of results is " << " ";
    if (aab_result = 1)
        cout << "true" << endl;
    else
        cout << "false" << endl;



/*

    //test signature
ofstream in;
in.open("hash.txt",ios::trunc);
    for (auto ptr = r3.accessible_data.begin(); ptr != r3.accessible_data.end(); ++ptr)
    {
        string signature1 = ptr->second.second.output_hash();
        in << signature1 << endl;
    }
in.close();

ofstream inx;
inx.open("message.txt",ios::trunc);
    for (auto ptr = r3.accessible_data.begin(); ptr != r3.accessible_data.end(); ++ptr)
    {
        string signature2 = ptr->second.second.output_message();
        inx << "  " << signature2 << endl;
    }
inx.close();
*/

	//cout << r3_result << endl;
	//cout << r3.get_vo_size() << endl;
	return 0;
}

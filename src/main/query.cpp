//dub_db
#include "../db/db.hpp"
#include "../utils/pbc_helper.h"
#include "../utils/serialization_helper.h"
#include "../db/sig.hpp"
#include "../db/data.hpp"
#include "../db/type.hpp"
#include "../db/rect.hpp"
#include "../abs/boolean_expression.hpp"
#include "../abs/boolean_expression_dnf.hpp"
#include "../abs/pbc_serialization.hpp"
#include "../abs/key.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
 

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <set>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <utility>

#include "../utils/timer.hpp"

using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;
using namespace boost::algorithm;

struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};

typedef Type<string,int> DB_Type;

int main() 
{
   cout << endl << "-----Querying Infomation------" << endl;

    datarecord teemo;
    for (int i = 0; i < 11; i++)
    {
        teemo.global_roles.insert(i);
    }
    teemo.roles = set<int>{1,8,4,5,9,7};
    teemo.pseudo_role = 0;
    teemo.range = Rectangle({1,1},{1000,600});
    

    Timer tm_db;
    tm_db.start();
    //create database
    //Database<DB_Type> db("100patient.db");
    Database<DB_Type> db("kdtree.db");    
    //db.create_meta(TYPE_A_PARAM, teemo.pseudo_role, teemo.global_roles, 2, teemo.range);
    //db.create_index(data, IndexType::grid);
    //db.save(true);
    //db.reset_sig();
    //db.create_data_sig();
    //db.create_grid_sig();
    db.load(true);
    

    tm_db.stop();
    cout << "     Time:" << tm_db.result() << "ms " << endl;

    auto mvk = db.get_mvk();

    //KeyDistributionCenter kdc;
    //kdc.setup(GlobalPBCPairing::get());

    //output mvk
    
    ofstream ofs("serialk.txt");
    {
        boost::archive::text_oarchive oa(ofs);
        oa & mvk;
    }

    auto inaccessible_policy = db.compute_inaccessible_policy(teemo.roles);
    
    Timer tm_query,tm_verify;

    cout << endl << "-------------test range query result---------" << endl;
    tm_query.start();
    auto r3 = db.range_query(Rectangle({1, 1}, {100, 7}), teemo.roles);

    tm_query.stop();
    cout << "Query Time:   " << tm_query.result() << "ms"  << endl;

    cout <<  "Data Size:    " << r3.accessible_data.size() << endl;

    //cout << r3.record.value << endl;
    //cout << r3.inaccessible_data.size() << endl;
    //cout << r3.inaccessible_grid.size() << endl;
/*  save accessible point
    vector<DB::Point> accessible_point;
    auto iter = r3.accessible_data.begin();
    for (; iter != r3.accessible_data.end(); iter++){
        //output rawdata
        //RawDataRecord<DB_Type> temp(iter->second.first);
        //cout << temp.value << endl;
        Point temp(iter->first);
        accessible_point.push_back(temp);
    }

    cout << "Accessible_point's Size is:  " << accessible_point.size() << endl;
    
    ofstream ofs("Acc_Point.txt");
    {
        boost::archive::text_oarchive oa(ofs);
        oa & accessible_point;
    }

    vector<DB::Point> accessible_point1;
    ifstream ifs("Acc_Point.txt");
    {
        boost::archive::text_iarchive ia(ifs);
        ia & accessible_point1;
    }    
    cout << "Test read: " << accessible_point1.size() << endl;
*/

    tm_verify.start();
    bool r3_result = r3.verify(mvk, Rectangle({1, 1}, {100, 7}), inaccessible_policy);
    tm_verify.stop();

    cout << "Verification Result is: " << r3_result << endl;

    cout << "Verify Times:  " << tm_verify.result() << "  ms" << endl;


    return 0;
}
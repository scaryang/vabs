#include "pbutton.h"
#include "qmessagebox.h"
#include "dialog2.h"

using namespace std;

PButton::PButton(QWidget *parent):QPushButton(parent)
{

}

void PButton::paintEvent(QPaintEvent *p)
{
    QPushButton::paintEvent(p);
}

void PButton::setclick()
{
    QObject::connect(this,SIGNAL(clicked()),this,SLOT(display()));
}

void PButton::display()
{
    //transfer value into dialog2
    QString index = PButton::text();
    string temp = index.toStdString();
    string sid,aid;
    //read related information from button text
    int i = 3;
    while (i < temp.size()){
        sid += temp[i];
        if (temp[i+1] == ',') break;
        i++;
    }

    aid = temp[i+2];

    QString ssid = QString::fromStdString(sid);
    QString aaid = QString::fromStdString(aid);
    dialog->transmit(ssid, aaid);
    dialog->show();
}

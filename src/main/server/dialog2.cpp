#include "dialog2.h"
#include "ui_dialog2.h"

//#include "../../db/rect.hpp"
#include "../../db/utils.hpp"

#include <vector>
#include <sqlite_modern_cpp.h>
#include <boost/algorithm/string.hpp>
#include <stdlib.h>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <sstream>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_free.hpp>

#include "../../utils/sfinae_helper.hpp"
#include <algorithm>
#include <boost/functional/hash.hpp>
#include <functional>
#include <list>
#include <prettyprint.hpp>
#include <ostream>
#include <fstream>


using namespace std;
using namespace DB;
using namespace boost::algorithm;
using namespace sqlite;

    class Point : public std::vector<int>
    {
    public:
        using std::vector<int>::vector;

        size_t get_vo_size() const { return size() * sizeof(int); }

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& boost::serialization::base_object<std::vector<int>>(*this);
        }
    };

dialog2::dialog2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dialog2)
{
    ui->setupUi(this);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);
    ui->tableWidget->verticalHeader()->setStretchLastSection(QHeaderView::Stretch);
    ui->tableWidget_2->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);

    ui->tableWidget->setWordWrap(false);
    ui->tableWidget_2->setWordWrap(false);    
}

dialog2::~dialog2()
{
    delete ui;
}

void dialog2::transmit(QString teemo, QString aid)
{
    //Test button input
    //ui->tableWidget->setItem(0,0,new QTableWidgetItem(teemo));
    //ui->tableWidget->setItem(1,0,new QTableWidgetItem(aid));

   database db("100patient.db");

    int pid_num = atoi(teemo.toStdString().c_str());
    int aid_num = atoi(aid.toStdString().c_str());

    //filter if no data, the window will show nothing
    vector<Point> accessible_point;
    ifstream fin("Acc_Point.txt");
    {
        boost::archive::text_iarchive ia(fin);
        ia & accessible_point;
    }
    Point aabs{pid_num, aid_num};
    vector<Point>::iterator iter=std::find(accessible_point.begin(),accessible_point.end(), aabs);
    if (iter == accessible_point.end()){
        return;
    }

    string ml;
    auto load = [&](unique_ptr<vector<char>> value){  
         Utils::blob_to_object(*value,ml);
    };

    //unique_ptr<vector<char>> value;
    std::vector<char> focus_s;
    focus_s = Utils::object_to_blob(aabs);
    db << "select value from Data where point = ?;" << focus_s >> load;
         
    vector<string> show;
    split(show, ml, is_any_of("\\"),token_compress_on);
    
    int circle_x = 0;
    int rowindex = 0;
    for (unsigned int r = 0; r < show.size(); ++r){
        
        if(r < 10)
            {
                QString aaa = QString::fromStdString(show[r]);
                ui->tableWidget->setItem(0,r,new QTableWidgetItem(aaa));           
            }
         else
            {

                QString bbb = QString::fromStdString(show[r]);
                if (circle_x%4 == 0)
                {
                    rowindex++;
                    ui->tableWidget_2->setRowCount(rowindex);
                }
                    ui->tableWidget_2->setItem(rowindex-1,circle_x%4, new QTableWidgetItem(bbb));
                    circle_x++;
                }
    }     

}
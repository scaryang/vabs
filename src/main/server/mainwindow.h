#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include <string>

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    
    ~MainWindow();

public slots:
    void paintEvent(QPaintEvent*);

private slots:

    void on_mainexit_2_clicked();

    void on_mainexit_clicked();

    void on_mainexit_3_clicked();

    void timeout();

    void checkbit();

private:
    Ui::MainWindow *ui;
    //vector<Point> accessible_point;
};

#endif // MAINWINDOW_H

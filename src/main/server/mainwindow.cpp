#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "customtabstyle.h"
#include <iostream>
#include <fstream>
#include <QTimer>
#include "dialog2.h"
#include "pbutton.h"
#include <QHBoxLayout>
#include <QtCore/QCoreApplication>
#include "crow.h"
#include <sqlite_modern_cpp.h>
#include <nlohmann/json.hpp>

#include "../../db_dup/db.hpp"
#include "../../db/db.hpp"
#include "./pbc_helper.h"
#include <map>
#include <vector>
#include <sstream>

#include <set>
#include <string>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/nvp.hpp>

#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_free.hpp>

#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>
#include <boost/algorithm/string.hpp>

#include "../../utils/timer.hpp"

//cryptopp
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;
#include "cryptopp/cryptlib.h"
using CryptoPP::Exception;

#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "cryptopp/filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include "cryptopp/aes.h"
using CryptoPP::AES;

#include "cryptopp/ccm.h"
using CryptoPP::CTR_Mode;

#include "assert.h"
#include <QThread>
#include <QTimer>
#include <stdlib.h>

using namespace std;
using namespace pbc;
//using namespace ABS;
using namespace DB;
using json = nlohmann::json;
using namespace oabe;
using namespace oabe::crypto;
using namespace boost::algorithm;
using namespace sqlite;

typedef unsigned char byte;

struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};


class ExampleLogHandler : public crow::ILogHandler {
    public:
        void log(std::string /*message*/, crow::LogLevel /*level*/) override {
//            cerr << "ExampleLogHandler -> " << message;
        }
};

struct ExampleMiddleware 
{
    std::string message;

    ExampleMiddleware() 
    {
        message = "foo";
    }

    void setMessage(std::string newMsg)
    {
        message = newMsg;
    }

    struct context
    {
    };

    void before_handle(crow::request& /*req*/, crow::response& /*res*/, context& /*ctx*/)
    {
        CROW_LOG_DEBUG << " - MESSAGE: " << message;
    }

    void after_handle(crow::request& /*req*/, crow::response& /*res*/, context& /*ctx*/)
    {
        // no-op
    }
};

    unordered_map<string,int> pid;
    datarecord teemo;
    typedef Type<string,int> DB_Type;
    int dbtype;
    Point focus;
    //vector<string> allaid;
    //vector<string> said;
    //string spid;
    vector<Point> accessible_point;

    Database<DB_Type> db("100patient.db");
    Database<DB_Type> dkb("kdtree.db");


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->setFixedSize(582,627);
    ui->setupUi(this);
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);
    ui->tableWidget->setRowCount(700);
    
    //set Grid-Tree is default on 
    ui->radioButton_1->setChecked(true);

    //Initate Query log
    ui->textBrowser->setText(tr("Crow/0.1 server is running at 0.0.0.0:18080 using 4 threads..."));
    

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
    timer->start(1000);

    //adjust database overview (delete second column and rename 1st column name)
    ui->tableWidget->removeColumn(1);
    ui->tableWidget->horizontalHeaderItem(0)->setText("Position");

    //load accessible point
    ifstream fin("Acc_Point.txt");
    {
        boost::archive::text_iarchive ia(fin);
        ia & accessible_point;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timeout()
{
    update();
}

//Polling function 
void MainWindow::paintEvent(QPaintEvent *)
{
    /*
    ifstream fin;
    string str;
    QString qstr;
    // working env
    fin.open("test.txt");
    ui->textBrowser->setText(tr("Test whether it can display dynamically"));
    while(!fin.eof())
    {
        getline(fin,str);
        qstr = QString::fromStdString(str);
        ui->textBrowser->append(qstr);
    }
    fin.close();
*/
    //read bit.txt then verdict use function. Last, update bit.txt (Yes/No)
    string str_flag;
{
    ifstream fin1("bit.txt");
    fin1 >> str_flag;
}
    if( str_flag == "Yes")
    { 
       //ui->tableWidget->item(1,1)->setBackgroundColor(QColor(200,0,0));
       checkbit();
       //modify the bit
       ofstream outfile("bit.txt", ofstream::out);
       outfile << "No";
       outfile.close();
    }
}

class WorkerThread : public QThread
{
   protected:
         void run()
{
    crow::App<ExampleMiddleware> app;
    app.get_middleware<ExampleMiddleware>().setMessage("hello");

    //create global pid map
    //unordered_map<string,int> pid;
    //only patientid
    string line;
    ifstream infile0;
    int num = 1;
    infile0.open("PatientCorePopulatedTable.txt");
    //init query attr
    while( getline(infile0,line) )
    {
        vector<string> result;
        split(result, line, is_any_of("\\"),token_compress_on);
        pid.insert(make_pair( result.at(0), num));
        num++;
        vector<string> free;
        result.swap(free);
    }
    infile0.close();

    for (int i = 0; i < 11; i++)
    {
        teemo.global_roles.insert(i);
    }
    teemo.roles = set<int>{1,8,4,5,9,7};
    teemo.pseudo_role = 0;
    teemo.range = Rectangle({1,1},{100,7});

    //db.load(true);
    //dkb.load(true);


    
    //set ip_route
    CROW_ROUTE(app, "/upload")
        .methods("POST"_method)
    ([](const crow::request& req){
        auto x = crow::json::load(req.body);
        auto y = json::parse(req.body);
        cout << "-------receive successfully!-------" << endl;      
        if (!x)
            return crow::response(400);   
     
    Database<DB_Type> db("100patient.db");
    Database<DB_Type> dkb("kdtree.db");
    db.load(true);
    dkb.load(true);
    
    auto mvk = db.get_mvk();
    auto mvkk = dkb.get_mvk();

    string login_name;
    {
        ifstream fint("login.txt");
        fint >> login_name;
    }
    //Two accounts info
    if(login_name == "Doctor"){
        teemo.roles = set<int>{4,5,8,9,7};
    }else if(login_name == "Admin"){
        teemo.roles = set<int>{4,5,8,9,7,1};
    }

    auto inaccessible_policy = db.compute_inaccessible_policy(teemo.roles);


 
    ofstream oofs("serial.txt");
    {
        boost::archive::text_oarchive oppp(oofs);
        oppp & mvk;
    }

    ofstream ooofs("serialk.txt");
    {
        boost::archive::text_oarchive opp(ooofs);
        opp & mvkk;
    }

    json xxx(y["pid"]);
    string trans_pid1 = xxx.dump();
    trans_pid1.erase(trans_pid1.begin(),trans_pid1.begin()+4);
    trans_pid1.erase(trans_pid1.end()-1,trans_pid1.end());
    int trans_pid = std::stoi(trans_pid1);

    json yyy(y["type"]);
    string query_type = yyy.dump();
    query_type.erase(query_type.begin(),query_type.begin()+1);
    query_type.erase(query_type.end()-1,query_type.end());
    cout << "query type is : " << query_type << endl;

    //cout << endl << trans_pid << endl;;
    //cout << endl << pid.at(trans_pid) << endl;

    //int trans_aid = x["aid"].i();
    //aid first element (C000X)
    json aid1(y["aid"]);
    string tran_aid = aid1.dump();
    tran_aid.erase(tran_aid.begin(),tran_aid.begin()+5); 
    tran_aid.erase(tran_aid.end()-1,tran_aid.end());
    int trans_aid = std::stoi(tran_aid);

    //aid second element (C000X)
    //int trans_aid2 = x["aaid"].i();
    json aid2(y["aaid"]);
    string tran_aid2 = aid2.dump();
    tran_aid2.erase(tran_aid2.begin(),tran_aid2.begin()+5);
    tran_aid2.erase(tran_aid2.end()-1,tran_aid2.end());
    int trans_aid2 = std::stoi(tran_aid2);

    int interval = trans_aid2 - trans_aid + 1;

    cout << endl << "-------------test range query result---------" << endl;
    //if(!( pid.at(trans_pid) < 100 && pid.at(trans_pid) > 0)) 
    if(!( trans_pid < 100 && trans_pid > 0))
    {
        cout << "error here~"  << endl;
        //exit(0);
    }
    
    ofstream queryinfo("queryinfo.txt");
    if (queryinfo.is_open())
    {
        queryinfo << "P00" + to_string(trans_pid);
        queryinfo << "\n";
        queryinfo << "C000" + to_string(trans_aid) + " To " + "C000" + to_string(trans_aid2);
        queryinfo << "\n";
        queryinfo << query_type;
        queryinfo.close();
    } 
    
    //ui->pidBrowser->setText(QString::fromStdString(trans_pid));
    //string trans_aaid = trans_aid + " To " + trans_aid2;
    //ui->aidBrowser->setText(QString::fromStdString(trans_aaid));

    DB::Point point1({trans_pid, trans_aid});
    DB::Point point2({trans_pid, trans_aid2});

    std::ostringstream strstr;

    vector<string> allaid;
    vector<string> said;
    string spid;

    if (query_type == ""){
        
        if (dbtype == 1){

            auto r3 = db.equality_query({trans_pid, trans_aid}, teemo.roles);

                boost::archive::text_oarchive oa(strstr);
                oa << r3;
                oa << inaccessible_policy;
                oa << point1;
                oa << point2;
        }
      else{
            auto r3 = dkb.equality_query({trans_pid, trans_aid}, teemo.roles);

                boost::archive::text_oarchive oa(strstr);
                oa << r3;
                oa << inaccessible_policy;
                oa << point1;
                oa << point2;
            
        }
        
    }
    else{   
        if (dbtype == 1)
            {
                auto r3 = db.range_query(Rectangle({trans_pid, trans_aid}, {trans_pid, trans_aid2}), teemo.roles);       
                
                    boost::archive::text_oarchive oa(strstr);
                    oa << r3;
                    oa << inaccessible_policy;
                    oa << point1;
                    oa << point2;
                    oa << (trans_aid2-trans_aid+1);

                if ( r3.accessible_data.size() >= 1){
                  for (auto iter = r3.accessible_data.begin(); iter != r3.accessible_data.end(); iter++)
                  {
                    Point tempaid(iter->first);
                    string acc_y = to_string(tempaid[1]);
                    said.push_back(acc_y);
                  }}
                
           }
        else
        {
            auto r3 = dkb.range_query(Rectangle({trans_pid, trans_aid}, {trans_pid, trans_aid2}), teemo.roles);       
            
                    boost::archive::text_oarchive oa(strstr);
                    oa << r3;
                    oa << inaccessible_policy;
                    oa << point1;
                    oa << point2;
                    oa << (trans_aid2-trans_aid+1);


            if ( r3.accessible_data.size() >= 1){
              for (auto iter = r3.accessible_data.begin(); iter != r3.accessible_data.end(); iter++)
               {
                 Point tempaid(iter->first);
                 string acc_y = to_string(tempaid[1]);
                 said.push_back(acc_y);
               }}
                     
        }
        
    }

    //update allaid && spid
    for (int u = 0; u < 9; u++){
        if (u >= trans_aid && u <= trans_aid2){
            allaid.push_back(to_string(u));
        }
    }
    //spid = trans_pid1;
    spid = to_string(trans_pid);
    //output queryinfo
{
    ofstream ifsak("query.txt");
    boost::archive::text_oarchive iak(ifsak);
    iak << allaid;
    iak << said;
    iak << spid;
}

    //auto r3 = db.range_query(Rectangle({pid.at(trans_pid), trans_aid}, {pid.at(trans_pid), trans_aid}), teemo.roles);
    
    //record accessible data in server side
    //range query

    //remind query log to update 
    string str_flag = "Yes";
{
    ofstream fin12138("bit.txt");
    fin12138 << str_flag;
}
    //remind vo update
    string str_teemo = "Yes";
{
    ofstream fin234("vo_bit.txt");
    fin234 << str_teemo;
}
    // vo number
{
    ofstream int_teemo("vo_number.txt");
    int_teemo << to_string(interval);
}

    //add cp-abe
    InitializeOpenABE();
    OpenABECryptoContext cpabe("CP-ABE");
    string ct;

    ifstream fin("mpk.txt");
    string mpk((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>());
    cpabe.importPublicParams(mpk);
    
    //aes ctr encrypt
    AutoSeededRandomPool prng;

    byte key[AES::DEFAULT_KEYLENGTH];
    prng.GenerateBlock(key, sizeof(key));

    byte iv[AES::BLOCKSIZE];
    prng.GenerateBlock(iv, sizeof(iv));

    string cipher, encoded, encoded1, encoded2, recovered;

    // string key
    //encoded.clear();
    StringSource(key, sizeof(key), true,
        new HexEncoder(
            new StringSink(encoded1)
        ) // HexEncoder
    ); // StringSource
    //cout << "key: " << encoded1 << endl;

    // string iv
    //encoded.clear();
    StringSource(iv, sizeof(iv), true,
        new HexEncoder(
            new StringSink(encoded2)
        ) // HexEncoder
    ); // StringSource

    //cp-abe encrypt key
    cpabe.encrypt("attr1 and attr2", encoded1, ct);
    ShutdownOpenABE();

{
    ofstream outf("key.txt");
    outf << ct;
}

{
    ofstream outx("iv.txt");
    outx << encoded2;
}

    try
    {
        //cout << "plain text: " << plain << endl;

        CTR_Mode< AES >::Encryption e;
        e.SetKeyWithIV(key, sizeof(key), iv);

        // The StreamTransformationFilter adds padding
        //  as required. ECB and CBC Mode must be padded
        //  to the block size of the cipher.
        StringSource(strstr.str(), true, 
            new StreamTransformationFilter(e,
                new StringSink(cipher)
            ) // StreamTransformationFilter      
        ); // StringSource
    }
    catch(const CryptoPP::Exception& e)
    {
        cerr << e.what() << endl;
        //exit(1);
    }

    /*********************************\
    \*********************************/

    // string cipher
    encoded.clear();
    StringSource(cipher, true,
        new HexEncoder(
            new StringSink(encoded)
        ) // HexEncoder
    ); // StringSource


    return crow::response{encoded};
});

    // enables all log
    app.loglevel(crow::LogLevel::DEBUG);
    //crow::logger::setHandler(std::make_shared<ExampleLogHandler>());

    app.port(18080)
        //.multithreaded()
        .run();
        }
};

void MainWindow::checkbit()
{ 
    //update query log
    string buffer,logshow;
    ifstream in("queryinfo.txt");
    int log = 0;

    if ( in.is_open() ){
        while (!in.eof())
        {
            getline(in,buffer);
            if ( log == 0)    {
                logshow = "(Query Information) Patient ID: " + buffer + ";";
            }
            if ( log == 1)    {
                logshow = logshow + "Check-Up No: (" + buffer + "); ";
            }
            if ( log == 2)    {
                logshow = logshow + "Query Type: " + buffer + ";";
            }          
            log++;
        }
    }
    ifstream in1("login.txt");
    in1 >> buffer;
    logshow = logshow + "Username: " + buffer; 
    ui->textBrowser->append(QString::fromStdString(logshow));
 
    vector<string> allaid1;
    vector<string> said1;
    string spid1;
{
    ifstream ifsak("query.txt");
    boost::archive::text_iarchive iak(ifsak);
    iak >> allaid1;
    iak >> said1;
    iak >> spid1;
}
    //clear tableWidget content
    ui->tableWidget->clearContents();

    //display database
    string dbname;
    if (dbtype == 1){
        dbname = "100patient.db";
    }else{
        dbname = "kdtree.db";
    }
    database dpb(dbname);

    //update the interface of database
    int query_number = allaid1.size();
    int i = 0;
    //query data + the whole database
    ui->tableWidget->setRowCount(query_number + accessible_point.size());

    //generate the data button
    //generate multi-button
    PButton *button[query_number];

    //set font in button
    QFont FontObj;
    FontObj.setFamily(QStringLiteral("Helvetica"));
    FontObj.setPointSize(14);
    FontObj.setUnderline(true);

    for (; i < query_number; i++)
    {    
        button[i] = new PButton(this);
        button[i]->setVisible(true);
        QString str = QString::fromStdString(allaid1.at(i));
        button[i]->setText("No." + QString::fromStdString(spid1) + "," + str);
        button[i]->setclick();
        button[i]->setBackgroundRole(QPalette::Base);
        button[i]->setFlat(true);
        button[i]->setFont(FontObj);
        button[i]->setStyleSheet("QPushButton {color: blue}");

        QWidget* pWidget = new QWidget();
        QHBoxLayout* pLayout = new QHBoxLayout(pWidget);
        pLayout->addWidget(button[i]);
        pWidget->setLayout(pLayout);
        pLayout->setAlignment(Qt::AlignCenter);
        pLayout->setContentsMargins(0, 0, 0, 0);
        ui->tableWidget->setCellWidget(i,0 ,pWidget);
        
        Point checkpoint{std::stoi(spid1),std::stoi(allaid1.at(i))};
        //filter inaccessible data
        vector<string>::iterator iter1 = std::find(said1.begin(),said1.end(),allaid1.at(i));
        //filter non-existent data
        vector<Point>::iterator iter = std::find(accessible_point.begin(),accessible_point.end(), checkpoint);

        if (iter == accessible_point.end() || iter1 == said1.end()){
            for (int j = 0; j < 4; j++){
                ui->tableWidget->setItem(i,j,new QTableWidgetItem);
                ui->tableWidget->item(i,j)->setBackgroundColor(QColor(200,0,0));
            }
            if (iter != accessible_point.end()){
                string db_temp;
                auto load = [&](unique_ptr<vector<char>> value){
                   Utils::blob_to_object(*value , db_temp);
                };
                vector<char> focus_s;
                focus_s = Utils::object_to_blob(checkpoint);
                dpb << "select value from Data where point = ?;" << focus_s >> load;

                vector<string> show;
                split(show, db_temp, is_any_of("\\"), token_compress_on);

                for (int j = 0; j < 4; j++){
                    QString tempdata;
                    if (j != 0){
                       tempdata = QString::fromStdString(show[j+6]);
                    }
                    ui->tableWidget->item(i,j)->setText(tempdata);;
                }                    
            }
        }else{
                string db_temp;
                auto load = [&](unique_ptr<vector<char>> value){
                   Utils::blob_to_object(*value , db_temp);
                };
                vector<char> focus_s;
                focus_s = Utils::object_to_blob(checkpoint);
                dpb << "select value from Data where point = ?;" << focus_s >> load;

                vector<string> show;
                split(show, db_temp, is_any_of("\\"), token_compress_on);

                for (int j = 0; j < 4; j++){
                    QString tempdata;
                    if (j != 0){
                       tempdata = QString::fromStdString(show[j+6]);
                    }
                    ui->tableWidget->setItem(i,j,new QTableWidgetItem(tempdata));
                    ui->tableWidget->item(i,j)->setBackgroundColor(QColor(0,200,0));
                }        
        }    
    }

    PButton *button1[accessible_point.size()];

    //display the whole database
    for (int y = 0; y < accessible_point.size(); y++ ){
        Point temp_p = accessible_point[y];

        button1[y] = new PButton(this);
        button1[y]->setVisible(true);
        QString xx = QString::number(temp_p[0]);
        QString yy = QString::number(temp_p[1]);
        button1[y]->setText("No." + xx + "," + yy);
        button1[y]->setclick();
        button1[y]->setBackgroundRole(QPalette::Base);
        button1[y]->setFlat(true);
        button1[y]->setFont(FontObj);
        button1[y]->setStyleSheet("QPushButton {color: blue}");

        QWidget* pWidget = new QWidget();
        QHBoxLayout* pLayout = new QHBoxLayout(pWidget);
        pLayout->addWidget(button1[y]);
        pWidget->setLayout(pLayout);
        pLayout->setAlignment(Qt::AlignCenter);
        pLayout->setContentsMargins(0, 0, 0, 0);
        ui->tableWidget->setCellWidget(y + query_number, 0 ,pWidget);

        //display data
        string db_temp;
        auto load = [&](unique_ptr<vector<char>> value){
            Utils::blob_to_object(*value , db_temp);
        };
        vector<char> focus_s;
        focus_s = Utils::object_to_blob(temp_p);
        dpb << "select value from Data where point = ?;" << focus_s >> load;

        vector<string> show;
        split(show, db_temp, is_any_of("\\"), token_compress_on);

        for (int j = 1; j < 4; j++){
            QString tempdata;
            tempdata = QString::fromStdString(show[j+6]);
            ui->tableWidget->setItem(y + allaid1.size(), j, new QTableWidgetItem(tempdata));
        }  
    }
}

void MainWindow::on_mainexit_2_clicked()
{
    //disable 'start server' button once it is pressed
    ui->mainexit_2->setEnabled(false);
    //Start Server
    dbtype = ui->radioButton_1->isChecked();
    // Output signal to Client
    ofstream ofs("dbtype_flag.txt");
    {
        boost::archive::text_oarchive oa(ofs);
        oa & dbtype;
    }
    WorkerThread *workerThread = new WorkerThread;
    workerThread->start();
}

//change server settings
void MainWindow::on_mainexit_clicked()
{
    //change the value of server database type.   
    dbtype = ui->radioButton_1->isChecked(); 
    
    // Output signal to Client
    ofstream ofs("dbtype_flag.txt");
    {
        boost::archive::text_oarchive oa(ofs);
        oa & dbtype;
    }
}

//attack button / update database
void MainWindow::on_mainexit_3_clicked()
{
    vector<string> allaid1;
    vector<string> said1;
    string spid1;
{
    ifstream ifsak("query.txt");
    boost::archive::text_iarchive iak(ifsak);
    iak >> allaid1;
    iak >> said1;
    iak >> spid1;
}
    //create current map in database
    vector<Point> cur_vec;
    vector<int> sig;
    for (int i = 0; i < allaid1.size(); i++){
        Point temp_data{std::stoi(spid1),std::stoi(allaid1[i])};
        cur_vec.push_back(temp_data);
        vector<Point>::iterator iter = std::find(accessible_point.begin(), accessible_point.end(), temp_data);
        if (iter == accessible_point.end()){
            sig.push_back(i);
        }
    }
    
    for (int i = 0; i < accessible_point.size(); i++){
        vector<string>::iterator iter = std::find(allaid1.begin(), allaid1.end(), to_string(accessible_point[i][1]));
        if (iter != allaid1.end() && to_string(accessible_point[i][0]) == spid1){
            int xxy = i + allaid1.size();
            sig.push_back(xxy);
        }  
    }
    cur_vec.insert(cur_vec.end(),accessible_point.begin(),accessible_point.end());
    
    //Switch database
    string dbname;
    if (dbtype == 1){
        dbname = "100patient.db";
    }else{
        dbname = "kdtree.db";
    }
    database dpb(dbname);

    //for (int i = 0; i < cur_vec.size(); i++){
    for (int i = 0; i < cur_vec.size(); i++){
        //int i = 0;
        //filter 
        vector<int>::iterator iter = std::find(sig.begin(), sig.end(), i);
        if (iter == sig.end()){
            //read data from db
            string db_temp;
            auto load = [&](unique_ptr<vector<char>> value){
                 Utils::blob_to_object(*value , db_temp);
            };      
            vector<char> focus_s;
            focus_s = Utils::object_to_blob(cur_vec[i]);
            dpb << "select value from Data where point = ?;" << focus_s >> load;

            vector<string> show;
            split(show, db_temp, is_any_of("\\"), token_compress_on);

            for (int j = 1; j < 4; j++){
               show[j+6] = ui->tableWidget->item(i,j)->text().toStdString();
            }
            string tempe;
            for (int k = 0; k < show.size()-1; k++){
                tempe += show[k];
                tempe += "\\";
            }   
            //test tempe and db_temp
            //if (tempe == db_temp){
            //    cout << "Wrong!" << endl;
            //}else{
            //    cout << "Correct!" << endl;
            //}

            //update for each row
            std::vector<char> update = Utils::object_to_blob(tempe);
            unique_ptr<vector<char>> value_p = std::make_unique<vector<char>>(update);   
            std::vector<char> focus_c;
            focus_c = Utils::object_to_blob(cur_vec[i]);
            dpb << "UPDATE Data SET value = ? WHERE point = ?;" << value_p << focus_c;
        }
    }
        cout << "attack successfully!" << endl;
        for (int m = 0; m < sig.size(); m++){
            cout << sig[m] << endl;
        }
}

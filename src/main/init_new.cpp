#include "../db_dup/db.hpp"
#include "../db/db.hpp"
#include "../utils/pbc_helper.h"
#include "../db/sig.hpp"
#include "../db/data.hpp"
#include "../db/type.hpp"
#include "../db/rect.hpp"
#include "../abs/boolean_expression.hpp"
#include "../abs/boolean_expression_dnf.hpp"


#include <iostream>
#include <vector>

#include <set>
#include <string>


using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;

struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};


typedef Type<string,int> DB_Type;    


int main(){
    //typedef Type<string,int> DB_Type;

    datarecord teemo;

    for (int i = 0; i < 11; i++)
    {
        teemo.global_roles.insert(i);
    }
    teemo.roles = set<int>{8,4,5};
    teemo.pseudo_role = 0;
    teemo.range = Rectangle({1,1},{100,7});

    DupDatabase<DB_Type> db("test.db");
    db.load(true);
    auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(teemo.roles);

    cout << "load successfully!" << endl;
    std::unordered_map<DB::Point, DB::RawDataRecord<DB_Type>> data;

    string result1;
    for(int m = 1; m <=100; m++)
    {
        for (int n = 1; n <= 7; n++)
        {
            auto r3 = db.range_query(Rectangle({m, n}, {m, n}), teemo.roles);
            for (auto iter = r3.accessible_data.begin(); iter != r3.accessible_data.end(); iter++){
                   RawDataRecord<DB_Type> temp(iter->second.first);
                   result1 += temp.value;
                   result1 += "\\"; }
            DB::Point point({m, n});
            data.insert(make_pair(point, DB::RawDataRecord<DB_Type>(result1, string("1 OR ((8 OR 9) AND (4 AND 5))"))));
        }
    }

    cout << "load data successfully!" << endl;

    //create database
    Database<DB_Type> db1("test1.db");
    db1.create_meta(TYPE_A_PARAM, teemo.pseudo_role, teemo.global_roles, 2, teemo.range);
    db1.create_index(data, IndexType::grid);
    db1.save(true);
    db1.reset_sig();
    db1.create_data_sig();
    db1.create_grid_sig();

    return 0;
}
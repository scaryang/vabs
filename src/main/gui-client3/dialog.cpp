#include "dialog.h"
#include "ui_dialog.h"
#include <iostream>
#include <fstream>
#include <map>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <QTimer>

using namespace std;

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    
    ui->tableWidget_2->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);
    //ui->tableWidget_2->verticalHeader()->setResizeMode(QHeaderView::Stretch);
    ui->tableWidget_2->verticalHeader()->setVisible(false);
    ui->tableWidget_2->horizontalHeader()->resizeSection(0,120);
    ui->tableWidget_2->verticalHeader()->setDefaultSectionSize(30);

    QTableWidgetItem *header2 = new QTableWidgetItem();
    header2->setText("VO Information");
    QTableWidgetItem *header3 = new QTableWidgetItem();
    header3->setText("Check-Up No.");
    ui->tableWidget_2->setHorizontalHeaderItem(1,header2);
    ui->tableWidget_2->setHorizontalHeaderItem(0,header3);


    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
    timer->start(1000);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::timeout()
{
    update();
}

void Dialog::paintEvent(QPaintEvent *)
{
	map<string,string> data;
    string tm;

    ifstream ifsa("voshow.txt");
    boost::archive::text_iarchive ia(ifsa);
    ia >> data;
    ia >> tm;
    int rowindex = 0;
    QString temp;
    QString temp1;

    tm += "ms";
    temp1 = QString::fromStdString(tm);
    ui->textBrowser->setText(temp1);
    ui->textBrowser->setAlignment(Qt::AlignCenter);

    for (auto iter = data.begin(); iter != data.end(); iter ++)
    {
        rowindex++;
        ui->tableWidget_2->setRowCount(rowindex);        
       
        temp = QString::fromStdString(iter->first);
        ui->tableWidget_2->setItem(rowindex-1, 0 , new QTableWidgetItem(temp));
        ui->tableWidget_2->item(rowindex-1, 0)->setTextAlignment(Qt::AlignCenter);

        temp = QString::fromStdString(iter->second);  
        ui->tableWidget_2->setItem(rowindex-1, 1 , new QTableWidgetItem(temp));
    }
}


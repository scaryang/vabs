#include "logindialog.h"
#include "ui_logindialog.h"
#include <QMessageBox>

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    //this->setStyleSheet("border:2px groove gray; border-radius:10px;");
    //this->setStyleSheet ("background-color: rgb(128,128,128);");
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_loginBtn_clicked()
{
    name = ui->usrLineEdit->text().trimmed();
    if(ui->usrLineEdit->text().trimmed() == tr("admin") &&
            ui->pwdLineEdit->text() == tr("123"))
    {
        accept();
    } else {
        QMessageBox::warning(this, tr("Waring"),
                                 tr("User Name or Password Error!"),
                                 QMessageBox::Yes);
    }
    ui->usrLineEdit->clear();
    ui->pwdLineEdit->clear();
    ui->usrLineEdit->setFocus();
}


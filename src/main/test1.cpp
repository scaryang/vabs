#include "../db/db.hpp"
#include "../utils/pbc_helper.h"

#include <iostream>

#include <set>
#include <string>

using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;

struct datarecord{
     datarecord()
     {
     	global_roles = set<int>{0,1,2,3,4,5,6,7,8,9};
     	roles = set<int>{1,2};
     	pseudo_role = 0;
     	range = Rectangle({0,0},{3,3});
     }
     ~datarecord(){}
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};

int main()
{
	datarecord cl;
	typedef Type<int,int> DB_Type;
	Database<DB_Type> db("dbfile.db");
	unordered_map<Point, RawDataRecord<DB_Type>> data{
		{{0,2}, RawDataRecord<DB_Type>(0,string("1"))},
		{{1,2}, RawDataRecord<DB_Type>(1,string("1 OR 2"))},
		{{2,2}, RawDataRecord<DB_Type>(2,string("1 AND 3"))},
		{{3,2}, RawDataRecord<DB_Type>(3,string("3"))},
		{{1,1}, RawDataRecord<DB_Type>(4,string("2"))},
	};
	db.create_meta(TYPE_A_PARAM, cl.pseudo_role, cl.global_roles, 2, cl.range);
	db.create_index(data, IndexType::grid);
	db.save(true);
	db.reset_sig();
	db.create_data_sig();
	db.create_grid_sig();
	auto mvk = db.get_mvk();
    auto inaccessible_policy = db.compute_inaccessible_policy(cl.roles);
	//test equality query;
	cout << "output r1" << endl;
	auto r1 = db.equality_query({1,2},cl.roles);
	cout << r1.accessible << endl;
	cout << r1.verify(mvk,{1,2}, inaccessible_policy) << endl;
	cout << r1.get_vo_size() << endl;
	cout << "---------------" << endl;

	cout << "output r2" << endl;
	auto r2 = db.equality_query({2,2},cl.roles);
	cout << r2.accessible << endl;
	cout << r2.verify(mvk,{2,2}, inaccessible_policy) << endl;
	cout << r2.get_vo_size() << endl;
	cout << "---------------" << endl;

	//test range query
	cout << "test range query result" << endl;
	auto r3 = db.range_query(Rectangle({1, 0}, {3, 2}), cl.roles);
	cout << r3.accessible_data.size() << endl;
	cout << r3.inaccessible_data.size() << endl;
	cout << r3.inaccessible_grid.size() << endl;
	bool r3_result = r3.verify(mvk, Rectangle({1, 0}, {3, 2}), inaccessible_policy);
	cout << r3_result << endl;
	cout << r3.get_vo_size() << endl;
	return 0;
}
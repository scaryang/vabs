#include "pbutton.h"
#include "qmessagebox.h"
#include "dialog2.h"

/*
PButton::PButton(int value, QWidget *parent):QPushButton(parent)
{

}
*/

void PButton::paintEvent(QPaintEvent *p)
{
    QPushButton::paintEvent(p);
}

void PButton::setclick()
{
    QObject::connect(this,SIGNAL(clicked()),this,SLOT(display()));
}

void PButton::display()
{
    //QMessageBox::information(NULL,"test", "hi");
    dialog2->transmit(value);
    dialog2->show();
}

#ifndef PBUTTON
#define PBUTTON

#include <QPushButton>
#include "dialog2.h"

class PButton : public QPushButton
{
    Q_OBJECT

public:
    PButton(int value, QWidget *parent)
    : value(value){}
    void paintEvent(QPaintEvent *);
    void setclick();

public slots:
    void display();
private:
    Dialog2 *dialog2 = new Dialog2;
    int value;
};

#endif // PBUTTON


#include "pbutton.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "logindialog.h"
#include <string>
#include <QTextCodec>
#include "dialog.h"
#include "dialog2.h"
#include <QtCore/QCoreApplication>
#include <qdatetime.h>
#include <QTimer>
#include <cstdlib>
#include <cstdio>
#include <cstring>

#include "../../utils/timer.hpp"
#include "../../utils/pbc_helper.h"
#include "../../utils/serialization_helper.h"

#include <nlohmann/json.hpp>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>
#include "../../db_dup/db.hpp"
#include "../../db/db.hpp"
//#include "./pbc_helper.h"
#include "../../abs/abs.hpp"
#include "../../abs/boolean_expression.hpp"
#include "../../abs/boolean_expression_dnf.hpp"
#include "../../abs/pbc_serialization.hpp"


#include <boost/serialization/utility.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/split_free.hpp>
#include <pbc/pbc.h>

#include "../../db/sig.hpp"
#include "../../db/data.hpp"
#include "../../db/type.hpp"
#include "../../db/rect.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <omp.h>
#include <boost/algorithm/string.hpp>


#include <set>
#include <string>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>

//cryptopp
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;
#include "cryptopp/cryptlib.h"
using CryptoPP::Exception;

#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "cryptopp/filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include "cryptopp/aes.h"
using CryptoPP::AES;

#include "cryptopp/ccm.h"
using CryptoPP::CTR_Mode;

#include "assert.h"

using namespace std;
using namespace pbc;
using namespace ABS;
using namespace DB;
using json = nlohmann::json;
using namespace oabe;
using namespace oabe::crypto;
using namespace boost::algorithm;


struct datarecord{
     set<int> global_roles;
     set<int> roles;
     int pseudo_role;
     Rectangle range;
};

typedef Type<string,int> DB_Type;
typedef unsigned char byte;

ABS::BooleanExpression<int> inac_policy{};
ABS::MasterVerifyingKey mvk;
//DB::DupRangeQueryResult<DB_Type> r3;
//DB::RangeQueryResult<DB_Type> r3;
DB::EqualityQueryResult<DB_Type> r4;
DB::RangeQueryResult<DB_Type> r5;
std::string querypid_str;
std::string queryaid_str;
std::string queryaid_str2;
pbc::PairingPtr pairing;  
int verify_flag; 
DB::Point point1;
DB::Point point2;
string verify_time;


/// Callback must be declared static, otherwise it won't link...
size_t WriteCallback(char* ptr, size_t size, size_t nmemb, void *f)
{
  FILE *file = (FILE *)f;
  return fwrite(ptr, size, nmemb, file);
};

std::string encode_hex(const std::string& src_str,bool upper) 
{
    const static char hex1[]="0123456789abcdEF";
    const static char hex2[]="0123456789ABCDEF";
    const char* hexp=upper?hex2:hex1;
    std::string dst;
    dst.assign(src_str.size()*2,' ');
    for(size_t i=0;i<src_str.size();++i) 
    {
        unsigned char c=(unsigned char)src_str[i];
        dst[i*2+1] = hexp[c&0xF];
        dst[i*2] = hexp[(c>>4)&0xF];
    }
    return dst;
}

MainWindow::MainWindow(QString temp,QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //this->setWindowFlags(Qt::FramelessWindowHint);
    //ui->tableWidget->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);

    //ui->tableWidget_2->horizontalHeader()->setStretchLastSection(QHeaderView::Stretch);

    QPixmap mypixmap;
    mypixmap.load(tr("/home/yang/Pictures/demo/outline-info-24px.svg"));

    ui->mainexit_2->clearMask();
    ui->mainexit_2->setBackgroundRole(QPalette::Base);
    ui->mainexit_2->setFixedSize(mypixmap.width(),mypixmap.height());
    ui->mainexit_2->setIcon(mypixmap);
    ui->mainexit_2->setIconSize(QSize(mypixmap.width(),mypixmap.height()));
    ui->mainexit_2->setFlat(true);

    xyz = temp;
    ui->label_4->setText(xyz);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
    timer->start(1000);

    ui->widget->setStyleSheet("background-color:rgb(135,206,250)");
    //this->setStyleSheet ("background-color: rgb(245,255,250);");
/*
    QString str;
    int i = 0;
    PButton *button[6];
    for (;i<6;i++)
    {
        button[i] = new PButton(this);
        button[i]->setVisible(true);
        str.setNum(i,10);
        button[i]->setText("button"+str);
        ui->verticalLayout->addWidget(button[i]);
        ui->verticalLayout->setStretchFactor(button[i],1);
        //button[i]->setGeometry(10,i*height+3, width,height);
        button[i]->setclick();
        button[i]->setBackgroundRole(QPalette::Base);
        button[i]->setFlat(true);
    }
*/
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->pidlineEdit->clear();
    ui->aidlineEdit->clear();
    ui->pidlineEdit->setFocus();
    ui->label_5->clear();
}

void MainWindow::on_querybtn_clicked()
{
    if (ui->pidlineEdit->text().isEmpty()||ui->aidlineEdit->text().isEmpty())
    {
        QMessageBox::warning(this,tr("warning"),tr("Query information can not be empty!"));
        return;
    }
    querypid = ui->pidlineEdit->text();
    queryaid = ui->aidlineEdit->text();
    // set a value
    //queryaid2 = ui->aidlineEdit->text();

    querypid_str = querypid.toUtf8().constData();
    queryaid_str = queryaid.toUtf8().constData();
    queryaid_str2 = queryaid.toUtf8().constData();

 
    char url[] = "127.0.0.1:18080/upload";
    char filename[] = "results.txt";

    json query;
    query["pid"] = "80AC01B2-BD55-4BE0-A59A-4024104CF4E9";
    //query["pid"] = querypid_str;
    query["aid"] = queryaid_str;
    query["aaid"] = queryaid_str2;

    query["type"] = "Equality Query";

    verify_flag = 0;

    std::string s = query.dump();
    cout << s << endl;
    std::istringstream myStream(s);
    int size = myStream.str().size();

    //set options of file
    char buf[100];
    //set options of client
    curlpp::Cleanup cleaner;
    curlpp::Easy request;

      
    std::list< std::string > headers;
    headers.push_back("Content-Type: text/*"); 
    sprintf(buf, "Content-Length: %d", size); 
    headers.push_back(buf);
      
    using namespace curlpp::Options;
    request.setOpt(new curlpp::options::CustomRequest{"POST"});
    curlpp::options::WriteFunctionCurlFunction
    myFunction(WriteCallback);

    FILE *file = stdout;
    if(filename != NULL)
    {
        file = fopen(filename, "wb");
        if(file == NULL)
        {
           fprintf(stderr, "%s/n", strerror(errno));
           return;
        }
    } 
    curlpp::OptionTrait<void *, CURLOPT_WRITEDATA> 
              myData(file);

    request.setOpt(myFunction);
    request.setOpt(myData);

    request.setOpt(new Verbose(true));
    request.setOpt(new ReadStream(&myStream));
    request.setOpt(new InfileSize(size));
    request.setOpt(new Upload(true));
    request.setOpt(new HttpHeader(headers));
    request.setOpt(new Url(url));
    request.perform();
          
    //remember to close file
    fclose(file);
    file = NULL;

    //time response
    Timer response_tm;
    response_tm.start();

    //verdict dbtype
    int flag;
    ifstream ifskm("dbtype_flag.txt");
    {
        boost::archive::text_iarchive ippkm(ifskm);
        ippkm & flag;
    }

    GlobalPBCPairing::set(TYPE_A_PARAM);
    if (flag == 1)
    {
        ifstream ifs("serial.txt");
        {
            boost::archive::text_iarchive ipp(ifs);
            ipp & mvk;
        }
    }
    else
    {
        ifstream ifs("serial_kd.txt");
        {
            boost::archive::text_iarchive ipp(ifs);
            ipp & mvk;
        }
    }

    // 1. Decode iv: 
    // At the moment our input is encoded in string format...
    // we need it in raw hex: 
    ifstream inx("iv.txt");
    string iv_string((istreambuf_iterator<char>(inx)),istreambuf_iterator<char>());    

    byte iv[CryptoPP::AES::BLOCKSIZE] = {};
    // this decoder would transform our std::string into raw hex:
    CryptoPP::HexDecoder decoder1;
    decoder1.Put((byte*)iv_string.data(), iv_string.size());
    decoder1.MessageEnd();
    decoder1.Get(iv, sizeof(iv));



    InitializeOpenABE();
    OpenABECryptoContext cpabe("CP-ABE");
    string pt2;
    
    //cpabe.generateParams();
    ifstream finx("msk.txt");
    string msk((istreambuf_iterator<char>(finx)),istreambuf_iterator<char>());
    cpabe.importSecretParams(msk);

    ifstream finc("mpk.txt");
    string mpk((istreambuf_iterator<char>(finc)),istreambuf_iterator<char>());
    cpabe.importPublicParams(mpk);
    cpabe.keygen("|attr1|attr2","key0");

    // 3. Decode the key:
    // And finally the same for the key:
    string key_string;
    ifstream inf("key.txt");
    string key_string2((istreambuf_iterator<char>(inf)),istreambuf_iterator<char>());  
    bool result123 = cpabe.decrypt("key0", key_string2, key_string);
    cout << "cp-abe result is: "<< result123 << endl;
    ShutdownOpenABE();


    byte key[CryptoPP::AES::DEFAULT_KEYLENGTH];
    {
        CryptoPP::HexDecoder decoder2;
        decoder2.Put((byte*)key_string.data(), key_string.size());
        decoder2.MessageEnd();
        decoder2.Get(key, sizeof(key));
    }    

    string tempin;
{
    ifstream fin("results.txt");
    //string tempin((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>()); 
    fin >> tempin;
}
    // 2. Decode cipher:
    // Next, we do a similar trick for cipher, only here we would leave raw hex
    //  in a std::string.data(), since it is convenient for us to pass this
    // std::string to the decryptor mechanism:
    std::string cipher_raw;
    {
        CryptoPP::HexDecoder decoder3;
        decoder3.Put((byte*)tempin.data(), tempin.size());
        decoder3.MessageEnd();

        long long size1 = decoder3.MaxRetrievable();
        cipher_raw.resize(size1);       
        decoder3.Get((byte*)cipher_raw.data(), cipher_raw.size());
        // If we print this string it's completely rubbish: 
        // std::cout << "Raw cipher: " << cipher_raw << std::endl;
    }

    // 4. Decrypt:
    std::string decrypted_text;
    
    CryptoPP::CTR_Mode<CryptoPP::AES>::Decryption d;
    d.SetKeyWithIV(key, sizeof(key), iv);

    CryptoPP::StringSource ss(
            cipher_raw, 
            true, 
            new CryptoPP::StreamTransformationFilter(
            d,
            new CryptoPP::StringSink(decrypted_text)
                ) // StreamTransformationFilter
    ); // StringSource

    cout << endl << "AES Decryption successfully!" << endl;


{
    ofstream fout("results7temp.txt");
    fout << decrypted_text;
}
    //string querydata;
    //int response_tm;

{
    ifstream ifsa("results7temp.txt");
    boost::archive::text_iarchive ia(ifsa);

    ia >> r4;
    ia >> inac_policy;
    ia >> point1;
    ia >> point2;
    //ia >> response_tm;
}

    response_tm.stop();
    cout << "response_tm is:" << response_tm.result() << endl;
    string response = to_string(response_tm.result());
    QString response_tm2 = QString::fromStdString(response);
    
    //display results
    //pretty print~

    //cout << "sig is " << r4.sig.output_message() << endl;
    //string result1 = r4.record.value;
    string result1 = "";
    if (r4.accessible)
    {
        result1 = r4.record.value;
    }

    QString filename2("/home/yang/Pictures/aaa/src/main/gui-client/right.png");
    QImage* img = new QImage;
    if (!(img->load(filename2)))
    {
        QMessageBox::information(this,tr("Fail to open image"),tr("Fail to open image!"));
        delete img;
        return;
    }

    QString filename1("/home/yang/Pictures/aaa/src/main/gui-client/wrong.png");
    QImage* img1 = new QImage;
    if (!(img1->load(filename1)))
    {
        QMessageBox::information(this,tr("Fail to open image"),tr("Fail to open image!"));
        delete img1;
        return;
    }

    Timer verify_tm;
    verify_tm.start();
    bool aab_result;

    aab_result = r4.verify(mvk, point1, inac_policy);

    verify_tm.stop();
    int verification_tm = verify_tm.result();
    verify_time = to_string(verification_tm);

    QString verify_tm1 = QString::fromStdString(verify_time);

    if (aab_result == 1)
    {
        if(res_verification.isEmpty())
               res_verification = tr("No Data Received!");
        img->scaled(ui->label_5->size(),Qt::KeepAspectRatio);
        ui->label_5->setScaledContents(true);
        ui->label_5->setPixmap(QPixmap::fromImage(*img));
    }
    else
    {
        if(res_verification.isEmpty())
               res_verification = tr("No Data Received!");
        img1->scaled(ui->label_5->size(),Qt::KeepAspectRatio);
        ui->label_5->setScaledContents(true);
        ui->label_5->setPixmap(QPixmap::fromImage(*img1));
    }

    // vo display
    map<string, string> voshow;
    string aaidd, aaidd1, aaidd2,aaidd3,aaidd4;


    aaidd1 = r4.sig.output_message();
    aaidd2 = encode_hex(aaidd1,0);
    voshow.insert(std::pair<string,string>(queryaid_str, aaidd2));

{
    ofstream ifsak("voshow.txt");
    boost::archive::text_oarchive iak(ifsak);
    //ia >> r3;
    iak << voshow;
    iak << verify_time;
}

    int detail_type = 0 ;
{
    ofstream ifsagk("detail_type.txt");
    boost::archive::text_oarchive iak(ifsagk);
    iak << detail_type;
}  
   
    map<string, string> detail;
    string ppidd, ppidd1;

    if ( r4.accessible == 1){

        ppidd = to_string(point1[1]);
        ppidd1 = r4.record.value;
        detail.insert(std::pair<string,string>(ppidd, ppidd1));
    }

{
    ofstream ifsag("detail.txt");
    boost::archive::text_oarchive iak(ifsag);
    iak << detail;
}  
    
    int show_num;
    if ( r4.accessible == 0)
        show_num = 0;
    else
        show_num =1;

        PButton *button;
        button = new PButton(point1[1],this);
        button->setVisible(true);

        QString ntt_s = QString::fromStdString(to_string(point1[1]));
        button->setText("Record for Check-Up No." + ntt_s);
        ui->verticalLayout->addWidget(button);
        ui->verticalLayout->setStretchFactor(button,1);
        //button[i]->setGeometry(10,i*height+3, width,height);
        button->setclick();
        button->setBackgroundRole(QPalette::Base);
        button->setFlat(true);
    
}


void MainWindow::on_pushButton_3_clicked()
{
    ui->pidlineEdit_2->clear();
    ui->aidlineEdit_2->clear();
    ui->aidlineEdit_3->clear();
    ui->label_5->clear();
    ui->pidlineEdit_2->setFocus();
}

void MainWindow::on_querybtn_2_clicked()
{

    if (ui->pidlineEdit_2->text().isEmpty()||ui->aidlineEdit_2->text().isEmpty()||ui->aidlineEdit_3->text().isEmpty())
    {
        QMessageBox::warning(this,tr("warning"),tr("Query information can not be empty!"));
        return;
    }
    querypid = ui->pidlineEdit_2->text();
    queryaid = ui->aidlineEdit_2->text();
    queryaid2 = ui->aidlineEdit_3->text();

    querypid_str = querypid.toUtf8().constData();
    queryaid_str = queryaid.toUtf8().constData();
    queryaid_str2 = queryaid2.toUtf8().constData();
 
    char url[] = "127.0.0.1:18080/upload";
    char filename[] = "results.txt";

    json query;
    query["pid"] = querypid_str;
    query["aid"] = queryaid_str;
    query["aaid"] = queryaid_str2;
    query["type"] = "Range Query";

    verify_flag = 1;

    std::string s = query.dump();
    cout << s << endl;
    std::istringstream myStream(s);
    int size = myStream.str().size();

    //set options of file
    char buf[100];
    //set options of client
    curlpp::Cleanup cleaner;
    curlpp::Easy request;

      
    std::list< std::string > headers;
    headers.push_back("Content-Type: text/*"); 
    sprintf(buf, "Content-Length: %d", size); 
    headers.push_back(buf);
      
    using namespace curlpp::Options;
    request.setOpt(new curlpp::options::CustomRequest{"POST"});
    curlpp::options::WriteFunctionCurlFunction
    myFunction(WriteCallback);

    FILE *file = stdout;
    if(filename != NULL)
    {
        file = fopen(filename, "wb");
        if(file == NULL)
        {
           fprintf(stderr, "%s/n", strerror(errno));
           return;
        }
    } 
    curlpp::OptionTrait<void *, CURLOPT_WRITEDATA> 
              myData(file);

    request.setOpt(myFunction);
    request.setOpt(myData);

    request.setOpt(new Verbose(true));
    request.setOpt(new ReadStream(&myStream));
    request.setOpt(new InfileSize(size));
    request.setOpt(new Upload(true));
    request.setOpt(new HttpHeader(headers));
    request.setOpt(new Url(url));
    request.perform();
          
    //remember to close file
    fclose(file);
    file = NULL;

    Timer response_tm2;
    response_tm2.start();


    GlobalPBCPairing::set(TYPE_A_PARAM);
    ifstream ifs("serial.txt");
    {
        boost::archive::text_iarchive ipp(ifs);
        ipp & mvk;
    }


    // 1. Decode iv: 
    // At the moment our input is encoded in string format...
    // we need it in raw hex: 
    ifstream inx("iv.txt");
    string iv_string((istreambuf_iterator<char>(inx)),istreambuf_iterator<char>());    

    byte iv[CryptoPP::AES::BLOCKSIZE] = {};
    // this decoder would transform our std::string into raw hex:
    CryptoPP::HexDecoder decoder1;
    decoder1.Put((byte*)iv_string.data(), iv_string.size());
    decoder1.MessageEnd();
    decoder1.Get(iv, sizeof(iv));

    InitializeOpenABE();
    OpenABECryptoContext cpabe("CP-ABE");
    string pt2;
    
    //cpabe.generateParams();
    ifstream finx("msk.txt");
    string msk((istreambuf_iterator<char>(finx)),istreambuf_iterator<char>());
    cpabe.importSecretParams(msk);

    ifstream finc("mpk.txt");
    string mpk((istreambuf_iterator<char>(finc)),istreambuf_iterator<char>());
    cpabe.importPublicParams(mpk);
    cpabe.keygen("|attr1|attr2","key0");


    // 3. Decode the key:
    // And finally the same for the key:
    string key_string;
    ifstream inf("key.txt");
    string key_string2((istreambuf_iterator<char>(inf)),istreambuf_iterator<char>());  
    bool result123 = cpabe.decrypt("key0", key_string2, key_string);
    cout << "cp-abe result is: "<< result123 << endl;
    ShutdownOpenABE();


    byte key[CryptoPP::AES::DEFAULT_KEYLENGTH];
    {
        CryptoPP::HexDecoder decoder2;
        decoder2.Put((byte*)key_string.data(), key_string.size());
        decoder2.MessageEnd();
        decoder2.Get(key, sizeof(key));
    }    

    string tempin;
{
    ifstream fin("results.txt");
    //string tempin((istreambuf_iterator<char>(fin)),istreambuf_iterator<char>()); 
    fin >> tempin;
}
    // 2. Decode cipher:
    // Next, we do a similar trick for cipher, only here we would leave raw hex
    //  in a std::string.data(), since it is convenient for us to pass this
    // std::string to the decryptor mechanism:
    std::string cipher_raw;
    {
        CryptoPP::HexDecoder decoder3;
        decoder3.Put((byte*)tempin.data(), tempin.size());
        decoder3.MessageEnd();

        long long size1 = decoder3.MaxRetrievable();
        cipher_raw.resize(size1);       
        decoder3.Get((byte*)cipher_raw.data(), cipher_raw.size());
    }

    // 4. Decrypt:
    std::string decrypted_text;
    
    CryptoPP::CTR_Mode<CryptoPP::AES>::Decryption d;
    d.SetKeyWithIV(key, sizeof(key), iv);

    CryptoPP::StringSource ss(
            cipher_raw, 
            true, 
            new CryptoPP::StreamTransformationFilter(
            d,
            new CryptoPP::StringSink(decrypted_text)
                ) // StreamTransformationFilter
    ); // StringSource

    cout << endl << "AES Decryption successfully!" << endl;

{
    ofstream fout("results7temp.txt");
    fout << decrypted_text;
}

    response_tm2.stop();
    string response = to_string(response_tm2.result());
    QString response_tm2x = QString::fromStdString(response);

    int gap;
{
    ifstream ifsa("results7temp.txt");
    boost::archive::text_iarchive ia(ifsa);
    //ia >> r3;
    ia >> r5;
    ia >> inac_policy;
    ia >> point1;
    ia >> point2;
    ia >> gap;
}


    QString filename2("right.png");
    QImage* img = new QImage;
    if (!(img->load(filename2)))
    {
        QMessageBox::information(this,tr("Fail to open image"),tr("Fail to open image!"));
        delete img;
        return;
    }

    QString filename1("wrong.png");
    QImage* img1 = new QImage;
    if (!(img1->load(filename1)))
    {
        QMessageBox::information(this,tr("Fail to open image"),tr("Fail to open image!"));
        delete img1;
        return;
    }

    Timer verify_tm;
    verify_tm.start();
    
    //verification
    bool aab_result;
    aab_result = r5.verify(mvk, Rectangle(point1, point2), inac_policy);

    verify_tm.stop();
    int verification_tm = verify_tm.result();
    verify_time = to_string(verification_tm);
    
    //authentication show
    if (aab_result == 1)
    {
        if(res_verification.isEmpty())
               res_verification = tr("No Data Received!");
        img->scaled(ui->label_5->size(),Qt::KeepAspectRatio);
        ui->label_5->setScaledContents(true);
        ui->label_5->setPixmap(QPixmap::fromImage(*img));
    }
    else
    {
        if(res_verification.isEmpty())
               res_verification = tr("No Data Received!");
        img1->scaled(ui->label_5->size(),Qt::KeepAspectRatio);
        ui->label_5->setScaledContents(true);
        ui->label_5->setPixmap(QPixmap::fromImage(*img1));
    }


    // vo display
    map<string, string> voshow;
    string aaidd, aaidd1, aaidd2,aaidd3,aaidd4;

    if ( r5.accessible_data.size() >= 1){
        for (auto iter = r5.accessible_data.begin(); iter != r5.accessible_data.end(); iter++)
        {
            Point temp(iter->first);
            aaidd = to_string(temp[1]);

            //boost::archive::text_oarchive ox(strstr);
            aaidd1 = iter->second.second.output_message();
            aaidd2 = encode_hex(aaidd1,0);

            voshow.insert(std::pair<string,string>( aaidd, aaidd2));
            cout << aaidd << ": " << aaidd2 << endl;
        }
    }

    if ( r5.inaccessible_data.size() >= 1){
        for (auto iter = r5.inaccessible_data.begin(); iter != r5.inaccessible_data.end(); iter++)
        {
            Point temp(iter->first);
            aaidd = to_string(temp[1]);

            aaidd1 = iter->second.output_message();
            aaidd2 = encode_hex(aaidd1,0);

            voshow.insert(std::pair<string,string>( aaidd, aaidd2));
            cout << aaidd << ": " << aaidd2 << endl;
        }
    }

    if ( r5.inaccessible_grid.size() >= 1){
        for (auto iter = r5.inaccessible_grid.begin(); iter != r5.inaccessible_grid.end(); iter++)
        {
            Rectangle temp(iter->first);
            Point temp1 = temp.get_p1();
            Point temp2 = temp.get_p2();

            aaidd3 = to_string(temp1[1]);
            aaidd4 = to_string(temp2[1]);
            aaidd = aaidd3 + "to" + aaidd4;

            aaidd1 = iter->second.output_message();
            aaidd2 = encode_hex(aaidd1,0);

            voshow.insert(std::pair<string,string>( aaidd, aaidd2));
            cout << aaidd << ": " << aaidd2 << endl;
        }
    }

{
    ofstream ifsak("voshow.txt");
    boost::archive::text_oarchive iak(ifsak);
    //ia >> r3;
    iak << voshow;
    iak << verify_time;
}

    map<string, string> detail;
    string ppidd, ppidd1;
    //save all aid
    vector<string> ntt;

    if ( r5.accessible_data.size() >= 1){
        for (auto iter = r5.accessible_data.begin(); iter != r5.accessible_data.end(); iter++)
        {
            Point temp(iter->first);
            ppidd = to_string(temp[1]);
            ntt.push_back(ppidd);
            ppidd1 = iter->second.first.value;
            detail.insert(std::pair<string,string>(ppidd, ppidd1));
        }
    }

{
    ofstream ifsag("detail.txt");
    boost::archive::text_oarchive iak(ifsag);
    iak << detail;
}  

    int detail_type = 1 ;
{
    ofstream ifsagk("detail_type.txt");
    boost::archive::text_oarchive iak(ifsagk);
    iak << detail_type;
}  
    int show_num = r5.accessible_data.size();

    PButton *button[show_num];
    for (int i = 0; i<show_num; i++)
    {
        int value_temp = stoi(ntt[ntt.size()-i-1]);
        button[i] = new PButton(value_temp,this);
        button[i]->setVisible(true);

        QString ntt_s = QString::fromStdString(ntt[ntt.size()-i-1]);
        button[i]->setText("Record for Check-Up No." + ntt_s);
        ui->verticalLayout->addWidget(button[i]);
        ui->verticalLayout->setStretchFactor(button[i],1);
        //button[i]->setGeometry(10,i*height+3, width,height);
        button[i]->setclick();
        button[i]->setBackgroundRole(QPalette::Base);
        button[i]->setFlat(true);
    }


}

void MainWindow::timerUpdate(void)
{
   QDateTime time = QDateTime::currentDateTime();
   QString str = time.toString("yyyy-MM-dd hh:mm:ss");
   ui->label_22->setText(str);
}


void MainWindow::on_mainexit_2_clicked()
{
    dialog->show();
}


void MainWindow::on_actionLogout_triggered()
{
    qApp->exit(2);
}


void MainWindow::on_actionUser_triggered()
{
    QMessageBox *msgBox;
    msgBox = new QMessageBox("User Information",
                             "Admin \nan administrator of the system",
                             QMessageBox::Information,
                             QMessageBox::Ok | QMessageBox::Default,
                             QMessageBox::Cancel | QMessageBox::Escape,
                             0);
    msgBox->show();
}


void MainWindow::on_actionContact_Us_triggered()
{
    QMessageBox *msgBox;
    msgBox = new QMessageBox("About System",
                             "Verifiable Attribute-Based Search System over Shared Data",
                             QMessageBox::Information,
                             QMessageBox::Ok | QMessageBox::Default,
                             QMessageBox::Cancel | QMessageBox::Escape,
                             0);
    msgBox->show();
}

void MainWindow::on_actionContact_Us_2_triggered()
{
    QMessageBox *msgBox;
    msgBox = new QMessageBox("About Us",
                             "Website: https://www.comp.hkbu.edu.hk/~db/",
                             QMessageBox::Information,
                             QMessageBox::Ok | QMessageBox::Default,
                             QMessageBox::Cancel | QMessageBox::Escape,
                             0);
    msgBox->show();
}

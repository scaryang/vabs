// flint is required to be imported before any pbc headers.
#include <flint/fmpz_matxx.h>
#include <flint/fmpzxx.h>

#include <fstream>
#include <pbc/pbc.hpp>
#include <string>

using namespace std;
using namespace pbc;

const PairingParamPtr TYPE_A_PARAM = []() -> PairingParamPtr {
    ifstream f("./a.param", ios::in);
    return PairingParam::init_from_str(
        string(istreambuf_iterator<char>(f), istreambuf_iterator<char>()));
}();

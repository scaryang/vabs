#pragma once

// Ref: http://stackoverflow.com/a/8707598

#include <algorithm>
#include <boost/functional/hash.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/string.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/variant.hpp>
#include <boost/variant/recursive_wrapper.hpp>
#include <iterator>
#include <ostream>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

#include "../utils/sfinae_helper.hpp"

namespace ABS
{
    using boost::variant;
    using boost::recursive_wrapper;
    namespace qi = boost::spirit::qi;
    namespace phx = boost::phoenix;

    template <class T>
    class BooleanGate;

    enum class BooleanGateType { AND, OR };

    template <class T>
    class BooleanExpression
        : public variant<T, recursive_wrapper<BooleanGate<T>>>
    {
    public:
        using variant<T, recursive_wrapper<BooleanGate<T>>>::variant;

        static BooleanExpression parse(const std::string& input)
        {
            auto f(std::begin(input)), l(std::end(input));
            Parser<decltype(f)> p;
            BooleanExpression expr;
            if (!qi::phrase_parse(f, l, p, qi::space, expr))
                throw std::runtime_error("invalid boolean expression: " +
                                         input);
            return expr;
        }

        bool operator==(const BooleanExpression& other) const
        {
            return boost::apply_visitor(EqualCheckVisitor(), *this, other);
        }

        bool operator!=(const BooleanExpression& other) const
        {
            return !operator==(other);
        }

        std::string dump() const
        {
            std::stringstream out;
            out << *this;
            return out.str();
        }

        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<Container,
                                                                 T>::value,
            bool>::type
        eval(const Container& input) const
        {
            return boost::apply_visitor(EvalVisitor<Container>(input), *this);
        }

        template <class Type>
        friend BooleanExpression<Type> operator or(
            const BooleanExpression<Type>&, const BooleanExpression<Type>&);
        template <class Type>
        friend BooleanExpression<Type> operator and(
            const BooleanExpression<Type>&, const BooleanExpression<Type>&);

    private:
        template <class It, class Skipper = qi::space_type>
        class Parser : public qi::grammar<It, BooleanExpression(), Skipper>
        {
        public:
            Parser() : Parser::base_type(expr_)
            {
                using namespace qi;
                expr_ = and_ | or_ | simple_;
                and_op_ = qi::string("AND") | qi::string("and");
                or_op_ = qi::string("OR") | qi::string("or");
                and_ = (simple_ >> +(and_op_ >> simple_))
                    [_val = phx::bind(
                         [](auto& first, auto& rest) {
                             auto all = std::vector<BooleanExpression>();
                             all.reserve(rest.size() + 1);
                             all.push_back(first);
                             all.insert(all.end(), rest.begin(), rest.end());
                             return BooleanGate<T>(all, BooleanGateType::AND);
                         },
                         _1, _2)];
                or_ = (simple_ >> +(or_op_ >> simple_))
                    [_val = phx::bind(
                         [](auto& first, auto& rest) {
                             auto all = std::vector<BooleanExpression>();
                             all.reserve(rest.size() + 1);
                             all.push_back(first);
                             all.insert(all.end(), rest.begin(), rest.end());
                             return BooleanGate<T>(all, BooleanGateType::OR);
                         },
                         _1, _2)];
                gate_ = ((and_op_[_a = BooleanGateType::AND] |
                          or_op_[_a = BooleanGateType::OR]) >>
                         ',' >> '[' >> simple_ >> +(',' >> simple_) >>
                         ']')[_val = phx::bind(
                                  [](auto& type, auto& first, auto& rest) {
                                      auto all =
                                          std::vector<BooleanExpression>();
                                      all.reserve(rest.size() + 1);
                                      all.push_back(first);
                                      all.insert(all.end(), rest.begin(),
                                                 rest.end());
                                      return BooleanGate<T>(all, type);
                                  },
                                  _a, _1, _2)];
                simple_ = ('(' > (gate_ | expr_) > ')') | var_;
                var_ = (raw[lexeme[+qi::char_("0-9a-zA-Z_.")]])
                    [_val = phx::bind(
                         [](auto& input) {
                             return boost::lexical_cast<T>(input);
                         },
                         _1)];
            }

        private:
            qi::rule<It, T(), Skipper> var_;
            qi::rule<It, Skipper> and_op_, or_op_;
            qi::rule<It, BooleanExpression(), Skipper> expr_, and_, or_,
                simple_;
            qi::rule<It, qi::locals<BooleanGateType>, BooleanExpression(),
                     Skipper>
                gate_;
        };

        template <class Container,
                  class = typename std::enable_if<
                      sfinae::is_container_with_fixed_type_and_find_method<
                          Container, T>::value>::type>
        class EvalVisitor : public boost::static_visitor<bool>
        {
        public:
            explicit EvalVisitor(const Container& input) : input_(input) {}
            bool operator()(const T& v) const
            {
                return input_.find(v) != input_.end();
            }

            bool operator()(const BooleanGate<T>& gate) const
            {
                return gate.eval(input_);
            }

        private:
            const Container& input_;
        };

        class EqualCheckVisitor : public boost::static_visitor<bool>
        {
        public:
            template <class T1, typename T2>
            bool operator()(const T1&, const T2&) const
            {
                return false;
            }

            template <typename T1>
            bool operator()(const T1& lhs, const T1& rhs) const
            {
                return lhs == rhs;
            }
        };

        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            boost::serialization::split_member(ar, *this, version);
        }
        template <class Archive>
        void save(Archive& ar, const unsigned int _version) const
        {
            ar << dump();
        }
        template <class Archive>
        void load(Archive& ar, const unsigned int _version)
        {
            std::string str;
            ar >> str;
            auto expr = BooleanExpression<T>::parse(str);
            this->swap(expr);
        }
    };

    template <class T>
    std::ostream& operator<<(std::ostream& o, const BooleanExpression<T>& expr)
    {
        boost::apply_visitor([&o](const auto& input) { o << input; }, expr);
        return o;
    }

    template <class T>
    class BooleanGate
    {
    public:
        BooleanGate() = default;
        BooleanGate(const BooleanGate<T>&) = default;
        explicit BooleanGate(const std::vector<BooleanExpression<T>>& exprs,
                             const BooleanGateType& type)
            : exprs(exprs), type(type)
        {
        }
        std::vector<BooleanExpression<T>> exprs;
        BooleanGateType type;

        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<Container,
                                                                 T>::value,
            bool>::type
        eval(const Container& input) const
        {
            size_t count = 0;
            for (const auto& exp : exprs) count += exp.eval(input) ? 1 : 0;
            return type == BooleanGateType::AND ? count >= exprs.size()
                                                : count >= 1;
        }

        bool operator==(const BooleanGate& other) const
        {
            return type == other.type && exprs.size() == other.exprs.size() &&
                   std::is_permutation(exprs.begin(), exprs.end(),
                                       other.exprs.begin());
        }
    };

    template <class T>
    std::ostream& operator<<(std::ostream& o, const BooleanGate<T>& gate)
    {
        o << "(" << (gate.type == BooleanGateType::AND ? "AND" : "OR") << ", [";
        std::copy(gate.exprs.begin(), gate.exprs.end() - 1,
                  std::ostream_iterator<BooleanExpression<T>>(o, ", "));
        if (!gate.exprs.empty()) o << *gate.exprs.rbegin();
        o << "])";
        return o;
    }
};

namespace std
{
    template <class T>
    struct hash<ABS::BooleanExpression<T>> {
    private:
        class HashVisitor : public boost::static_visitor<size_t>
        {
        public:
            template <class Type>
            size_t operator()(const Type& val) const
            {
                return std::hash<Type>{}(val);
            }
        };

    public:
        size_t operator()(const ABS::BooleanExpression<T>& expr) const
        {
            return boost::apply_visitor(HashVisitor(), expr);
        }
    };

    template <class T>
    struct hash<ABS::BooleanGate<T>> {
        size_t operator()(const ABS::BooleanGate<T>& gate) const
        {
            size_t seed =
                boost::hash_range(gate.exprs.begin(), gate.exprs.end());
            boost::hash_combine(seed, gate.type);
            return seed;
        }
    };
};

namespace boost
{
    template <class T>
    size_t hash_value(const ABS::BooleanExpression<T>& val)
    {
        return std::hash<ABS::BooleanExpression<T>>{}(val);
    }

    template <class T>
    size_t hash_value(const ABS::BooleanGate<T>& gate)
    {
        return std::hash<ABS::BooleanGate<T>>{}(gate);
    }
};

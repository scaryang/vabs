#pragma once

// flint is required to be imported before any pbc headers.
#include <flint/fmpz_matxx.h>
#include <flint/fmpzxx.h>

#include "../utils/sfinae_helper.hpp"
#include "./boolean_expression.hpp"
#include "./key.hpp"
#include "./linear_algebra.hpp"
#include "./monotone_span_program.hpp"
#include "./pbc_serialization.hpp"
#include "./utils.hpp"

#include <algorithm>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <iterator>
#include <pbc/pbc.hpp>
#include <tuple>
#include <vector>

namespace ABS
{
    template <class T>
    class Signature
    {
    public:
        Signature() = default;
        Signature(const Signature<T>&) = default;
        const BooleanExpression<T>& get_predicate() const { return expr; }
        size_t get_size() const
        {
            size_t size = 0;
            size += Y.bytes_length();
            size += W.bytes_length();
            if (!S.empty()) size += S.size() * S[0].bytes_length();
            if (!P.empty()) size += P.size() * P[0].bytes_length();
            size += 16;  // tag (UUID) is 128 bits.
            return size;
        }
        size_t get_size_compressed() const
        {
            size_t size = 0;
            size += Y.compressed_bytes_length();
            size += W.compressed_bytes_length();
            if (!S.empty()) size += S.size() * S[0].compressed_bytes_length();
            if (!P.empty()) size += P.size() * P[0].compressed_bytes_length();
            size += 16;  // tag (UUID) is 128 bits.
            return size;
        }
        std::string get_tag() const { return tag; }
        bool operator==(const Signature<T>& other) const
        {
            return Y == other.Y && W == other.W && S == other.S &&
                   P == other.P && expr == other.expr && tag == other.tag;
        }
        template <class Type>
        friend Signature<Type> sign(const MasterVerifyingKey&,
                                    const SigningKey<Type>&, const std::string&,
                                    const BooleanExpression<Type>&);
        template <class Type>
        friend bool verify(const MasterVerifyingKey&, const Signature<Type>&,
                           const std::string&);
        template <class Type, class Container>
        friend typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<Container,
                                                                 Type>::value,
            Signature<Type>>::type
        relax(const MasterVerifyingKey&, const Signature<Type>&,
              const std::string&, const Container&);

    private:
        pbc::Element Y, W;
        std::vector<pbc::Element> S, P;
        BooleanExpression<T> expr;
        std::string tag;

        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& Y;
            ar& W;
            ar& S;
            ar& P;
            ar& expr;
            ar& tag;
        }
    };

    template <class T>
    Signature<T> sign(const MasterVerifyingKey& mvk, const SigningKey<T>& key,
                      const std::string& message,
                      const BooleanExpression<T>& expr)
    {
        boost::uuids::uuid uuid = boost::uuids::random_generator()();
        std::string tag;
        tag.reserve(uuid.size());
        tag.insert(tag.begin(), uuid.begin(), uuid.end());

        MonotoneSpanProgram<T> msp(expr);
        size_t rows, cols;
        std::tie(rows, cols) = msp.get_size();
        linalg::Integer mod;
        fmpz_set_mpz(mod._fmpz(), mvk.pairing->g1_order().get_mpz_t());
        linalg::Matrix V = msp.compute_vector(key.K_u, mod);

        pbc::Element hash, r0;
        hash = Utils::hash_to_element(tag + message, mvk.pairing);
        Utils::ElementPowerHelper Cg(mvk.C * mvk.g_p->pow(hash), rows);
        r0.init_zr(mvk.pairing);
        r0.random();
        std::vector<pbc::Element> r;
        std::vector<std::unique_ptr<Utils::ElementPowerHelper>> AB;
        r.reserve(rows);
        AB.reserve(rows);
        for (size_t i = 0; i < rows; ++i) {
            r.push_back(
                pbc::Element(mvk.pairing, pbc::ElementType::Zr).random());
            pbc::Element u =
                Utils::hash_to_element(msp.get_label(i), mvk.pairing);
            AB.push_back(std::make_unique<Utils::ElementPowerHelper>(
                mvk.A * mvk.B_p->pow(u), cols));
        }

        Signature<T> s;
        s.expr = expr;
        s.tag = tag;
        s.Y = key.K_base->pow(r0);
        s.W = key.K_0->pow(r0);
        s.S.reserve(rows);
        s.P.reserve(cols);
        for (size_t i = 0; i < rows; ++i) {
            pbc::Element si;
            si = Cg.pow(r[i]);
            if (!V.at(0, i).is_zero()) {
                pbc::Element vi =
                    linalg::integer_to_element(mvk.pairing, V.at(0, i));
                pbc::FixedBasePowerPtr K_u =
                    key.K_u.find(msp.get_label(i))->second;
                si *= K_u->pow(vi * r0);
            }
            s.S.push_back(si);
        }
        for (size_t j = 0; j < cols; ++j) {
            pbc::Element pj;
            pj.init_g2(mvk.pairing);
            pj.set_one();
            for (size_t i = 0; i < rows; ++i) {
                pbc::Element mij = linalg::integer_to_element(
                    mvk.pairing, msp.get_matrix().at(i, j));
                pj *= AB[i]->pow(mij * r[i]);
            }
            s.P.push_back(pj);
        }
        return s;
    }

    template <class T>
    bool verify(const MasterVerifyingKey& mvk, const Signature<T>& signature,
                const std::string& message)
    {
        bool result = true;
        MonotoneSpanProgram<T> msp(signature.expr);
        size_t rows, cols;
        std::tie(rows, cols) = msp.get_size();

        pbc::Element hash, Cg;
        hash = Utils::hash_to_element(signature.tag + message, mvk.pairing);
        Cg = mvk.C * mvk.g_p->pow(hash);

        result &= !signature.Y.is_one();
        result &= pbc::e(signature.W, mvk.A0) == pbc::e(signature.Y, mvk.h0);

        std::vector<std::unique_ptr<Utils::ElementPowerHelper>> AB;
        AB.reserve(rows);
        for (size_t i = 0; i < rows; ++i) {
            pbc::Element u =
                Utils::hash_to_element(msp.get_label(i), mvk.pairing);
            AB.push_back(std::make_unique<Utils::ElementPowerHelper>(
                mvk.A * mvk.B_p->pow(u), cols));
        }
        for (size_t j = 0; j < cols; ++j) {
            std::vector<pbc::Element> ABM(rows);
            for (size_t i = 0; i < rows; ++i) {
                pbc::Element mij = linalg::integer_to_element(
                    mvk.pairing, msp.get_matrix().at(i, j));
                ABM[i] = AB[i]->pow(mij);
            }
            pbc::Element left, right;
            left = pbc::e_prod(signature.S, ABM);
            right = pbc::e(Cg, signature.P[j]);
            if (j == 0) right *= pbc::e(signature.Y, mvk.h);
            result &= left == right;
        }

        return result;
    }

    template <class T>
    class RelaxPurgeWalkerState
    {
    public:
        bool success;
        size_t rows, cols;
        std::vector<T> labels;
        std::vector<size_t> kept_rows;
        std::vector<size_t> kept_cols;
    };

    template <class T, class Container,
              class = typename std::enable_if<
                  sfinae::is_container_with_fixed_type_and_find_method<
                      Container, T>::value>::type>
    class RelaxPurgeWalker
        : public boost::static_visitor<RelaxPurgeWalkerState<T>>
    {
    public:
        explicit RelaxPurgeWalker(const Container& attributes)
            : attributes(attributes)
        {
        }
        RelaxPurgeWalkerState<T> operator()(const T& v) const
        {
            RelaxPurgeWalkerState<T> state;
            state.success = attributes.find(v) != attributes.end();
            state.rows = state.cols = 1;
            state.labels.push_back(v);
            state.kept_rows.push_back(0);
            state.kept_cols.push_back(0);
            return state;
        }
        RelaxPurgeWalkerState<T> operator()(const BooleanGate<T>& gate) const
        {
            RelaxPurgeWalkerState<T> state;
            state.success = gate.type == BooleanGateType::OR;
            state.rows = gate.exprs.size();
            state.cols = gate.type == BooleanGateType::AND ? state.rows : 1;
            state.kept_cols.push_back(0);
            size_t mi = 0;
            for (size_t i = 0; i < gate.exprs.size(); ++i) {
                auto new_state = boost::apply_visitor(*this, gate.exprs[i]);
                state.labels.insert(state.labels.end(),
                                    new_state.labels.begin(),
                                    new_state.labels.end());

                if (new_state.success) {
                    std::transform(new_state.kept_rows.begin(),
                                   new_state.kept_rows.end(),
                                   new_state.kept_rows.begin(),
                                   [mi](size_t idx) { return idx + mi; });
                    std::transform(
                        new_state.kept_cols.begin(), new_state.kept_cols.end(),
                        new_state.kept_cols.begin(), [state](size_t idx) {
                            return idx ? idx + state.cols - 1 : 0;
                        });
                }

                if (gate.type == BooleanGateType::AND) {
                    if (new_state.success) {
                        state.kept_rows.swap(new_state.kept_rows);
                        state.kept_cols.swap(new_state.kept_cols);
                        if (i != 0) state.kept_cols.push_back(i);
                    }
                    state.success |= new_state.success;
                } else {  // BoolGateType::OR
                    if (new_state.success) {
                        state.kept_rows.reserve(state.kept_rows.size() +
                                                new_state.kept_rows.size());
                        std::copy(new_state.kept_rows.begin(),
                                  new_state.kept_rows.end(),
                                  std::back_inserter(state.kept_rows));
                        state.kept_cols.reserve(state.kept_cols.size() +
                                                new_state.kept_cols.size() - 1);
                        std::copy_if(new_state.kept_cols.begin(),
                                     new_state.kept_cols.end(),
                                     std::back_inserter(state.kept_cols),
                                     [](size_t idx) { return idx != 0; });
                    }
                    state.success &= new_state.success;
                }

                state.rows += new_state.rows - 1;
                state.cols += new_state.cols - 1;
                mi += new_state.rows;
            }
            return state;
        }

    private:
        const Container& attributes;
    };

    template <class T, class Container>
    typename std::enable_if<
        sfinae::is_container_with_fixed_type_and_find_method<Container,
                                                             T>::value,
        Signature<T>>::type
    relax(const MasterVerifyingKey& mvk, const Signature<T>& signature,
          const std::string& message, const Container& new_attributes)
    {
        linalg::Integer mod;
        fmpz_set_mpz(mod._fmpz(), mvk.pairing->g1_order().get_mpz_t());
        Signature<T> new_signature;
        std::vector<BooleanExpression<T>> exprs;

        new_signature.tag = signature.tag;
        new_signature.Y = signature.Y;
        new_signature.W = signature.W;
        new_signature.S.reserve(new_attributes.size());
        new_signature.P.reserve(1);
        exprs.reserve(new_attributes.size());

        RelaxPurgeWalkerState<T> state;
        std::map<T, std::vector<size_t>>
            kept_attrs_map;  // kept attrs T <-> kept attrs rows
        // step 1: purge unwanted attributes
        {
            state = boost::apply_visitor(
                RelaxPurgeWalker<T, Container>(new_attributes), signature.expr);
            if (!state.success)
                throw std::runtime_error(
                    "Failed to purge unwanted attributes.");

            for (size_t idx : state.kept_rows) {
                T label = state.labels[idx];
                auto ptr = kept_attrs_map.find(label);
                if (ptr == kept_attrs_map.end())
                    kept_attrs_map.insert(std::pair<T, std::vector<size_t>>(
                        label, std::vector<size_t>{idx}));
                else
                    ptr->second.push_back(idx);
            }

            pbc::Element p;
            p.init_g2(mvk.pairing);
            p.set_one();

            for (size_t idx : state.kept_cols) {
                p *= signature.P[idx];
            }

            new_signature.P.push_back(p);
        }
        // step 2-3:
        {
            pbc::Element hash;
            hash = Utils::hash_to_element(signature.tag + message, mvk.pairing);
            Utils::ElementPowerHelper Cg(
                mvk.C * mvk.g_p->pow(hash),
                new_attributes.size() - kept_attrs_map.size());

            for (const T& label : new_attributes) {
                auto ptr = kept_attrs_map.find(label);
                if (ptr != kept_attrs_map.end()) {
                    // step 2: merge duplicated attributes
                    pbc::Element s;
                    s.init_g1(mvk.pairing);
                    s.set_one();
                    for (size_t idx : ptr->second) s *= signature.S[idx];
                    new_signature.S.push_back(s);
                    exprs.push_back(BooleanExpression<T>(ptr->first));
                } else {
                    // step 3: append missing attributes
                    pbc::Element r;
                    r.init_zr(mvk.pairing);
                    r.random();
                    pbc::Element u = Utils::hash_to_element(label, mvk.pairing);
                    new_signature.S.push_back(Cg.pow(r));
                    new_signature.P[0] *= (mvk.A * mvk.B_p->pow(u)).pow(r);
                    exprs.push_back(BooleanExpression<T>(label));
                }
            }

            if (exprs.size() == 1)
                new_signature.expr = exprs[0];
            else
                new_signature.expr = BooleanExpression<T>(
                    BooleanGate<T>(exprs, BooleanGateType::OR));
        }
        // step 4: randomize signature
        {
            pbc::Element r;
            r.init_zr(mvk.pairing);
            r.random();
            new_signature.Y = new_signature.Y.pow(r);
            new_signature.W = new_signature.W.pow(r);
            for (size_t i = 0; i < new_attributes.size(); ++i)
                new_signature.S[i] = new_signature.S[i].pow(r);
            new_signature.P[0] = new_signature.P[0].pow(r);
        }

        return new_signature;
    }
};

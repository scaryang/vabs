#pragma once

#include <map>
#include <ostream>
#include <string>
#include <tuple>
#include <vector>

#include "../utils/sfinae_helper.hpp"
#include "./boolean_expression.hpp"
#include "./linear_algebra.hpp"

namespace ABS
{
    template <class T>
    class MonotoneSpanProgram
    {
    public:
        MonotoneSpanProgram() = default;
        MonotoneSpanProgram(const MonotoneSpanProgram<T>&) = default;
        explicit MonotoneSpanProgram(const std::string& expr_str)
            : MonotoneSpanProgram(BooleanExpression<T>::parse(expr_str))
        {
        }
        explicit MonotoneSpanProgram(const BooleanExpression<T>& expr)
            : MonotoneSpanProgram(
                  boost::apply_visitor(SpanProgramBuilder(), expr))
        {
        }
        explicit MonotoneSpanProgram(const linalg::MatrixSrcRef m,
                                     const std::vector<T>& labels)
            : m(m), labels(labels)
        {
        }
        explicit MonotoneSpanProgram(linalg::Matrix&& m,
                                     std::vector<T>&& labels)
            : m(0, 0), labels(std::move(labels))
        {
            std::swap(m._mat(), this->m._mat());
        }

        bool operator==(const MonotoneSpanProgram& other) const
        {
            return m == other.m && labels == other.labels;
        }

        std::tuple<size_t, size_t> get_size() const
        {
            return std::make_tuple(m.rows(), m.cols());
        }
        const linalg::MatrixSrcRef get_matrix() const { return m; }
        const std::vector<T>& get_labels() const { return labels; }
        T get_label(size_t idx) const { return labels[idx]; }
        bool eval(const linalg::MatrixSrcRef v,
                  const linalg::IntegerSrcRef mod) const
        {
            if (v.rows() != 1)
                throw std::runtime_error("v.rows() != 1");
            else if (v.cols() != m.rows())
                throw std::runtime_error("v.cols() != m.rows()");

            linalg::Matrix vM = linalg::mod_multiply(v, m, mod);
            bool result = true;
            result &= vM.at(0, 0).is_one();

            for (size_t j = 1; j < (size_t)vM.cols(); ++j)
                result &= vM.at(0, j).is_zero();
            return result;
        }

        template <class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_type_and_find_method<Container,
                                                                 T>::value,
            linalg::Matrix>::type
        compute_vector(const Container& input,
                       const linalg::IntegerSrcRef mod) const
        {
            std::vector<size_t> row_ids;
            row_ids.reserve(labels.size());
            for (size_t i = 0; i < labels.size(); ++i) {
                if (input.find(labels[i]) != input.end()) row_ids.push_back(i);
            }

            if (row_ids.empty())
                throw std::runtime_error(
                    "cannot computer vector, row_ids is empty.");

            linalg::Matrix A(m.cols(), row_ids.size());
            linalg::Matrix b = linalg::Matrix::zero(m.cols(), 1);
            b.at(0, 0).set_one();
            for (size_t i = 0; i < (size_t)A.rows(); ++i) {
                for (size_t j = 0; j < (size_t)A.cols(); ++j)
                    A.at(i, j) = m.at(row_ids[j], i);
            }
            linalg::Matrix x = linalg::solve_linear_diophantine(A, b, mod);
            linalg::Matrix result = linalg::Matrix::zero(1, labels.size());
            for (size_t i = 0; i < (size_t)x.rows(); ++i)
                result.at(0, row_ids[i]) = x.at(i, 0);

            return result.evaluate();
        }

    private:
        linalg::Matrix m;
        std::vector<T> labels;

        // Ref: V. Nikov and S. Nikova, “New Monotone Span Programs from Old.,”
        //      IACR, 2004.
        class SpanProgramBuilder
            : public boost::static_visitor<MonotoneSpanProgram>
        {
        public:
            MonotoneSpanProgram operator()(const T& v) const
            {
                return MonotoneSpanProgram(std::move(linalg::Matrix::one(1, 1)),
                                           std::move(std::vector<T>{v}));
            }
            MonotoneSpanProgram operator()(const BooleanGate<T>& gate) const
            {
                size_t rows = gate.exprs.size();
                size_t cols = gate.type == BooleanGateType::AND ? rows : 1;
                linalg::Matrix m(rows, cols);
                if (gate.type == BooleanGateType::AND) {
                    for (size_t i = 0; i < rows; ++i) m.at(i, i).set_one();
                    for (size_t j = 1; j < cols; ++j)
                        m.at(0, j) = linalg::Integer(-1);
                } else {
                    for (size_t i = 0; i < rows; ++i) m.at(i, 0).set_one();
                }

                std::vector<T> labels;

                size_t mi = 0;
                for (size_t i = 0; i < gate.exprs.size(); ++i) {
                    MonotoneSpanProgram msp(gate.exprs[i]);
                    labels.insert(labels.end(), msp.labels.begin(),
                                  msp.labels.end());

                    size_t m_rows = m.rows();
                    size_t m_cols = m.cols();
                    size_t msp_m_rows = msp.m.rows();
                    size_t msp_m_cols = msp.m.cols();
                    size_t new_m_rows = m_rows + msp_m_rows - 1;
                    size_t new_m_cols = m_cols + msp_m_cols - 1;

                    linalg::Matrix new_m(new_m_rows, new_m_cols);

                    for (size_t _i = 0; _i < mi; ++_i) {
                        for (size_t _j = 0; _j < m_cols; ++_j)
                            new_m.at(_i, _j) = m.at(_i, _j);
                        for (size_t _j = m_cols; _j < new_m_cols; ++_j)
                            new_m.at(_i, _j).set_zero();
                    }

                    for (size_t _i = mi; _i < mi + msp_m_rows; ++_i) {
                        for (size_t _j = 0; _j < m_cols; ++_j)
                            new_m.at(_i, _j) =
                                msp.m.at(_i - mi, 0) * m.at(mi, _j);
                        for (size_t _j = m_cols; _j < new_m_cols; ++_j)
                            new_m.at(_i, _j) =
                                msp.m.at(_i - mi, _j - m_cols + 1);
                    }

                    for (size_t _i = mi + msp_m_rows; _i < new_m_rows; ++_i) {
                        for (size_t _j = 0; _j < m_cols; ++_j)
                            new_m.at(_i, _j) = m.at(_i - msp_m_rows + 1, _j);
                        for (size_t _j = m_cols; _j < new_m_cols; ++_j)
                            new_m.at(_i, _j).set_zero();
                    }

                    mi += msp_m_rows;
                    std::swap(m._mat(), new_m._mat());
                }

                return MonotoneSpanProgram(std::move(m), std::move(labels));
            }
        };
    };

    template <class T>
    std::ostream& operator<<(std::ostream& o, const MonotoneSpanProgram<T>& msp)
    {
        const auto& m = msp.get_matrix();

        o << "MSP[" << m.rows() << ", " << m.cols() << "](" << std::endl;
        for (size_t i = 0; i < (size_t)m.rows(); ++i) {
            o << msp.get_label(i) << ":";
            for (size_t j = 0; j < (size_t)m.cols(); ++j) {
                if (j != 0) o << " ";
                o << m.at(i, j);
            }
            o << std::endl;
        }

        o << ")";
        return o;
    }
};

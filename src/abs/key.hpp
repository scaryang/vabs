#pragma once

// flint is required to be imported before any pbc headers.
#include <flint/fmpz_matxx.h>
#include <flint/fmpzxx.h>

#include <boost/serialization/map.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_member.hpp>
#include <map>
#include <pbc/pbc.hpp>

#include "../utils/sfinae_helper.hpp"
#include "./pbc_serialization.hpp"
#include "./utils.hpp"

namespace ABS
{
    class MasterSigningKey
    {
    public:
        MasterSigningKey() = default;
        MasterSigningKey(const MasterSigningKey&) = default;
        bool operator==(const MasterSigningKey& other) const
        {
            return a0 == other.a0 && a == other.a && b == other.b;
        }
        pbc::Element a0, a, b;

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            ar& a0;
            ar& a;
            ar& b;
        }
    };

    class MasterVerifyingKey
    {
    public:
        MasterVerifyingKey() = default;
        MasterVerifyingKey(const MasterVerifyingKey&) = default;
        bool operator==(const MasterVerifyingKey& other) const
        {
            return g == other.g && h0 == other.h0 && h == other.h &&
                   A0 == other.A0 && A == other.A && B == other.B &&
                   C == other.C;
        }
        pbc::Element g, h0, h, A0, A, B, C;
        pbc::FixedBasePowerPtr g_p, h0_p, h_p, B_p;
        pbc::PairingPtr pairing;

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            boost::serialization::split_member(ar, *this, version);
        }
        template <class Archive>
        void save(Archive& ar, const unsigned int _version) const
        {
            ar << g << h0 << h << A0 << A << B << C;
        }
        template <class Archive>
        void load(Archive& ar, const unsigned int _version)
        {
            ar >> g >> h0 >> h >> A0 >> A >> B >> C;
            g_p = std::make_shared<pbc::FixedBasePower>(g);
            h0_p = std::make_shared<pbc::FixedBasePower>(h0);
            h_p = std::make_shared<pbc::FixedBasePower>(h);
            B_p = std::make_shared<pbc::FixedBasePower>(B);
            pairing = g.pairing();
        }
    };

    template <class T>
    class SigningKey
    {
    public:
        SigningKey() = default;
        SigningKey(const SigningKey&) = default;
        pbc::FixedBasePowerPtr K_base, K_0;
        std::map<T, pbc::FixedBasePowerPtr> K_u;
        bool operator==(const SigningKey<T>& other) const
        {
            if (K_base->get_base() != other.K_base->get_base()) return false;
            if (K_0->get_base() != other.K_0->get_base()) return false;
            if (K_u.size() != other.K_u.size()) return false;
            auto p1 = K_u.begin();
            auto p2 = other.K_u.begin();
            for (; p1 != K_u.end(); ++p1, ++p2) {
                if (p1->first != p2->first) return false;
                if (p2->second->get_base() != p2->second->get_base())
                    return false;
            }
            return true;
        }

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            boost::serialization::split_member(ar, *this, version);
        }
        template <class Archive>
        void save(Archive& ar, const unsigned int _version) const
        {
            ar << K_base->get_base() << K_0->get_base();
            std::map<T, pbc::Element> K_u_elements;
            for (auto& val : K_u)
                K_u_elements.insert(
                    std::make_pair(val.first, val.second->get_base()));
            ar << K_u_elements;
        }
        template <class Archive>
        void load(Archive& ar, const unsigned int _version)
        {
            pbc::Element K_base_element, K_0_element;
            std::map<T, pbc::Element> K_u_elements;
            ar >> K_base_element >> K_0_element >> K_u_elements;
            K_base = std::make_shared<pbc::FixedBasePower>(K_base_element);
            K_0 = std::make_shared<pbc::FixedBasePower>(K_0_element);
            for (auto& val : K_u_elements)
                K_u.insert(std::make_pair(
                    val.first,
                    std::make_shared<pbc::FixedBasePower>(val.second)));
        }
    };

    class KeyDistributionCenter
    {
    public:
        KeyDistributionCenter() = default;
        KeyDistributionCenter(const KeyDistributionCenter&) = default;
        void setup(const pbc::PairingPtr& pairing)
        {
            mvk.pairing = pairing;
            mvk.g.init_g1(pairing);
            mvk.g.random();
            mvk.g_p = std::make_shared<pbc::FixedBasePower>(mvk.g);
            mvk.C.init_g1(pairing);
            mvk.C.random();
            mvk.h0.init_g2(pairing);
            mvk.h0.random();
            mvk.h0_p = std::make_shared<pbc::FixedBasePower>(mvk.h0);
            mvk.h.init_g2(pairing);
            mvk.h.random();
            mvk.h_p = std::make_shared<pbc::FixedBasePower>(mvk.h);
            msk.a0.init_zr(pairing);
            msk.a0.random();
            msk.a.init_zr(pairing);
            msk.a.random();
            msk.b.init_zr(pairing);
            msk.b.random();
            mvk.A0 = mvk.h0_p->pow(msk.a0);
            mvk.A = mvk.h_p->pow(msk.a);
            mvk.B = mvk.h_p->pow(msk.b);
            mvk.B_p = std::make_shared<pbc::FixedBasePower>(mvk.B);
        }

        template <class T, class Container>
        typename std::enable_if<
            sfinae::is_container_with_fixed_value_type<Container, T>::value,
            SigningKey<T>>::type
        generate_key(const Container& attributes) const
        {
            SigningKey<T> key;
            pbc::Element k_base, k_0;
            k_base.init_g1(mvk.pairing);
            k_base.random();
            key.K_base = std::make_shared<pbc::FixedBasePower>(k_base);
            k_0 = key.K_base->pow(msk.a0.inverse());
            key.K_0 = std::make_shared<pbc::FixedBasePower>(k_0);
            for (T a : attributes) {
                pbc::Element u = Utils::hash_to_element(a, mvk.pairing);
                u = (msk.a + msk.b * u).inverse();
                key.K_u.insert(std::pair<T, pbc::FixedBasePowerPtr>(
                    a,
                    std::make_shared<pbc::FixedBasePower>(key.K_base->pow(u))));
            }
            return key;
        }

        const MasterVerifyingKey& get_master_verifying_key() const
        {
            return mvk;
        }
        const MasterSigningKey& get_master_signing_key() const { return msk; }

    private:
        MasterSigningKey msk;
        MasterVerifyingKey mvk;
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& msk;
            ar& mvk;
        }
    };
};

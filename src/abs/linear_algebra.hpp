#pragma once

// flint is required to be imported before any pbc headers.
#include <flint/fmpz_matxx.h>
#include <flint/fmpzxx.h>

#include <gmpxx.h>
#include <ostream>
#include <pbc/pbc.hpp>
#include <tuple>
#include <utility>

namespace ABS
{
    namespace linalg
    {
        typedef flint::fmpzxx Integer;
        typedef flint::fmpz_matxx Matrix;
        typedef flint::fmpzxx_srcref IntegerSrcRef;
        typedef flint::fmpz_matxx_srcref MatrixSrcRef;

        std::ostream& operator<<(std::ostream& o, const Integer& i)
        {
            o << i.to_string();
            return o;
        }
        std::ostream& operator<<(std::ostream& o, const Matrix& m)
        {
            o << "[";
            for (size_t i = 0; i < (size_t)m.rows(); ++i) {
                if (i != 0) o << "; ";
                for (size_t j = 0; j < (size_t)m.cols(); ++j) {
                    if (j != 0) o << " ";
                    o << m.at(i, j);
                }
            }
            o << "]";
            return o;
        }

        pbc::Element integer_to_element(const pbc::PairingPtr& pairing,
                                        const IntegerSrcRef input)
        {
            mpz_class temp;
            fmpz_get_mpz(temp.get_mpz_t(), input._fmpz());
            return pbc::Element::from_mpz_class(pairing, temp);
        }

        // return A*B (mod m)
        Matrix mod_multiply(const MatrixSrcRef A, const MatrixSrcRef B,
                            const IntegerSrcRef m)
        {
            Matrix C = (A * B).evaluate();
            for (size_t i = 0; i < (size_t)C.rows(); ++i) {
                for (size_t j = 0; j < (size_t)C.cols(); ++j)
                    C.at(i, j) = C.at(i, j) % m;
            }
            return C.evaluate();
        }

        // compute smith normal form of A as D.
        // return tuple<D, L, R> s.t. D = LAR.
        // Ref: R. Kannan and A. Bachem, Polynomial algorithms for computing
        //      and the smith and hermite normal forms of an integer matrix,
        //      SIAM J. Computation 9 (1979), 499–507.
        // https://math.berkeley.edu/~mcivor/math115su12/lectures/lecture23.pdf
        // https://github.com/wbhart/flint2/blob/flint-3.0/fmpz_mat/snf_kannan_bachem.c
        std::tuple<Matrix, Matrix, Matrix> smith_normal_form(
            const MatrixSrcRef A)
        {
            size_t m = A.rows();
            size_t n = A.cols();
            size_t d = std::min(m, n);
            Matrix D(A);
            Matrix L = Matrix::one(m, m);
            Matrix R = Matrix::one(n, n);
            Integer g, u, v, r1g, r2g, a, b;
            bool col_done;

            for (size_t k = 0; k < d; ++k) {
                do {
                    // clear column
                    for (size_t i = k + 1; i < m; ++i) {
                        // reduce row i - 1 with row i
                        if (D.at(i - 1, k).is_zero()) continue;
                        if (D.at(i - 1, k).abs() == D.at(i, k).abs()) {
                            if (D.at(i - 1, k) == D.at(i, k)) {
                                for (size_t j = k; j < n; ++j)
                                    D.at(i - 1, j) -= D.at(i, j);
                                for (size_t j = 0; j < m; ++j)
                                    L.at(i - 1, j) -= L.at(i, j);
                            } else {
                                for (size_t j = k; j < n; ++j)
                                    D.at(i - 1, j) += D.at(i, j);
                                for (size_t j = 0; j < m; ++j)
                                    L.at(i - 1, j) += L.at(i, j);
                            }
                            continue;
                        }
                        flint::ltupleref(g, u, v) =
                            xgcd(D.at(i, k), D.at(i - 1, k));
                        r1g = D.at(i, k).divexact(g).evaluate();
                        r2g = D.at(i - 1, k).divexact(g).evaluate();
                        for (size_t j = k; j < n; ++j) {
                            a = (D.at(i - 1, j) * r1g - D.at(i, j) * r2g)
                                    .evaluate();
                            b = (D.at(i, j) * u + D.at(i - 1, j) * v)
                                    .evaluate();
                            D.at(i - 1, j) = a;
                            D.at(i, j) = b;
                        }
                        for (size_t j = 0; j < m; ++j) {
                            a = (L.at(i - 1, j) * r1g - L.at(i, j) * r2g)
                                    .evaluate();
                            b = (L.at(i, j) * u + L.at(i - 1, j) * v)
                                    .evaluate();
                            L.at(i - 1, j) = a;
                            L.at(i, j) = b;
                        }
                    }
                    std::swap(L._mat()->rows[m - 1], L._mat()->rows[k]);
                    std::swap(D._mat()->rows[m - 1], D._mat()->rows[k]);

                    // clear row
                    for (size_t j = k + 1; j < n; ++j) {
                        // reduce col j with col k
                        if (D.at(k, j).is_zero()) continue;
                        if (D.at(k, j).abs() == D.at(k, k).abs()) {
                            if (D.at(k, j) == D.at(k, k)) {
                                for (size_t i = k; i < m; ++i)
                                    D.at(i, j) -= D.at(i, k);
                                for (size_t i = 0; i < n; ++i)
                                    R.at(i, j) -= R.at(i, k);
                            } else {
                                for (size_t i = k; i < m; ++i)
                                    D.at(i, j) += D.at(i, k);
                                for (size_t i = 0; i < n; ++i)
                                    R.at(i, j) += R.at(i, k);
                            }
                            continue;
                        }
                        flint::ltupleref(g, u, v) =
                            xgcd(D.at(k, k), D.at(k, j));
                        r1g = D.at(k, k).divexact(g).evaluate();
                        r2g = D.at(k, j).divexact(g).evaluate();
                        for (size_t i = k; i < m; ++i) {
                            a = (D.at(i, j) * r1g - D.at(i, k) * r2g)
                                    .evaluate();
                            b = (D.at(i, k) * u + D.at(i, j) * v).evaluate();
                            D.at(i, j) = a;
                            D.at(i, k) = b;
                        }
                        for (size_t i = 0; i < n; ++i) {
                            a = (R.at(i, j) * r1g - R.at(i, k) * r2g)
                                    .evaluate();
                            b = (R.at(i, k) * u + R.at(i, j) * v).evaluate();
                            R.at(i, j) = a;
                            R.at(i, k) = b;
                        }
                    }

                    col_done = true;
                    for (size_t i = 0; i < m && i != k; ++i) {
                        if (!D.at(i, k).is_zero()) {
                            col_done = false;
                            break;
                        }
                    }
                } while (!col_done);
            }

            return std::make_tuple(D, L, R);
        }

        // solve linear Diophantine equations Ax = b (mod m),
        // using Smith normal form.
        // Ref:
        // https://math.berkeley.edu/~mcivor/math115su12/lectures/lecture23.pdf
        Matrix solve_linear_diophantine(const MatrixSrcRef A,
                                        const MatrixSrcRef b,
                                        const IntegerSrcRef m)
        {
            if (b.cols() != 1)
                throw std::runtime_error("b.cols() != 1");
            else if (A.rows() != b.rows())
                throw std::runtime_error("A.rows() != b.rows()");

            auto snf = smith_normal_form(A);
            Matrix D = std::get<0>(snf);
            Matrix L = std::get<1>(snf);
            Matrix R = std::get<2>(snf);
            Matrix c = (L * b).evaluate();
            Matrix y = Matrix::zero(A.cols(), 1);
            for (size_t i = 0; i < (size_t)D.rank(); ++i)
                y.at(i, 0) = c.at(i, 0) * (D.at(i, i).invmod(m));
            for (size_t i = D.rank(); i < (size_t)c.rows(); ++i) {
                if (!c.at(i, 0).is_zero())
                    throw std::runtime_error("system has no solution.");
            }
            return mod_multiply(R, y, m).evaluate();
        }
    };
};

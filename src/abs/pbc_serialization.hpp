#pragma once

// flint is required to be imported before any pbc headers.
#include <flint/fmpz_matxx.h>
#include <flint/fmpzxx.h>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/string.hpp>
#include <pbc/pbc.hpp>
#include <string>

namespace ABS
{
    template <typename T>
    struct static_holder {
        static T resource;
    };
    template <typename T>
    T static_holder<T>::resource;

    class GlobalPBCPairing : private static_holder<pbc::PairingPtr>
    {
    public:
        static void set(const std::string& param)
        {
            set(pbc::Pairing::init_from_param_str(param));
        }
        static void set(const pbc::PairingParamPtr& ptr)
        {
            set(pbc::Pairing::init_from_param(ptr));
        }
        static void set(const pbc::PairingPtr& ptr) { resource = ptr; }
        static void clear() { resource.reset(); }
        static pbc::PairingPtr& get() { return resource; }
    };
};

namespace boost
{
    namespace serialization
    {
        template <class Archive>
        void serialize(Archive& ar, pbc::Element& e, const unsigned int version)
        {
            boost::serialization::split_free(ar, e, version);
        }
        template <class Archive>
        void save(Archive& ar, const pbc::Element& e,
                  const unsigned int _version)
        {
            ar << e.type();
            if (e.type() == pbc::ElementType::G1 ||
                e.type() == pbc::ElementType::G2)
                ar << e.to_bytes_compressed();
            else
                ar << e.to_bytes();
        }
        template <class Archive>
        void load(Archive& ar, pbc::Element& e, const unsigned int _version)
        {
            auto pairing = ABS::GlobalPBCPairing::get();
            if (!pairing) throw std::runtime_error("invalid global pairing.");
            std::string buf;
            pbc::ElementType type;
            ar >> type >> buf;
            if (type == pbc::ElementType::G1 || type == pbc::ElementType::G2)
                pbc::Element::from_bytes_compressed(pairing, type, buf).swap(e);
            else
                pbc::Element::from_bytes(pairing, type, buf).swap(e);
        }
    };
};

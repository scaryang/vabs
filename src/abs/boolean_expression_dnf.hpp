#pragma once

#include <boost/functional/hash.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/unordered_set.hpp>
#include <initializer_list>
#include <unordered_set>
#include <vector>

#include "./boolean_expression.hpp"

namespace ABS
{
    template <class T>
    class BooleanValueAndExpr : public std::unordered_set<T>
    {
    public:
        using std::unordered_set<T>::unordered_set;

        // return true if this \subset other
        bool is_subset_of(const BooleanValueAndExpr& other) const
        {
            for (auto ptr = this->begin(); ptr != this->end(); ++ptr) {
                if (other.find(*ptr) == other.end()) return false;
            }
            return true;
        }

        BooleanValueAndExpr operator and(const BooleanValueAndExpr& other) const
        {
            BooleanValueAndExpr result(*this);
            result.insert(other.begin(), other.end());
            return result;
        }

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& boost::serialization::base_object<std::unordered_set<T>>(*this);
        }
    };

    template <class T>
    class BooleanExpressionDNF
    {
    public:
        BooleanExpressionDNF() = default;
        BooleanExpressionDNF(const BooleanExpressionDNF<T>&) = default;
        BooleanExpressionDNF(
            const std::initializer_list<std::initializer_list<T>>& l)
        {
            dnf.reserve(l.size());
            for (const auto& _l : l) dnf.insert(BooleanValueAndExpr<T>(_l));
        }
        BooleanExpressionDNF(
            const std::unordered_set<BooleanValueAndExpr<T>>& dnf)
            : dnf(dnf)
        {
        }
        BooleanExpressionDNF(const BooleanExpression<T>& expr)
            : BooleanExpressionDNF(boost::apply_visitor(DNFBuilder(), expr))
        {
        }
        void simplify()
        {
            std::unordered_set<BooleanValueAndExpr<T>> new_dnf;
            new_dnf.reserve(dnf.size());
            for (auto ptr = dnf.begin(); ptr != dnf.end(); ++ptr) {
                bool skip = false;
                for (auto new_ptr = new_dnf.begin(); new_ptr != new_dnf.end();
                     ++new_ptr) {
                    if (ptr->is_subset_of(*new_ptr)) {
                        new_dnf.erase(new_ptr);
                        break;
                    } else if (new_ptr->is_subset_of(*ptr)) {
                        skip = true;
                        break;
                    }
                }

                if (skip) continue;
                new_dnf.insert(*ptr);
            }
            dnf.swap(new_dnf);
        }
        BooleanExpressionDNF operator or(
            const BooleanExpressionDNF& other) const
        {
            if (empty())
                return other;
            else if (other.empty())
                return *this;
            BooleanExpressionDNF result(*this);
            result.dnf.insert(other.dnf.begin(), other.dnf.end());
            result.simplify();
            return result;
        }
        BooleanExpressionDNF operator and(
            const BooleanExpressionDNF& other) const
        {
            if (empty())
                return other;
            else if (other.empty())
                return *this;
            BooleanExpressionDNF result;
            for (auto ptr = dnf.begin(); ptr != dnf.end(); ++ptr) {
                for (auto other_ptr = other.dnf.begin();
                     other_ptr != other.dnf.end(); ++other_ptr) {
                    result.dnf.insert((*ptr) and (*other_ptr));
                }
            }
            result.simplify();
            return result;
        }
        BooleanExpression<T> to_expr() const
        {
            BooleanGate<T> result;
            result.type = BooleanGateType::OR;
            result.exprs.reserve(dnf.size());
            for (auto& and_expr : dnf) {
                if (and_expr.size() == 1) {
                    result.exprs.push_back(*and_expr.begin());
                } else if (and_expr.size() > 1) {
                    BooleanGate<T> and_gate;
                    and_gate.type = BooleanGateType::AND;
                    and_gate.exprs.reserve(and_expr.size());
                    for (const T& val : and_expr) and_gate.exprs.push_back(val);
                    result.exprs.push_back(BooleanExpression<T>(and_gate));
                }
            }
            if (result.exprs.size() == 0)
                return BooleanExpression<T>();
            else if (result.exprs.size() == 1)
                return result.exprs[0];
            else
                return BooleanExpression<T>(result);
        }
        bool operator==(const BooleanExpressionDNF& other) const
        {
            return to_expr() == other.to_expr();
        }
        bool operator!=(const BooleanExpressionDNF& other) const
        {
            return !operator==(other);
        }
        bool empty() const { return dnf.empty(); }
        size_t size() const { return dnf.size(); }
        std::unordered_set<BooleanValueAndExpr<T>> get_dnf() const
        {
            return dnf;
        }
        BooleanExpressionDNF cap(const BooleanExpressionDNF& other) const
        {
            BooleanExpressionDNF result;
            for (auto ptr = dnf.begin(); ptr != dnf.end(); ++ptr) {
                if (other.dnf.find(*ptr) != other.dnf.end())
                    result.dnf.insert(*ptr);
            }
            return result;
        }
        BooleanExpressionDNF cup(const BooleanExpressionDNF& other) const
        {
            BooleanExpressionDNF result(*this);
            result.dnf.insert(other.dnf.begin(), other.dnf.end());
            return result;
        }

    private:
        std::unordered_set<BooleanValueAndExpr<T>> dnf;

        class DNFBuilder : public boost::static_visitor<BooleanExpressionDNF>
        {
        public:
            BooleanExpressionDNF operator()(const T& v) const
            {
                return BooleanExpressionDNF{{v}};
            }
            BooleanExpressionDNF operator()(const BooleanGate<T>& gate) const
            {
                BooleanExpressionDNF result;
                if (gate.type == BooleanGateType::OR) {
                    for (auto expr : gate.exprs)
                        result = result or boost::apply_visitor(*this, expr);
                } else {
                    for (auto expr : gate.exprs)
                        result = result and boost::apply_visitor(*this, expr);
                }
                return result;
            }
        };
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive& ar, const unsigned int _version)
        {
            ar& dnf;
        }
    };

    template <class T>
    std::ostream& operator<<(std::ostream& o, const BooleanExpressionDNF<T> dnf)
    {
        o << dnf.to_expr();
        return o;
    }

    template <class T>
    BooleanExpression<T> operator or(const BooleanExpression<T>& a,
                                     const BooleanExpression<T>& b)
    {
        BooleanExpressionDNF<T> _a(a), _b(b);
        return (_a or _b).to_expr();
    }

    template <class T>
    BooleanExpression<T> operator and(const BooleanExpression<T>& a,
                                      const BooleanExpression<T>& b)
    {
        BooleanExpressionDNF<T> _a(a), _b(b);
        return (_a and _b).to_expr();
    }
};

namespace std
{
    template <class T>
    struct hash<ABS::BooleanValueAndExpr<T>> {
        size_t operator()(const ABS::BooleanValueAndExpr<T>& and_expr) const
        {
            return boost::hash_range(and_expr.begin(), and_expr.end());
        }
    };
}

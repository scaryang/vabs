#pragma once

// flint is required to be imported before any pbc headers.
#include <flint/fmpz_matxx.h>
#include <flint/fmpzxx.h>

#include <cryptopp/sha.h>
#include <algorithm>
#include <functional>
#include <pbc/pbc.hpp>
#include <string>
#include <vector>

namespace ABS
{
    namespace Utils
    {
        // hash a value and convert it into an element in Zr field.
        template <class T>
        pbc::Element hash_to_element(const T& value,
                                     const pbc::PairingPtr& pairing)
        {
            std::string digest(CryptoPP::SHA1::DIGESTSIZE, 0);
            CryptoPP::SHA1 sha1;
            sha1.CalculateDigest((unsigned char*)digest.data(),
                                 (const unsigned char*)&value, sizeof(value));
            return pbc::Element::from_bytes(pairing, pbc::ElementType::Zr,
                                            digest);
        }
        template <>
        pbc::Element hash_to_element(const std::string& value,
                                     const pbc::PairingPtr& pairing)
        {
            std::string digest(CryptoPP::SHA1::DIGESTSIZE, 0);
            CryptoPP::SHA1 sha1;
            sha1.CalculateDigest((unsigned char*)digest.data(),
                                 (const unsigned char*)value.data(),
                                 value.size());
            return pbc::Element::from_bytes(pairing, pbc::ElementType::Zr,
                                            digest);
        }

        // compute a.pow(b) using pbc::Element#pow and pbc::FixedBasePower#apply
        // based on how many times it will be invoked.
        class ElementPowerHelper
        {
        public:
            ElementPowerHelper(const pbc::Element& base, size_t estimated_times)
            {
                // the condition is obtained from the result of
                // pbc_power_benchmark
                if (estimated_times < 5) {
                    auto base_p = std::make_shared<pbc::Element>(base);
                    f = [base_p](const pbc::Element& exponent) {
                        return base_p->pow(exponent);
                    };
                } else {
                    auto base_p = std::make_shared<pbc::FixedBasePower>(base);
                    f = [base_p](const pbc::Element& exponent) {
                        return base_p->pow(exponent);
                    };
                }
            }
            pbc::Element pow(const pbc::Element& exponent) const
            {
                return f(exponent);
            }

        private:
            std::function<pbc::Element(const pbc::Element&)> f;
        };
    };
};

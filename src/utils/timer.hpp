#pragma once

#include <chrono>
#include <iostream>

class Timer
{
public:
    Timer() { reset(); }
    void start() { start_time = std::chrono::high_resolution_clock::now(); }
    void stop()
    {
        elapsed += std::chrono::high_resolution_clock::now() - start_time;
    }
    void reset() { elapsed = decltype(elapsed)::zero(); }
    // unit is in ms
    double result() const { return elapsed.count(); }

private:
    std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
    std::chrono::duration<double, std::milli> elapsed;
};

# CMake generated Testfile for 
# Source directory: /home/yang/auth-access-control-demo/test
# Build directory: /home/yang/auth-access-control-demo/data/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(db "/home/yang/auth-access-control-demo/data/bin/test/db")
set_tests_properties(db PROPERTIES  WORKING_DIRECTORY "/home/yang/auth-access-control-demo/data")
